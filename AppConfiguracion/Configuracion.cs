﻿/****************************************************************************************************************************************************************************************
 PROGRAMA: Configuracion.cs
 VERSION : 1.0
 OBJETIVO: Clase de funciones comunes
 FECHA   : 21/06/2021
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/
using System;
using System.IO;
using System.Net;
using CEN;
using Newtonsoft.Json;

namespace AppConfiguracion
{
    public class Configuracion
    {


        public string getDateNowLima()
        {
             // DESCRIPCION: FUNCION PARA OBTENER FECHA EN FORMATO DD/MM/YYYY  DE BOGOTA/LIMA

            TimeZoneInfo cstZone = TimeZoneInfo.FindSystemTimeZoneById(EN_Constante.g_cost_nameTimeDefect);

            DateTime cstTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, cstZone);
            return cstTime.ToString(EN_Constante.g_const_formfech);

        }
        public string getDateHourNowLima()
        {
             // DESCRIPCION: FUNCION PARA OBTENER HORA DE BOGOTA/LIMA

            TimeZoneInfo cstZone = TimeZoneInfo.FindSystemTimeZoneById(EN_Constante.g_cost_nameTimeDefect);
            DateTime cstTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, cstZone);
            return cstTime.ToString(EN_Constante.g_const_formhora);
        }

         public DateTime getDateTimeNowLima()
        {
              // DESCRIPCION: FUNCION PARA OBTENER DATETIME DE BOGOTA/LIMA

            TimeZoneInfo cstZone = TimeZoneInfo.FindSystemTimeZoneById(EN_Constante.g_cost_nameTimeDefect);
            DateTime cstTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, cstZone);
            return cstTime;
        }

        public void EnviarXmlDocumentoElectronico(EN_RequestEnviodoc data, ref string msjWA, ref int codRspta, ref int contError, string p_url)
        {
            // DESCRIPCION: Este metodo se encarga de enviar a SUNAT el comprobante generado

            var objConf =new Configuracion();                                   // Clase de Configuracion
            string dataSerializado= EN_Constante.g_const_vacio;                 // Data serializado

            // validar xml (AQUI DEBE IR VALIDACION XSD Y XSL)
            try
            {
                    dataSerializado = System.Text.Json.JsonSerializer.Serialize(data);
                    dynamic responseWA= sendWebApi(p_url, dataSerializado);
                    if(responseWA.RptaRegistroCpeDoc.FlagVerificacion){
                        msjWA= responseWA.RptaRegistroCpeDoc.MensajeResp;
                        codRspta = EN_Constante.g_const_0;
                        contError= contError+0;
                    }else{
                        msjWA= "Error al enviar XML- " +responseWA.RptaRegistroCpeDoc.MensajeResp + " | Error al enviar XML-"+responseWA.ErrorWebService.DescripcionErr;
                        codRspta = EN_Constante.g_const_1;
                        contError= contError+1;
                    }
            }
            catch (Exception ex)
            {
                msjWA = "Se generó el XML - Error al enviar XML: " + ex.Message;
                codRspta = EN_Constante.g_const_2;
                contError= contError+1;
            
            }

        }

        public ClassRptaGenerarCpeDoc sendWebApi(string urlWA, string dataSerializado)
        {
             // DESCRIPCION: FUNCION PARA COMUNICACION ENTRE APIS

            ServicePointManager.ServerCertificateValidationCallback= delegate{return true;};

                var url = $"{urlWA}";
                var request = (HttpWebRequest)WebRequest.Create(url);
                string json = dataSerializado;
                request.Method = "POST";
                request.ContentType = "application/json";
                request.Accept = "application/json";
                // request.au="";
                using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                {
                    streamWriter.Write(json);
                }
                
                    using (WebResponse response = request.GetResponse())
                    {
                        using (Stream strReader = response.GetResponseStream())
                        {
                            using (StreamReader objReader = new StreamReader(strReader))
                            {
                                string responseBody = objReader.ReadToEnd();
                            
                                ClassRptaGenerarCpeDoc rpt=  (ClassRptaGenerarCpeDoc)JsonConvert.DeserializeObject(responseBody,typeof(ClassRptaGenerarCpeDoc));
                                // Do something with responseBody
                                objReader.Dispose();
                                strReader.Dispose();
                                response.Dispose();
                                return rpt;
                            }
                        }
                    
                    }

        }


    public EN_RegistrarGuiaElectronica sendWebApiGuia(string urlWA, string dataSerializado)
    {
        //DESCRIPCION: Envio a la SUNAT
        try
        {
            ServicePointManager.ServerCertificateValidationCallback= delegate{return true;};

            var url = $"{urlWA}";
            var request = (HttpWebRequest)WebRequest.Create(url);
            string json = dataSerializado;
            request.Method = "POST";
            request.ContentType = "application/json";
            request.Accept = "application/json";
            using (var streamWriter = new StreamWriter(request.GetRequestStream()))
            {
                streamWriter.Write(json);
            }
              
                using (WebResponse response = request.GetResponse())
                {
                    using (Stream strReader = response.GetResponseStream())
                    {
                        using (StreamReader objReader = new StreamReader(strReader))
                        {
                            string responseBody = objReader.ReadToEnd();
                        
                            EN_RegistrarGuiaElectronica rpt=  (EN_RegistrarGuiaElectronica)JsonConvert.DeserializeObject(responseBody,typeof(EN_RegistrarGuiaElectronica));
                            objReader.Dispose();
                            strReader.Dispose();
                            response.Dispose();
                            return rpt;
                        }
                    }
                
                  } 
        }
        catch (Exception ex)
        {
            EN_RegistrarGuiaElectronica respuesta= new  EN_RegistrarGuiaElectronica();
            respuesta.RespRegistrarGuiaElectronica.FlagVerificacion = false;
            respuesta.RespRegistrarGuiaElectronica.DescRespuesta = EN_Constante.g_const_error_interno;
            respuesta.ErrorWebServ.CodigoError = EN_Constante.g_const_3000;
            respuesta.ErrorWebServ.TipoError = EN_Constante.g_const_1;
            respuesta.ErrorWebServ.DescripcionErr = ex.Message;
            return respuesta;
        }
        

    }

    public string GetCadenaConexionFromWA(string p_url,string p_puerto,string p_api, string p_region,  string p_version, string p_secrectname)
       {
        // DESCRIPCION: FUNCION PARA OBTENER CADENA DE CONEXION DESDE API SERVICE MANAGER


           string cadenaConexion= EN_Constante.g_const_vacio;
           string urlWA     = EN_Constante.g_const_vacio;
       

           try
           {
            
                urlWA= p_url+":"+p_puerto+"/"+p_api;


                using(WebClient webClient = new WebClient())
                {
                    webClient.QueryString.Add("secretName", p_secrectname);
                    webClient.QueryString.Add("region", p_region);
                    webClient.QueryString.Add("versionStage", p_version);
                    cadenaConexion = webClient.DownloadString(urlWA);
                    return cadenaConexion;

                }
                
            
           }
           catch (System.Exception ex)
           {
               cadenaConexion = EN_Constante.g_const_vacio;
               return cadenaConexion;
               
           }
       }



    
    
    
    
    }
}