/****************************************************************************************************************************************************************************************
 PROGRAMA: GenerarResumen.cs
 VERSION : 1.0
 OBJETIVO: Clase para generar resumen
 FECHA   : 21/06/2021
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CEN;
using CLN;
using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;

namespace AppServicioDoc
{
    public partial class GenerarResumen
    {
        private string pClase = "GenerarResumen";     //  nombre de clase
       
        public GenerarResumen()
        {
            //DESCRIPCION: CONTRUCTOR DE CLASE GenerarResumen
        }
      

       
        private EN_ComprobanteSunat buildComprobante()
        {
            //DESCRIPCION: FUNCION PARA CONTRUIR ID DE COMPROBANTES EN CLASE COMPROBANTE SUNAT
                var oCompSunat = new EN_ComprobanteSunat(); // clase de entidad comprobante sunat

                oCompSunat.IdFC = EN_ConfigConstantes.Instance.const_IdFC;
                oCompSunat.IdBV = EN_ConfigConstantes.Instance.const_IdBV;
                oCompSunat.IdNC = EN_ConfigConstantes.Instance.const_IdNC;
                oCompSunat.IdND = EN_ConfigConstantes.Instance.const_IdND;
                oCompSunat.IdGR = EN_ConfigConstantes.Instance.const_IdGR;

                oCompSunat.IdCB =  EN_ConfigConstantes.Instance.const_IdCB;
                oCompSunat.IdRD = EN_ConfigConstantes.Instance.const_IdRD;
                oCompSunat.IdRR =  EN_ConfigConstantes.Instance.const_IdRR;

                oCompSunat.IdCP = EN_ConfigConstantes.Instance.const_IdCP;
                oCompSunat.IdCR =  EN_ConfigConstantes.Instance.const_IdCR;

                oCompSunat.codigoEtiquetaErrorDoc = EN_ConfigConstantes.Instance.const_codigoEtiquetaErrorDoc;
                oCompSunat.codigoEtiquetaErrorRes = EN_ConfigConstantes.Instance.const_codigoEtiquetaErrorRes;
                return oCompSunat;
        }

        public  void CrearResumenBVPendientes(int numdias, int valEmpId,ref string msjrspta, ref int codrspta, ref int contError)
        {
            // DESCRIPCION: Genera el Resumen Diario de Boletas Pendientes

            var oCompSunat = new EN_ComprobanteSunat(); // Entidad comprobante sunat
            NE_Proceso objProc = new NE_Proceso();  //clase negocio proceso
            var oProceso = new EN_Proceso();    //clase entidad proceso
            List<EN_Proceso> listaProc = new List<EN_Proceso>(); //lista de clase entidad proceso
            AppConfiguracion.Configuracion confg= new AppConfiguracion.Configuracion(); // Clase configuración
            string p_tipo_docs;
            try
            {

            oCompSunat = buildComprobante();

                //buscamos en tabla parametro si procesamos para todas las empresas o una en particular
                oProceso.Empresa_id = valEmpId;
                oProceso.Estado = EN_Constante.g_const_estado_proceso;
                p_tipo_docs=oCompSunat.IdBV+","+oCompSunat.IdNC+","+oCompSunat.IdND;

                // Busca Procesos Activados
                listaProc = (List<EN_Proceso>)objProc.listarProcesosActivos(oProceso.Empresa_id, p_tipo_docs, oProceso.Estado);

                if (listaProc.Count > 0)
                {
                    foreach (var item in listaProc.ToList())
                    {
                        // Obtener ID de empresa
                        EN_Empresa empresaDocumento = new EN_Empresa();
                        empresaDocumento = GetEmpresaById(item.Empresa_id);
                        // Obtener ID de certificado
                        EN_Certificado certificadoEmpresa = new EN_Certificado();
                        certificadoEmpresa = GetCertificadoByEmpresaId(item.Empresa_id);
                        // Numero de dias desde 7 días atrás para enviar resumen
                        for (var k = numdias; k <= -1; k++)
                        {
                            // Listo las Boletas pendientes para generar el resumen
                            NE_Documento obj = new NE_Documento();
                            var oDocumento = new EN_Documento();
                            List<EN_Documento> listaDocumentos = new List<EN_Documento>();
                            oDocumento.Id = "";
                            oDocumento.Idempresa = item.Empresa_id;
                            oDocumento.Idtipodocumento = item.Tipodocumento_id;
                            oDocumento.Estado = EN_Constante.g_const_estado_nuevo;
                            oDocumento.Situacion = EN_Constante.g_const_situacion_pendiente;
                            oDocumento.Serie = EN_Constante.g_const_inicial_boleta;
                            oDocumento.Numero = "";
                            DateTime datetimenow= confg.getDateTimeNowLima();

                            oDocumento.FechaIni = datetimenow.AddDays(k).ToString(EN_Constante.g_const_formfech);
                            oDocumento.FechaFin = datetimenow.AddDays(k).ToString(EN_Constante.g_const_formfech);

                            oDocumento.CadenaAleatoria = "";
                            oDocumento.UsuarioSession = EN_Constante.g_const_usuario_session;
                            listaDocumentos = ( List<EN_Documento> )obj.listarDocumentosResumen(oDocumento);
                            if (listaDocumentos.Count > 0)
                            {
                                AppServicioDoc.GenerarResumen objEnvioResDiario = new AppServicioDoc.GenerarResumen();
                                ResumenSunat oResDiario = new ResumenSunat();
                                {
                                    var withBlock = oResDiario;
                                    withBlock.Empresa_id = item.Empresa_id;
                                    withBlock.Tiporesumen = EN_ConfigConstantes.Instance.const_IdRD;
                                    withBlock.FechaGeneraResumen = confg.getDateNowLima();
                                    withBlock.FechaEmisionDocumento = datetimenow.AddDays(k).ToString(EN_Constante.g_const_formfech);

                                    withBlock.Estado = EN_Constante.g_const_estado_nuevo;
                                    withBlock.Situacion = EN_Constante.g_const_situacion_pendiente;
                                    withBlock.UsuarioSession = EN_Constante.g_const_usuario_session;
                                    withBlock.Usuario = certificadoEmpresa.UserName;
                                    withBlock.Clave = certificadoEmpresa.Clave;
                                }
                                for (var i = 0; i <= listaDocumentos.Count - 1; i++)
                                {
                                    EN_DetalleResumen oDetalle = new EN_DetalleResumen();
                                    {
                                        var withBlock = oDetalle;
                                        string tipdoccli = "0";
                                        if (listaDocumentos[i].Tipodoccliente.Length == 1)
                                            tipdoccli = listaDocumentos[i].Tipodoccliente;
                                        else if (listaDocumentos[i].Tipodoccliente == "RUC")
                                            tipdoccli = "6";
                                        else if (listaDocumentos[i].Tipodoccliente == "DNI")
                                            tipdoccli = "1";
                                        else if (listaDocumentos[i].Tipodoccliente == "CAE")
                                            tipdoccli = "4";
                                        else if (listaDocumentos[i].Tipodoccliente == "PAS")
                                            tipdoccli = "7";
                                        else if (listaDocumentos[i].Tipodoccliente == "CDI")
                                            tipdoccli = "A";
                                        else if (listaDocumentos[i].Tipodoccliente == "DPR")
                                            tipdoccli = "B";
                                        else if (listaDocumentos[i].Tipodoccliente == "TAX")
                                            tipdoccli = "C";
                                        else if (listaDocumentos[i].Tipodoccliente == "IND")
                                            tipdoccli = "D";
                                        else
                                            tipdoccli = "0";
                                        withBlock.Tipodocumentoid = listaDocumentos[i].Idtipodocumento;
                                        withBlock.Serie = listaDocumentos[i].Serie;
                                        withBlock.Correlativo = listaDocumentos[i].Numero;
                                        withBlock.DocCliente = tipdoccli;
                                        withBlock.NroDocCliente = listaDocumentos[i].Nrodoccliente;
                                        withBlock.Condicion = 1;
                                        withBlock.Moneda = listaDocumentos[i].Moneda;
                                        withBlock.Opegravadas = (decimal)listaDocumentos[i].Valorpergravadas;
                                        withBlock.Opeexoneradas = (decimal)listaDocumentos[i].Valorperexoneradas;
                                        withBlock.Opeinafectas = (decimal)listaDocumentos[i].Valorperinafectas;
                                        withBlock.Opeexportacion = (decimal)listaDocumentos[i].Valorperexportacion;
                                        withBlock.Opegratuitas = (decimal)listaDocumentos[i].Valorpergratuitas;
                                        withBlock.Otroscargos = (decimal)listaDocumentos[i].OtrosCargos;
                                        withBlock.TotalIGV = (decimal)listaDocumentos[i].Igvventa;
                                        withBlock.TotalISC = (decimal)listaDocumentos[i].Iscventa;
                                        withBlock.Otrostributos = (decimal)listaDocumentos[i].OtrosTributos;
                                        withBlock.Importeventa = (decimal)listaDocumentos[i].Importeventa;
                                        withBlock.Regimenpercep = (string)listaDocumentos[i].Regimenpercep;
                                        withBlock.Porcentpercep = (decimal)listaDocumentos[i].Porcentpercep;
                                        withBlock.Importepercep = (decimal)listaDocumentos[i].Importepercep;
                                        withBlock.Importefinal = (decimal)listaDocumentos[i].Importefinal;

                                        // Buscamos referencia de NC y ND
                                        if (listaDocumentos[i].Idtipodocumento == "07" | listaDocumentos[i].Idtipodocumento == "08")
                                        {
                                            NE_Documento objDocumentoRef = new NE_Documento();
                                            EN_DocReferencia oDocumentoRef = new EN_DocReferencia();
                                            List<EN_DocReferencia> listaNCNDDocRefencia = new List<EN_DocReferencia>();
                                            oDocumentoRef.Idempresa = listaDocumentos[i].Idempresa;
                                            oDocumentoRef.Iddocumento = listaDocumentos[i].Id;
                                            oDocumentoRef.Idtipodocumento = listaDocumentos[i].Idtipodocumento;
                                            listaNCNDDocRefencia = (List<EN_DocReferencia>)objDocumentoRef.listarDocumentoReferencia(oDocumentoRef, "" ,  oDocumento.Idempresa );
                                            if (listaNCNDDocRefencia.Count > 0)
                                            {
                                                withBlock.Tipodocref = listaNCNDDocRefencia[0].Idtipodocumentoref;
                                                withBlock.Nrodocref = listaNCNDDocRefencia[0].Numerodocref;
                                            }
                                        }
                                    }
                                    oResDiario.ListaDetalleResumen.Add(oDetalle);
                                }
                                objEnvioResDiario.CrearResumenElectronico(oResDiario,ref  codrspta,ref msjrspta);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                // throw;
                throw ex;
            }
        }


        public ResumenSunat CrearResumenElectronico(ResumenSunat oResumen, ref int codRspta, ref string msjeRspta)
        {
            //DESCRIPCION: FUNCION PARA CREAR EL RESUMEN ELECTRONICO 

            string rutabase, rutadocs;                                  //ruta base y ruta de documento
            string nombreArchivoXml = EN_Constante.g_const_vacio;       //nombre de archivo xml
            string valorResumen = EN_Constante.g_const_vacio;           //valor resumen
            string firma = EN_Constante.g_const_vacio;                  //firma
            EN_Empresa empresaDocumento = new EN_Empresa();             // clase entidad empresa
            EN_Certificado certificadoDocumento = new EN_Certificado(); // clase entidad certificado
            EN_ComprobanteSunat oCompSunat ;                            //clase entidad comprobante sunat
            NE_Resumen obj = new NE_Resumen();                          //clase de negocio resumen
            var objResumen = new EN_Resumen();                          //clase entidad resumen
            List<EN_Resumen> listaResumens = new List<EN_Resumen>();    // lista de clase resumen
        
            List<EN_DetalleResumen> detalleResumen = new List<EN_DetalleResumen>(); // lista de clase detalle resumen
            int idresumen = EN_Constante.g_const_0;                     //id de resumen
            string mensaje = EN_Constante.g_const_vacio;                // mensaje
            string cadenaXML = EN_Constante.g_const_vacio;              // cadena xml

            {
                var withBlock = oResumen;
                try
                {
                    
                    if (GenerarXML.ValidarUsuarioEmpresa(oResumen.Empresa_id, oResumen.Usuario, oResumen.Clave, oResumen.Id.ToString(), oResumen.Empresa_id))
                    {
                        empresaDocumento = GetEmpresaById(withBlock.Empresa_id);
                        certificadoDocumento = GetCertificadoByEmpresaId(empresaDocumento.Id);

                        rutabase= EN_ConfigConstantes.Instance.const_rutaTemporalPse_archivos;
                        rutadocs= string.Format("{0}/{1}/", rutabase, empresaDocumento.Nrodocumento);

                        //COMPROBAMOS CARPETAS Y ARCHIVOS INICIALES  DE S3 Y LOCAL TEMPORAL
                        AppConfiguracion.FuncionesS3.InitialS3(empresaDocumento.Nrodocumento,certificadoDocumento.Nombre,empresaDocumento.Imagen);


                        oCompSunat = buildComprobante();

                        if (withBlock.Tiporesumen.Trim() == oCompSunat.IdRD)
                        {
                            oResumen.EstadoResumen= EN_Constante.g_const_1.ToString();
                            idresumen = GuardarResumenSunat(oResumen);
                        
                            objResumen.Id = idresumen;
                            objResumen.Empresa_id = oResumen.Empresa_id;
                            objResumen.Estado = EN_Constante.g_const_estado_nuevo;
                            objResumen.Situacion = EN_Constante.g_const_situacion_pendiente;
                            objResumen.Correlativo = EN_Constante.g_const_0;
                            objResumen.CadenaAleatoria = EN_Constante.g_const_vacio;
                            objResumen.UsuarioSession = EN_Constante.g_const_vacio;
                            objResumen.FechaIni=EN_Constante.g_const_vacio;
                            objResumen.FechaFin=EN_Constante.g_const_vacio;
                            listaResumens = (List<EN_Resumen>) obj.listarResumen(objResumen);
                            if (listaResumens.Count > EN_Constante.g_const_0)
                            {
                                if (GenerarXML.GenerarXmlResumen(listaResumens[0], oCompSunat, empresaDocumento, certificadoDocumento, rutadocs, rutabase, ref nombreArchivoXml, ref valorResumen, ref firma, ref mensaje,ref cadenaXML))
                                {
                                    if (GenerarXML.GuardarResumenNombreValorResumenFirma(listaResumens[0], nombreArchivoXml, valorResumen, firma, cadenaXML))
                                    {
                                        withBlock.NombreFichero = nombreArchivoXml;
                                        withBlock.ValorResumen = valorResumen;
                                        withBlock.ValorFirma = firma;
                                        codRspta = EN_Constante.g_const_0;
                                        msjeRspta = mensaje;
                                    }
                                }
                                else
                                {
                                    withBlock.NombreFichero = EN_Constante.g_const_vacio;
                                    withBlock.ValorResumen = EN_Constante.g_const_vacio;
                                    withBlock.ValorFirma = EN_Constante.g_const_vacio;
                                    codRspta = EN_Constante.g_const_1;
                                    msjeRspta = mensaje;
                                }
                            }
                            else
                            {
                                withBlock.NombreFichero = EN_Constante.g_const_vacio;
                                withBlock.ValorResumen = EN_Constante.g_const_vacio;
                                withBlock.ValorFirma = EN_Constante.g_const_vacio;
                                codRspta = EN_Constante.g_const_1;
                                msjeRspta = "No se encuentra el comprobante electrónico generado.";
                            }
                        }
                        else
                        {
                            withBlock.NombreFichero = EN_Constante.g_const_vacio;
                            withBlock.ValorResumen = EN_Constante.g_const_vacio;
                            withBlock.ValorFirma = EN_Constante.g_const_vacio;
                            codRspta = EN_Constante.g_const_1;
                            msjeRspta = "El codigo de resumen a generar no es válido (RC).";
                        }
                    }
                    else
                    {
                        withBlock.NombreFichero = EN_Constante.g_const_vacio;
                        withBlock.ValorResumen = EN_Constante.g_const_vacio;
                        withBlock.ValorFirma = EN_Constante.g_const_vacio;
                        codRspta = EN_Constante.g_const_1;
                        msjeRspta = "No se ha encontrado el usuario del certificado de la empresa ingresada";
                    }
                }
                catch (Exception ex)
                {
                    withBlock.NombreFichero = EN_Constante.g_const_vacio;
                    withBlock.ValorResumen = EN_Constante.g_const_vacio;
                    withBlock.ValorFirma = EN_Constante.g_const_vacio;
                    codRspta = EN_Constante.g_const_1;
                    msjeRspta = "Hubo una Excepción al generar el resumen:" + ex.Message;
                }
            }
            return oResumen;
        }

        public void ProcesaResumenSunat(int numdias, int valEmpId,ref string msjrspta, ref int codrspta, ref int contError)
        {
            string rutabase,  rutadocs; //ruta base y ruta de documentos
            string nombre;  // variable nombre
            string msjCDR = ""; // mensaje de CDR
            int codRpt=0; // codigo respuesta
            string fileS3XML =EN_Constante.g_const_vacio;        //Ruta de xml en S3
            bool existeXmlS3;                                   // Booleano para existencia de objecto en S3


            var oCompSunat = new EN_ComprobanteSunat(); // clase entidad comprobante sunat
            EN_Empresa empresaDocumento = new EN_Empresa(); // clase entidad empresa
            NE_Proceso objProc = new NE_Proceso(); // clase negocio proceso
            var oProceso = new EN_Proceso();        // clase entidad proceso
            List<EN_Proceso> listaProc = new List<EN_Proceso>(); // lista de clase entidad proceso
            EN_Certificado certificadoEmpresa = new EN_Certificado(); // clase entidad certificado
            NE_Resumen obj = new NE_Resumen();  //clase negocio resumen
            var oResumen = new EN_Resumen();        //clase entidad resumen
            List<EN_Resumen> listaResumen = new List<EN_Resumen>(); // lista de calse entidad resumen
            AppServicioDoc.GenerarResumen objGenerar = new AppServicioDoc.GenerarResumen(); //clase GenerarResumen
            List<EN_DetalleResumen> detalleResumen = new List<EN_DetalleResumen>(); // lista de clase detalle resumen

            string p_tipo_docs=EN_Constante.g_const_vacio;      // Tipo de documentos
            

            try
            {
                

                oCompSunat = buildComprobante();
            
                oProceso.Empresa_id = valEmpId;
                oProceso.Estado = EN_Constante.g_const_estado_proceso;

                
                oProceso.Empresa_id = valEmpId;
                oProceso.Estado = EN_Constante.g_const_estado_proceso;
                p_tipo_docs=oCompSunat.IdRD ;

                listaProc = (List<EN_Proceso>)objProc.listarProcesosActivos(oProceso.Empresa_id, p_tipo_docs, oProceso.Estado);

                if (listaProc.Count > 0)
                {
                    foreach (var item in listaProc.ToList())
                    {
                        // Obtener ID de empresa
                        empresaDocumento = GetEmpresaById(item.Empresa_id);
                        // Obtener ID de certificado
                        certificadoEmpresa = GetCertificadoByEmpresaId(item.Empresa_id);

                        // Listamos los resumenes pendientes de enviar a SUNAT
                        oResumen.Id = EN_Constante.g_const_0;
                        oResumen.Empresa_id = item.Empresa_id;
                        oResumen.Estado = EN_Constante.g_const_estado_nuevo;
                        oResumen.Situacion = EN_Constante.g_const_situacion_pendiente+","+EN_Constante.g_const_situacion_error;
                        oResumen.Correlativo = EN_Constante.g_const_0;
                        oResumen.CadenaAleatoria = EN_Constante.g_const_vacio;
                        oResumen.UsuarioSession = EN_Constante.g_const_usuario_session;
                        oResumen.FechaIni= EN_Constante.g_const_vacio;
                        oResumen.FechaFin= EN_Constante.g_const_vacio;
                        listaResumen =(List<EN_Resumen>)obj.listarResumen(oResumen);
                        if (listaResumen.Count > 0)
                        {
                            for (var i = 0; i <= listaResumen.Count - 1; i++)
                            {
                                // GenerarXML objGenerar = new GenerarXML();
                                
                                rutabase= EN_ConfigConstantes.Instance.const_rutaTemporalPse_archivos;
                                rutadocs= string.Format("{0}/{1}/", rutabase, empresaDocumento.Nrodocumento);

                                // nombre del resumen
                                nombre = listaResumen[i].Nrodocumento.Trim() + "-" + listaResumen[i].Tiporesumen.Trim() + "-" +
                                        Strings.Format(Conversions.ToDate(listaResumen[i].FechaGeneraResumen), "yyyyMMdd")
                                        + "-" + listaResumen[i].Correlativo.ToString();

                                fileS3XML=  string.Format("/{0}/{1}{2}", empresaDocumento.Nrodocumento,EN_Constante.g_const_rutaSufijo_xml,nombre+EN_Constante.g_const_extension_xml);
                                // Si existe el XML en S3
                                existeXmlS3= AppConfiguracion.FuncionesS3.VerificarObjectS3(fileS3XML).Result;
                                if(existeXmlS3)
                                {
                                    
                                    string vResumen = "";
                                    string vFirma = "";
                                    // string msjeRspta = "";
                                    GenerarXML.EnviarXmlResumenElectronico(listaResumen[i],  certificadoEmpresa, oCompSunat, rutadocs, nombre.Trim(), vResumen, vFirma, ref msjCDR, ref codRpt, empresaDocumento.Produccion, EN_Constante.g_const_0);
                                    
                                }
                                else
                                {
                                    // Se genera el XML del resumen diario
                                    string nombrexml = "";
                                    string vResumen = "";
                                    string vFirma = "";
                                    string msjeRspta = "";
                                    // Si el directorio no existe, crearlo
                                    if ((!System.IO.Directory.Exists(rutadocs)))
                                        System.IO.Directory.CreateDirectory(rutadocs);
                                    if (GenerarXML.GenerarXmlResumenElectronico(listaResumen[i], empresaDocumento, certificadoEmpresa, oCompSunat, rutadocs, rutabase,ref nombrexml, ref vResumen, ref vFirma,ref  msjeRspta))
                                    {
                                    
                                            GenerarXML.EnviarXmlResumenElectronico(listaResumen[i],  certificadoEmpresa, oCompSunat, rutadocs, nombrexml, vResumen, vFirma, ref msjCDR, ref codRpt, empresaDocumento.Produccion, EN_Constante.g_const_0);
                                            
                                    
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    
            public int GuardarResumenSunat(ResumenSunat ResumenSunat)
            {
                NE_GeneracionService negocioGeneracionService = new NE_GeneracionService();
                return negocioGeneracionService.GuardarResumenSunat(ResumenSunat);
            }


            public void EnviarComunicadoBajas(int numdias, int valEmpId,ref string msjrspta, ref int codrspta, ref int contError)
            {
            // DESCRIPCION: Genera el Resumen Diario de Boletas Pendientes

            string p_tipo_docs= EN_Constante.g_const_vacio ;        // parametro tipo de documentos
            string fecha_comunicacion = EN_Constante.g_const_vacio; // parametro tipo de documentos
            int flag;                                               // flag 0 para Comunicado de Baja, 1 para Resumen de Baja
            string rutabase,rutadocs;                                        // Varible ruta de documento
            string fileS3XML =EN_Constante.g_const_vacio;        //Ruta de xml en S3
            bool existeXmlS3;                                   // Booleano para existencia de objecto en S3


            var oCompSunat = new EN_ComprobanteSunat();             // Entidad comprobante sunat
            NE_Proceso objProc = new NE_Proceso();                  //clase negocio proceso
            var oProceso = new EN_Proceso();                        //clase entidad proceso
            List<EN_Proceso> listaProc = new List<EN_Proceso>();    //lista de clase entidad proceso
            ClassRptaGenerarCpeDoc responseWA;                      // Clase de Respuesta de comprobante
            EN_Parametro bus_par;                                   // Clase entidad parametro para datos de búsqueda
            EN_Parametro res_par;                                   // Clase entidad parametro para datos de resultado
            EN_Empresa empresaDocumento;                            // Clase entidad empresa
            EN_Certificado certificadoEmpresa;                      // Clase entidad certificado empresa
            List<EN_RequestEnvioResDiario> listaDocumentos;         // Lista de clase entidad respues de envio de resumen diario
            AppConfiguracion.Configuracion confg= new AppConfiguracion.Configuracion(); // Clase configuración
            
        
        
            try
            {
                
                oCompSunat = buildComprobante();
                //buscamos en tabla parametro número de días permitidos para envio a sunat
                bus_par = new EN_Parametro();
                res_par = new EN_Parametro();
                
                bus_par.par_conceptopfij=EN_Constante.g_const_1;
                bus_par.par_conceptocorr=EN_Constante.g_const_114;
                res_par =objProc.buscar_tablaParametro(bus_par);
                numdias = Convert.ToInt32(res_par.par_descripcion);

                //buscamos en tabla parametro si procesamos para todas las empresas o una en particular
                bus_par.par_conceptopfij=EN_Constante.g_const_1;
                bus_par.par_conceptocorr=EN_Constante.g_const_5;
                res_par =objProc.buscar_tablaParametro(bus_par);
                valEmpId =  Convert.ToInt32(res_par.par_tipo);

                oProceso.Empresa_id = valEmpId;
                oProceso.Estado = EN_Constante.g_const_estado_proceso;
                p_tipo_docs=oCompSunat.IdCB+","+oCompSunat.IdRD;

                // Busca Procesos Activados
                listaProc = (List<EN_Proceso>)objProc.listarProcesosActivos(oProceso.Empresa_id, p_tipo_docs, oProceso.Estado);

                if (listaProc.Count > 0)
                {
                    foreach (var item in listaProc.ToList())
                    {
                        // Obtener ID de empresa
                        empresaDocumento = new EN_Empresa();
                        empresaDocumento = GetEmpresaById(item.Empresa_id);
                        // Obtener ID de certificado
                        certificadoEmpresa = new EN_Certificado();
                        certificadoEmpresa = GetCertificadoByEmpresaId(item.Empresa_id);
                            // Listo las Boletas pendientes para generar el resumen
                            NE_Baja obj = new NE_Baja();
                        
                            listaDocumentos = new List<EN_RequestEnvioResDiario>();
                            
                            if(item.Tipodocumento_id==oCompSunat.IdCB) flag=0; else flag=1;

                            DateTime datetimenow= confg.getDateTimeNowLima();

                            fecha_comunicacion =  datetimenow.AddDays(numdias).ToString(EN_Constante.g_const_formfech);
                        
                            listaDocumentos = ( List<EN_RequestEnvioResDiario> )obj.listarDocumentosComunicadoBaja(empresaDocumento, certificadoEmpresa, item.Empresa_id,  fecha_comunicacion,  EN_Constante.g_const_estado_nuevo,  EN_Constante.g_const_situacion_pendiente,  flag);
                            if (listaDocumentos.Count > EN_Constante.g_const_0)
                            {
                                
                                for (var i = 0; i <= listaDocumentos.Count - 1; i++)
                                {
                                    rutabase= EN_ConfigConstantes.Instance.const_rutaTemporalPse_archivos;
                                    rutadocs= string.Format("{0}/{1}/", rutabase, empresaDocumento.Nrodocumento);
                                    
                                    fileS3XML=  string.Format("/{0}/{1}{2}", empresaDocumento.Nrodocumento,EN_Constante.g_const_rutaSufijo_xml,listaDocumentos[i].nombreArchivo.Trim()+EN_Constante.g_const_extension_xml);
                                    // Si existe el XML en S3
                                    existeXmlS3= AppConfiguracion.FuncionesS3.VerificarObjectS3(fileS3XML).Result;
                                    if(existeXmlS3)
                                    {
                                        responseWA = new ClassRptaGenerarCpeDoc();
                                        responseWA= GenerarXML.EnviarXmlComunicadBaja(listaDocumentos[i], ref msjrspta, ref codrspta, ref contError);

                                        
                    
                                    }
                                }
                                Console.WriteLine(msjrspta);
                                Console.WriteLine(contError);
                            }
                        // }
                    }
                
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }



        public  void CrearResumenBajapendiente(int valEmpId,  ref int codRspta, ref string msjeRspta, ref string comprobante, ref bool success)
        {
            // DESCRIPCION: Genera el Resumen Diario de Boletas Pendientes

            var oCompSunat = new EN_ComprobanteSunat();                     // Entidad comprobante sunat
            var empresaDocumento = new EN_Empresa();                        // CLASE ENTIDAD EMPRESA
            var certificadoDocumento = new EN_Certificado();                // CLASE ENTIDAD CERTIFICADO
            NE_Proceso objProc = new NE_Proceso();                          //clase negocio proceso
            NE_Baja objBaja = new NE_Baja();                                //clase negocio baja
            var oProceso = new EN_Proceso();                                //clase entidad proceso
            List<EN_Proceso> listaProc = new List<EN_Proceso>();            //lista de clase entidad proceso
            List<EN_ResumenBajaPendiente> listaResumenBaja = new List<EN_ResumenBajaPendiente>(); // lista de clase resumen baja pendiente
            AppConfiguracion.Configuracion confg= new AppConfiguracion.Configuracion(); // Clase configuración
            BajaSunat oBaja;                                        // CLASE ENTIDAD BAJA SUNAT
            ResumenSunat oResDiario;                                // CLASE DE RESUMEN SUNAT
            NE_Resumen objNegResumen = new NE_Resumen();            // CLASE DE NEGOCIO RESUMEN
            int idresumen=0;                                        // ID DE RESUMEN
            string mensaje = "";                                    // MENSAJE
            string cadenaXML = "";                                  // CADENA XML
            string rutabase, rutadocs;                              // RUTA BASE Y RUTA DE DOCUMENTOS
            string nombreArchivoXml = "";                           // NOMBRE DE ARCHIVO XML
            string valorResumen = "";                               // VALOR DEL RESUMEN
            string firma = "";                                      // FIRMA
            string p_tipo_docs;                                     // TIPO DE DOCUMENTOS PARA PROCESAR
            try
            {

                oCompSunat = buildComprobante();

                //buscamos en tabla parametro si procesamos para todas las empresas o una en particular
                oProceso.Empresa_id = valEmpId;
                oProceso.Estado = EN_Constante.g_const_estado_proceso;
                p_tipo_docs=oCompSunat.IdRD;

                // Busca Procesos Activados
                listaProc = (List<EN_Proceso>)objProc.listarProcesosActivos(oProceso.Empresa_id, p_tipo_docs, oProceso.Estado);

                if (listaProc.Count > 0)
                {
                    foreach (var item in listaProc.ToList())
                    {
                        listaResumenBaja=( List<EN_ResumenBajaPendiente>) objBaja.listarResumenBajaPendientes(item.Empresa_id);
                        if(listaResumenBaja.Count > 0)
                        {
                            for (var ibp = 0; ibp <= listaResumenBaja.Count - 1; ibp++)
                            {
                                oBaja= (BajaSunat)objBaja.listarDetalleResBajaPend(listaResumenBaja[ibp].id);

                                 empresaDocumento = GetEmpresaById(oBaja.Empresa_id);
                                certificadoDocumento = GetCertificadoByEmpresaId(empresaDocumento.Id);

                                
                                rutabase= EN_ConfigConstantes.Instance.const_rutaTemporalPse_archivos;
                                rutadocs = string.Format("{0}/{1}/", rutabase, empresaDocumento.Nrodocumento);

                                //COMPROBAMOS CARPETAS Y ARCHIVOS INICIALES  DE S3 Y LOCAL TEMPORAL
                                AppConfiguracion.FuncionesS3.InitialS3(empresaDocumento.Nrodocumento,certificadoDocumento.Nombre,empresaDocumento.Imagen);

                                oResDiario = new ResumenSunat(); 
                                oResDiario.Empresa_id = oBaja.Empresa_id;
                                oResDiario.Tiporesumen = oBaja.Tiporesumen;
                                oResDiario.FechaGeneraResumen = oBaja.FechaComunicacion;
                                oResDiario.FechaEmisionDocumento = oBaja.FechaDocumento;
                                oResDiario.Estado = EN_Constante.g_const_estado_nuevo;
                                oResDiario.Situacion = EN_Constante.g_const_situacion_pendiente;
                                oResDiario.UsuarioSession = oBaja.UsuarioSession;
                                oResDiario.Usuario = oBaja.Usuario;
                                oResDiario.Clave = oBaja.Clave;
                                oResDiario.EstadoResumen= EN_Constante.g_const_estadoResumen_baja.ToString();
                                  List<EN_Documento> listaDocumentos = new List<EN_Documento>();

                                foreach (var itemDD in oBaja.ListaDetalleBaja)
                                {

                                    string IdDocumento = itemDD.Tipodocumentoid.ToString().Trim()+itemDD.Serie.ToString().Trim()+"-"+itemDD.Numero.ToString().Trim();   
                                    listaDocumentos= ( List<EN_Documento> )objNegResumen.listarDocumentosBajaResumen(IdDocumento,oBaja.Empresa_id, oBaja.Idpuntoventa);
                                        if (listaDocumentos.Count > 0)
                                        {

                                             for (var i = 0; i <= listaDocumentos.Count - 1; i++)
                                            {
                                                EN_DetalleResumen oDetalle = new EN_DetalleResumen();
                                                {

                                                    var withBlock = oDetalle;
                                                    string tipdoccli = "0";
                                                    if (listaDocumentos[i].Tipodoccliente.Length == 1)
                                                        tipdoccli = listaDocumentos[i].Tipodoccliente;
                                                    else if (listaDocumentos[i].Tipodoccliente == "RUC")
                                                        tipdoccli = "6";
                                                    else if (listaDocumentos[i].Tipodoccliente == "DNI")
                                                        tipdoccli = "1";
                                                    else if (listaDocumentos[i].Tipodoccliente == "CAE")
                                                        tipdoccli = "4";
                                                    else if (listaDocumentos[i].Tipodoccliente == "PAS")
                                                        tipdoccli = "7";
                                                    else if (listaDocumentos[i].Tipodoccliente == "CDI")
                                                        tipdoccli = "A";
                                                    else if (listaDocumentos[i].Tipodoccliente == "DPR")
                                                        tipdoccli = "B";
                                                    else if (listaDocumentos[i].Tipodoccliente == "TAX")
                                                        tipdoccli = "C";
                                                    else if (listaDocumentos[i].Tipodoccliente == "IND")
                                                        tipdoccli = "D";
                                                    else
                                                        tipdoccli = "0";
                                                    withBlock.Tipodocumentoid = listaDocumentos[i].Idtipodocumento;
                                                    withBlock.Serie = listaDocumentos[i].Serie;
                                                    withBlock.Correlativo = listaDocumentos[i].Numero;
                                                    withBlock.DocCliente = tipdoccli;
                                                    withBlock.NroDocCliente = listaDocumentos[i].Nrodoccliente;
                                                    withBlock.Condicion = 1;
                                                    withBlock.Moneda = listaDocumentos[i].Moneda;
                                                    withBlock.Opegravadas = (decimal)listaDocumentos[i].Valorpergravadas;
                                                    withBlock.Opeexoneradas = (decimal)listaDocumentos[i].Valorperexoneradas;
                                                    withBlock.Opeinafectas = (decimal)listaDocumentos[i].Valorperinafectas;
                                                    withBlock.Opeexportacion = (decimal)listaDocumentos[i].Valorperexportacion;
                                                    withBlock.Opegratuitas = (decimal)listaDocumentos[i].Valorpergratuitas;
                                                    withBlock.Otroscargos = (decimal)listaDocumentos[i].OtrosCargos;
                                                    withBlock.TotalIGV = (decimal)listaDocumentos[i].Igvventa;
                                                    withBlock.TotalISC = (decimal)listaDocumentos[i].Iscventa;
                                                    withBlock.Otrostributos = (decimal)listaDocumentos[i].OtrosTributos;
                                                    withBlock.Importeventa = (decimal)listaDocumentos[i].Importeventa;
                                                    withBlock.Regimenpercep = (string)listaDocumentos[i].Regimenpercep;
                                                    withBlock.Porcentpercep = (decimal)listaDocumentos[i].Porcentpercep;
                                                    withBlock.Importepercep = (decimal)listaDocumentos[i].Importepercep;
                                                    withBlock.Importefinal = (decimal)listaDocumentos[i].Importefinal;
                                                 
                                                }
                                                oResDiario.ListaDetalleResumen.Add(oDetalle);
                                            }
                                            
                                        }

                                }


                                idresumen= objNegResumen.GuardarResumenSunat(oResDiario);
                        
                                oResDiario.Id = idresumen;
                                oResDiario.Empresa_id = oBaja.Empresa_id;
                                oResDiario.Estado = EN_Constante.g_const_estado_nuevo;
                                oResDiario.Situacion = EN_Constante.g_const_situacion_pendiente;
                                oResDiario.Correlativo = 0;
                                oResDiario.CadenaAleatoria = EN_Constante.g_const_vacio;
                                oResDiario.UsuarioSession = EN_Constante.g_const_vacio;
                                oResDiario.FechaIni=EN_Constante.g_const_vacio;
                                oResDiario.FechaFin=EN_Constante.g_const_vacio;

                                List<EN_Resumen> listaResumens = new List<EN_Resumen>();    // lista de clase resumen
                                
                                listaResumens = (List<EN_Resumen>) objNegResumen.listarResumen(oResDiario);

                                if (listaResumens.Count > EN_Constante.g_const_0)
                                {
                                    if (GenerarXML.GenerarXmlResumen(listaResumens[EN_Constante.g_const_0], oCompSunat, empresaDocumento, certificadoDocumento, rutadocs, rutabase, ref nombreArchivoXml, ref valorResumen, ref firma, ref mensaje,ref cadenaXML))
                                    {
                                        if (GenerarXML.GuardarResumenNombreValorResumenFirma(listaResumens[EN_Constante.g_const_0], nombreArchivoXml, valorResumen, firma, cadenaXML))
                                        {
                                            oBaja.NombreFichero = nombreArchivoXml;
                                            oBaja.ValorResumen = valorResumen;
                                            oBaja.ValorFirma = firma;
                                            codRspta = 0;
                                            msjeRspta = mensaje;
                                        }

                                        //actualizamos estado de resumenbajapendiente

                                        objNegResumen.ActualizarEstadoResumenBajaPend(listaResumenBaja[ibp].id, EN_Constante.g_const_estadoSi);

                                        comprobante = listaResumens[EN_Constante.g_const_0].IdentificaResumen;
                                        success=true;
                                    }
                                    else
                                    {
                                        oBaja.NombreFichero = "";
                                        oBaja.ValorResumen = "";
                                        oBaja.ValorFirma = "";
                                        codRspta = 1;
                                        msjeRspta = mensaje;
                                         success=false;
                                    }
                                }


                            }
                            
                        }

                        
                    }
                }
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }


        public EN_Empresa GetEmpresaById(int idEmpresa)
        {
            //DESCRIPCION: FUNCION PARA OBTENER DATOS DE EMPRESA POR ID DE EMPRESA

            EN_Empresa listaEmpresa;
            EN_Empresa oEmpresa = new EN_Empresa();
            NE_Empresa objEmpresa = new NE_Empresa();
            oEmpresa.Id = idEmpresa;
            oEmpresa.Nrodocumento = "";
            oEmpresa.RazonSocial = "";
            oEmpresa.Estado = "";
            listaEmpresa = (EN_Empresa)objEmpresa.listaEmpresa(oEmpresa, 0, "");
            return listaEmpresa;
        }

        
        public EN_Certificado GetCertificadoByEmpresaId(int empresaId)
        {
            //DESCRIPCION: FUNCION PARA OBTENER CERTIFICADO POR IDE DE EMPRESA

            EN_Certificado oCertif = new EN_Certificado();  // clase de entidad certificado
            NE_Certificado objCertif = new NE_Certificado();    // clase de negocio certificado
            EN_Certificado listaCertificado;      // lista de clase entidad certificado

            oCertif.Id = 0;
            oCertif.IdEmpresa = empresaId;
            oCertif.Nombre = "";
            oCertif.UsuarioSession = "";
            oCertif.CadenaAleatoria = "";
            listaCertificado = (EN_Certificado)objCertif.listarCertificado(oCertif);
            return listaCertificado;
        }
        
        

        
    }

}