using System;
using CEN;
using CLN;
using CAD;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.VisualBasic;
using AppConfiguracion;
using System.Xml;
using System.Security.Cryptography.Xml;
using System.Security.Cryptography.X509Certificates;
using System.Xml.Xsl;
using Microsoft.VisualBasic.CompilerServices;
using System.Xml.Schema;
using System.Xml.XPath;
using System.IO.Compression;
using System.Text;

namespace AppServicioDoc
{
     public static class GenerarXML
    {
        
 
        private static string pClase = "GenerarXML";

        // /*************************INI VERSION UBL 2.1*****************************/
        // Esta función genera el comprobante electrónico de los datos guardados en la BD
        // Devuelve como valores "nombre", "vResumen" y "vFirma" 
        // --- nombre: es el nombre del archivo XML
        // --- vResumen: es el valor Resumen (campo DigestValue del XML generado)
        // --- vFirma: es el valor de la firma digital adjunta en el XML (campo SignatureValue del XML generado)
        
        private static void AgregarFirma(EN_Certificado oCertificado, string rutadoc, string rutabase, string Nombre, ref string valorResumen, ref string firma, string TipoDoc, EN_ComprobanteSunat oCompSunat, int numExtension, ref string cadenaXML)
        {
            // DESCRIPCION: Inicio Funciones para agregar Firma XML
            try
            {
                string local_xmlArchivo = rutadoc + Nombre + ".XML";
                string local_typoDocumento;
                local_typoDocumento = TipoDoc;

                X509Certificate2 MiCertificado = new X509Certificate2(rutabase + oCertificado.Nombre, oCertificado.ClaveCertificado, X509KeyStorageFlags.MachineKeySet | X509KeyStorageFlags.PersistKeySet | X509KeyStorageFlags.Exportable);
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.PreserveWhitespace = true;
                xmlDoc.Load(local_xmlArchivo);

                SignedXml signedXml = new SignedXml(xmlDoc);
                signedXml.SigningKey = MiCertificado.PrivateKey;

                KeyInfo KeyInfo = new KeyInfo();

                Reference Reference = new Reference();
                Reference.Uri = "";

                Reference.AddTransform(new XmlDsigEnvelopedSignatureTransform());

                signedXml.AddReference(Reference);

                X509Chain X509Chain = new X509Chain();
                X509Chain.Build(MiCertificado);

                X509ChainElement local_element = X509Chain.ChainElements[0];
                KeyInfoX509Data x509Data = new KeyInfoX509Data(local_element.Certificate);
                string subjectName = local_element.Certificate.Subject;

                x509Data.AddSubjectName(subjectName);
                KeyInfo.AddClause(x509Data);

                signedXml.KeyInfo = KeyInfo;
                signedXml.ComputeSignature();

                XmlElement signature = signedXml.GetXml();
                signature.Prefix = "ds";
                signedXml.ComputeSignature();

                foreach (XmlNode node in signature.SelectNodes("descendant-or-self::*[namespace-uri()='http://www.w3.org/2000/09/xmldsig#']"))
                {
                    if (node.LocalName == "Signature")
                    {
                        XmlAttribute newAttribute = xmlDoc.CreateAttribute("Id");
                        newAttribute.Value = oCertificado.IdSignature;
                        node.Attributes.Append(newAttribute);
                        break;
                    }
                }

                string local_xpath = "";
                XmlNamespaceManager nsMgr;
                nsMgr = new XmlNamespaceManager(xmlDoc.NameTable);

                switch (local_typoDocumento)
                {
                    case var @case when @case == oCompSunat.IdFC:
                    case var case1 when case1 == oCompSunat.IdBV: // Factura / boleta  En la cabecera tiene [3] nodos UBLExtension
                
                        {
                            nsMgr.AddNamespace("tns", "urn:oasis:names:specification:ubl:schema:xsd:Invoice-2");
                            if (numExtension == 2)
                                local_xpath = "/tns:Invoice/ext:UBLExtensions/ext:UBLExtension[2]/ext:ExtensionContent";
                            else if (numExtension == 3)
                                local_xpath = "/tns:Invoice/ext:UBLExtensions/ext:UBLExtension[3]/ext:ExtensionContent";
                            else
                                local_xpath = "/tns:Invoice/ext:UBLExtensions/ext:UBLExtension[1]/ext:ExtensionContent";
                            break;
                        }

                case var case2 when case2 == oCompSunat.IdNC: // Nota de credito En la cabecera tiene [3] nodos UBLExtension
            
                        {
                            nsMgr.AddNamespace("tns", "urn:oasis:names:specification:ubl:schema:xsd:CreditNote-2");
                            if (numExtension == 2)
                                local_xpath = "/tns:CreditNote/ext:UBLExtensions/ext:UBLExtension[2]/ext:ExtensionContent";
                            else if (numExtension == 3)
                                local_xpath = "/tns:CreditNote/ext:UBLExtensions/ext:UBLExtension[3]/ext:ExtensionContent";
                            else
                                local_xpath = "/tns:CreditNote/ext:UBLExtensions/ext:UBLExtension[1]/ext:ExtensionContent";
                            break;
                        }

                    case var case3 when case3 == oCompSunat.IdND: // Nota de debito En la cabecera tiene [3] nodos UBLExtension
            
                        {
                            nsMgr.AddNamespace("tns", "urn:oasis:names:specification:ubl:schema:xsd:DebitNote-2");
                            if (numExtension == 2)
                                local_xpath = "/tns:DebitNote/ext:UBLExtensions/ext:UBLExtension[2]/ext:ExtensionContent";
                            else if (numExtension == 3)
                                local_xpath = "/tns:DebitNote/ext:UBLExtensions/ext:UBLExtension[3]/ext:ExtensionContent";
                            else
                                local_xpath = "/tns:DebitNote/ext:UBLExtensions/ext:UBLExtension[1]/ext:ExtensionContent";
                            break;
                        }

                    case var case4 when case4 == oCompSunat.IdCB: // Communicacion de baja
            
                        {
                            nsMgr.AddNamespace("tns", "urn:sunat:names:specification:ubl:peru:schema:xsd:VoidedDocuments-1");
                            local_xpath = "/tns:VoidedDocuments/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent";
                            break;
                        }

                    case var case5 when case5 == oCompSunat.IdRD: // Resumen de diario
            
                        {
                            nsMgr.AddNamespace("tns", "urn:sunat:names:specification:ubl:peru:schema:xsd:SummaryDocuments-1");
                            local_xpath = "/tns:SummaryDocuments/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent";
                            break;
                        }
                }
                nsMgr.AddNamespace("cac", "urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2");
                nsMgr.AddNamespace("cbc", "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2");
                if (local_typoDocumento == oCompSunat.IdFC | local_typoDocumento == oCompSunat.IdBV | local_typoDocumento == oCompSunat.IdNC | local_typoDocumento == oCompSunat.IdND)
                    nsMgr.AddNamespace("ccts", "urn:un:unece:uncefact:documentation:2");
                nsMgr.AddNamespace("ds", "http://www.w3.org/2000/09/xmldsig#");
                nsMgr.AddNamespace("ext", "urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2");
                if (local_typoDocumento == oCompSunat.IdFC | local_typoDocumento == oCompSunat.IdBV | local_typoDocumento == oCompSunat.IdNC | local_typoDocumento == oCompSunat.IdND)
                    nsMgr.AddNamespace("qdt", "urn:oasis:names:specification:ubl:schema:xsd:QualifiedDatatypes-2");
                nsMgr.AddNamespace("sac", "urn:sunat:names:specification:ubl:peru:schema:xsd:SunatAggregateComponents-1");
                if (local_typoDocumento == oCompSunat.IdFC | local_typoDocumento == oCompSunat.IdBV | local_typoDocumento == oCompSunat.IdNC | local_typoDocumento == oCompSunat.IdND)
                    nsMgr.AddNamespace("udt", "urn:un:unece:uncefact:data:specification:UnqualifiedDataTypesSchemaModule:2");
                nsMgr.AddNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");

                xmlDoc.SelectSingleNode(local_xpath, nsMgr).AppendChild(xmlDoc.ImportNode(signature, true));

                valorResumen = signature.ChildNodes.Item(0).ChildNodes.Item(2).ChildNodes.Item(2).FirstChild.Value;
                firma = signature.ChildNodes.Item(1).FirstChild.Value;


                System.IO.StreamWriter tw = new System.IO.StreamWriter(local_xmlArchivo, false, new System.Text.UTF8Encoding(false));
                xmlDoc.Save(tw);

                XmlNodeList nodeList = xmlDoc.GetElementsByTagName("ds:Signature");
                
                // el nodo <ds:Signature> debe existir unicamente 1 vez
                if (nodeList.Count != 1){
                    throw new Exception("Se produjo un error en la firma del documento");
                }
                    

                signedXml.LoadXml((XmlElement)nodeList[0]);

                cadenaXML = xmlDoc.OuterXml.ToString().Trim();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        
        // Validaciones    
        public static bool ValidarUsuarioEmpresa(int p_idempresa, string p_usuario, string p_clave, string documentoId, int Idpuntoventa)
        {
            NE_Certificado objCert = new NE_Certificado();
            if ((bool)objCert.validarEmpresaCertificado(p_idempresa, p_usuario, p_clave,  documentoId,  Idpuntoventa) == true)
                return true;
            else
                return false;
        }

    // /*************************FIN VERSION UBL 2.1*****************************/

        ﻿private static void Extraer2(string rutaFE, string Nombre)
        {
            ZipStorer zip = ZipStorer.Open(rutaFE + "R-" + Nombre + ".ZIP", FileAccess.Read);
            List<ZipStorer.ZipFileEntry> dir = zip.ReadCentralDir();
            string path__1 = rutaFE + "R-" + Nombre + ".XML";
            bool result;
            foreach (ZipStorer.ZipFileEntry entry in dir)
                result = zip.ExtractFile(entry, path__1);
            zip.Close();
        }

    private static void VerificarSunatCDR(XmlElement nodos, string codEtiqueta, ref string msjnodo, ref string codigo)
        {
            if (nodos.HasChildNodes)
            {
                if (nodos.ChildNodes[0].NodeType != XmlNodeType.Text & nodos.ChildNodes[0].NodeType != XmlNodeType.CDATA)
                {
                    foreach (XmlElement nodo in nodos.ChildNodes)
                    {
                        if (nodo.Name == "cac:DocumentResponse")
                        {
                            VerificarSunatCDR(nodo, codEtiqueta, ref msjnodo, ref codigo);
                        }
                        else if (nodo.Name == "cac:Response")
                        {
                            VerificarSunatCDR(nodo, codEtiqueta, ref msjnodo, ref codigo);
                        }
                        else if (nodo.Name == "cbc:ResponseCode")
                        {
                            if (nodo.FirstChild.Value.ToString().Replace(codEtiqueta, "").Trim() != "0")
                            {
                                // Rechazada con motivo
                                codigo = nodo.FirstChild.Value.ToString().Replace(codEtiqueta, "").Trim();
                                msjnodo = nodo.NextSibling.FirstChild.Value;
                            }
                            else
                            {
                                // Aceptada correctamente
                                codigo = "";
                                msjnodo = nodo.NextSibling.FirstChild.Value;
                            }

                            break;
                        }
                    }
                }
            }
        }

        




    public static bool GenerarXmlResumen(EN_Resumen oResumen, EN_ComprobanteSunat oCompSunat, EN_Empresa oEmpresa, EN_Certificado oCertificado, string ruta, string rutabase, ref string nombre, ref string valorResumen, ref string firma, ref string msjRspta, ref string cadenaXML)
        {
            //DESCRIPCION: FUNCION PARA GENERAR XML RESUMEN

            XmlWriterSettings settings = new XmlWriterSettings();   // settings
            string prefixCac;   // prefijo Cac
            string prefixCbc;   // prefijo Cbc
            string prefixDs;    // prefijo Ds
            string prefixExt;   // prefijo Ext
            string prefixSac;   // prefijo Sac
            string cadCac = "urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2"; //cadCac
            string cadCbc = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"; //cadCbc
            string cadDs = "http://www.w3.org/2000/09/xmldsig#";        //cadDs
            string cadExt = "urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2"; // cadExt
            string cadSac = "urn:sunat:names:specification:ubl:peru:schema:xsd:SunatAggregateComponents-1"; // cadSac
            string cadXsi = "http://www.w3.org/2001/XMLSchema-instance";    //cadXsi
            NE_Resumen objProceso = new NE_Resumen();       // clase de negocio resumen
            List<EN_DetalleResumen> listaDetalle;           // lista de clase entidad detalle resumen

            string rutaemp;     // ruta temporal

            rutaemp = string.Format("{0}/{1}/", rutabase, oResumen.Nrodocumento);
    

            // Validamos si existe directorio del documento, de lo contrario lo creamos
            if ((!System.IO.Directory.Exists(ruta)))
                System.IO.Directory.CreateDirectory(ruta);

            // CREAMOS TODOS LOS DIRECTORIOS NECESARIOS. SI NO EXISTE
            
            if ((!System.IO.Directory.Exists(ruta.Trim() + EN_Constante.g_const_rutaSufijo_xml)))
                System.IO.Directory.CreateDirectory(ruta.Trim() + EN_Constante.g_const_rutaSufijo_xml);

            if ((!System.IO.Directory.Exists(ruta.Trim() + EN_Constante.g_const_rutaSufijo_pdf)))
                System.IO.Directory.CreateDirectory(ruta.Trim() + EN_Constante.g_const_rutaSufijo_pdf);

                if ((!System.IO.Directory.Exists(ruta.Trim() + EN_Constante.g_const_rutaSufijo_cdr)))
                System.IO.Directory.CreateDirectory(ruta.Trim() + EN_Constante.g_const_rutaSufijo_cdr);


            ruta=ruta + EN_Constante.g_const_rutaSufijo_xml;

            // Validamos si existe comprobante en XML, ZIP y PDF generado y eliminamos
            if (File.Exists(ruta.Trim() + nombre.Trim() + EN_Constante.g_const_extension_xml))
                File.Delete(ruta.Trim() + nombre.Trim() + EN_Constante.g_const_extension_xml);
            if (File.Exists(ruta.Trim() + nombre.Trim() + EN_Constante.g_const_extension_zip))
                File.Delete(ruta.Trim() + nombre.Trim() + EN_Constante.g_const_extension_zip);
            if (File.Exists(ruta.Trim() + nombre.Trim() + EN_Constante.g_const_extension_pdf))
                File.Delete(ruta.Trim() + nombre.Trim() + EN_Constante.g_const_extension_pdf);
            // Validamos si existe CDR en XML y ZIP y eliminamos
            if (File.Exists(ruta.Trim() + EN_Constante.g_const_prefijoXmlCdr_respuesta + nombre.Trim() + EN_Constante.g_const_extension_xml))
                File.Delete(ruta.Trim() + EN_Constante.g_const_prefijoXmlCdr_respuesta + nombre.Trim() + EN_Constante.g_const_extension_xml);
            if (File.Exists(ruta.Trim() + EN_Constante.g_const_prefijoXmlCdr_respuesta + nombre.Trim() + EN_Constante.g_const_extension_zip))
                File.Delete(ruta.Trim() + EN_Constante.g_const_prefijoXmlCdr_respuesta + nombre.Trim() + EN_Constante.g_const_extension_zip);

            try
            {
                listaDetalle = (List<EN_DetalleResumen>)objProceso.listarDetalleResumen(oResumen);
                nombre = oResumen.Nrodocumento.Trim() + "-" + oResumen.Tiporesumen.Trim() + "-" +
                Strings.Format(Conversions.ToDate(oResumen.FechaGeneraResumen), "yyyyMMdd")
                + "-" + oResumen.Correlativo.ToString();
                settings.Indent = true;
                // Create XmlWriter.
                settings.Encoding = System.Text.UTF8Encoding.UTF8;
                using (XmlWriter writer = XmlWriter.Create(ruta.Trim() + nombre.Trim() + EN_Constante.g_const_extension_xml, settings))
                {
                    prefixCac = writer.LookupPrefix(cadCac);
                    prefixExt = writer.LookupPrefix(cadExt);
                    prefixSac = writer.LookupPrefix(cadSac);
                    prefixCbc = writer.LookupPrefix(cadCbc);
                    prefixDs = writer.LookupPrefix(cadDs);
                    // Begin writing.
                    writer.WriteStartDocument();
                    writer.WriteStartElement("SummaryDocuments", "urn:sunat:names:specification:ubl:peru:schema:xsd:SummaryDocuments-1");

                    writer.WriteAttributeString("xmlns", "cac", null/* TODO Change to default(_) if this is not a reference type */, cadCac);
                    writer.WriteAttributeString("xmlns", "cbc", null/* TODO Change to default(_) if this is not a reference type */, cadCbc);
                    writer.WriteAttributeString("xmlns", "ds", null/* TODO Change to default(_) if this is not a reference type */, cadDs);
                    writer.WriteAttributeString("xmlns", "ext", null/* TODO Change to default(_) if this is not a reference type */, cadExt);
                    writer.WriteAttributeString("xmlns", "sac", null/* TODO Change to default(_) if this is not a reference type */, cadSac);
                    writer.WriteAttributeString("xmlns", "xsi", null/* TODO Change to default(_) if this is not a reference type */, cadXsi);

                    ObtenerExtensionesResumen(writer, prefixExt, prefixSac, prefixCbc, prefixDs, cadExt, cadSac, cadCbc, cadDs);
                    ObtenerDatosCabeceraResumen(writer, prefixCbc, prefixCac, cadCbc, cadCac, oResumen, oEmpresa);
                    if (listaDetalle.Count > 0)
                    {
                        foreach (var item in listaDetalle.ToList())
                            ObtenerDatosDetalleResumen(writer, prefixCbc, prefixCac, prefixSac, cadCbc, cadCac, cadSac, item);
                    }
                    writer.WriteEndElement();
                    writer.WriteEndDocument();
                }
                AgregarFirma(oCertificado, ruta, rutaemp, nombre, ref valorResumen,ref firma, oResumen.Tiporesumen, oCompSunat, 2, ref cadenaXML);

                //COPIAMOS XML A S3 Y ELIMINAMOS DE TEMPORAL
                AppConfiguracion.FuncionesS3.CopyS3DeleteLocal(oResumen.Nrodocumento.Trim(),rutabase,EN_Constante.g_const_rutaSufijo_xml,nombre.Trim(),EN_Constante.g_const_extension_xml);

                msjRspta = "Resumen Diario Generado Correctamente";
                return true;
            }
            catch (Exception ex)
            {
                // En caso de error eliminamos el XML generado
                if (File.Exists(ruta.Trim() + nombre.Trim() + ".XML"))
                    File.Delete(ruta.Trim() + nombre.Trim() + ".XML");

                // Guardamos en el LOG que hubo error al generar el XML
                msjRspta = "Error al generar Resumen Diario: " + ex.Message.ToString();
                return false;
            }
        }
        // Inicio Resumen Electronico
        public static bool GenerarXmlResumenElectronico(EN_Resumen oResumen, EN_Empresa oEmpresa, EN_Certificado oCertificado, EN_ComprobanteSunat oCompSunat, string ruta, string rutabase, ref string nombre, ref string vResumen, ref string vFirma, ref string MsjRspta)
        {
            List<EN_DetalleResumen> detalleResumen = new List<EN_DetalleResumen>();
            NE_Resumen objResumen = new NE_Resumen();
            string mensaje = "";
            string cadenaXML = "";
            try
            {
                // Generamos el XML
                if (GenerarXmlResumen(oResumen, oCompSunat, oEmpresa, oCertificado, ruta, rutabase, ref nombre, ref vResumen, ref vFirma, ref mensaje, ref cadenaXML))
                {
                    // Actualizo la tabla documento los campos nombrexml valorresumen y valorfirma
                    if (GuardarResumenNombreValorResumenFirma(oResumen, nombre, vResumen, vFirma, cadenaXML))
                    {
                        detalleResumen = (List<EN_DetalleResumen>)objResumen.listarDetalleResumen(oResumen);
                        return true;
                    }
                    else
                    {
                        nombre = "";
                        vResumen = "";
                        vFirma = "";
                        MsjRspta = "Error al guardar el valor resumen, firma digital y nombre del archivo XML.";
                        return false;
                    }
                }
                else
                {
                    nombre = "";
                    vResumen = "";
                    vFirma = "";
                    MsjRspta = "Error generar XML de Resumen.";
                    return false;
                }
            }
            catch (Exception ex)
            {
                
                return false;
                throw ex;
            }
        }

        private static XmlWriter ObtenerExtensionesResumen(XmlWriter writer, string prefixExt, string prefixSac, string prefixCbc, string prefixDs, string cadExt, string cadSac, string cadCbc, string cadDs)
        {
            writer.WriteStartElement(prefixExt, "UBLExtensions", cadExt);
            writer.WriteStartElement(prefixExt, "UBLExtension", cadExt);
            writer.WriteStartElement(prefixExt, "ExtensionContent", cadExt);
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            return writer;
        }
        private static XmlWriter ObtenerDatosCabeceraResumen(XmlWriter writer, string prefixCbc, string prefixCac, string cadCbc, string cadCac, EN_Resumen oResumen, EN_Empresa oEmpresa)
        {
            //DESCRIPCION: FUNCION PARA OBTENER DATOS DE CABECERA RESUMEN

            NE_Facturacion objFacturacion = new NE_Facturacion();   // Clase de negocio facturación
            NE_DocPersona objDocPersona = new NE_DocPersona();      // clase de negocio docpersona
            EN_DocPersona oDocPersona = new EN_DocPersona();    // clase de entidad docpersona
            List<EN_DocPersona> listaDocPersona = new List<EN_DocPersona>();    // lista de clase entidad docpersona

            // Listamos el codigo del tipo de documento RUC
            oDocPersona.Codigo = "RUC";
            oDocPersona.Estado = "";
            oDocPersona.Indpersona = "";
            listaDocPersona =(List<EN_DocPersona>)objDocPersona.listarDocPersona(oDocPersona, oResumen.Id.ToString(), oEmpresa.Id);

            writer.WriteElementString("UBLVersionID", cadCbc, "2.0");
            writer.WriteElementString("CustomizationID", cadCbc, "1.1");

            // Datos del resumen
            writer.WriteElementString("ID", cadCbc, oResumen.Tiporesumen.Trim() + "-" +
            Strings.Format(Conversions.ToDate(oResumen.FechaGeneraResumen), "yyyyMMdd")
            + "-" + oResumen.Correlativo.ToString());
            
            writer.WriteElementString("ReferenceDate", cadCbc, Convert.ToDateTime(oResumen.FechaEmisionDocumento).ToString("yyyy-MM-dd"));
            writer.WriteElementString("IssueDate", cadCbc,  Convert.ToDateTime(oResumen.FechaGeneraResumen).ToString("yyyy-MM-dd"));

            // Datos de la empresa
            writer.WriteStartElement(prefixCac, "Signature", cadCac);
            writer.WriteElementString("ID", cadCbc, oEmpresa.SignatureID.Trim());
            writer.WriteStartElement(prefixCac, "SignatoryParty", cadCac);
            writer.WriteStartElement(prefixCac, "PartyIdentification", cadCac);
            writer.WriteElementString("ID", cadCbc, oEmpresa.Nrodocumento.Trim());
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCac, "PartyName", cadCac);
            writer.WriteStartElement(prefixCbc, "Name", cadCbc);
            writer.WriteCData(oEmpresa.RazonSocial.Trim());
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCac, "DigitalSignatureAttachment", cadCac);
            writer.WriteStartElement(prefixCac, "ExternalReference", cadCac);
            writer.WriteElementString("URI", cadCbc, oEmpresa.SignatureURI.Trim());
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCac, "AccountingSupplierParty", cadCac);
            writer.WriteElementString("CustomerAssignedAccountID", cadCbc, oEmpresa.Nrodocumento.Trim());
            writer.WriteElementString("AdditionalAccountID", cadCbc, listaDocPersona[0].CodigoSunat.Trim());
            writer.WriteStartElement(prefixCac, "Party", cadCac);
            writer.WriteStartElement(prefixCac, "PartyLegalEntity", cadCac);
            writer.WriteStartElement(prefixCbc, "RegistrationName", cadCbc);
            writer.WriteCData(oEmpresa.RazonSocial.Trim());
            writer.WriteEndElement();
            writer.WriteEndElement();

            writer.WriteEndElement();
            writer.WriteEndElement();

            return writer;
        }
        private static XmlWriter ObtenerDatosDetalleResumen(XmlWriter writer, string prefixCbc, string prefixCac, string prefixSac, string cadCbc, string cadCac, string cadSac, EN_DetalleResumen listaDetalle)
        {
            //DESCRIPCION: FUNCION PARA OBTENER DATOS  DE DETALLE RESUMEN

            NE_Facturacion objFacturacion = new NE_Facturacion();   // Clase de negocio facturación
            string moneda;      // moneda
            // Datos del item de Resumen
            moneda = listaDetalle.Moneda.Trim();
            writer.WriteStartElement(prefixSac, "SummaryDocumentsLine", cadSac);

            // Nro de Linea de Resumen
            writer.WriteElementString("LineID", cadCbc, listaDetalle.Item.ToString());
            // Serie y número de comprobante
            writer.WriteElementString("DocumentTypeCode", cadCbc, listaDetalle.Tipodocumentoid);
            writer.WriteElementString("ID", cadCbc, listaDetalle.Serie + "-" + listaDetalle.Correlativo);

            // Documento de identidad del cliente
            if (listaDetalle.NroDocCliente.Trim() != "")
            {
                writer.WriteStartElement(prefixCac, "AccountingCustomerParty", cadCac);
                writer.WriteElementString("CustomerAssignedAccountID", cadCbc, listaDetalle.NroDocCliente);
                writer.WriteElementString("AdditionalAccountID", cadCbc, listaDetalle.DocCliente);
                writer.WriteEndElement();
            }

            // Documento de Referencia
            if (listaDetalle.Tipodocumentoid == "07" | listaDetalle.Tipodocumentoid == "08")
            {
                writer.WriteStartElement(prefixCac, "BillingReference", cadCac);
                writer.WriteStartElement(prefixCac, "InvoiceDocumentReference", cadCac);
                writer.WriteElementString("ID", cadCbc, listaDetalle.Nrodocref);
                writer.WriteElementString("DocumentTypeCode", cadCbc, listaDetalle.Tipodocref);
                writer.WriteEndElement();
                writer.WriteEndElement();
            }

            // Datos de la percepción
            if (listaDetalle.Regimenpercep != "")
            {
                writer.WriteStartElement(prefixSac, "SUNATPerceptionSummaryDocumentReference", cadSac);
                writer.WriteElementString("SUNATPerceptionSystemCode", cadSac, listaDetalle.Regimenpercep);
                writer.WriteElementString("SUNATPerceptionPercent", cadSac, (Math.Round(listaDetalle.Porcentpercep, 3)).ToString().Replace(",", "."));
                writer.WriteStartElement(prefixCbc, "TotalInvoiceAmount", cadCbc);
                writer.WriteAttributeString("currencyID", moneda);
                writer.WriteValue(listaDetalle.Importepercep.ToString("F2").Replace(",", "."));
                writer.WriteEndElement();
                // writer.WriteElementString("SUNATTotalCashed", cadSac, listaDetalle.Importefinal)
                writer.WriteStartElement(prefixSac, "SUNATTotalCashed", cadSac);
                writer.WriteAttributeString("currencyID", moneda);
                writer.WriteValue(listaDetalle.Importefinal.ToString("F2").Replace(",", "."));
                writer.WriteEndElement();
                writer.WriteStartElement(prefixCbc, "TaxableAmount", cadCbc);
                writer.WriteAttributeString("currencyID", moneda);
                writer.WriteValue(listaDetalle.Importeventa.ToString("F2").Replace(",", "."));
                writer.WriteEndElement();
                writer.WriteEndElement();
            }

            // Estado del ítem
            writer.WriteStartElement(prefixCac, "Status", cadCac);
            writer.WriteElementString("ConditionCode", cadCbc, listaDetalle.Condicion.ToString());
            writer.WriteEndElement();

            // Importe total de la venta
            writer.WriteStartElement(prefixSac, "TotalAmount", cadSac);
            writer.WriteAttributeString("currencyID", moneda);
            writer.WriteValue(listaDetalle.Importefinal.ToString("F2").Replace(",", "."));
            writer.WriteEndElement();

            // Operaciones Gravadas
            if (listaDetalle.Opegravadas != 0)
            {
                writer.WriteStartElement(prefixSac, "BillingPayment", cadSac);
                writer.WriteStartElement(prefixCbc, "PaidAmount", cadCbc);
                writer.WriteAttributeString("currencyID", moneda);
                writer.WriteValue(listaDetalle.Opegravadas.ToString("F2").Replace(",", "."));
                writer.WriteEndElement();
                writer.WriteStartElement(prefixCbc, "InstructionID", cadCbc);
                writer.WriteValue("01");
                writer.WriteEndElement();
                writer.WriteEndElement();
            }

            // Operaciones Exoneradas
            if (listaDetalle.Opeexoneradas != 0)
            {
                writer.WriteStartElement(prefixSac, "BillingPayment", cadSac);
                writer.WriteStartElement(prefixCbc, "PaidAmount", cadCbc);
                writer.WriteAttributeString("currencyID", moneda);
                writer.WriteValue(listaDetalle.Opeexoneradas.ToString("F2").Replace(",", "."));
                writer.WriteEndElement();
                writer.WriteStartElement(prefixCbc, "InstructionID", cadCbc);
                writer.WriteValue("02");
                writer.WriteEndElement();
                writer.WriteEndElement();
            }

            // Operaciones Inafectas
            if (listaDetalle.Opeinafectas != 0)
            {
                writer.WriteStartElement(prefixSac, "BillingPayment", cadSac);
                writer.WriteStartElement(prefixCbc, "PaidAmount", cadCbc);
                writer.WriteAttributeString("currencyID", moneda);
                writer.WriteValue(listaDetalle.Opeinafectas.ToString("F2").Replace(",", "."));
                writer.WriteEndElement();
                writer.WriteStartElement(prefixCbc, "InstructionID", cadCbc);
                writer.WriteValue("03");
                writer.WriteEndElement();
                writer.WriteEndElement();
            }

            // Operaciones Exportación
            if (listaDetalle.Opeexportacion != 0)
            {
                writer.WriteStartElement(prefixSac, "BillingPayment", cadSac);
                writer.WriteStartElement(prefixCbc, "PaidAmount", cadCbc);
                writer.WriteAttributeString("currencyID", moneda);
                writer.WriteValue(listaDetalle.Opeexportacion.ToString("F2").Replace(",", "."));
                writer.WriteEndElement();
                writer.WriteStartElement(prefixCbc, "InstructionID", cadCbc);
                writer.WriteValue("04");
                writer.WriteEndElement();
                writer.WriteEndElement();
            }

            // Operaciones Gratuitas
            if (listaDetalle.Opegratuitas != 0)
            {
                writer.WriteStartElement(prefixSac, "BillingPayment", cadSac);
                writer.WriteStartElement(prefixCbc, "PaidAmount", cadCbc);
                writer.WriteAttributeString("currencyID", moneda);
                writer.WriteValue(listaDetalle.Opegratuitas.ToString("F2").Replace(",", "."));
                writer.WriteEndElement();
                writer.WriteStartElement(prefixCbc, "InstructionID", cadCbc);
                writer.WriteValue("05");
                writer.WriteEndElement();
                writer.WriteEndElement();
            }

            // Otros cargos del item
            if (listaDetalle.Otroscargos != 0)
            {
                writer.WriteStartElement(prefixCac, "AllowanceCharge", cadCac);
                writer.WriteElementString("ChargeIndicator", cadCbc, "true");
                writer.WriteStartElement(prefixCbc, "Amount", cadCbc);
                writer.WriteAttributeString("currencyID", moneda);
                writer.WriteValue(listaDetalle.Otroscargos.ToString("F2").Replace(",", "."));
                writer.WriteEndElement();
                writer.WriteEndElement();
            }

            // ISC
            if (listaDetalle.TotalISC != 0)
            {
                writer.WriteStartElement(prefixCac, "TaxTotal", cadCac);
                writer.WriteStartElement(prefixCbc, "TaxAmount", cadCbc);
                writer.WriteAttributeString("currencyID", moneda);
                writer.WriteValue(listaDetalle.TotalISC.ToString("F2").Replace(",", "."));
                writer.WriteEndElement();
                writer.WriteStartElement(prefixCac, "TaxSubtotal", cadCac);
                writer.WriteStartElement(prefixCbc, "TaxAmount", cadCbc);
                writer.WriteAttributeString("currencyID", moneda);
                writer.WriteValue(listaDetalle.TotalISC.ToString("F2").Replace(",", "."));
                writer.WriteEndElement();
                writer.WriteStartElement(prefixCac, "TaxCategory", cadCac);
                writer.WriteStartElement(prefixCac, "TaxScheme", cadCac);
                writer.WriteElementString("ID", cadCbc, "2000");
                writer.WriteStartElement(prefixCbc, "Name", cadCbc);
                writer.WriteCData(objFacturacion.ObtenerDescripcionPorCodElemento("005", "2000").Trim());
                writer.WriteEndElement();
                writer.WriteElementString("TaxTypeCode", cadCbc, objFacturacion.ObtenerCodigoSunat("005", "2000").Trim());
                writer.WriteEndElement();
                writer.WriteEndElement();
                writer.WriteEndElement();
                writer.WriteEndElement();
            }

            // IGV
            writer.WriteStartElement(prefixCac, "TaxTotal", cadCac);
            writer.WriteStartElement(prefixCbc, "TaxAmount", cadCbc);
            writer.WriteAttributeString("currencyID", moneda);
            writer.WriteValue(listaDetalle.TotalIGV.ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCac, "TaxSubtotal", cadCac);
            writer.WriteStartElement(prefixCbc, "TaxAmount", cadCbc);
            writer.WriteAttributeString("currencyID", moneda);
            writer.WriteValue(listaDetalle.TotalIGV.ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCac, "TaxCategory", cadCac);
            writer.WriteStartElement(prefixCac, "TaxScheme", cadCac);
            writer.WriteElementString("ID", cadCbc, "1000");
            writer.WriteStartElement(prefixCbc, "Name", cadCbc);
            writer.WriteCData(objFacturacion.ObtenerDescripcionPorCodElemento("005", "1000").Trim());
            writer.WriteEndElement();
            writer.WriteElementString("TaxTypeCode", cadCbc, objFacturacion.ObtenerCodigoSunat("005", "1000").Trim());
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();

            // Otros Tributos
            if (listaDetalle.Otrostributos != 0)
            {
                writer.WriteStartElement(prefixCac, "TaxTotal", cadCac);
                writer.WriteStartElement(prefixCbc, "TaxAmount", cadCbc);
                writer.WriteAttributeString("currencyID", moneda);
                writer.WriteValue(listaDetalle.Otrostributos.ToString("F2").Replace(",", "."));
                writer.WriteEndElement();
                writer.WriteStartElement(prefixCac, "TaxSubtotal", cadCac);
                writer.WriteStartElement(prefixCbc, "TaxAmount", cadCbc);
                writer.WriteAttributeString("currencyID", moneda);
                writer.WriteValue(listaDetalle.Otrostributos.ToString("F2").Replace(",", "."));
                writer.WriteEndElement();
                writer.WriteStartElement(prefixCac, "TaxCategory", cadCac);
                writer.WriteStartElement(prefixCac, "TaxScheme", cadCac);
                writer.WriteElementString("ID", cadCbc, "9999");
                writer.WriteStartElement(prefixCbc, "Name", cadCbc);
                writer.WriteCData(objFacturacion.ObtenerDescripcionPorCodElemento("005", "9999").Trim());
                writer.WriteEndElement();
                writer.WriteElementString("TaxTypeCode", cadCbc, objFacturacion.ObtenerCodigoSunat("005", "9999").Trim());
                writer.WriteEndElement();
                writer.WriteEndElement();
                writer.WriteEndElement();
                writer.WriteEndElement();
            }

            writer.WriteEndElement();

            return writer;
        }
        public static void EnviarXmlResumenElectronico(EN_Resumen oResumen, EN_Certificado oCertificado, EN_ComprobanteSunat oCompSunat, string ruta, string nombre, string vResumen, string vFirma, ref string msjWA, ref int codRspta,string produccion, int Idpuntoventa)
        {

            //DESCRIPCION: FUNCION PARA ENVIAR XML DE RESUMEN ELECTRONICO A SUNAT


            NE_Facturacion objFact = new NE_Facturacion();      // clase de negocio facturación

            try
            {
        
                
                EN_RequestEnvioResDiario data=new EN_RequestEnvioResDiario() ;

                data.flagOse= oCertificado.flagOSE;
                data.ruc    = oResumen.Nrodocumento;
                data.nombreArchivo= nombre.Trim();
                data.certUserName = oCertificado.UserName;
                data.certClave = oCertificado.Clave;
                data.Produccion= produccion;
                data.resumenId= oResumen.Id.ToString();
                data.empresaId = oResumen.Empresa_id.ToString(); 
                data.Idpuntoventa = Idpuntoventa.ToString();
                data.tiporesumenId = oResumen.Tiporesumen.ToString();
                data.FechaGeneraResumen = oResumen.FechaGeneraResumen;
                data.correlativo= oResumen.Correlativo.ToString();
                
                var objConf =new Configuracion();
                var urlWA =  EN_ConfigConstantes.Instance.const_urlServiceDoc +  EN_ConfigConstantes.Instance.const_apiSendResumenDiario;
                
                string dataSerializado = System.Text.Json.JsonSerializer.Serialize(data);

                dynamic responseWA= objConf.sendWebApi(urlWA, dataSerializado);
                if(responseWA.RptaRegistroCpeDoc.FlagVerificacion){
                    msjWA= responseWA.RptaRegistroCpeDoc.MensajeResp;
                    codRspta = EN_Constante.g_const_0;

                }else{
                    msjWA= "Error al enviar XML Resumen Diario- " +responseWA.RptaRegistroCpeDoc.MensajeResp;
                    codRspta = EN_Constante.g_const_1;
                }
                
                
            }
            catch (Exception ex)
            {

                throw new Exception("Error en catch al enviar XML: "+ex.Message);
                
            }
        
        }

        public static ClassRptaGenerarCpeDoc EnviarXmlComunicadBaja(EN_RequestEnvioResDiario data, ref string msjWA, ref int codRspta, ref int contError)
        {

            //DESCRIPCION: FUNCION PARA ENVIAR XML DE COMUNICADO DE BAJA A SUNAT

    

            NE_Facturacion objFact = new NE_Facturacion();      // clase de negocio facturación
            ClassRptaGenerarCpeDoc responseWA= new ClassRptaGenerarCpeDoc();    // Clase de respuesta comprobante
            var objConf =new Configuracion();                                   // Clase de Configuracion
            string urlWA= EN_Constante.g_const_vacio;                           // Dirección Url para envio de xml
            string dataSerializado= EN_Constante.g_const_vacio;                 // Variable de data serializado
            try
            {
                
                if(data.tiporesumenId== EN_ConfigConstantes.Instance.const_IdCB)
                {
                    urlWA =  EN_ConfigConstantes.Instance.const_urlServiceDoc +  EN_ConfigConstantes.Instance.const_apiSendComunicadoBaja;
                
                }
                else {
                    urlWA =  EN_ConfigConstantes.Instance.const_urlServiceDoc +  EN_ConfigConstantes.Instance.const_apiSendResumenDiario;
                    
                }
                
                
                dataSerializado= System.Text.Json.JsonSerializer.Serialize(data);
                
                responseWA= objConf.sendWebApi(urlWA, dataSerializado);
                
                if(responseWA.RptaRegistroCpeDoc.FlagVerificacion)
                {
                    msjWA =msjWA+ data.tiporesumenId+"-ID-A: "+data.resumenId+", ";
                    codRspta = EN_Constante.g_const_0;
                }
                else
                {
                    msjWA =msjWA+ data.tiporesumenId+"-ID-E: "+data.resumenId+", ";
                    codRspta = EN_Constante.g_const_1;
                    contError++;
                }

                return responseWA;
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
        
        }


        public static void EnviarXmlDocumentoElectronico(EN_Documento oDocumento, EN_Certificado oCertificado, EN_ComprobanteSunat oCompSunat, string produccion,string ruta, string nombre, ref string msjWA, ref int codRspta, ref int contError)
        {
            // DESCRIPCION: Este metodo se encarga de enviar a SUNAT el comprobante generado
            string rutaXml=ruta+ EN_Constante.g_const_rutaSufijo_xml;     // RUTA XML
            string rutaCdr=ruta+ EN_Constante.g_const_rutaSufijo_cdr;     // RUTA CDR
            EN_RequestEnviodoc data=new EN_RequestEnviodoc() ;      // CLASE ENTIDAD SOLICITUD ENVIO DOCUMENTO
            
            var objConf =new Configuracion();                                   // Clase de Configuracion

            // ParallelEnumerable validar xml (AQUI DEBE IR VALIDACION XSD Y XSL)
            try
            {
            
                    data.flagOse= oCertificado.flagOSE;
                    data.rutaDoc= ruta;
                    data.nombreArchivo= nombre.Trim();
                    data.certUserName = oCertificado.UserName;
                    data.certClave = oCertificado.Clave;
                    data.Produccion= produccion;
                    data.documentoId= oDocumento.Id;
                    data.empresaId = oDocumento.Idempresa.ToString(); 
                    data.Idpuntoventa = oDocumento.Idpuntoventa.ToString();
                    data.tipodocumentoId = oDocumento.Idtipodocumento.ToString();
                    data.fecha = oDocumento.Fecha;
                    data.Serie= oDocumento.Serie;
                    data.Numero= oDocumento.Numero;
                    
                    var urlWA =  EN_ConfigConstantes.Instance.const_urlServiceDoc +  EN_ConfigConstantes.Instance.const_apiSendDoc;
                    string dataSerializado = System.Text.Json.JsonSerializer.Serialize(data);

                    dynamic responseWA= objConf.sendWebApi(urlWA, dataSerializado);
                    if(responseWA.RptaRegistroCpeDoc.FlagVerificacion){
                        msjWA= responseWA.RptaRegistroCpeDoc.MensajeResp;
                        codRspta = EN_Constante.g_const_0;
                        contError= contError+0;

                    }else{
                        msjWA= "Error al enviar XML- " +responseWA.RptaRegistroCpeDoc.MensajeResp + " | Error al enviar XML-"+responseWA.ErrorWebService.DescripcionErr;
                        codRspta = EN_Constante.g_const_1;
                        contError= contError+1;
                    }
                
                    
            }
            catch (Exception ex)
            {
                msjWA = "Se generó el XML - Error al enviar XML: " + ex.Message;
                codRspta = EN_Constante.g_const_2;
                contError= contError+1;
            
            }

        }


        public static bool GuardarResumenNombreValorResumenFirma(EN_Resumen oResumen, string nombreXML, string valorResumen, string valorFirma, string xmlgenerado)
        {
            NE_Resumen objBaja = new NE_Resumen();
            return objBaja.GuardarResumenNombreValorResumenFirma(oResumen, nombreXML, valorResumen, valorFirma, xmlgenerado);
        }

    
    }
    
}
