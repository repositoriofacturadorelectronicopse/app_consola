/****************************************************************************************************************************************************************************************
 PROGRAMA: ProcesarXmlFactura.cs
 VERSION : 1.0
 OBJETIVO: Clase para procesar facturas
 FECHA   : 21/06/2021
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using CEN;
using CLN;

namespace AppServicioDoc
{
    public partial class ProcesarXmlFactura
    {
        
       
        public ProcesarXmlFactura()
        {
            //DESCRIPCION: CONTRUCTOR DE CLASE ProcesarXmlFactura
        }
      
       
        private EN_ComprobanteSunat buildComprobante()
        {
            //DESCRIPCION: FUNCION PARA CONTRUIR ID DE COMPROBANTES EN CLASE COMPROBANTE SUNAT
                var oCompSunat = new EN_ComprobanteSunat(); // clase de entidad comprobante sunat

                oCompSunat.IdFC = EN_ConfigConstantes.Instance.const_IdFC;
                oCompSunat.IdBV = EN_ConfigConstantes.Instance.const_IdBV;
                oCompSunat.IdNC = EN_ConfigConstantes.Instance.const_IdNC;
                oCompSunat.IdND = EN_ConfigConstantes.Instance.const_IdND;
                oCompSunat.IdGR = EN_ConfigConstantes.Instance.const_IdGR;

                oCompSunat.IdCB =  EN_ConfigConstantes.Instance.const_IdCB;
                oCompSunat.IdRD = EN_ConfigConstantes.Instance.const_IdRD;
                oCompSunat.IdRR =  EN_ConfigConstantes.Instance.const_IdRR;

                oCompSunat.IdCP = EN_ConfigConstantes.Instance.const_IdCP;
                oCompSunat.IdCR =  EN_ConfigConstantes.Instance.const_IdCR;

                oCompSunat.codigoEtiquetaErrorDoc = EN_ConfigConstantes.Instance.const_codigoEtiquetaErrorDoc;
                oCompSunat.codigoEtiquetaErrorRes = EN_ConfigConstantes.Instance.const_codigoEtiquetaErrorRes;
                return oCompSunat;
        }

        

        public void ProcesaFacturaSunat(int numdias, int valEmpId,ref string msjrspta, ref int codrspta, ref int contError)
        {
            string rutabase,  rutadocs; //ruta base y ruta de documentos
            string nombre;  // variable nombre
            int codRpt=0; // codigo respuesta
            string fileS3XML =EN_Constante.g_const_vacio;        //Ruta de xml en S3
            bool existeXmlS3;                                   // Booleano para existencia de objecto en S3

            var oCompSunat = new EN_ComprobanteSunat(); // clase entidad comprobante sunat
            EN_Empresa empresaDocumento = new EN_Empresa(); // clase entidad empresa
            NE_Proceso objProc = new NE_Proceso(); // clase negocio proceso
            var oProceso = new EN_Proceso();        // clase entidad proceso
            List<EN_Proceso> listaProc = new List<EN_Proceso>(); // lista de clase entidad proceso
            EN_Certificado certificadoEmpresa = new EN_Certificado(); // clase entidad certificado
            NE_Documento obj = new NE_Documento();  //clase negocio documento
            var oDocumento = new EN_Documento();        //clase entidad documento
            List<EN_Documento> listaDocumento = new List<EN_Documento>(); // lista de calse entidad documento
            AppConfiguracion.Configuracion confg= new AppConfiguracion.Configuracion(); // Clase configuración
            EN_RequestEnviodoc data;                              // CLASE ENTIDAD SOLICITUD ENVIO DOCUMENTO
            

            string mensajeWA=EN_Constante.g_const_vacio;

            string p_tipo_docs=EN_Constante.g_const_vacio;
            string p_url=EN_Constante.g_const_vacio;

            try
            {
                DateTime datetimenow= confg.getDateTimeNowLima();
                
                oCompSunat = buildComprobante();
                // Busca Procesos Activados
                oProceso.Empresa_id = valEmpId;
                oProceso.Estado = EN_Constante.g_const_estado_proceso;

                p_tipo_docs= oCompSunat.IdFC + "," + oCompSunat.IdNC + "," + oCompSunat.IdND;

                listaProc = (List<EN_Proceso>)objProc.listarProcesosActivos(oProceso.Empresa_id, p_tipo_docs, oProceso.Estado);

                if (listaProc.Count > 0)
                {
                    foreach (var item in listaProc.ToList())
                    {
                        empresaDocumento = GetEmpresaById(item.Empresa_id);
                        certificadoEmpresa = GetCertificadoByIdEmpresa(item.Empresa_id);

                        oDocumento.Id = EN_Constante.g_const_vacio;
                        oDocumento.Idempresa = item.Empresa_id;
                        oDocumento.Estado = EN_Constante.g_const_estado_nuevo;
                        oDocumento.CadenaAleatoria = EN_Constante.g_const_vacio;
                        oDocumento.UsuarioSession = EN_Constante.g_const_usuario_session;
                        
                        oDocumento.FechaIni= datetimenow.AddDays(numdias).ToString(EN_Constante.g_const_formfech);
                        oDocumento.FechaFin=  datetimenow.ToString(EN_Constante.g_const_formfech);

                        listaDocumento =(List<EN_Documento>)obj.listarFacturaNCD(oDocumento, p_tipo_docs);
                        if (listaDocumento.Count > 0)
                        {
                            for (var i = 0; i <= listaDocumento.Count - 1; i++)
                            {
                                rutabase= EN_ConfigConstantes.Instance.const_rutaTemporalPse_archivos;
                                rutadocs = string.Format("{0}/{1}/", rutabase, empresaDocumento.Nrodocumento);

                                nombre = listaDocumento[i].nombreXML.Trim();
                                
                                fileS3XML=  string.Format("/{0}/{1}{2}", empresaDocumento.Nrodocumento,EN_Constante.g_const_rutaSufijo_xml,nombre+EN_Constante.g_const_extension_xml);
                                // Si existe el XML en S3
                                existeXmlS3= AppConfiguracion.FuncionesS3.VerificarObjectS3(fileS3XML).Result;
                                if(existeXmlS3)
                                {
                                    p_url =  EN_ConfigConstantes.Instance.const_urlServiceDoc +  EN_ConfigConstantes.Instance.const_apiSendDoc;

                                    data=new EN_RequestEnviodoc() ; 
                                    data.flagOse= certificadoEmpresa.flagOSE;
                                    data.ruc= empresaDocumento.Nrodocumento;
                                    data.nombreArchivo= nombre.Trim();
                                    data.certUserName = certificadoEmpresa.UserName;
                                    data.certClave = certificadoEmpresa.Clave;
                                    data.Produccion= empresaDocumento.Produccion;
                                    data.documentoId= listaDocumento[i].Id;
                                    data.empresaId = listaDocumento[i].Idempresa.ToString(); 
                                    data.Idpuntoventa = listaDocumento[i].Idpuntoventa.ToString();
                                    data.tipodocumentoId = listaDocumento[i].Idtipodocumento.ToString();
                                    data.fecha = listaDocumento[i].Fecha;
                                    data.Serie= listaDocumento[i].Serie;
                                    data.Numero= listaDocumento[i].Numero;
                                    confg.EnviarXmlDocumentoElectronico(data,ref mensajeWA, ref codRpt , ref contError, p_url);
                                }
                            
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        
            


        public static EN_Empresa GetEmpresaById(int idEmpresa)
        {
            EN_Empresa listaEmpresa;
            EN_Empresa oEmpresa = new EN_Empresa();
            NE_Empresa objEmpresa = new NE_Empresa();
            oEmpresa.Id = idEmpresa;
            oEmpresa.Nrodocumento = "";
            oEmpresa.RazonSocial = "";
            oEmpresa.Estado = "";
            listaEmpresa = (EN_Empresa)objEmpresa.listaEmpresa(oEmpresa, 0, "");
            return listaEmpresa;
        }


        // Obtiene datos del certificado digital de la empresa por ID de empresa
        public static EN_Certificado GetCertificadoByIdEmpresa(int idEmpresa)
        {
            EN_Certificado listaCertificado;
            EN_Certificado oCertif = new EN_Certificado();
            NE_Certificado objCertif = new NE_Certificado();
            oCertif.Id = 0;
            oCertif.IdEmpresa = idEmpresa;
            oCertif.Nombre = "";
            oCertif.UsuarioSession = "";
            oCertif.CadenaAleatoria = "";
            listaCertificado = (EN_Certificado)objCertif.listarCertificado(oCertif);
            return listaCertificado;
        }
        
            

        
    }

}