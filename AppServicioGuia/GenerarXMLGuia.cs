using CEN;
using CLN;
using AppConfiguracion;
using System;
namespace AppServicioGuia
{
    public class GenerarXMLGuia
    {
        public EN_RegistrarGuiaElectronica validarEnvioGuiaSunat(EN_Guia oGuia, EN_Certificado oCertificado, EN_ComprobanteSunat oCompSunat, string produccion,string ruc, string nombre, ref string msjWA, ref int codRspta) 
        {
            EN_RegistrarGuiaElectronica respuesta = new EN_RegistrarGuiaElectronica();
            EN_RequestEnvioGuia envioGuia = new EN_RequestEnvioGuia();
            EN_RegistrarGuiaElectronica senEnvioGuia = new EN_RegistrarGuiaElectronica();
            Configuracion objConf =new Configuracion();
            NE_Guia clnGuia = new NE_Guia();
              string urlServicio = string.Empty;
              string urllocalhost = string.Empty;
              string urlWA = string.Empty;
              string dataSerializado = string.Empty; //data serializado

              try
              {
                if (oGuia.id == null || oGuia.id.Trim() == "") 
                 {
                     respuesta.RespRegistrarGuiaElectronica.FlagVerificacion = false;
                     respuesta.RespRegistrarGuiaElectronica.DescRespuesta = "ID de Guia no encontrado";
                     return respuesta;
                 }
                 if (oGuia.empresa_id <= EN_Constante.g_const_0) 
                 {
                    respuesta.RespRegistrarGuiaElectronica.FlagVerificacion = false;
                     respuesta.RespRegistrarGuiaElectronica.DescRespuesta = "ID de Empresa no encontrado";
                     return respuesta;
                 }
                 if (oGuia.tipodocumento_id == null || oGuia.tipodocumento_id.Trim() == "" ) 
                 {
                     respuesta.RespRegistrarGuiaElectronica.FlagVerificacion = false;
                     respuesta.RespRegistrarGuiaElectronica.DescRespuesta = "Tipo de Documento de Guia no encontrado";
                     return respuesta;

                 }
                 if (oCertificado.flagOSE == null || oCertificado.flagOSE.Trim() == "") 
                 {
                     respuesta.RespRegistrarGuiaElectronica.FlagVerificacion = false;
                     respuesta.RespRegistrarGuiaElectronica.DescRespuesta = "Flag OSE no encontrado";
                     return respuesta;
                 }
                 if (oCertificado.UserName == null || oCertificado.UserName.Trim() == "") 
                 {
                     respuesta.RespRegistrarGuiaElectronica.FlagVerificacion = false;
                     respuesta.RespRegistrarGuiaElectronica.DescRespuesta = "Usuario de Ceritificado no encontrado";
                     return respuesta;

                 }
                 if (oCertificado.Clave == null || oCertificado.Clave.Trim() == "") 
                 {
                    respuesta.RespRegistrarGuiaElectronica.FlagVerificacion = false;
                    respuesta.RespRegistrarGuiaElectronica.DescRespuesta = "Usuario de Ceritificado no encontrado";
                    return respuesta;
                 }
                 if (ruc == null || ruc.Trim() == "") 
                 {
                    respuesta.RespRegistrarGuiaElectronica.FlagVerificacion = false;
                    respuesta.RespRegistrarGuiaElectronica.DescRespuesta = "Ruta de Archivo XML no encontrado";
                    return respuesta;
                 }
                 if (nombre == null || nombre.Trim() == "") 
                 {
                    respuesta.RespRegistrarGuiaElectronica.FlagVerificacion = false;
                    respuesta.RespRegistrarGuiaElectronica.DescRespuesta = "NOmbre de Archivo XML no encontrado";
                    return respuesta;
                 }

                envioGuia.flagOse = oCertificado.flagOSE;
                 envioGuia.certUserName = oCertificado.UserName;
                 envioGuia.certClave = oCertificado.Clave;
                 envioGuia.Id = oGuia.id;
                 envioGuia.empresaId = oGuia.empresa_id;
                 envioGuia.tipodocumentoId = oGuia.tipodocumento_id;
                 envioGuia.ruc = ruc;
                 envioGuia.nombreArchivo = nombre;
                 envioGuia.Produccion = produccion;
           
                 respuesta.RespRegistrarGuiaElectronica = clnGuia.buscarParametroAppSettings(EN_Constante.g_const_1,EN_Constante.g_const_152);
                 if (respuesta.RespRegistrarGuiaElectronica.FlagVerificacion) 
                 {
                      urllocalhost = respuesta.RespRegistrarGuiaElectronica.DescRespuesta;
                      respuesta.RespRegistrarGuiaElectronica = clnGuia.buscarParametroAppSettings(EN_Constante.g_const_1,EN_Constante.g_const_153);
                      if (respuesta.RespRegistrarGuiaElectronica.FlagVerificacion) 
                      {
                           urlServicio = respuesta.RespRegistrarGuiaElectronica.DescRespuesta;
                           urlWA = urllocalhost + urlServicio; 
                           dataSerializado = System.Text.Json.JsonSerializer.Serialize(envioGuia);
                           senEnvioGuia = objConf.sendWebApiGuia(urlWA, dataSerializado);
                            if(senEnvioGuia.RespRegistrarGuiaElectronica.FlagVerificacion)
                            {
                                 msjWA= senEnvioGuia.RespRegistrarGuiaElectronica.DescRespuesta;
                                codRspta = EN_Constante.g_const_0;
                                respuesta.RespRegistrarGuiaElectronica.FlagVerificacion = true;
                                respuesta.RespRegistrarGuiaElectronica.DescRespuesta = msjWA;
                                respuesta.ErrorWebServ = senEnvioGuia.ErrorWebServ;
                                
                            } else {
                                 msjWA= "Error al enviar XML- " +senEnvioGuia.RespRegistrarGuiaElectronica.DescRespuesta;
                                codRspta = EN_Constante.g_const_1;
                                respuesta.RespRegistrarGuiaElectronica.FlagVerificacion = false;
                                respuesta.RespRegistrarGuiaElectronica.DescRespuesta = msjWA;
                                respuesta.ErrorWebServ = senEnvioGuia.ErrorWebServ;
                            }
                      }
                 }  
                 return   respuesta; 
              }
              catch ( Exception ex)
              {
                  
                respuesta.RespRegistrarGuiaElectronica.FlagVerificacion = false;
                respuesta.RespRegistrarGuiaElectronica.DescRespuesta = EN_Constante.g_const_error_interno;
                respuesta.ErrorWebServ.TipoError = EN_Constante.g_const_1;
                respuesta.ErrorWebServ.CodigoError = EN_Constante.g_const_3000;
                respuesta.ErrorWebServ.DescripcionErr = ex.Message;
                return respuesta;
              }

        }
        
    }
}