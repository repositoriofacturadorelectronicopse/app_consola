using CEN;
using CLN;
using CAD;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
namespace AppServicioGuia
{
    public class ProcesarXMLGuia
    {
        public EN_RespuestaData ProcesaGuiasSunat (int numdias, int valEmpId,ref string msjrspta, ref int codrspta, ref int contError) 
        {
            EN_RespuestaData respuesta = new EN_RespuestaData();
            EN_RespuestaData responsedata = new EN_RespuestaData();
            EN_ComprobanteSunat oCompSunat = new EN_ComprobanteSunat();
            EN_Empresa empresaDocumento = new EN_Empresa(); // clase entidad empresa
            NE_Proceso objProc = new NE_Proceso(); // clase negocio proceso
            EN_Proceso oProceso = new EN_Proceso();        // clase entidad proceso
            List<EN_Proceso> listaProc = new List<EN_Proceso>(); // lista de clase entidad proceso
            EN_Certificado certificadoEmpresa = new EN_Certificado(); // clase entidad certificado
            AD_Guia cadGuia = new AD_Guia();
            NE_Guia objGuia = new NE_Guia();  //clase negocio guia
            NE_Empresa objEmpresa = new NE_Empresa();
            EN_Guia oGuia = new EN_Guia();        //clase entidad guia
            List<EN_Guia> listaGuia = new List<EN_Guia>(); // lista de clase entidad guia

            EN_RespuestaListaProceso respuesListProceso = new EN_RespuestaListaProceso();
            EN_RespuestaEmpresa respEmpresa = new EN_RespuestaEmpresa();
            EN_RespuestaEmpresa respParamEmpresa = new EN_RespuestaEmpresa();
            EN_RespuestaListaGuia respuesListGuia = new EN_RespuestaListaGuia();
            EN_ErrorWebService errorservice = new EN_ErrorWebService();
            EN_RegistrarGuiaElectronica respuEnvioGuia = new EN_RegistrarGuiaElectronica();

            string rutaTemp; //ruta temporal
            string rutabase,  rutadocs; //ruta base y ruta de documentos
            string nombre;  // variable nombre
            // string estCDR = ""; // estado de CDR
            int codRpt=0; // codigo respuesta
            int flag=EN_Constante.g_const_0;
            string codTabla = "001";
            string codAlterno = "09";
            string fileS3XML =EN_Constante.g_const_vacio;        //Ruta de xml en S3
            bool existeXmlS3;                                   // Booleano para ex
            GenerarXMLGuia GenerarXMLGuia = new GenerarXMLGuia(); 
            try
            {
                respuesta = objGuia.listarConstantesTablaSunat(flag,codTabla,codAlterno);
                if (respuesta.ResplistaGuia.FlagVerificacion) 
                {
                // Busca Procesos Activados
                   oProceso.Empresa_id = valEmpId;
                   oProceso.Estado = EN_Constante.g_const_estado_proceso;
                   oCompSunat.IdGR = respuesta.ResplistaGuia.DescRespuesta;
                   respuesListProceso = objProc.ListarProcesosBatchGuia(oProceso,oCompSunat);
                   respuesta.ResplistaGuia = respuesListProceso.ResplistaGuia;
                   if (respuesta.ResplistaGuia.FlagVerificacion) 
                   {
                       if (respuesListProceso.listProceso.Count > EN_Constante.g_const_0) 
                       {
                           foreach (var item in respuesListProceso.listProceso)
                           {
                               respEmpresa = GetEmpresaById(item.Empresa_id);
                               if (respEmpresa.RespRegistro.FlagVerificacion) {

                                   respParamEmpresa = objEmpresa.buscarParametroEmpresa(respEmpresa.objEmpresa.Id,EN_Constante.g_const_0);
                                   if (respParamEmpresa.RespRegistro.FlagVerificacion) {
                                  
                                    certificadoEmpresa = GetCertificadoByIdEmpresa(item.Empresa_id);
                                    // Listamos los resumenes pendientes de enviar a SUNAT
                                     oGuia.id = EN_Constante.g_const_vacio;
                                     oGuia.empresa_id = item.Empresa_id;
                                     oGuia.estado = EN_Constante.g_const_estado_nuevo;
                                     oGuia.CadenaAleatoria = EN_Constante.g_const_vacio;
                                     oGuia.Usuariosession = EN_Constante.g_const_usuario_session;
                                     oGuia.fechaIni= DateTime.Now.AddDays(numdias).ToString(EN_Constante.g_const_formfech);
                                     oGuia.fechaFin=  DateTime.Now.ToString(EN_Constante.g_const_formfech);
                                      string p_tipo_doc = oCompSunat.IdGR;
                                      respuesListGuia = objGuia.listarGuiasPendientes(oGuia,p_tipo_doc);
                                      if (respuesListGuia.ResplistaGuia.FlagVerificacion) 
                                      {
                                          for (var i = 0; i < respuesListGuia.listDetGuia.Count; i++)
                                          {

                                            rutabase= EN_ConfigConstantes.Instance.const_rutaTemporalPse_archivos;
                                            rutadocs = string.Format("{0}/{1}/", rutabase, respEmpresa.objEmpresa.Nrodocumento);

                                            nombre = respuesListGuia.listDetGuia[i].nombreXML.Trim();
                                            fileS3XML=  string.Format("/{0}/{1}{2}", respEmpresa.objEmpresa.Nrodocumento,EN_Constante.g_const_rutaSufijo_xml,nombre+EN_Constante.g_const_extension_xml);
                                            // Si existe el XML en S3
                                            existeXmlS3= AppConfiguracion.FuncionesS3.VerificarObjectS3(fileS3XML).Result;
                                            if(existeXmlS3)  
                                            {
                                                string mensajeWA=EN_Constante.g_const_vacio;
                                                respuEnvioGuia = GenerarXMLGuia.validarEnvioGuiaSunat(respuesListGuia.listDetGuia[i],certificadoEmpresa,oCompSunat,respParamEmpresa.objEmpresa.Produccion,respEmpresa.objEmpresa.Nrodocumento, nombre.Trim(), ref mensajeWA, ref codRpt);
                                                respuesta.ResplistaGuia = respuEnvioGuia.RespRegistrarGuiaElectronica;
                                                respuesta.errorService = respuEnvioGuia.ErrorWebServ;
                                            
                                                if (respuesta.ResplistaGuia.FlagVerificacion) 
                                                {
                                                    respuesta.ResplistaGuia.DescRespuesta = EN_Constante.g_const_cnxexitosa;
                                                    errorservice.DescripcionErr = EN_Constante.g_const_cnxexitosa;
                                                    errorservice.CodigoError = EN_Constante.g_const_2000;
                                                    errorservice.TipoError = EN_Constante.g_const_0;
                                                    respuesta.errorService = errorservice;

                                                } 
        
                                            }
                                          }

                                      } else {
                                          if (respuesListGuia.ResplistaGuia.dato == 1000) {
                                              respuesta.ResplistaGuia.FlagVerificacion = true;
                                              respuesta.ResplistaGuia.DescRespuesta = EN_Constante.g_const_consultaexitosa;
                                              respuesta.errorService.TipoError = EN_Constante.g_const_0;
                                              respuesta.errorService.CodigoError = EN_Constante.g_const_2000;
                                              respuesta.errorService.DescripcionErr = EN_Constante.g_const_valExito;

                                          } else
                                          {
                                            respuesta.ResplistaGuia.FlagVerificacion = respuesListGuia.ResplistaGuia.FlagVerificacion;
                                            respuesta.ResplistaGuia.DescRespuesta = EN_Constante.g_const_error_interno;
                                            respuesta.ResplistaGuia.dato = respuesListGuia.ResplistaGuia.dato;
                                            respuesta.errorService.TipoError = EN_Constante.g_const_1;
                                            respuesta.errorService.CodigoError = EN_Constante.g_const_3000;
                                            respuesta.errorService.DescripcionErr = respuesListGuia.ResplistaGuia.DescRespuesta;
                                          }
                                          
                                      }
                                   } else 
                                   {
                                      respuesta.ResplistaGuia = respParamEmpresa.RespRegistro;
                                      respuesta.errorService = respParamEmpresa.errorService;
                                   }
          


                               } else 
                               {
                                   respuesta.ResplistaGuia.FlagVerificacion = respEmpresa.RespRegistro.FlagVerificacion;
                                   respuesta.ResplistaGuia.DescRespuesta = respEmpresa.RespRegistro.DescRespuesta;
                                   respuesta.errorService = respEmpresa.errorService;

                               }


                           }

                       }


                   } else 
                   {

                       if (respuesta.ResplistaGuia.dato == 1000) 
                       {
                       respuesta.ResplistaGuia.FlagVerificacion = true;
                       respuesta.ResplistaGuia.DescRespuesta = EN_Constante.g_const_consultaexitosa;
                       respuesta.errorService.TipoError = EN_Constante.g_const_0;
                       respuesta.errorService.CodigoError = EN_Constante.g_const_2000;
                       respuesta.errorService.DescripcionErr = EN_Constante.g_const_valExito;

                       }
                       if (respuesta.ResplistaGuia.dato == 3000) 
                       {
                            respuesta.ResplistaGuia.FlagVerificacion = respuesListProceso.ResplistaGuia.FlagVerificacion;
                            respuesta.errorService.TipoError = EN_Constante.g_const_1;
                            respuesta.errorService.CodigoError = EN_Constante.g_const_3000;
                            respuesta.errorService.DescripcionErr = respuesta.ResplistaGuia.DescRespuesta;
                            respuesta.ResplistaGuia.DescRespuesta = EN_Constante.g_const_error_interno;

                       }
            


                   }

                } 
              
             
            return respuesta;
            }
            catch (Exception ex)
            {
                
                respuesta.ResplistaGuia.FlagVerificacion = false;
                respuesta.ResplistaGuia.DescRespuesta = EN_Constante.g_const_error_interno;
                respuesta.errorService.CodigoError = EN_Constante.g_const_3000;
                respuesta.errorService.TipoError = EN_Constante.g_const_1;
                respuesta.errorService.DescripcionErr = ex.Message;
                return respuesta;
            }
        }
    public EN_RespuestaEmpresa GetEmpresaById(int idEmpresa)
    {
        EN_RespuestaEmpresa resEmpresa = new EN_RespuestaEmpresa();
        EN_Empresa oEmpresa = new EN_Empresa();
        NE_Empresa objEmpresa = new NE_Empresa();
        try
        {
           oEmpresa.Id = idEmpresa;
           oEmpresa.Nrodocumento = "";
           oEmpresa.RazonSocial = "";
           oEmpresa.Estado = "";
           resEmpresa = objEmpresa.listarEmpresa(oEmpresa, EN_Constante.g_const_0);
           return resEmpresa;
             
        }
        catch (Exception ex)
        {
            
            throw ex;
        }
        
    }
    public  EN_Certificado GetCertificadoByIdEmpresa(int idEmpresa)
    {
        EN_Certificado listaCertificado;
        EN_Certificado oCertif = new EN_Certificado();
        NE_Certificado objCertif = new NE_Certificado();
     
        try
        {
             oCertif.Id = EN_Constante.g_const_0;
             oCertif.IdEmpresa = idEmpresa;
             oCertif.Nombre = "";
             oCertif.UsuarioSession = "";
             oCertif.CadenaAleatoria = "";
             listaCertificado = (EN_Certificado)objCertif.listarCertificadoGuia(oCertif, "", idEmpresa);
             return listaCertificado;

             
        }
        catch (Exception ex)
        {
            
            throw ex;
        }
 
    }
    }
}