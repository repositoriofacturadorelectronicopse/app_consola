/****************************************************************************************************************************************************************************************
 PROGRAMA: ProcesarXmlFactura.cs
 VERSION : 1.0
 OBJETIVO: Clase para procesar otros documentos
 FECHA   : 21/06/2021
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using AppConfiguracion;
using CEN;
using CLN;


namespace AppServicioOtroscpe
{
    public partial class ProcesarXmlOtroscpe
    {
        
       
        public ProcesarXmlOtroscpe()
        {
            //DESCRIPCION: CONTRUCTOR DE CLASE ProcesarXmlOtroscpe
        }
      
        
        
        private EN_ComprobanteSunat buildComprobante()
        {
            //DESCRIPCION: FUNCION PARA CONTRUIR ID DE COMPROBANTES EN CLASE COMPROBANTE SUNAT
                var oCompSunat = new EN_ComprobanteSunat(); // clase de entidad comprobante sunat

                oCompSunat.IdCP = EN_ConfigConstantes.Instance.const_IdCP;
                oCompSunat.IdCR =  EN_ConfigConstantes.Instance.const_IdCR;
                oCompSunat.IdRR =  EN_ConfigConstantes.Instance.const_IdRR;

                oCompSunat.codigoEtiquetaErrorDoc = EN_ConfigConstantes.Instance.const_codigoEtiquetaErrorDoc;
                return oCompSunat;
        }

        
        public void ProcesaOtrosCpeSunat(int numdias, int valEmpId,ref string msjrspta, ref int codrspta, ref int contError)
        {
            //DESCRIPCION:  FUNCION PARA ENVIO DE OTROS CPE (RETENCION Y PERCEPCION) A SUNAT

           
            var oCompSunat = new EN_ComprobanteSunat();                                 // clase entidad comprobante sunat
            EN_Empresa empresaDocumento = new EN_Empresa();                             // clase entidad empresa
            NE_Proceso objProc = new NE_Proceso();                                      // clase negocio proceso
            var oProceso = new EN_Proceso();                                            // clase entidad proceso
            List<EN_Proceso> listaProc = new List<EN_Proceso>();                        // lista de clase entidad proceso
            EN_Certificado certificadoEmpresa = new EN_Certificado();                   // clase entidad certificado
            NE_Documento obj = new NE_Documento();                                      //clase negocio documento
            var oDocumento = new EN_OtrosCpe();                                         //clase entidad documento
            List<EN_OtrosCpe> listaDocumento = new List<EN_OtrosCpe>();                 // lista de calse entidad documento
            AppConfiguracion.Configuracion confg= new AppConfiguracion.Configuracion(); // Clase configuración

            string fileS3XML =EN_Constante.g_const_vacio;                               //Ruta de xml en S3
            bool existeXmlS3;                                                           // Booleano para existencia de objecto en S3
            string rutabase,  rutadocs;                                                 //ruta base y ruta de documentos
            string nombre;                                                              // variable nombre
            string RutaCarpetaDocs;                                                     // ruta de carpeta de documentos
            int codRpt          = EN_Constante.g_const_0;                               // codigo respuesta
            string mensajeWA    =EN_Constante.g_const_vacio;                            // Mensaje respuesta de WebApi
            string p_tipo_docs  =EN_Constante.g_const_vacio;                            // Tipo de documentos
            string p_url        =EN_Constante.g_const_vacio;                            // Url
            EN_RequestEnviodoc data;                                                    // CLASE ENTIDAD SOLICITUD ENVIO DOCUMENTO


            try
            {
                
                DateTime datetimenow= confg.getDateTimeNowLima();
                RutaCarpetaDocs = EN_ConfigConstantes.Instance.const_RutaCarpetaDocs;
                oCompSunat = buildComprobante();

                oProceso.Empresa_id = valEmpId;
                oProceso.Estado = EN_Constante.g_const_estado_proceso;
                p_tipo_docs=oCompSunat.IdCP + "," + oCompSunat.IdCR;
                listaProc = (List<EN_Proceso>)objProc.listarProcesosActivos(oProceso.Empresa_id, p_tipo_docs, oProceso.Estado);
                
                if (listaProc.Count > 0)
                {
                    foreach (var item in listaProc.ToList())
                    {
                        empresaDocumento = GetEmpresaById(item.Empresa_id);
                        certificadoEmpresa = GetCertificadoByIdEmpresa(item.Empresa_id);

                        oDocumento.id = EN_Constante.g_const_vacio;
                        oDocumento.empresa_id = item.Empresa_id;
                        oDocumento.estado = EN_Constante.g_const_estado_nuevo;
                        oDocumento.CadenaAleatoria = EN_Constante.g_const_vacio;
                        oDocumento.UsuarioSession = EN_Constante.g_const_usuario_session;
                        oDocumento.fechaIni= datetimenow.AddDays(numdias).ToString(EN_Constante.g_const_formfech);
                        oDocumento.fechaFin=  datetimenow.ToString(EN_Constante.g_const_formfech);

                        listaDocumento =(List<EN_OtrosCpe>)obj.listarOtrosCpe(oDocumento, p_tipo_docs);
                        if (listaDocumento.Count > 0)
                        {
                            for (var i = 0; i <= listaDocumento.Count - 1; i++)
                            {
                                rutabase= EN_ConfigConstantes.Instance.const_rutaTemporalPse_archivos;
                                rutadocs = string.Format("{0}/{1}/", rutabase, empresaDocumento.Nrodocumento);
                                
                                nombre = listaDocumento[i].nombreXML.Trim();
                                 fileS3XML=  string.Format("/{0}/{1}{2}", empresaDocumento.Nrodocumento,EN_Constante.g_const_rutaSufijo_xml,nombre+EN_Constante.g_const_extension_xml);
                                // Si existe el XML en S3
                                existeXmlS3= AppConfiguracion.FuncionesS3.VerificarObjectS3(fileS3XML).Result;
                                if(existeXmlS3)
                                {
                                    p_url =  EN_ConfigConstantes.Instance.const_urlServiceOtroscpe +  EN_ConfigConstantes.Instance.const_apiSendOtroscpe;

                                     data=new EN_RequestEnviodoc() ; 
                                    data.flagOse= certificadoEmpresa.flagOSE;
                                    data.ruc= empresaDocumento.Nrodocumento;
                                    data.nombreArchivo= nombre.Trim();
                                    data.certUserName = certificadoEmpresa.UserName;
                                    data.certClave = certificadoEmpresa.Clave;
                                    data.Produccion= empresaDocumento.Produccion;
                                    data.documentoId= listaDocumento[i].id;
                                    data.empresaId = listaDocumento[i].empresa_id.ToString(); 
                                    data.Idpuntoventa = listaDocumento[i].puntoventa_id.ToString();
                                    data.tipodocumentoId = listaDocumento[i].tipodocumento_id.ToString();
                                    data.fecha = listaDocumento[i].fechaemision;
                                    data.Serie= listaDocumento[i].serie;
                                    data.Numero= listaDocumento[i].numero;

                                    confg.EnviarXmlDocumentoElectronico(data, ref mensajeWA, ref codRpt, ref contError, p_url);
                                }
                            
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public void EnviarResumenReversion(int numdias, int valEmpId,ref string msjrspta, ref int codrspta, ref int contError)
            {
            // DESCRIPCION: Genera el Resumen Diario de Boletas Pendientes

            string p_tipo_docs= EN_Constante.g_const_vacio ;        // parametro tipo de documentos
            string fecha_comunicacion = EN_Constante.g_const_vacio; // parametro tipo de documentos
            string rutabase,rutadocs;                                        // Varible ruta de documento
            string fileS3XML =EN_Constante.g_const_vacio;        //Ruta de xml en S3
            bool existeXmlS3;                                   // Booleano para existencia de objecto en S3
            var oCompSunat = new EN_ComprobanteSunat();             // Entidad comprobante sunat
            NE_Proceso objProc = new NE_Proceso();                  //clase negocio proceso
            var oProceso = new EN_Proceso();                        //clase entidad proceso
            List<EN_Proceso> listaProc = new List<EN_Proceso>();    //lista de clase entidad proceso
            ClassRptaGenerarCpeDoc responseWA;                      // Clase de Respuesta de comprobante
            EN_Parametro bus_par;                                   // Clase entidad parametro para datos de búsqueda
            EN_Parametro res_par;                                   // Clase entidad parametro para datos de resultado
            EN_Empresa empresaDocumento;                            // Clase entidad empresa
            EN_Certificado certificadoEmpresa;                      // Clase entidad certificado empresa
            List<EN_RequestEnvioResDiario> listaDocumentos;         // Lista de clase entidad respues de envio de resumen diario
            AppConfiguracion.Configuracion confg= new AppConfiguracion.Configuracion(); // Clase configuración
            
        
        
            try
            {
                
                oCompSunat = buildComprobante();
                //buscamos en tabla parametro número de días permitidos para envio a sunat
                bus_par = new EN_Parametro();
                res_par = new EN_Parametro();
                
                bus_par.par_conceptopfij=EN_Constante.g_const_1;
                bus_par.par_conceptocorr=EN_Constante.g_const_114;
                res_par =objProc.buscar_tablaParametro(bus_par);
                numdias = Convert.ToInt32(res_par.par_descripcion);

                //buscamos en tabla parametro si procesamos para todas las empresas o una en particular
                bus_par.par_conceptopfij=EN_Constante.g_const_1;
                bus_par.par_conceptocorr=EN_Constante.g_const_5;
                res_par =objProc.buscar_tablaParametro(bus_par);
                valEmpId =  Convert.ToInt32(res_par.par_tipo);

                oProceso.Empresa_id = valEmpId;
                oProceso.Estado = EN_Constante.g_const_estado_proceso;
                p_tipo_docs=oCompSunat.IdRR;

                // Busca Procesos Activados
                listaProc = (List<EN_Proceso>)objProc.listarProcesosActivos(oProceso.Empresa_id, p_tipo_docs, oProceso.Estado);

                if (listaProc.Count > 0)
                {
                    foreach (var item in listaProc.ToList())
                    {
                        // Obtener ID de empresa
                        empresaDocumento = new EN_Empresa();
                        empresaDocumento = GetEmpresaById(item.Empresa_id);
                        // Obtener ID de certificado
                        certificadoEmpresa = new EN_Certificado();
                        certificadoEmpresa = GetCertificadoByIdEmpresa(item.Empresa_id);
                            // Listo las Boletas pendientes para generar el resumen
                            NE_Reversion obj = new NE_Reversion();
                        
                            listaDocumentos = new List<EN_RequestEnvioResDiario>();

                            DateTime datetimenow= confg.getDateTimeNowLima();

                            fecha_comunicacion =  datetimenow.AddDays(numdias).ToString(EN_Constante.g_const_formfech);
                        
                            listaDocumentos = ( List<EN_RequestEnvioResDiario> )obj.listarDocumentosReversion(empresaDocumento, certificadoEmpresa, item.Empresa_id,  fecha_comunicacion,  EN_Constante.g_const_estado_nuevo,  EN_Constante.g_const_situacion_pendiente);
                            if (listaDocumentos.Count > EN_Constante.g_const_0)
                            {
                                
                                for (var i = 0; i <= listaDocumentos.Count - 1; i++)
                                {
                                    rutabase= EN_ConfigConstantes.Instance.const_rutaTemporalPse_archivos;
                                    rutadocs= string.Format("{0}/{1}/", rutabase, empresaDocumento.Nrodocumento);

                                    fileS3XML=  string.Format("/{0}/{1}{2}", empresaDocumento.Nrodocumento,EN_Constante.g_const_rutaSufijo_xml,listaDocumentos[i].nombreArchivo.Trim()+EN_Constante.g_const_extension_xml);
                                    // Si existe el XML en S3
                                    existeXmlS3= AppConfiguracion.FuncionesS3.VerificarObjectS3(fileS3XML).Result;
                                    if(existeXmlS3)
                                    {
                                        responseWA = new ClassRptaGenerarCpeDoc();
                                        responseWA= EnviarXmlResumenReversion(listaDocumentos[i], ref msjrspta, ref codrspta, ref contError);

                                        
                    
                                    }
                                }
                     
                            }
                    }
                
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


         public static ClassRptaGenerarCpeDoc EnviarXmlResumenReversion(EN_RequestEnvioResDiario data, ref string msjWA, ref int codRspta, ref int contError)
        {

            //DESCRIPCION: FUNCION PARA ENVIAR XML DE REVERSION A SUNAT

    

            NE_Facturacion objFact = new NE_Facturacion();      // clase de negocio facturación
            ClassRptaGenerarCpeDoc responseWA= new ClassRptaGenerarCpeDoc();    // Clase de respuesta comprobante
            var objConf =new Configuracion();                                   // Clase de Configuracion
            string urlWA= EN_Constante.g_const_vacio;                           // Dirección Url para envio de xml
            string dataSerializado= EN_Constante.g_const_vacio;                 // Variable de data serializado
            try
            {
                

                urlWA = EN_ConfigConstantes.Instance.const_urlServiceOtroscpe +  EN_ConfigConstantes.Instance.const_apiSendReversion;
                
                
                dataSerializado= System.Text.Json.JsonSerializer.Serialize(data);
                
                responseWA= objConf.sendWebApi(urlWA, dataSerializado);
                
                if(responseWA.RptaRegistroCpeDoc.FlagVerificacion)
                {
                    msjWA =msjWA+ data.tiporesumenId+"-ID-A: "+data.resumenId+", ";
                    codRspta = EN_Constante.g_const_0;
                }
                else
                {
                    msjWA =msjWA+ data.tiporesumenId+"-ID-E: "+data.resumenId+", ";
                    codRspta = EN_Constante.g_const_1;
                    contError++;
                }

                return responseWA;
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
        
        }



    
            


        public static EN_Empresa GetEmpresaById(int idEmpresa)
        {
            EN_Empresa listaEmpresa;
            EN_Empresa oEmpresa = new EN_Empresa();
            NE_Empresa objEmpresa = new NE_Empresa();
            oEmpresa.Id = idEmpresa;
            oEmpresa.Nrodocumento = "";
            oEmpresa.RazonSocial = "";
            oEmpresa.Estado = "";
            listaEmpresa = (EN_Empresa)objEmpresa.listaEmpresa(oEmpresa, 0, "");
            return listaEmpresa;
        }


        // Obtiene datos del certificado digital de la empresa por ID de empresa
        public static EN_Certificado GetCertificadoByIdEmpresa(int idEmpresa)
        {
            EN_Certificado listaCertificado;
            EN_Certificado oCertif = new EN_Certificado();
            NE_Certificado objCertif = new NE_Certificado();
            oCertif.Id = 0;
            oCertif.IdEmpresa = idEmpresa;
            oCertif.Nombre = "";
            oCertif.UsuarioSession = "";
            oCertif.CadenaAleatoria = "";
            listaCertificado = (EN_Certificado)objCertif.listarCertificado(oCertif);
            return listaCertificado;
        }
        
        

        
    }

}