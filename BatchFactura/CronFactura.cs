﻿/****************************************************************************************************************************************************************************************
 PROGRAMA: Configuracion.cs
 VERSION : 1.0
 OBJETIVO: Clase de cron
 FECHA   : 21/06/2021
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/

using System;
using CEN;
using CLN;
using AppServicioDoc;
using System.Threading;
using Newtonsoft.Json;
using CAD;
using System.Globalization;
using AppServicioGuia;
using AppServicioOtroscpe;


namespace BatchFactura
{
    class CronFactura
    {

                
     
         public CronFactura( ){
        
        }
        static void Main(string[] args)
        {

             var culture = new System.Globalization.CultureInfo("es-PE");
            CultureInfo.DefaultThreadCurrentCulture = culture;
            CultureInfo.DefaultThreadCurrentUICulture = culture;
            Thread.CurrentThread.CurrentCulture = culture;
            Thread.CurrentThread.CurrentUICulture = culture;
       

            DAO_ConfigConstantes daoConfConst = new DAO_ConfigConstantes();
            daoConfConst.guardar_ConfigConstantes();

            
            var factura = new CronFactura();

            //Envio de facturas pendientes 
            factura.EnnviarFacturasPendientes();

            // Crea resumen y envío de resumen
            factura.CrearResumenBVPendientes();
            factura.ProcesaResumenSunat();

            //Envío de comunicados de baja
            factura.EnviarComunicadoBajas();

            //Crear comunicado de baja por resumen pendientes de envio
            factura.CrearResumenBajasPendientes();
            factura.ProcesaResumenSunat();

            //Envío de guias de remision
            factura.EnviarGuiasPendientes();

            // //Envio de otros comprobantes pendientes 
            factura.EnviarOtrosCpePendientes();

            //Envio de reversión pendiente
            factura.EnviarReversionPendientes();

        }

         
          

        public void EnnviarFacturasPendientes()
        {
            //DESCRIPCION Envia las facturas pendientes a SUNAT

             Int32 ntraLog = EN_Constante.g_const_0;                 // Número de transacción
            EN_Parametro res_par = new EN_Parametro();              // clase entidad parametro para resultado
            EN_Parametro bus_par = new EN_Parametro();              // clase entidad parametro para búsqueda
            NE_Proceso objProc = new NE_Proceso();                  //clase negocio proceso
            AD_clsLog adlog =  new AD_clsLog();                     // clase Log
            ProcesarXmlFactura factura = new ProcesarXmlFactura();          // Generar Factura
            EN_Concepto concepto;                                   //entidad concepto
            NE_Consulta consulta = new NE_Consulta();               // consulta
            ClassRptaGenerarCpeDoc respuesta = new ClassRptaGenerarCpeDoc() ;       // respuesta Generar comprobante documento
            EN_ErrorWebService ErrorWebSer = new EN_ErrorWebService();              //error
            AppConfiguracion.Configuracion confg= new AppConfiguracion.Configuracion(); // Clase configuración

            int numdias;                                            // número de días permitidos para envío a sunat   
            int valEmpId = EN_Constante.g_const_0;                  // Valor de id de empresa a procesar               
            int codrspta = EN_Constante.g_const_0;                  // código respuesta
            string msjrspta = EN_Constante.g_const_vacio;           // mensaje respuesta
            int contError = EN_Constante.g_const_0;    

            try
            {
                bus_par.par_conceptopfij=EN_Constante.g_const_1;
                bus_par.par_conceptocorr=EN_Constante.g_const_112;
                res_par =objProc.buscar_tablaParametro(bus_par);
                numdias = Convert.ToInt32(res_par.par_descripcion);

                //buscamos en tabla parametro si procesamos para todas las empresas o una en particular
                bus_par.par_conceptopfij=EN_Constante.g_const_1;
                bus_par.par_conceptocorr=EN_Constante.g_const_8;
                res_par =objProc.buscar_tablaParametro(bus_par);
                valEmpId =  Convert.ToInt32(res_par.par_tipo);

                ntraLog = adlog.registrar_log_inicio(EN_Constante.g_const_programa,EN_Constante.g_const_0.ToString(),EN_Constante.g_const_null, 
                                                    valEmpId, EN_Constante.g_const_0,  EN_Constante.g_const_vacio, EN_Constante.g_const_1,
                                                    EN_Constante.g_const_funcion_generarFac, EN_Constante.g_const_url_fac, EN_Constante.g_const_1,EN_Constante.g_const_1,
                                                    EN_Constante.g_const_0,EN_Constante.g_const_0, EN_Constante.g_const_vacio,EN_Constante.g_const_vacio,
                                                    confg.getDateNowLima(), confg.getDateHourNowLima(),
                                                    EN_Constante.g_const_null, EN_Constante.g_const_null, EN_Constante.g_const_null,EN_Constante.g_const_user_log);
                
                factura.ProcesaFacturaSunat(numdias, valEmpId, ref msjrspta, ref codrspta, ref contError);

                if(contError == EN_Constante.g_const_0)
                {
                    concepto = new EN_Concepto();
                    concepto= consulta.ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_2006);
                    respuesta.RptaRegistroCpeDoc.FlagVerificacion = true;
                    respuesta.RptaRegistroCpeDoc.DescripcionResp = concepto.conceptodesc;
                    respuesta.RptaRegistroCpeDoc.MensajeResp= msjrspta;
                    respuesta.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_2000;
                    ErrorWebSer = consulta.LlenarErrorWebSer(EN_Constante.g_const_0, concepto.conceptocorr, concepto.conceptodesc);
                    respuesta.ErrorWebService = ErrorWebSer;

                }
                else
                {
                    concepto = new EN_Concepto();
                        concepto= consulta.ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_2007);
                        respuesta.RptaRegistroCpeDoc.FlagVerificacion = false;
                        respuesta.RptaRegistroCpeDoc.DescripcionResp = concepto.conceptodesc;
                        respuesta.RptaRegistroCpeDoc.MensajeResp= msjrspta;
                        respuesta.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_3000;
                        ErrorWebSer = consulta.LlenarErrorWebSer(EN_Constante.g_const_1, concepto.conceptocorr,msjrspta);
                        respuesta.ErrorWebService = ErrorWebSer;
                }
                adlog.registrar_log_fin(ntraLog, EN_Constante.g_const_0, EN_Constante.g_const_5,
                                    respuesta.RptaRegistroCpeDoc.DescripcionResp, 
                                    EN_Constante.g_const_valExito, confg.getDateHourNowLima(),
                                                            JsonConvert.SerializeObject(respuesta), JsonConvert.SerializeObject(EN_Constante.g_const_0));
                    

            }
            catch (System.Exception e)
            {
                    concepto = new EN_Concepto();
                    concepto= consulta.ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_3000);
                    respuesta.RptaRegistroCpeDoc.FlagVerificacion = false;
                    respuesta.RptaRegistroCpeDoc.DescripcionResp = concepto.conceptodesc;
                    respuesta.RptaRegistroCpeDoc.MensajeResp= EN_Constante.g_const_error_interno;
                    respuesta.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_3000;
                    ErrorWebSer = consulta.LlenarErrorWebSer(EN_Constante.g_const_menos1, concepto.conceptocorr, e.Message.ToString());
                    respuesta.ErrorWebService = ErrorWebSer;

                    //registramos log fin
                    adlog.registrar_log_fin(ntraLog, EN_Constante.g_const_1, EN_Constante.g_const_5, EN_Constante.g_const_errlogCons + EN_Constante.g_const_funcion_generarCpe, e.Message.ToString(), 
                                        confg.getDateHourNowLima(),JsonConvert.SerializeObject(respuesta), JsonConvert.SerializeObject(valEmpId));


            }

        }


        public void EnviarOtrosCpePendientes()
        {
            //DESCRIPCION Envia Otros documentos (Percepción y retencion)

             Int32 ntraLog = EN_Constante.g_const_0;                 // Número de transacción
            EN_Parametro res_par = new EN_Parametro();              // clase entidad parametro para resultado
            EN_Parametro bus_par = new EN_Parametro();              // clase entidad parametro para búsqueda
            NE_Proceso objProc = new NE_Proceso();                  //clase negocio proceso
            AD_clsLog adlog =  new AD_clsLog();                     // clase Log
            ProcesarXmlOtroscpe factura = new ProcesarXmlOtroscpe();// Generar Factura
            EN_Concepto concepto;                                   //entidad concepto
            NE_Consulta consulta = new NE_Consulta();               // consulta
            ClassRptaGenerarCpeDoc respuesta = new ClassRptaGenerarCpeDoc() ;       // respuesta Generar comprobante documento
            EN_ErrorWebService ErrorWebSer = new EN_ErrorWebService();              //error
            AppConfiguracion.Configuracion confg= new AppConfiguracion.Configuracion(); // Clase configuración

            int numdias;                                            // número de días permitidos para envío a sunat   
            int valEmpId = EN_Constante.g_const_0;                  // Valor de id de empresa a procesar               
            int codrspta = EN_Constante.g_const_0;                  // código respuesta
            string msjrspta = EN_Constante.g_const_vacio;           // mensaje respuesta
            int contError = EN_Constante.g_const_0;    

            try
            {
                bus_par.par_conceptopfij=EN_Constante.g_const_1;
                bus_par.par_conceptocorr=EN_Constante.g_const_112;
                res_par =objProc.buscar_tablaParametro(bus_par);
                numdias = Convert.ToInt32(res_par.par_descripcion);

                //buscamos en tabla parametro si procesamos para todas las empresas o una en particular
                bus_par.par_conceptopfij=EN_Constante.g_const_1;
                bus_par.par_conceptocorr=EN_Constante.g_const_8;
                res_par =objProc.buscar_tablaParametro(bus_par);
                valEmpId =  Convert.ToInt32(res_par.par_tipo);

                ntraLog = adlog.registrar_log_inicio(EN_Constante.g_const_programa,EN_Constante.g_const_0.ToString(),EN_Constante.g_const_null, 
                                                    valEmpId, EN_Constante.g_const_0,  EN_Constante.g_const_vacio, EN_Constante.g_const_1,
                                                    EN_Constante.g_const_funcion_otrosCpe, EN_Constante.g_const_url_fac, EN_Constante.g_const_1,EN_Constante.g_const_1,
                                                    EN_Constante.g_const_0,EN_Constante.g_const_0, EN_Constante.g_const_vacio,EN_Constante.g_const_vacio,
                                                    confg.getDateNowLima(), confg.getDateHourNowLima(),
                                                    EN_Constante.g_const_null, EN_Constante.g_const_null, EN_Constante.g_const_null,EN_Constante.g_const_user_log);
                
                factura.ProcesaOtrosCpeSunat(numdias, valEmpId, ref msjrspta, ref codrspta, ref contError);

                if(contError == EN_Constante.g_const_0)
                {
                    concepto = new EN_Concepto();
                    concepto= consulta.ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_2006);
                    respuesta.RptaRegistroCpeDoc.FlagVerificacion = true;
                    respuesta.RptaRegistroCpeDoc.DescripcionResp = concepto.conceptodesc;
                    respuesta.RptaRegistroCpeDoc.MensajeResp= msjrspta;
                    respuesta.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_2000;
                    ErrorWebSer = consulta.LlenarErrorWebSer(EN_Constante.g_const_0, concepto.conceptocorr, concepto.conceptodesc);
                    respuesta.ErrorWebService = ErrorWebSer;

                }
                else
                {
                    concepto = new EN_Concepto();
                        concepto= consulta.ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_2007);
                        respuesta.RptaRegistroCpeDoc.FlagVerificacion = false;
                        respuesta.RptaRegistroCpeDoc.DescripcionResp = concepto.conceptodesc;
                        respuesta.RptaRegistroCpeDoc.MensajeResp= msjrspta;
                        respuesta.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_3000;
                        ErrorWebSer = consulta.LlenarErrorWebSer(EN_Constante.g_const_1, concepto.conceptocorr,msjrspta);
                        respuesta.ErrorWebService = ErrorWebSer;
                }
                adlog.registrar_log_fin(ntraLog, EN_Constante.g_const_0, EN_Constante.g_const_5,
                                    respuesta.RptaRegistroCpeDoc.DescripcionResp, 
                                    EN_Constante.g_const_valExito,confg.getDateHourNowLima(), JsonConvert.SerializeObject(respuesta), JsonConvert.SerializeObject(EN_Constante.g_const_0));
                    

            }
            catch (System.Exception e)
            {
                    concepto = new EN_Concepto();
                    concepto= consulta.ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_3000);
                    respuesta.RptaRegistroCpeDoc.FlagVerificacion = false;
                    respuesta.RptaRegistroCpeDoc.DescripcionResp = concepto.conceptodesc;
                    respuesta.RptaRegistroCpeDoc.MensajeResp= EN_Constante.g_const_error_interno;
                    respuesta.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_3000;
                    ErrorWebSer = consulta.LlenarErrorWebSer(EN_Constante.g_const_menos1, concepto.conceptocorr, e.Message.ToString());
                    respuesta.ErrorWebService = ErrorWebSer;

                    //registramos log fin
                    adlog.registrar_log_fin(ntraLog, EN_Constante.g_const_1, EN_Constante.g_const_5, EN_Constante.g_const_errlogCons + EN_Constante.g_const_funcion_generarCpe, e.Message.ToString(), 
                                        confg.getDateHourNowLima(),JsonConvert.SerializeObject(respuesta), JsonConvert.SerializeObject(valEmpId));


            }

        }

        public void CrearResumenBVPendientes()
        {
            //DESCRIPCION: Crea resumen de boletas y notas con afectación a boletas
            
            Int32 ntraLog = EN_Constante.g_const_0;                 // Número de transacción
            EN_Parametro res_par = new EN_Parametro();              // clase entidad parametro para resultado
            EN_Parametro bus_par = new EN_Parametro();              // clase entidad parametro para búsqueda
            NE_Proceso objProc = new NE_Proceso();                  //clase negocio proceso
            AD_clsLog adlog =  new AD_clsLog();                     // clase Log
            GenerarResumen resumen = new GenerarResumen();          // Generar Resumen
            EN_Concepto concepto;                                   //entidad concepto
            NE_Consulta consulta = new NE_Consulta();               // consulta
            ClassRptaGenerarCpeDoc respuesta = new ClassRptaGenerarCpeDoc() ;       // respuesta Generar comprobante documento
            EN_ErrorWebService ErrorWebSer = new EN_ErrorWebService();              //error
            AppConfiguracion.Configuracion confg= new AppConfiguracion.Configuracion(); // Clase configuración

            int numdias;                                            // número de días permitidos para envío a sunat   
            int valEmpId = EN_Constante.g_const_0;                  // Valor de id de empresa a procesar               
            int codrspta = EN_Constante.g_const_0;                  // código respuesta
            string msjrspta = EN_Constante.g_const_vacio;           // mensaje respuesta
            int contError = EN_Constante.g_const_0;                 // contador de error

            try
            {
                bus_par.par_conceptopfij=EN_Constante.g_const_1;
                bus_par.par_conceptocorr=EN_Constante.g_const_113;
                res_par =objProc.buscar_tablaParametro(bus_par);
                numdias = Convert.ToInt32(res_par.par_descripcion);

                //buscamos en tabla parametro si procesamos para todas las empresas o una en particular
                bus_par.par_conceptopfij=EN_Constante.g_const_1;
                bus_par.par_conceptocorr=EN_Constante.g_const_5;
                res_par =objProc.buscar_tablaParametro(bus_par);
                valEmpId =  Convert.ToInt32(res_par.par_tipo);

                ntraLog = adlog.registrar_log_inicio(EN_Constante.g_const_programa,EN_Constante.g_const_0.ToString(),EN_Constante.g_const_null, 
                                                    valEmpId, EN_Constante.g_const_0,  EN_Constante.g_const_vacio, EN_Constante.g_const_1,
                                                    EN_Constante.g_const_funcion_generarRes, EN_Constante.g_const_url_res, EN_Constante.g_const_1,EN_Constante.g_const_1,
                                                    EN_Constante.g_const_0,EN_Constante.g_const_0, EN_Constante.g_const_vacio,EN_Constante.g_const_vacio,
                                                    confg.getDateNowLima(), confg.getDateHourNowLima(),
                                                    EN_Constante.g_const_null, EN_Constante.g_const_null, EN_Constante.g_const_null,EN_Constante.g_const_user_log);
                
                resumen.CrearResumenBVPendientes(numdias, valEmpId, ref msjrspta, ref codrspta, ref contError);

                if(codrspta == EN_Constante.g_const_0)
                {
                    concepto = new EN_Concepto();
                    concepto= consulta.ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_2008);
                    respuesta.RptaRegistroCpeDoc.FlagVerificacion = true;
                    respuesta.RptaRegistroCpeDoc.DescripcionResp = concepto.conceptodesc;
                    respuesta.RptaRegistroCpeDoc.MensajeResp= msjrspta;
                    respuesta.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_2000;
                    ErrorWebSer = consulta.LlenarErrorWebSer(EN_Constante.g_const_0, concepto.conceptocorr, concepto.conceptodesc);
                    respuesta.ErrorWebService = ErrorWebSer;

                }
                else
                {
                    concepto = new EN_Concepto();
                        concepto= consulta.ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_2009);
                        respuesta.RptaRegistroCpeDoc.FlagVerificacion = false;
                        respuesta.RptaRegistroCpeDoc.DescripcionResp = concepto.conceptodesc;
                        respuesta.RptaRegistroCpeDoc.MensajeResp= msjrspta;
                        respuesta.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_3000;
                        ErrorWebSer = consulta.LlenarErrorWebSer(EN_Constante.g_const_1, concepto.conceptocorr,msjrspta);
                        respuesta.ErrorWebService = ErrorWebSer;
                }
                adlog.registrar_log_fin(ntraLog, EN_Constante.g_const_0, EN_Constante.g_const_5,
                                    respuesta.RptaRegistroCpeDoc.DescripcionResp, 
                                    EN_Constante.g_const_valExito,  confg.getDateHourNowLima(),
                                                            JsonConvert.SerializeObject(respuesta), JsonConvert.SerializeObject(EN_Constante.g_const_0));
                    

            }
            catch (System.Exception e)
            {
                    concepto = new EN_Concepto();
                    concepto= consulta.ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_3000);
                    respuesta.RptaRegistroCpeDoc.FlagVerificacion = false;
                    respuesta.RptaRegistroCpeDoc.DescripcionResp = concepto.conceptodesc;
                    respuesta.RptaRegistroCpeDoc.MensajeResp= EN_Constante.g_const_error_interno;
                    respuesta.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_3000;
                    ErrorWebSer = consulta.LlenarErrorWebSer(EN_Constante.g_const_menos1, concepto.conceptocorr, e.Message.ToString());
                    respuesta.ErrorWebService = ErrorWebSer;

                    //registramos log fin
                    adlog.registrar_log_fin(ntraLog, EN_Constante.g_const_1, EN_Constante.g_const_5, EN_Constante.g_const_errlogCons + EN_Constante.g_const_funcion_generarRes, e.Message.ToString(),
                                        confg.getDateHourNowLima(),JsonConvert.SerializeObject(respuesta), JsonConvert.SerializeObject(valEmpId));

                
            }

                

            

        }

        public void ProcesaResumenSunat()
        {
            //DESCRIPCION: Envia los Resumenes Diario de Boletas generados a SUNAT


            Int32 ntraLog = EN_Constante.g_const_0;                 // Número de transacción
            EN_Parametro res_par = new EN_Parametro();              // clase entidad parametro para resultado
            EN_Parametro bus_par = new EN_Parametro();              // clase entidad parametro para búsqueda
            NE_Proceso objProc = new NE_Proceso();                  //clase negocio proceso
            AD_clsLog adlog =  new AD_clsLog();                     // clase Log
            GenerarResumen resumen = new GenerarResumen();          // Generar Resumen
            EN_Concepto concepto;                                   //entidad concepto
            NE_Consulta consulta = new NE_Consulta();               // consulta
            ClassRptaGenerarCpeDoc respuesta = new ClassRptaGenerarCpeDoc() ;       // respuesta Generar comprobante documento
            EN_ErrorWebService ErrorWebSer = new EN_ErrorWebService();              //error
            AppConfiguracion.Configuracion confg= new AppConfiguracion.Configuracion(); // Clase configuración

            int numdias;                                            // número de días permitidos para envío a sunat   
            int valEmpId = EN_Constante.g_const_0;                  // Valor de id de empresa a procesar               
            int codrspta = EN_Constante.g_const_0;                  // código respuesta
            string msjrspta = EN_Constante.g_const_vacio;           // mensaje respuesta
            int contError = EN_Constante.g_const_0;                 // contador de error

            try
            {
                bus_par.par_conceptopfij=EN_Constante.g_const_1;
                bus_par.par_conceptocorr=EN_Constante.g_const_113;
                res_par =objProc.buscar_tablaParametro(bus_par);
                numdias = Convert.ToInt32(res_par.par_descripcion);

                //buscamos en tabla parametro si procesamos para todas las empresas o una en particular
                bus_par.par_conceptopfij=EN_Constante.g_const_1;
                bus_par.par_conceptocorr=EN_Constante.g_const_5;
                res_par =objProc.buscar_tablaParametro(bus_par);
                valEmpId =  Convert.ToInt32(res_par.par_tipo);

                ntraLog = adlog.registrar_log_inicio(EN_Constante.g_const_programa,EN_Constante.g_const_0.ToString(),EN_Constante.g_const_null, 
                                                    valEmpId, EN_Constante.g_const_0,  EN_Constante.g_const_vacio, EN_Constante.g_const_1,
                                                    EN_Constante.g_const_funcion_generarResProc, EN_Constante.g_const_url_res, EN_Constante.g_const_1,EN_Constante.g_const_1,
                                                    EN_Constante.g_const_0,EN_Constante.g_const_0, EN_Constante.g_const_vacio,EN_Constante.g_const_vacio,
                                                    confg.getDateNowLima(), confg.getDateHourNowLima(),
                                                    EN_Constante.g_const_null, EN_Constante.g_const_null, EN_Constante.g_const_null,EN_Constante.g_const_user_log);
                
                resumen.ProcesaResumenSunat(numdias, valEmpId, ref msjrspta, ref codrspta, ref contError);

                if(codrspta == EN_Constante.g_const_0)
                {
                    concepto = new EN_Concepto();
                    concepto= consulta.ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_2008);
                    respuesta.RptaRegistroCpeDoc.FlagVerificacion = true;
                    respuesta.RptaRegistroCpeDoc.DescripcionResp = concepto.conceptodesc;
                    respuesta.RptaRegistroCpeDoc.MensajeResp= msjrspta;
                    respuesta.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_2000;
                    ErrorWebSer = consulta.LlenarErrorWebSer(EN_Constante.g_const_0, concepto.conceptocorr, concepto.conceptodesc);
                    respuesta.ErrorWebService = ErrorWebSer;

                }
                else
                {
                    concepto = new EN_Concepto();
                        concepto= consulta.ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_2009);
                        respuesta.RptaRegistroCpeDoc.FlagVerificacion = false;
                        respuesta.RptaRegistroCpeDoc.DescripcionResp = concepto.conceptodesc;
                        respuesta.RptaRegistroCpeDoc.MensajeResp= msjrspta;
                        respuesta.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_3000;
                        ErrorWebSer = consulta.LlenarErrorWebSer(EN_Constante.g_const_1, concepto.conceptocorr,msjrspta);
                        respuesta.ErrorWebService = ErrorWebSer;
                }
                adlog.registrar_log_fin(ntraLog, EN_Constante.g_const_0, EN_Constante.g_const_5,
                                    respuesta.RptaRegistroCpeDoc.DescripcionResp, 
                                    EN_Constante.g_const_valExito, confg.getDateHourNowLima(),
                                                            JsonConvert.SerializeObject(respuesta), JsonConvert.SerializeObject(EN_Constante.g_const_0));
                    

            }
            catch (System.Exception e)
            {
                    concepto = new EN_Concepto();
                    concepto= consulta.ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_3000);
                    respuesta.RptaRegistroCpeDoc.FlagVerificacion = false;
                    respuesta.RptaRegistroCpeDoc.DescripcionResp = concepto.conceptodesc;
                    respuesta.RptaRegistroCpeDoc.MensajeResp= EN_Constante.g_const_error_interno;
                    respuesta.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_3000;
                    ErrorWebSer = consulta.LlenarErrorWebSer(EN_Constante.g_const_menos1, concepto.conceptocorr, e.Message.ToString());
                    respuesta.ErrorWebService = ErrorWebSer;

                    //registramos log fin
                    adlog.registrar_log_fin(ntraLog, EN_Constante.g_const_1, EN_Constante.g_const_5, EN_Constante.g_const_errlogCons + EN_Constante.g_const_funcion_generarResProc, e.Message.ToString(), confg.getDateHourNowLima(),
                                        JsonConvert.SerializeObject(respuesta), JsonConvert.SerializeObject(valEmpId));


                    
                
            }

                

            

        }


        
        public void EnviarComunicadoBajas()
        {
            //DESCRIPCION: ENVIA LOS COMUNICADOS  DE BAJA PENDIENTES A SUNAT
            
        

            Int32 ntraLog = EN_Constante.g_const_0;                 // Número de transacción
            EN_Parametro res_par = new EN_Parametro();              // clase entidad parametro para resultado
            EN_Parametro bus_par = new EN_Parametro();              // clase entidad parametro para búsqueda
            NE_Proceso objProc = new NE_Proceso();                  //clase negocio proceso
            AD_clsLog adlog =  new AD_clsLog();                     // clase Log
            GenerarResumen resumen = new GenerarResumen();          // Generar Resumen
            EN_Concepto concepto;                                   //entidad concepto
            NE_Consulta consulta = new NE_Consulta();               // consulta
            ClassRptaGenerarCpeDoc respuesta = new ClassRptaGenerarCpeDoc() ;       // respuesta Generar comprobante documento
            EN_ErrorWebService ErrorWebSer = new EN_ErrorWebService();              //error
            AppConfiguracion.Configuracion confg= new AppConfiguracion.Configuracion(); // Clase configuración

            int numdias;                                            // número de días permitidos para envío a sunat   
            int valEmpId = EN_Constante.g_const_0;                  // Valor de id de empresa a procesar               
            int codrspta = EN_Constante.g_const_0;                  // código respuesta
            string msjrspta = EN_Constante.g_const_vacio;           // mensaje respuesta
            int contError = EN_Constante.g_const_0;                 // contador de error

            try
            {
                bus_par.par_conceptopfij=EN_Constante.g_const_1;
                bus_par.par_conceptocorr=EN_Constante.g_const_114;
                res_par =objProc.buscar_tablaParametro(bus_par);
                numdias = Convert.ToInt32(res_par.par_descripcion);

                //buscamos en tabla parametro si procesamos para todas las empresas o una en particular
                bus_par.par_conceptopfij=EN_Constante.g_const_1;
                bus_par.par_conceptocorr=EN_Constante.g_const_5;
                res_par =objProc.buscar_tablaParametro(bus_par);
                valEmpId =  Convert.ToInt32(res_par.par_tipo);

                ntraLog = adlog.registrar_log_inicio(EN_Constante.g_const_programa,EN_Constante.g_const_0.ToString(),EN_Constante.g_const_null, 
                                                    valEmpId, EN_Constante.g_const_0,  EN_Constante.g_const_vacio, EN_Constante.g_const_1,
                                                    EN_Constante.g_const_funcion_generarCpe, EN_Constante.g_const_url, EN_Constante.g_const_1,EN_Constante.g_const_1,
                                                    EN_Constante.g_const_0,EN_Constante.g_const_0, EN_Constante.g_const_vacio,EN_Constante.g_const_vacio,
                                                    confg.getDateNowLima(), confg.getDateHourNowLima(),
                                                    EN_Constante.g_const_null, EN_Constante.g_const_null, EN_Constante.g_const_null,EN_Constante.g_const_user_log);
                
                resumen.EnviarComunicadoBajas(numdias, valEmpId, ref msjrspta, ref codrspta, ref contError);

                if(contError < EN_Constante.g_const_1)
                {
                    concepto = new EN_Concepto();
                    concepto= consulta.ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_2004);
                    respuesta.RptaRegistroCpeDoc.FlagVerificacion = true;
                    respuesta.RptaRegistroCpeDoc.DescripcionResp = concepto.conceptodesc;
                    respuesta.RptaRegistroCpeDoc.MensajeResp= msjrspta;
                    respuesta.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_2004;
                    ErrorWebSer = consulta.LlenarErrorWebSer(EN_Constante.g_const_0, concepto.conceptocorr, concepto.conceptodesc);
                    respuesta.ErrorWebService = ErrorWebSer;

                }
                else
                {
                    concepto = new EN_Concepto();
                        concepto= consulta.ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_2005);
                        respuesta.RptaRegistroCpeDoc.FlagVerificacion = false;
                        respuesta.RptaRegistroCpeDoc.DescripcionResp = concepto.conceptodesc;
                        respuesta.RptaRegistroCpeDoc.MensajeResp= msjrspta;
                        respuesta.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_2005;
                        ErrorWebSer = consulta.LlenarErrorWebSer(EN_Constante.g_const_menos1, concepto.conceptocorr,msjrspta);
                        respuesta.ErrorWebService = ErrorWebSer;
                }
                adlog.registrar_log_fin(ntraLog, EN_Constante.g_const_0, EN_Constante.g_const_5,
                                    respuesta.RptaRegistroCpeDoc.DescripcionResp, 
                                    EN_Constante.g_const_valExito, confg.getDateHourNowLima(),
                                                            JsonConvert.SerializeObject(respuesta), JsonConvert.SerializeObject(EN_Constante.g_const_0));
                    

            }
            catch (System.Exception e)
            {
                    concepto = new EN_Concepto();
                    concepto= consulta.ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_3000);
                    respuesta.RptaRegistroCpeDoc.FlagVerificacion = false;
                    respuesta.RptaRegistroCpeDoc.DescripcionResp = concepto.conceptodesc;
                    respuesta.RptaRegistroCpeDoc.MensajeResp= EN_Constante.g_const_error_interno;
                    respuesta.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_3000;
                    ErrorWebSer = consulta.LlenarErrorWebSer(EN_Constante.g_const_menos1, concepto.conceptocorr, e.Message.ToString());
                    respuesta.ErrorWebService = ErrorWebSer;

                    //registramos log fin
                    adlog.registrar_log_fin(ntraLog, EN_Constante.g_const_1, EN_Constante.g_const_5, EN_Constante.g_const_errlogCons + EN_Constante.g_const_funcion_generarCpe, e.Message.ToString(), 
                                        confg.getDateHourNowLima(), JsonConvert.SerializeObject(respuesta), JsonConvert.SerializeObject(valEmpId));

                
            }

                

            

        }


        public void CrearResumenBajasPendientes()
        {
            //DESCRIPCION: Crea resumen de boletas y notas con afectación a boletas
            
            Int32 ntraLog = EN_Constante.g_const_0;                 // Número de transacción
            EN_Parametro res_par = new EN_Parametro();              // clase entidad parametro para resultado
            EN_Parametro bus_par = new EN_Parametro();              // clase entidad parametro para búsqueda
            NE_Proceso objProc = new NE_Proceso();                  //clase negocio proceso
            AD_clsLog adlog =  new AD_clsLog();                     // clase Log
            GenerarResumen resumen = new GenerarResumen();          // Generar Resumen
            EN_Concepto concepto;                                   //entidad concepto
            NE_Consulta consulta = new NE_Consulta();               // consulta
            ClassRptaGenerarCpeDoc respuesta = new ClassRptaGenerarCpeDoc() ;       // respuesta Generar comprobante documento
            EN_ErrorWebService ErrorWebSer = new EN_ErrorWebService();              //error
            AppConfiguracion.Configuracion confg= new AppConfiguracion.Configuracion(); // Clase configuración

            int valEmpId = EN_Constante.g_const_0;                  // Valor de id de empresa a procesar               
            int codrspta = EN_Constante.g_const_0;                  // código respuesta
            string msjrspta = EN_Constante.g_const_vacio;           // mensaje respuesta
            string comprobante = EN_Constante.g_const_vacio;        // comprobante
            bool success = false;                                   // Success

            try
            {

                //buscamos en tabla parametro si procesamos para todas las empresas o una en particular
                bus_par.par_conceptopfij=EN_Constante.g_const_1;
                bus_par.par_conceptocorr=EN_Constante.g_const_5;
                res_par =objProc.buscar_tablaParametro(bus_par);
                valEmpId =  Convert.ToInt32(res_par.par_tipo);

                ntraLog = adlog.registrar_log_inicio(EN_Constante.g_const_programa,EN_Constante.g_const_0.ToString(),EN_Constante.g_const_null, 
                                                    valEmpId, EN_Constante.g_const_0,  EN_Constante.g_const_vacio, EN_Constante.g_const_1,
                                                    EN_Constante.g_const_funcion_crearResBajasPend, EN_Constante.g_const_url_res, EN_Constante.g_const_1,EN_Constante.g_const_1,
                                                    EN_Constante.g_const_0,EN_Constante.g_const_0, EN_Constante.g_const_vacio,EN_Constante.g_const_vacio,
                                                    confg.getDateNowLima(), confg.getDateHourNowLima(),
                                                    EN_Constante.g_const_null, EN_Constante.g_const_null, EN_Constante.g_const_null,EN_Constante.g_const_user_log);
                
                resumen.CrearResumenBajapendiente(valEmpId, ref codrspta , ref msjrspta, ref comprobante, ref success);

                if(success)
                {
                    concepto = new EN_Concepto();
                    concepto= consulta.ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_2008);
                    respuesta.RptaRegistroCpeDoc.FlagVerificacion = true;
                    respuesta.RptaRegistroCpeDoc.DescripcionResp = concepto.conceptodesc;
                    respuesta.RptaRegistroCpeDoc.MensajeResp= msjrspta;
                    respuesta.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_2000;
                    respuesta.RptaRegistroCpeDoc.comprobante = comprobante;
                    ErrorWebSer = consulta.LlenarErrorWebSer(EN_Constante.g_const_0, concepto.conceptocorr, concepto.conceptodesc);
                    respuesta.ErrorWebService = ErrorWebSer;

                }
                else
                {
                    concepto = new EN_Concepto();
                        concepto= consulta.ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_2009);
                        respuesta.RptaRegistroCpeDoc.FlagVerificacion = false;
                        respuesta.RptaRegistroCpeDoc.DescripcionResp = concepto.conceptodesc;
                        respuesta.RptaRegistroCpeDoc.MensajeResp= msjrspta;
                        respuesta.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_3000;
                        ErrorWebSer = consulta.LlenarErrorWebSer(EN_Constante.g_const_1, concepto.conceptocorr,msjrspta);
                        respuesta.ErrorWebService = ErrorWebSer;
                }
                adlog.registrar_log_fin(ntraLog, EN_Constante.g_const_0, EN_Constante.g_const_5,
                                    respuesta.RptaRegistroCpeDoc.DescripcionResp, 
                                    EN_Constante.g_const_valExito,  confg.getDateHourNowLima(),
                                                            JsonConvert.SerializeObject(respuesta), JsonConvert.SerializeObject(EN_Constante.g_const_0));
                    

            }
            catch (System.Exception e)
            {
                    concepto = new EN_Concepto();
                    concepto= consulta.ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_3000);
                    respuesta.RptaRegistroCpeDoc.FlagVerificacion = false;
                    respuesta.RptaRegistroCpeDoc.DescripcionResp = concepto.conceptodesc;
                    respuesta.RptaRegistroCpeDoc.MensajeResp= EN_Constante.g_const_error_interno;
                    respuesta.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_3000;
                    ErrorWebSer = consulta.LlenarErrorWebSer(EN_Constante.g_const_menos1, concepto.conceptocorr, e.Message.ToString());
                    respuesta.ErrorWebService = ErrorWebSer;

                    //registramos log fin
                    adlog.registrar_log_fin(ntraLog, EN_Constante.g_const_1, EN_Constante.g_const_5, EN_Constante.g_const_errlogCons + EN_Constante.g_const_funcion_generarRes, e.Message.ToString(),
                                        confg.getDateHourNowLima(),JsonConvert.SerializeObject(respuesta), JsonConvert.SerializeObject(valEmpId));

                
            }

                

            

        }

        public void EnviarReversionPendientes()
        {
            //DESCRIPCION: ENVIA LOS RESUMENES DE REVERSION PENDIENTES A SUNAT
            
        

            Int32 ntraLog = EN_Constante.g_const_0;                 // Número de transacción
            EN_Parametro res_par = new EN_Parametro();              // clase entidad parametro para resultado
            EN_Parametro bus_par = new EN_Parametro();              // clase entidad parametro para búsqueda
            NE_Proceso objProc = new NE_Proceso();                  //clase negocio proceso
            AD_clsLog adlog =  new AD_clsLog();                     // clase Log
            ProcesarXmlOtroscpe objReversion = new ProcesarXmlOtroscpe();          // Generar Resumen
            EN_Concepto concepto;                                   //entidad concepto
            NE_Consulta consulta = new NE_Consulta();               // consulta
            ClassRptaGenerarCpeDoc respuesta = new ClassRptaGenerarCpeDoc() ;       // respuesta Generar comprobante documento
            EN_ErrorWebService ErrorWebSer = new EN_ErrorWebService();              //error
            AppConfiguracion.Configuracion confg= new AppConfiguracion.Configuracion(); // Clase configuración

            int numdias;                                            // número de días permitidos para envío a sunat   
            int valEmpId = EN_Constante.g_const_0;                  // Valor de id de empresa a procesar               
            int codrspta = EN_Constante.g_const_0;                  // código respuesta
            string msjrspta = EN_Constante.g_const_vacio;           // mensaje respuesta
            int contError = EN_Constante.g_const_0;                 // contador de error

            try
            {
                bus_par.par_conceptopfij=EN_Constante.g_const_1;
                bus_par.par_conceptocorr=EN_Constante.g_const_114;
                res_par =objProc.buscar_tablaParametro(bus_par);
                numdias = Convert.ToInt32(res_par.par_descripcion);

                //buscamos en tabla parametro si procesamos para todas las empresas o una en particular
                bus_par.par_conceptopfij=EN_Constante.g_const_1;
                bus_par.par_conceptocorr=EN_Constante.g_const_5;
                res_par =objProc.buscar_tablaParametro(bus_par);
                valEmpId =  Convert.ToInt32(res_par.par_tipo);

                ntraLog = adlog.registrar_log_inicio(EN_Constante.g_const_programa,EN_Constante.g_const_0.ToString(),EN_Constante.g_const_null, 
                                                    valEmpId, EN_Constante.g_const_0,  EN_Constante.g_const_vacio, EN_Constante.g_const_1,
                                                    EN_Constante.g_const_funcion_reversionCpe, EN_Constante.g_const_url_reversion, EN_Constante.g_const_1,EN_Constante.g_const_1,
                                                    EN_Constante.g_const_0,EN_Constante.g_const_0, EN_Constante.g_const_vacio,EN_Constante.g_const_vacio,
                                                    confg.getDateNowLima(), confg.getDateHourNowLima(),
                                                    EN_Constante.g_const_null, EN_Constante.g_const_null, EN_Constante.g_const_null,EN_Constante.g_const_user_log);
                
                objReversion.EnviarResumenReversion(numdias, valEmpId, ref msjrspta, ref codrspta, ref contError);

                if(contError < EN_Constante.g_const_1)
                {
                    concepto = new EN_Concepto();
                    concepto= consulta.ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_2004);
                    respuesta.RptaRegistroCpeDoc.FlagVerificacion = true;
                    respuesta.RptaRegistroCpeDoc.DescripcionResp = concepto.conceptodesc;
                    respuesta.RptaRegistroCpeDoc.MensajeResp= msjrspta;
                    respuesta.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_2004;
                    ErrorWebSer = consulta.LlenarErrorWebSer(EN_Constante.g_const_0, concepto.conceptocorr, concepto.conceptodesc);
                    respuesta.ErrorWebService = ErrorWebSer;

                }
                else
                {
                    concepto = new EN_Concepto();
                        concepto= consulta.ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_2005);
                        respuesta.RptaRegistroCpeDoc.FlagVerificacion = false;
                        respuesta.RptaRegistroCpeDoc.DescripcionResp = concepto.conceptodesc;
                        respuesta.RptaRegistroCpeDoc.MensajeResp= msjrspta;
                        respuesta.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_2005;
                        ErrorWebSer = consulta.LlenarErrorWebSer(EN_Constante.g_const_menos1, concepto.conceptocorr,msjrspta);
                        respuesta.ErrorWebService = ErrorWebSer;
                }
                adlog.registrar_log_fin(ntraLog, EN_Constante.g_const_0, EN_Constante.g_const_5,
                                    respuesta.RptaRegistroCpeDoc.DescripcionResp, 
                                    EN_Constante.g_const_valExito, confg.getDateHourNowLima(),
                                                            JsonConvert.SerializeObject(respuesta), JsonConvert.SerializeObject(EN_Constante.g_const_0));
                    

            }
            catch (System.Exception e)
            {
                    concepto = new EN_Concepto();
                    concepto= consulta.ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_3000);
                    respuesta.RptaRegistroCpeDoc.FlagVerificacion = false;
                    respuesta.RptaRegistroCpeDoc.DescripcionResp = concepto.conceptodesc;
                    respuesta.RptaRegistroCpeDoc.MensajeResp= EN_Constante.g_const_error_interno;
                    respuesta.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_3000;
                    ErrorWebSer = consulta.LlenarErrorWebSer(EN_Constante.g_const_menos1, concepto.conceptocorr, e.Message.ToString());
                    respuesta.ErrorWebService = ErrorWebSer;

                    //registramos log fin
                    adlog.registrar_log_fin(ntraLog, EN_Constante.g_const_1, EN_Constante.g_const_5, EN_Constante.g_const_errlogCons + EN_Constante.g_const_funcion_reversionCpe, e.Message.ToString(), 
                                        confg.getDateHourNowLima(), JsonConvert.SerializeObject(respuesta), JsonConvert.SerializeObject(valEmpId));

                
            }

                

            

        }




          public EN_RespuestaData EnviarGuiasPendientes() {
            EN_RespuestaData response = new EN_RespuestaData();
            EN_Guia entidad = new EN_Guia();
            EN_Parametro bus_par = new EN_Parametro();
            EN_RespuestaParametro resParametro = new EN_RespuestaParametro();
            AppConfiguracion.Configuracion confg= new AppConfiguracion.Configuracion(); // Clase configuración
            NE_Proceso objProc = new NE_Proceso();  
            AD_Guia cadGuia = new AD_Guia(); // CAPA DE DATOS DE GUIA
            ProcesarXMLGuia proGuia = new ProcesarXMLGuia();
            int numdias;                                            // número de días permitidos para envío a sunat   
            int valEmpId = EN_Constante.g_const_0;                  // Valor de id de empresa a procesar               
            int codrspta = EN_Constante.g_const_0;                  // código respuesta
            string msjrspta = EN_Constante.g_const_vacio;           // mensaje respuesta
            int contError = EN_Constante.g_const_0;   
            Int32 ntraLog = EN_Constante.g_const_0; // Número de transacción 
            try
            {
                bus_par.par_conceptopfij=EN_Constante.g_const_1;
                bus_par.par_conceptocorr=EN_Constante.g_const_112;
                resParametro = objProc.buscar_tablaParametroGuia(bus_par);
                response.ResplistaGuia = resParametro.RespRegistro;
                response.errorService = resParametro.errorService;
                if (response.ResplistaGuia.FlagVerificacion)
                {
                    numdias = Convert.ToInt32(resParametro.objParametro.par_descripcion);
                    bus_par.par_conceptopfij=EN_Constante.g_const_1;
                    bus_par.par_conceptocorr=EN_Constante.g_const_8;
                    resParametro =objProc.buscar_tablaParametroGuia(bus_par);
                    response.ResplistaGuia = resParametro.RespRegistro;
                    response.errorService = resParametro.errorService;
                    if (response.ResplistaGuia.FlagVerificacion) 
                    {
                        valEmpId =  Convert.ToInt32(resParametro.objParametro.par_tipo);

                        ntraLog = cadGuia.registrar_log_inicio(EN_Constante.g_const_programa,EN_Constante.g_const_0.ToString(),EN_Constante.g_const_null, 
                                                    valEmpId, EN_Constante.g_const_0,  EN_Constante.g_const_vacio, EN_Constante.g_const_1,
                                                    EN_Constante.g_const_funcion_generarGuia, EN_Constante.g_const_url_Guia, EN_Constante.g_const_1,EN_Constante.g_const_1,
                                                    EN_Constante.g_const_0,EN_Constante.g_const_0, EN_Constante.g_const_vacio,EN_Constante.g_const_vacio,
                                                    confg.getDateNowLima(), confg.getDateHourNowLima(),
                                                    EN_Constante.g_const_null, EN_Constante.g_const_null, EN_Constante.g_const_null,EN_Constante.g_const_user_log);

                    
                      response = proGuia.ProcesaGuiasSunat(numdias, valEmpId, ref msjrspta, ref codrspta, ref contError);
                      if (response.ResplistaGuia.FlagVerificacion) 
                      {
                          response.ResplistaGuia.DescRespuesta = EN_Constante.g_const_consultaexitosa;
                          response.errorService.TipoError = EN_Constante.g_const_0;
                          response.errorService.CodigoError = EN_Constante.g_const_2000;
                          response.errorService.DescripcionErr = EN_Constante.g_const_batch_guia_exito;                         
                      } else 
                      {
                          response.ResplistaGuia.FlagVerificacion = false;
                          response.ResplistaGuia.DescRespuesta = EN_Constante.g_const_error_interno;
                          response.errorService.TipoError = EN_Constante.g_const_1;
                          response.errorService.CodigoError = EN_Constante.g_const_3000;
                          response.errorService.DescripcionErr = EN_Constante.g_const_batch_guia_error;                         

                      }
                      cadGuia.registrar_log_fin(ntraLog, EN_Constante.g_const_0, EN_Constante.g_const_5,
                                    response.ResplistaGuia.DescRespuesta, 
                                    EN_Constante.g_const_valExito, confg.getDateHourNowLima(),
                                    JsonConvert.SerializeObject(response), JsonConvert.SerializeObject(EN_Constante.g_const_0));
                   
                    }

                    

                }
               

                 return response;
            }
            catch (Exception ex)
            {
                
                response.ResplistaGuia.FlagVerificacion = false;
                response.ResplistaGuia.DescRespuesta = EN_Constante.g_const_error_interno;
                response.errorService.TipoError = EN_Constante.g_const_1;
                response.errorService.CodigoError = EN_Constante.g_const_3000;
                response.errorService.DescripcionErr = ex.Message;   
                 cadGuia.registrar_log_fin(ntraLog, EN_Constante.g_const_0, EN_Constante.g_const_5,
                                    EN_Constante.g_const_errlogCons + EN_Constante.g_const_funcion_EnvioSunat, 
                                    response.ResplistaGuia.DescRespuesta, confg.getDateHourNowLima(),
                                    JsonConvert.SerializeObject(response), JsonConvert.SerializeObject(EN_Constante.g_const_0)); 
                return response;                  
            }
            

            
        }

        
    }
}

