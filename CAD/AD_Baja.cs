﻿/****************************************************************************************************************************************************************************************
 PROGRAMA: AD_Baja.cs
 VERSION : 1.0
 OBJETIVO: Clase entidad de datos baja
 FECHA   : 28/01/2022
 AUTOR   : JUAN ALARCON -  IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 ***************************************************************************************************************************************************************************************/
using System;
using System.Collections.Generic;
using CEN;
using System.Data.SqlClient;
using System.Data;
using Microsoft.VisualBasic.CompilerServices;
using Microsoft.VisualBasic;

namespace CAD
{
    public class AD_Baja
    {
        private readonly AD_Cado _datosConexion;
        private readonly SqlConnection _sqlConexion;
        private readonly SqlCommand _sqlCommand;
        private string TiempoEspera {get;}

        public AD_Baja()
        {
            _datosConexion = new AD_Cado();
            _sqlConexion = new SqlConnection(_datosConexion.CadenaConexion());
            _sqlCommand = new SqlCommand();
            TiempoEspera=EN_ConfigConstantes.Instance.const_TiempoEspera;
        }
        
        public List<EN_RequestEnvioResDiario>  listarDocumentosComunicadoBaja(EN_Empresa oEmpresa,EN_Certificado oCertificado,int empresa_id, string fecha_comunicacion, string estado, string situacion, int flag)
        {
            // DESCRIPCION: FUNCION LISTAR DOCUMENTOS DE BAJA

            EN_RequestEnvioResDiario reqRes;                                                    // Clase entidad Envio Resumen Diario
            List<EN_RequestEnvioResDiario> ListreqRes = new List<EN_RequestEnvioResDiario>();   // Lista de Clase entidad Envio Resumen Diario
            SqlDataReader Rs; // DATA READER
            try
            {
                

                _sqlConexion.Open();
                _sqlCommand.CommandType = CommandType.StoredProcedure;
                _sqlCommand.Connection = _sqlConexion;
                _sqlCommand.CommandText = "Usp_listar_comunicadosBaja";
                _sqlCommand.Parameters.AddWithValue("@empresa_id", empresa_id);
                _sqlCommand.Parameters.AddWithValue("@fecha_comunicacion", fecha_comunicacion);
                _sqlCommand.Parameters.AddWithValue("@estado", estado);
                _sqlCommand.Parameters.AddWithValue("@situacion", situacion);
                _sqlCommand.Parameters.AddWithValue("@flag", flag);
                

                Rs = _sqlCommand.ExecuteReader();
                if (Rs.HasRows)
                        {
                            while (Rs.Read())
                            {
                                // id,empresa_id, tiporesumen, correlativo, fecgenera_resumen, nombrexml, situacion
                                reqRes = new EN_RequestEnvioResDiario();
                                reqRes.flagOse = Conversions.ToString(oCertificado.flagOSE);
                                reqRes.Produccion = Conversions.ToString(oEmpresa.Produccion);
                                reqRes.certUserName = Conversions.ToString(oCertificado.UserName);
                                reqRes.certClave= Conversions.ToString(oCertificado.Clave);
                                reqRes.ruc = Conversions.ToString(oEmpresa.Nrodocumento);
                                reqRes.nombreArchivo = Conversions.ToString(Rs["nombrexml"]);
                                reqRes.resumenId= Conversions.ToString(Rs["id"]);
                                reqRes.empresaId= Conversions.ToString(Rs["empresa_id"]);
                                reqRes.Idpuntoventa = Conversions.ToString(Rs["puntoventa_id"]);
                                reqRes.tiporesumenId = Conversions.ToString(Rs["tiporesumen"]);
                                
                                reqRes.FechaGeneraResumen= Conversions.ToString(Rs["fecha"]);
                                reqRes.FechaComunicacion= Conversions.ToString(Rs["fecha"]);
                                
                                reqRes.correlativo= Conversions.ToString(Rs["correlativo"]);
                                                    
                                ListreqRes.Add(reqRes);
                            }
                            
                        }

                        Rs.Close();
                        return ListreqRes;
            }
            catch (Exception ex)
            {
            
                throw ex;
            }
            finally
            {
                _sqlConexion.Close();
            }
        }

        public List<EN_ResumenBajaPendiente>  listarResumenBajaPendientes( int empresa_id)
        {
            // DESCRIPCION: FUNCION LISTAR DOCUMENTOS DE BAJA

            EN_ResumenBajaPendiente reqRes;                                                    // Clase entidad Envio Resumen Diario
            List<EN_ResumenBajaPendiente> ListreqRes = new List<EN_ResumenBajaPendiente>();   // Lista de Clase entidad Envio Resumen Diario
            SqlDataReader Rs; // DATA READER
            
            try
            {
                

                _sqlConexion.Open();
                _sqlCommand.CommandType = CommandType.StoredProcedure;
                _sqlCommand.Connection = _sqlConexion;
                _sqlCommand.CommandText = "Usp_listar_resumenbajaspendientes";
                _sqlCommand.Parameters.AddWithValue("@empresa_id", empresa_id);

                Rs = _sqlCommand.ExecuteReader();
                if (Rs.HasRows)
                        {
                            while (Rs.Read())
                            {
                                reqRes = new EN_ResumenBajaPendiente();
                                reqRes.id= (int)(Rs["resumenbaja_id"]);
                                ListreqRes.Add(reqRes);
                            }
                            
                        }

                        Rs.Close();
                        return ListreqRes;
            }
            catch (Exception ex)
            {
            
                throw ex;
            }
            finally
            {
                _sqlConexion.Close();
            }
        }
        

         public BajaSunat  listarDetalleResBajaPend( int resumenbaja_id)
        {
            // DESCRIPCION: FUNCION LISTAR DOCUMENTOS DE BAJA

             BajaSunat obBaja  = new BajaSunat();                                                    // Clase entidad Envio Resumen Diario

            EN_DetalleBaja reqRes;                                                    // Clase entidad Envio Resumen Diario
            List<EN_DetalleBaja> ListreqRes = new List<EN_DetalleBaja>();   // Lista de Clase entidad Envio Resumen Diario
            SqlDataReader Rs; // DATA READER
            SqlDataReader RsData; // DATA READER
            SqlCommand dataCommand = new SqlCommand();
            AppConfiguracion.Configuracion confg= new AppConfiguracion.Configuracion(); // Clase configuración
            try
            {
                

                _sqlConexion.Open();
                _sqlCommand.CommandType = CommandType.StoredProcedure;
                _sqlCommand.Connection = _sqlConexion;
                _sqlCommand.CommandText = "Usp_listar_detalleResBajaPend";
                _sqlCommand.Parameters.AddWithValue("@baja_id", resumenbaja_id);

                Rs = _sqlCommand.ExecuteReader();
                if (Rs.HasRows)
                {
                    while (Rs.Read())
                    {
                        reqRes = new EN_DetalleBaja();

                        reqRes.Tipodocumentoid  = (string)(Rs["documento_idtipodocumento"]);
                        reqRes.Serie            = (string)(Rs["serie"]);
                        reqRes.Numero           = (string)(Rs["numero"]);
                        reqRes.MotivoBaja       = (string)(Rs["motivo_baja"]);
                        ListreqRes.Add(reqRes);
                    }
                    
                }

                        Rs.Close();

                dataCommand.CommandType = CommandType.StoredProcedure;
                dataCommand.Connection = _sqlConexion;
                dataCommand.CommandText = "Usp_listar_dataResumenBajaPend";
                dataCommand.Parameters.AddWithValue("@baja_id", resumenbaja_id);

                RsData = dataCommand.ExecuteReader();

                if (RsData.HasRows)
                {
                    while (RsData.Read())
                    {
                        obBaja = new BajaSunat();

                        obBaja.Usuario  = (string)(RsData["username"]);
                        obBaja.Clave            = (string)(RsData["clave"]);
                        obBaja.FechaDocumento           = RsData["fecemision_documento"].ToString();
                        obBaja.Empresa_id  = (int)(RsData["empresa_id"]);
                        obBaja.Idpuntoventa  = (int)(RsData["puntoventa_id"]);
                        obBaja.UsuarioSession = (string)Interaction.IIf(Information.IsDBNull(RsData["usureg"]), "", RsData["usureg"]);
                        obBaja.Estado = EN_Constante.g_const_estado_nuevo;
                        obBaja.Situacion = EN_Constante.g_const_situacion_pendiente;
                        obBaja.Tiporesumen = EN_ConfigConstantes.Instance.const_IdRD;
                        obBaja.FechaComunicacion= confg.getDateNowLima();
                        obBaja.ListaDetalleBaja= ListreqRes;
                        
                    }

                    
                }
                RsData.Close();
                
                return obBaja;
            }
            catch (Exception ex)
            {
            
                throw ex;
            }
            finally
            {
                _sqlConexion.Close();
            }
        }
        
  
    
     
    }
}