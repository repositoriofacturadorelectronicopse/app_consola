﻿using System;
using System.Collections.Generic;
using Microsoft.VisualBasic;
using CEN;
using System.Data.SqlClient;
using System.Collections;
using System.Data;

namespace CAD
{
   
    public class AD_Certificado
    {
        private readonly AD_Cado _datosConexion;
        private readonly SqlConnection _sqlConexion;
        private readonly SqlCommand _sqlCommand;
        private string TiempoEspera {get;}
        public AD_Certificado()
        {
            _datosConexion = new AD_Cado();
            _sqlConexion = new SqlConnection(_datosConexion.CadenaConexion());
            _sqlCommand = new SqlCommand();

            TiempoEspera=EN_ConfigConstantes.Instance.const_TiempoEspera;
        }
        public EN_Certificado listarCertificado(EN_Certificado pCertif)
        {
            SqlCommand cmd; // comando para certificado
            SqlCommand cmdfose; // comando para flag ose
            SqlDataReader Rs;   //Data Reader para certificado
            SqlDataReader Rsfose;   //Data Reader para flag ose
            var withBlock = new EN_Certificado(); //entidad certificado

            try
            {
                
                   _sqlConexion.Open();
                    cmd= new SqlCommand("Usp_listar_certificado",_sqlConexion);
                    cmd.CommandType= CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@id",  pCertif.Id);
                    cmd.Parameters.AddWithValue("@idEmpresa", pCertif.IdEmpresa);
                    cmd.Parameters.AddWithValue("@nombre", pCertif.Nombre);

                    Rs = cmd.ExecuteReader();

                if (Rs.HasRows)
                {
                    while (Rs.Read())
                    {
                       
                            withBlock.Id = (int)Rs["id"];
                            withBlock.IdEmpresa = (int)Rs["empresa_id"];
                            withBlock.Nombre = (string)Interaction.IIf(Information.IsDBNull(Rs["nombre"]), "", Rs["nombre"]);
                            withBlock.Ubicacion = (string)Interaction.IIf(Information.IsDBNull(Rs["ubicacion"]), "", Rs["ubicacion"]);
                            withBlock.UserName = (string)Interaction.IIf(Information.IsDBNull(Rs["username"]), "", Rs["username"]);
                            withBlock.Clave = (string)Interaction.IIf(Information.IsDBNull(Rs["clave"]), "", Rs["clave"]);
                            withBlock.ClaveCertificado = (string)Interaction.IIf(Information.IsDBNull(Rs["clavecertificado"]), "", Rs["clavecertificado"]);
                            withBlock.IdSignature = (string)Interaction.IIf(Information.IsDBNull(Rs["idSignature"]), "", Rs["idSignature"]);
                          
                        
                    }
                }
                Rs.Close();
                
                cmdfose = new SqlCommand("Usp_buscar_parametro",_sqlConexion);
                //buscamos en tabla parámetro  flagose 
                cmdfose.CommandType= CommandType.StoredProcedure;
                cmdfose.Parameters.AddWithValue("@flag",  EN_Constante.g_const_1);
                cmdfose.Parameters.AddWithValue("@idBusqueda1", pCertif.IdEmpresa );
                cmdfose.Parameters.AddWithValue("@idBusqueda2", null );
                Rsfose = cmdfose.ExecuteReader();
                if(Rsfose.HasRows)
                {
                    while(Rsfose.Read())
                    {
                        withBlock.flagOSE = (string)Interaction.IIf(Information.IsDBNull(Rsfose["par_tipo"].ToString()), "0", Rsfose["par_tipo"].ToString());           
                    }
                }
                Rsfose.Close();
                // Guardamos final operacion en el log
                // objLog.MensajeAuditoriaOk = "listado correctamente";
                // AD_clsLog.GuardarFinLogOperacion(objLog, true);
                return withBlock;
            }
            catch (Exception ex)
            {
              
                throw ex;
            }
            finally
            {
                _sqlConexion.Close();
            }
        }
       

         public bool validarEmpresaCertificado(int p_idempresa, string p_usuario, string p_clave)
        {
           SqlDataReader Rs ;
            SqlCommand cmd ;
            try
            {
               


                    _sqlConexion.Open();
                    cmd = new SqlCommand("Usp_validar_empresacertificado",_sqlConexion);
                    cmd.CommandType= CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@id_empresa",  p_idempresa);
                    cmd.Parameters.AddWithValue("@usuario", p_usuario);
                    cmd.Parameters.AddWithValue("@clave", p_clave);

                 Rs = cmd.ExecuteReader();

                if (Rs.HasRows)
                {
                  
                    return true;
                    
                }
                
                else
                {
                   
                    return false;
                }
                // Rs.Close();
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
            finally
            {
                _sqlConexion.Close();
            }
        }
       
        
    }

}
