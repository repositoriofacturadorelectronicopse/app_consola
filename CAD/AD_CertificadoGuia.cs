using System;
using System.Collections.Generic;
using Microsoft.VisualBasic;
using CEN;
using System.Data.SqlClient;
using System.Collections;
using System.Data;
namespace CAD
{
    public class AD_CertificadoGuia
    {
        private SqlConnection connection;   // Variable de conexion
        private readonly AD_Conector _datosConexionGuia;
        private readonly SqlCommand _sqlCommand;
        public AD_CertificadoGuia() {
        _sqlCommand = new SqlCommand();
        _datosConexionGuia = new AD_Conector();
        connection= new SqlConnection(_datosConexionGuia.CxSQLFacturacion());

        }
        public EN_Certificado listarCertificadoGuia(EN_Certificado pCertif)
        {
            SqlCommand cmd; // comando para certificado
            SqlCommand cmdfose; // comando para flag ose
            SqlDataReader Rs;   //Data Reader para certificado
            SqlDataReader Rsfose;   //Data Reader para flag ose
            var withBlock = new EN_Certificado(); //entidad certificado

            try
            {
                
                   connection.Open();
                    cmd= new SqlCommand("Usp_listar_certificado",connection);
                    cmd.CommandType= CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@id",  pCertif.Id);
                    cmd.Parameters.AddWithValue("@idEmpresa", pCertif.IdEmpresa);
                    cmd.Parameters.AddWithValue("@nombre", pCertif.Nombre);

                    Rs = cmd.ExecuteReader();

                if (Rs.HasRows)
                {
                    while (Rs.Read())
                    {
                       
                            withBlock.Id = (int)Rs["id"];
                            withBlock.IdEmpresa = (int)Rs["empresa_id"];
                            withBlock.Nombre = (string)Interaction.IIf(Information.IsDBNull(Rs["nombre"]), "", Rs["nombre"]);
                            withBlock.Ubicacion = (string)Interaction.IIf(Information.IsDBNull(Rs["ubicacion"]), "", Rs["ubicacion"]);
                            withBlock.UserName = (string)Interaction.IIf(Information.IsDBNull(Rs["username"]), "", Rs["username"]);
                            withBlock.Clave = (string)Interaction.IIf(Information.IsDBNull(Rs["clave"]), "", Rs["clave"]);
                            withBlock.ClaveCertificado = (string)Interaction.IIf(Information.IsDBNull(Rs["clavecertificado"]), "", Rs["clavecertificado"]);
                            withBlock.IdSignature = (string)Interaction.IIf(Information.IsDBNull(Rs["idSignature"]), "", Rs["idSignature"]);
                          
                        
                    }
                }
                Rs.Close();
                
                cmdfose = new SqlCommand("Usp_buscar_parametro",connection);
                //buscamos en tabla parámetro  flagose 
                cmdfose.CommandType= CommandType.StoredProcedure;
                cmdfose.Parameters.AddWithValue("@flag",  EN_Constante.g_const_1);
                cmdfose.Parameters.AddWithValue("@idBusqueda1", pCertif.IdEmpresa );
                cmdfose.Parameters.AddWithValue("@idBusqueda2", null );
                Rsfose = cmdfose.ExecuteReader();
                if(Rsfose.HasRows)
                {
                    while(Rsfose.Read())
                    {
                        withBlock.flagOSE = (string)Interaction.IIf(Information.IsDBNull(Rsfose["par_tipo"].ToString()), "0", Rsfose["par_tipo"].ToString());           
                    }
                }
                Rsfose.Close();
                // Guardamos final operacion en el log
                // objLog.MensajeAuditoriaOk = "listado correctamente";
                // AD_clsLog.GuardarFinLogOperacion(objLog, true);
                return withBlock;
            }
            catch (Exception ex)
            {
              
                throw ex;
            }
            finally
            {
                connection.Close();
            }

        }

        
    }
}