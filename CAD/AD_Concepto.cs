/****************************************************************************************************************************************************************************************
 PROGRAMA: AD_Concepto.cs
 VERSION : 1.0
 OBJETIVO: Clase de acceso a datos de conceptos
 FECHA   : 21/06/2021
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/

using System.Data.SqlClient;
using CEN;
using System.Data;
using System;

namespace CAD
{
    public class AD_Concepto
    {
         private SqlConnection Connection; 
        public AD_Concepto()
        {
            // DESCRIPCION: CONSTRUCTOR DE CLASE

        }

        public EN_Concepto Obtener_desc_gbcon(int p_conpfij, int p_concorr)
        {
            //DESCRIPCION: OBTENER DESCRIPCION DE CONCEPTO

            AD_Cado CadCx = new AD_Cado();          // Conexión
            SqlDataReader dr;                       // Data Reader
            EN_Concepto gbcon = new EN_Concepto();  // Clase concepto
            try
            {
                using (Connection = new SqlConnection(CadCx.CadenaConexion()))
                {
                    Connection.Open();
                     using(SqlCommand cmd = new SqlCommand("Usp_listar_concepto", Connection))
                    {
                        cmd.CommandType= CommandType.StoredProcedure;
                        cmd.Parameters.Add("@prefijo", SqlDbType.Int).Value= p_conpfij;
                        cmd.Parameters.Add("@correlativo", SqlDbType.Int).Value= p_concorr;
                        cmd.CommandTimeout = EN_Constante.g_const_0;
                        dr = cmd.ExecuteReader();
                         if (dr.Read())
                        {
                            gbcon.conceptopfij = Convert.ToInt32(dr[EN_Constante.g_const_0]);
                            gbcon.conceptocorr = Convert.ToInt32(dr[EN_Constante.g_const_1]);
                            gbcon.conceptodesc = Convert.ToString(dr[EN_Constante.g_const_2]);
                        }
                    }
                    dr.Close();
                }
                return gbcon;
            }
            catch (Exception)
            {
                return null;
            }
            finally
            {
                Connection.Close();
            }
        }



    }
}

