﻿/****************************************************************************************************************************************************************************************
 PROGRAMA: AD_DocPersona.cs
 VERSION : 1.0
 OBJETIVO: Clase de acceso a datos Doc persona
 FECHA   : 21/06/2021
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/
using System;
using System.Collections.Generic;
using CEN;
using System.Data.SqlClient;
using System.Data;

namespace CAD
{

    public class AD_DocPersona
    {
        private readonly AD_Cado _datosConexion;
        private readonly SqlConnection _sqlConexion;
        private readonly SqlCommand _sqlCommand;
         private string TiempoEspera {get;}
        public AD_DocPersona()
        {
            _datosConexion = new AD_Cado();
            _sqlConexion = new SqlConnection(_datosConexion.CadenaConexion());
            _sqlCommand = new SqlCommand();
             TiempoEspera=EN_ConfigConstantes.Instance.const_TiempoEspera;
        }
        public List<EN_DocPersona> listarDocPersona(EN_DocPersona pDocPersona)
        {
            try
            {
                List<EN_DocPersona> olstDP = new List<EN_DocPersona>();
                EN_DocPersona oDP_EN;
             
                    _sqlConexion.Open();
                    SqlCommand cmd = new SqlCommand("Usp_listar_docpersona",_sqlConexion);
                    cmd.CommandType= CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@codigo",  pDocPersona.Codigo);
                    cmd.Parameters.AddWithValue("@estado", pDocPersona.Estado);
                    cmd.Parameters.AddWithValue("@indpersona", pDocPersona.Indpersona);

                    SqlDataReader Rs = cmd.ExecuteReader();

                if (Rs.HasRows)
                {
                    while (Rs.Read())
                    {
                        oDP_EN = new EN_DocPersona();
                        {
                            var withBlock = oDP_EN;
                            withBlock.Codigo =(string) Rs["codigo"];
                            withBlock.CodigoSunat =(string) Rs["codigosunat"];
                            withBlock.Nombre =(string) Rs["nombre"];
                            withBlock.Longitud =Convert.ToString(Rs["longitud"]);
                            withBlock.Tipo =(string) Rs["tipo"];
                            withBlock.Indcontrib =(string) Rs["indcontrib"];
                            withBlock.Indlongexacta =(string) Rs["indlongexacta"];
                            withBlock.Indpersona =(string) Rs["indpersona"];
                            withBlock.Orden =Convert.ToString(Rs["orden"]);
                            withBlock.Estado =(string) Rs["estado"];
                        }
                        olstDP.Add(oDP_EN);
                    }
                }
                Rs.Close();
                return olstDP;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                _sqlConexion.Close();
            }
        }
    }

}
