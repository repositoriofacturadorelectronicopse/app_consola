﻿using System;
using System.Collections.Generic;
using Microsoft.VisualBasic;
using CEN;
using System.Data.SqlClient;
using System.Collections;
using System.Data;
using Microsoft.VisualBasic.CompilerServices;



namespace CAD
{

 

        public partial class AD_Documento
        {
            private readonly AD_Cado _datosConexion;
            private readonly SqlConnection _sqlConexion;
            private readonly SqlCommand _sqlCommand;
             private string TiempoEspera {get;}

            public AD_Documento()
            {
                _datosConexion = new AD_Cado();
                _sqlConexion = new SqlConnection(_datosConexion.CadenaConexion());
                _sqlCommand = new SqlCommand();
                 TiempoEspera=EN_ConfigConstantes.Instance.const_TiempoEspera;
            }



             public List<EN_Documento> listarDocumentosBatch(EN_Documento pDoc, string tipo_docs)
            {
                //DESCRIPCION: LISTAR DOCUMENTOS PARA PROCESAR A ENVIO A SUNAT

                SqlCommand cmd; // comando para certificado
                SqlDataReader Rs =null;   //Data Reader para certificado
                
                try
                {
                    
                    List<EN_Documento> olstDoc = new List<EN_Documento>();
                    EN_Documento oDoc_EN;

                    _sqlConexion.Open();
                    cmd = new SqlCommand("Usp_listar_documentoBatch",_sqlConexion);
               
                    
                    cmd.CommandType= CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Idempresa",  pDoc.Idempresa);
                    cmd.Parameters.AddWithValue("@Estado", pDoc.Estado);
                    cmd.Parameters.AddWithValue("@FechaIni", pDoc.FechaIni);
                    cmd.Parameters.AddWithValue("@FechaFin",  pDoc.FechaFin );
                    cmd.Parameters.AddWithValue("@tipo_docs",  tipo_docs );

                    Rs = cmd.ExecuteReader();

                    if (Rs.HasRows)
                    {
                        while (Rs.Read())
                        {
                            oDoc_EN = new EN_Documento();
                            {

                                var withBlock = oDoc_EN;
                                withBlock.Id = (string)Rs["id"];
                                withBlock.Idempresa = (int)Rs["empresa_id"];
                                withBlock.Idpuntoventa = (int)Rs["puntoventa_id"];
                                withBlock.Serie = (string)Rs["serie_comprobante"];
                                withBlock.Numero = (string)Rs["nro_comprobante"];
                                withBlock.Fecha = Rs["fecha"].ToString();
                                withBlock.Idtipodocumento = (string) Rs["tipodocumento_id"];
                                withBlock.Estado = (string)Rs["estado"];
                                withBlock.Situacion = (string)Rs["situacion"];
                                withBlock.EnviadoMail = (string)Interaction.IIf(Information.IsDBNull(Rs["enviado_email"]), "NO", Rs["enviado_email"]);
                                withBlock.nombreXML = (string)Interaction.IIf(Information.IsDBNull(Rs["nombrexml"]), "", Rs["nombrexml"]);
                                withBlock.vResumen = (string)Interaction.IIf(Information.IsDBNull(Rs["valorresumen"]), "", Rs["valorresumen"]);
                                withBlock.vFirma = (string)Interaction.IIf(Information.IsDBNull(Rs["valorfirma"]), "", Rs["valorfirma"]);
                            }
                            olstDoc.Add(oDoc_EN);
                        }
                    }
                    
                    Rs.Close();
                
                    return olstDoc;
                }
                catch (SqlException odbcEx) {
                    // Handle more specific SqlException exception here.
                           throw new Exception(odbcEx.InnerException.ToString());
            }
                catch (Exception ex)
                {
                
                    throw ex;
                }
                finally
                {
                    _sqlConexion.Close();
                }
            }

            public List<EN_OtrosCpe> listarOtrosCpe(EN_OtrosCpe pDoc, string tipo_docs)
            {
                //DESCRIPCION: LISTAR DOCUMENTOS PARA PROCESAR A ENVIO A SUNAT

                SqlCommand cmd; // comando para certificado
                SqlDataReader Rs;   //Data Reader para certificado
                 List<EN_OtrosCpe> olstDoc = new List<EN_OtrosCpe>();       // Lista de Clase Entidad Otros Comprobantes
                EN_OtrosCpe oDoc_EN;
                
                try
                {
                    
                   

                    _sqlConexion.Open();
                    cmd = new SqlCommand("Usp_listar_otrosCpeBatch",_sqlConexion);
               
                    
                    cmd.CommandType= CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Idempresa",  pDoc.empresa_id);
                    cmd.Parameters.AddWithValue("@Estado", pDoc.estado);
                    cmd.Parameters.AddWithValue("@FechaIni", pDoc.fechaIni);
                    cmd.Parameters.AddWithValue("@FechaFin",  pDoc.fechaFin );
                    cmd.Parameters.AddWithValue("@tipo_docs",  tipo_docs );
                  
                    Rs = cmd.ExecuteReader();

                    if (Rs.HasRows)
                    {
                        while (Rs.Read())
                        {
                            oDoc_EN = new EN_OtrosCpe();
                            {

                                var withBlock = oDoc_EN;
                                withBlock.id = (string)Rs["id"];
                                withBlock.empresa_id = (int)Rs["empresa_id"];
                                // withBlock.nombreXML = (string)Rs["nombrexml"];
                                withBlock.puntoventa_id = (int)Rs["puntoventa_id"];
                                withBlock.serie = (string)Rs["serie"];
                                withBlock.numero = (string)Rs["numero"];
                                withBlock.fechaemision = Rs["fechaemision"].ToString();
                                withBlock.tipodocumento_id = (string) Rs["tipodocumento_id"];
                                // withBlock.FechaGeneraResumen = Rs["fecgenera_resumen"].ToString();
                                // withBlock.IdentificaResumen = (string)Interaction.IIf(Information.IsDBNull(Rs["identifica_resumen"]), "", Rs["identifica_resumen"]);
                                withBlock.estado = (string)Rs["estado"];
                                withBlock.situacion = (string)Rs["situacion"];
                                withBlock.enviado_email = (string)Interaction.IIf(Information.IsDBNull(Rs["enviado_email"]), "NO", Rs["enviado_email"]);
                                withBlock.nombreXML = (string)Interaction.IIf(Information.IsDBNull(Rs["nombrexml"]), "", Rs["nombrexml"]);
                                withBlock.vResumen = (string)Interaction.IIf(Information.IsDBNull(Rs["valorresumen"]), "", Rs["valorresumen"]);
                                withBlock.vFirma = (string)Interaction.IIf(Information.IsDBNull(Rs["valorfirma"]), "", Rs["valorfirma"]);
                                // withBlock.XMLEnviado = (string)Interaction.IIf(Information.IsDBNull(Rs["xmlgenerado"]), "", Rs["xmlgenerado"]);
                            }
                            olstDoc.Add(oDoc_EN);
                        }
                    }
                    Rs.Close();
                
                    return olstDoc;
                }
                catch (Exception ex)
                {
                
                    throw ex;
                }
                finally
                {
                    _sqlConexion.Close();
                }
            }
            public List<EN_Documento> listarDocumento(EN_Documento pDocumento)
            {
            
                string opeafec = EN_ConfigConstantes.Instance.const_TipoAfectOnerosas;
                string opeinafec = EN_ConfigConstantes.Instance.const_TipoInafectOnerosas;
                string operetir = EN_ConfigConstantes.Instance.const_TipoRetiroGratuito;
                string opexoner = EN_ConfigConstantes.Instance.const_TipoExonerado ;
                string opexport = EN_ConfigConstantes.Instance.const_TipoExportacion;

                
                try
                {
                    var olstDocumento = new List<EN_Documento>();
                    EN_Documento oDocumento_EN;
                    
                    // Para el tipocalculoisc 02 Aplicacion al Monto Fijo - Por confirmar formula
                  
                    string fechaIni=null;
                    string fechaFin=null;
                    if (pDocumento.FechaIni != null & pDocumento.FechaFin != null)
                    {
                       
                        fechaIni=Strings.Format(Conversions.ToDate(pDocumento.FechaIni), "yyyy-MM-dd");
                        fechaFin=Strings.Format(Conversions.ToDate(pDocumento.FechaFin), "yyyy-MM-dd");
                    }

                      _sqlConexion.Open();
                    SqlCommand cmd = new SqlCommand("Usp_listar_documento",_sqlConexion);
                    cmd.CommandType= CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@id",  pDocumento.Id.ToString());
                    cmd.Parameters.AddWithValue("@Idempresa", pDocumento.Idempresa.ToString());
                    cmd.Parameters.AddWithValue("@Idtipodocumento", pDocumento.Idtipodocumento.ToString());
                    cmd.Parameters.AddWithValue("@Estado", pDocumento.Estado.ToString());
                    cmd.Parameters.AddWithValue("@Situacion", pDocumento.Situacion.ToString());
                    cmd.Parameters.AddWithValue("@Serie",pDocumento.Serie.ToString());
                    cmd.Parameters.AddWithValue("@Numero",pDocumento.Numero.ToString());
                    cmd.Parameters.AddWithValue("@FechaIni", fechaIni);
                    cmd.Parameters.AddWithValue("@FechaFin", fechaFin);
                    cmd.Parameters.AddWithValue("@opeafec", opeafec);
                    cmd.Parameters.AddWithValue("@opeinafec", opeinafec);
                    cmd.Parameters.AddWithValue("@operetir",operetir);
                    cmd.Parameters.AddWithValue("@opexoner", opexoner);
                    cmd.Parameters.AddWithValue("@opexport", opexport);
            


                    SqlDataReader Rs = cmd.ExecuteReader();

                    if (Rs.HasRows)
                    {
                        while (Rs.Read())
                        {
                            oDocumento_EN = new EN_Documento();
                            oDocumento_EN.Idempresa= (int)Rs["empresa_id"];
                            oDocumento_EN.Id= (string)Rs["id"];
                            oDocumento_EN.Idtipodocumento= (string)Rs["tipodocumento_id"];
                            oDocumento_EN.NroDocEmpresa= (string)Rs["empnrodoc"];
                            oDocumento_EN.DescripcionEmpresa= (string)Rs["empnombre"];
                            oDocumento_EN.Tipodocumento= (string)Rs["tipodocumento"];
                            oDocumento_EN.Serie= (string)Rs["serie_comprobante"];
                            oDocumento_EN.Numero= (string)Rs["nro_comprobante"];
                            oDocumento_EN.Idpuntoventa =(int)Interaction.IIf(Information.IsDBNull(Rs["puntoventa_id"]), (object)0, Rs["puntoventa_id"]);
                            oDocumento_EN.DescripcionPuntoVenta =(string)Interaction.IIf(Information.IsDBNull(Rs["puntoventadescripcion"]), "", Rs["puntoventadescripcion"]);
                            oDocumento_EN.CodigoEstablecimiento =(string)Interaction.IIf(Information.IsDBNull(Rs["codigoestablec"]), "0000", Rs["codigoestablec"]);
                            oDocumento_EN.TipoOperacion =(string)Interaction.IIf(Information.IsDBNull(Rs["tipooperacion"]), "", Rs["tipooperacion"]);
                            oDocumento_EN.TipoNotaCredNotaDeb =(string)Interaction.IIf(Information.IsDBNull(Rs["tiponotacreddeb"]), "", Rs["tiponotacreddeb"]);
                            oDocumento_EN.Tipodoccliente =(string)Interaction.IIf(Information.IsDBNull(Rs["cliente_tipodoc"]), "", Rs["cliente_tipodoc"]);
                            oDocumento_EN.Nrodoccliente =(string)Interaction.IIf(Information.IsDBNull(Rs["cliente_nrodoc"]), "", Rs["cliente_nrodoc"]);
                            oDocumento_EN.Nombrecliente =(string)Interaction.IIf(Information.IsDBNull(Rs["cliente_nombre"]), "", Rs["cliente_nombre"]);
                            oDocumento_EN.Direccioncliente =(string)Interaction.IIf(Information.IsDBNull(Rs["cliente_direccion"]), "", Rs["cliente_direccion"]);
                            oDocumento_EN.Emailcliente =(string)Interaction.IIf(Information.IsDBNull(Rs["cliente_email"]), "", Rs["cliente_email"]);
                            oDocumento_EN.Fecha= Rs["fecha"].ToString();
                            oDocumento_EN.Hora =(string) Rs["hora"];
                            oDocumento_EN.Fechavencimiento =(string)Interaction.IIf(Information.IsDBNull(Rs["fechavencimientopago"].ToString()), "", Rs["fechavencimientopago"].ToString());
                            oDocumento_EN.Glosa =(string)Interaction.IIf(Information.IsDBNull(Rs["glosa"]), "", Rs["glosa"]);
                            oDocumento_EN.Moneda= (string)Rs["moneda"];
                            oDocumento_EN.Tipocambio =(double)Interaction.IIf(Information.IsDBNull(Rs["tipocambio"]), (object)0, Rs["tipocambio"]);
                            oDocumento_EN.Igvventa =(double)Interaction.IIf(Information.IsDBNull(Rs["igvventa"]), (object)0, Rs["igvventa"]);
                            oDocumento_EN.Iscventa =(double)Interaction.IIf(Information.IsDBNull(Rs["iscventa"]), (object)0, Rs["iscventa"]);
                            oDocumento_EN.OtrosTributos =(double)Interaction.IIf(Information.IsDBNull(Rs["otrostributos"]), (object)0, Rs["otrostributos"]);
                            oDocumento_EN.Descuento =(double)Interaction.IIf(Information.IsDBNull(Rs["descuento"]), (object)0, Rs["descuento"]);
                            oDocumento_EN.OtrosCargos =(double)Interaction.IIf(Information.IsDBNull(Rs["otroscargos"]), (object)0, Rs["otroscargos"]);
                            oDocumento_EN.Importeventa= (double)Rs["importeventa"];
                            oDocumento_EN.Regimenpercep =(string)Interaction.IIf(Information.IsDBNull(Rs["regimenpercep"]), "", Rs["regimenpercep"]);
                            oDocumento_EN.Porcentpercep =(double)Interaction.IIf(Information.IsDBNull(Rs["porcentpercep"]), (object)0, Rs["porcentpercep"]);
                            oDocumento_EN.DescRegimenpercep =(string)Interaction.IIf(Information.IsDBNull(Rs["descregimenpercep"]), "", Rs["descregimenpercep"]);
                            oDocumento_EN.Importepercep =(double)Interaction.IIf(Information.IsDBNull(Rs["importepercep"]), (object)0, Rs["importepercep"]);
                            oDocumento_EN.Porcentdetrac =(double)Interaction.IIf(Information.IsDBNull(Rs["porcentdetrac"]), (object)0, Rs["porcentdetrac"]);
                            oDocumento_EN.Importedetrac =(double)Interaction.IIf(Information.IsDBNull(Rs["importedetrac"]), (object)0, Rs["importedetrac"]);
                            oDocumento_EN.Importerefdetrac =(double)Interaction.IIf(Information.IsDBNull(Rs["importerefdetrac"]), (object)0, Rs["importerefdetrac"]);
                            oDocumento_EN.Importefinal= (double)Rs["importefinal"];
                            oDocumento_EN.Importeanticipo =(double)Interaction.IIf(Information.IsDBNull(Rs["importeanticipo"]), (object)0, Rs["importeanticipo"]);
                            oDocumento_EN.IndicaAnticipo =(string)Interaction.IIf(Information.IsDBNull(Rs["indicaanticipo"]), "", Rs["indicaanticipo"]);
                            oDocumento_EN.Estado= (string)Rs["estado"];
                            oDocumento_EN.Situacion= (string)Rs["situacion"];
                            oDocumento_EN.Numeroelectronico =(string)Interaction.IIf(Information.IsDBNull(Rs["numeroelectronico"]), (object)0, Rs["numeroelectronico"]);
                            oDocumento_EN.Estransgratuita =(string)Interaction.IIf(Information.IsDBNull(Rs["estransgratuita"]), "NO", Rs["estransgratuita"]);
                            oDocumento_EN.EnviadoMail =(string)Interaction.IIf(Information.IsDBNull(Rs["enviado_email"]), "NO", Rs["enviado_email"]);
                            oDocumento_EN.Envioexterno =(string)Interaction.IIf(Information.IsDBNull(Rs["enviado_externo"]), "NO", Rs["enviado_externo"]);
                            oDocumento_EN.Valorpergravadas =(double)Interaction.IIf(Information.IsDBNull(Rs["valopergravadas"]), (object)0, Rs["valopergravadas"]);
                            oDocumento_EN.Valorperinafectas =(double)Interaction.IIf(Information.IsDBNull(Rs["valoperinafectas"]), (object)0, Rs["valoperinafectas"]);
                            oDocumento_EN.Valorperexoneradas =(double)Interaction.IIf(Information.IsDBNull(Rs["valoperexoneradas"]), (object)0, Rs["valoperexoneradas"]);
                            oDocumento_EN.Valorperexportacion =(double)Interaction.IIf(Information.IsDBNull(Rs["valoperexportacion"]), (object)0, Rs["valoperexportacion"]);
                            oDocumento_EN.Valorpergratuitas =(double)Interaction.IIf(Information.IsDBNull(Rs["valopergratuitas"]), (object)0, Rs["valopergratuitas"]);
                            oDocumento_EN.Valorperiscreferenc =(double)Interaction.IIf(Information.IsDBNull(Rs["valoperiscreferenc"]), (object)0, Rs["valoperiscreferenc"]);
                            oDocumento_EN.SumaIgvGratuito =(double)Interaction.IIf(Information.IsDBNull(Rs["sumaigvgratuito"]), (double)0, Rs["sumaigvgratuito"]);
                            oDocumento_EN.SumaIscGratuito =(double)Interaction.IIf(Information.IsDBNull(Rs["sumaiscgratuito"]), (double)0, Rs["sumaiscgratuito"]);
                            oDocumento_EN.TotalDsctoItem =(double)Interaction.IIf(Information.IsDBNull(Rs["totaldsctoitem"]), (double)0, Rs["totaldsctoitem"]);
                            oDocumento_EN.TotalCargoItem =(double)Interaction.IIf(Information.IsDBNull(Rs["totalcargoitem"]), (double)0, Rs["totalcargoitem"]);
                            oDocumento_EN.TipoGuiaRemision =(string)Interaction.IIf(Information.IsDBNull(Rs["tipoguiaremision"]), "", Rs["tipoguiaremision"]);
                            oDocumento_EN.GuiaRemision =(string)Interaction.IIf(Information.IsDBNull(Rs["guiaremision"]), "", Rs["guiaremision"]);
                            oDocumento_EN.TipoDocOtroDocRef =(string)Interaction.IIf(Information.IsDBNull(Rs["tipodocotrodocref"]), "", Rs["tipodocotrodocref"]);
                            oDocumento_EN.OtroDocumentoRef =(string)Interaction.IIf(Information.IsDBNull(Rs["otrodocumentoref"]), "", Rs["otrodocumentoref"]);
                            oDocumento_EN.OrdenCompra =(string)Interaction.IIf(Information.IsDBNull(Rs["ordencompra"]), "", Rs["ordencompra"]);
                            oDocumento_EN.PlacaVehiculo =(string)Interaction.IIf(Information.IsDBNull(Rs["placavehiculo"]), "", Rs["placavehiculo"]);
                            oDocumento_EN.MontoFise =(double)Interaction.IIf(Information.IsDBNull(Rs["montofise"]), Convert.ToDouble(0), Convert.ToDouble(Rs["montofise"]));
                            oDocumento_EN.TipoRegimen =(string)Interaction.IIf(Information.IsDBNull(Rs["tiporegimen"]), "", Rs["tiporegimen"]);
                            oDocumento_EN.CodigoBBSSSujetoDetrac =(string)Interaction.IIf(Information.IsDBNull(Rs["codigobbsssujetodetrac"]), "", Rs["codigobbsssujetodetrac"]);
                            oDocumento_EN.NumCtaBcoNacion =(string)Interaction.IIf(Information.IsDBNull(Rs["numctabconacion"]), "", Rs["numctabconacion"]);
                            oDocumento_EN.BienTransfAmazonia =(string)Interaction.IIf(Information.IsDBNull(Rs["bientransfamazonia"]), "", Rs["bientransfamazonia"]);
                            oDocumento_EN.ServiTransfAmazonia =(string)Interaction.IIf(Information.IsDBNull(Rs["servitransfamazonia"]), "", Rs["servitransfamazonia"]);
                            oDocumento_EN.ContratoConstAmazonia =(string)Interaction.IIf(Information.IsDBNull(Rs["contratoconstamazonia"]), "", Rs["contratoconstamazonia"]);
                            oDocumento_EN.nombreXML =(string)Interaction.IIf(Information.IsDBNull(Rs["nombrexml"]), "", Rs["nombrexml"]);
                            oDocumento_EN.vResumen =(string)Interaction.IIf(Information.IsDBNull(Rs["valorresumen"]), "", Rs["valorresumen"]);
                            oDocumento_EN.vFirma =(string)Interaction.IIf(Information.IsDBNull(Rs["valorfirma"]), "", Rs["valorfirma"]);
                            oDocumento_EN.xmlEnviado =(string)Interaction.IIf(Information.IsDBNull(Rs["xmlgenerado"]), "", Rs["xmlgenerado"]);
                            oDocumento_EN.cod_aux_01 =(string)Interaction.IIf(Information.IsDBNull(Rs["cod_aux_01"]), "", Rs["cod_aux_01"]);
                            oDocumento_EN.text_aux_01 =(string)Interaction.IIf(Information.IsDBNull(Rs["text_aux_01"]), "", Rs["text_aux_01"]);
                            oDocumento_EN.cod_aux_02 =(string)Interaction.IIf(Information.IsDBNull(Rs["cod_aux_02"]), "", Rs["cod_aux_02"]);
                            oDocumento_EN.text_aux_02 =(string)Interaction.IIf(Information.IsDBNull(Rs["text_aux_02"]), "", Rs["text_aux_02"]);
                            oDocumento_EN.cod_aux_03 =(string)Interaction.IIf(Information.IsDBNull(Rs["cod_aux_03"]), "", Rs["cod_aux_03"]);
                            oDocumento_EN.text_aux_03 =(string)Interaction.IIf(Information.IsDBNull(Rs["text_aux_03"]), "", Rs["text_aux_03"]);
                            oDocumento_EN.cod_aux_04 =(string)Interaction.IIf(Information.IsDBNull(Rs["cod_aux_04"]), "", Rs["cod_aux_04"]);
                            oDocumento_EN.text_aux_04 =(string)Interaction.IIf(Information.IsDBNull(Rs["text_aux_04"]), "", Rs["text_aux_04"]);
                            oDocumento_EN.cod_aux_05 =(string)Interaction.IIf(Information.IsDBNull(Rs["cod_aux_05"]), "", Rs["cod_aux_05"]);
                            oDocumento_EN.text_aux_05 =(string)Interaction.IIf(Information.IsDBNull(Rs["text_aux_05"]), "", Rs["text_aux_05"]);
                            oDocumento_EN.cod_aux_06 =(string)Interaction.IIf(Information.IsDBNull(Rs["cod_aux_06"]), "", Rs["cod_aux_06"]);
                            oDocumento_EN.text_aux_06 =(string)Interaction.IIf(Information.IsDBNull(Rs["text_aux_06"]), "", Rs["text_aux_06"]);
                            oDocumento_EN.cod_aux_07 =(string)Interaction.IIf(Information.IsDBNull(Rs["cod_aux_07"]), "", Rs["cod_aux_07"]);
                            oDocumento_EN.text_aux_07 =(string)Interaction.IIf(Information.IsDBNull(Rs["text_aux_07"]), "", Rs["text_aux_07"]);
                            oDocumento_EN.cod_aux_08 =(string)Interaction.IIf(Information.IsDBNull(Rs["cod_aux_08"]), "", Rs["cod_aux_08"]);
                            oDocumento_EN.text_aux_08 =(string)Interaction.IIf(Information.IsDBNull(Rs["text_aux_08"]), "", Rs["text_aux_08"]);
                            oDocumento_EN.cod_aux_09 =(string)Interaction.IIf(Information.IsDBNull(Rs["cod_aux_09"]), "", Rs["cod_aux_09"]);
                            oDocumento_EN.text_aux_09 =(string)Interaction.IIf(Information.IsDBNull(Rs["text_aux_09"]), "", Rs["text_aux_09"]);
                            oDocumento_EN.cod_aux_10 =(string)Interaction.IIf(Information.IsDBNull(Rs["cod_aux_10"]), "", Rs["cod_aux_10"]);
                            oDocumento_EN.text_aux_10 =(string)Interaction.IIf(Information.IsDBNull(Rs["text_aux_10"]), "", Rs["text_aux_10"]);
                            oDocumento_EN.cod_aux_11 =(string)Interaction.IIf(Information.IsDBNull(Rs["cod_aux_11"]), "", Rs["cod_aux_11"]);
                            oDocumento_EN.text_aux_11 =(string)Interaction.IIf(Information.IsDBNull(Rs["text_aux_11"]), "", Rs["text_aux_11"]);
                            oDocumento_EN.descripciondetrac =(string)Interaction.IIf(Information.IsDBNull(Rs["descripciondetrac"]), "", Rs["descripciondetrac"]);
                            oDocumento_EN.desctiponotcreddeb =(string)Interaction.IIf(Information.IsDBNull(Rs["desctiponotcreddeb"]), "", Rs["desctiponotcreddeb"]);
                            oDocumento_EN.Formapago= (string)Rs["Formapago"];
                            oDocumento_EN.jsonpagocredito =(string)Rs["jsonpagocredito"];
                            oDocumento_EN.hasRetencionIgv=(string)Rs["hasRetencionIgv"];
                            oDocumento_EN.porcRetencionIgv=(double)Interaction.IIf(Information.IsDBNull(Rs["porcRetIgv"]),(double)0, Rs["porcRetIgv"]);
                            oDocumento_EN.impRetencionIgv=(decimal)Interaction.IIf(Information.IsDBNull(Rs["impRetIgv"]), (decimal)0, Rs["impRetIgv"]);
                            oDocumento_EN.impOperacionRetencionIgv=(decimal)Interaction.IIf(Information.IsDBNull(Rs["impOpeRetIgv"]), (decimal)0, Rs["impOpeRetIgv"]);
                            

                            olstDocumento.Add(oDocumento_EN);
                        }
                    }

                    Rs.Close();
                    return olstDocumento;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    _sqlConexion.Close();
                }
            }

          

            public List<EN_DocReferencia> listarDocumentoReferencia(EN_DocReferencia pDocumento)
            {
               
                try
                {
                    var olstDocumento = new List<EN_DocReferencia>();
                    EN_DocReferencia oDocumento_EN;
                   
                    

                    _sqlConexion.Open();
                    SqlCommand cmd = new SqlCommand("Usp_listar_documentoreferencia",_sqlConexion);
                    cmd.CommandType= CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@id",  pDocumento.Iddocumento.ToString());
                    cmd.Parameters.AddWithValue("@Idempresa", pDocumento.Idempresa.ToString());
                    cmd.Parameters.AddWithValue("@Idtipodocumento",pDocumento.Idtipodocumento.ToString());

                    SqlDataReader Rs = cmd.ExecuteReader();
                    if (Rs.HasRows)
                    {
                        while (Rs.Read())
                        {
                            oDocumento_EN = new EN_DocReferencia();
                            oDocumento_EN.Idempresa= (int)Rs["empresa_id"];
                            oDocumento_EN.Iddocumento= (string)Rs["documento_id"];
                            oDocumento_EN.Idtipodocumento= (string)Rs["tipodocumento_id"];
                            oDocumento_EN.Iddocumentoref= (string)Rs["documentoref_id"];
                            oDocumento_EN.Idtipodocumentoref= (string)Rs["tipodocumentoref_id"];
                            oDocumento_EN.Numerodocref= (string)Rs["numerodocreferencia"];
                            oDocumento_EN.DescripcionTipoDoc= (string)Rs["descripcion"];
                            olstDocumento.Add(oDocumento_EN);
                        }
                    }

                    Rs.Close();
                   
                    return olstDocumento;
                }
                catch (Exception ex)
                {
                    
                    throw ex;
                }
                finally
                {
                    _sqlConexion.Close();
                }
            }



         public List<EN_Documento> listarDocumentosResumen(EN_Documento pDocumento)
            {
                 // DESCRIPCION: FUNCION LISTAR DOCUMENTOS PARA RESUMEN DIARIO

                string opeafec = EN_ConfigConstantes.Instance.const_TipoAfectOnerosas;      // TIPO DE AFECTACION ONEROSA
                string opeinafec = EN_ConfigConstantes.Instance.const_TipoInafectOnerosas;  // TIPO INAFECTO ONEROSA
                string operetir = EN_ConfigConstantes.Instance.const_TipoRetiroGratuito;    // TIPO RETIRO GRATUITO
                string opexoner = EN_ConfigConstantes.Instance.const_TipoExonerado ;        // TIPO EXONERADO
                string opexport = EN_ConfigConstantes.Instance.const_TipoExportacion;       // TIPO EXPORTACION
                
                List<EN_Documento> olstDocumento = new List<EN_Documento>();                // LISTA DE CLASE ENTIDAD DOCUMENTO
                EN_Documento oDocumento_EN;                                                 // CLASE ENTIDAD DOCUMENTO
                SqlDataReader Rs;                                                           // DATA READER
              
                try
                {
                   
                     _sqlConexion.Open();
                    _sqlCommand.CommandType = CommandType.StoredProcedure;
                    _sqlCommand.Connection = _sqlConexion;
                    _sqlCommand.CommandText = "Usp_listar_documentosResumen";

                    _sqlCommand.Parameters.AddWithValue("@Idempresa", pDocumento.Idempresa.ToString());
                    _sqlCommand.Parameters.AddWithValue("@Idtipodocumento", pDocumento.Idtipodocumento.ToString());
                    _sqlCommand.Parameters.AddWithValue("@Estado", pDocumento.Estado.ToString());
                    _sqlCommand.Parameters.AddWithValue("@Situacion", pDocumento.Situacion.ToString());
                    _sqlCommand.Parameters.AddWithValue("@FechaIni", pDocumento.FechaIni);
                    _sqlCommand.Parameters.AddWithValue("@FechaFin", pDocumento.FechaIni);
                    _sqlCommand.Parameters.AddWithValue("@opeafec", opeafec);
                    _sqlCommand.Parameters.AddWithValue("@opeinafec", opeinafec);
                    _sqlCommand.Parameters.AddWithValue("@operetir",operetir);
                    _sqlCommand.Parameters.AddWithValue("@opexoner", opexoner);
                    _sqlCommand.Parameters.AddWithValue("@opexport", opexport);

                     Rs = _sqlCommand.ExecuteReader();
                    if (Rs.HasRows)
                    {
                        while (Rs.Read())
                        {
                            oDocumento_EN = new EN_Documento();
                            {
                                var withBlock = oDocumento_EN;
                                withBlock.Idempresa = (int)Rs["empresa_id"];
                                withBlock.Id = (string)Rs["id"];
                                withBlock.Idtipodocumento = (string)Rs["tipodocumento_id"];
                                withBlock.Serie = (string)Rs["serie_comprobante"];
                                withBlock.Numero = (string)Rs["nro_comprobante"];
                                withBlock.Tipodoccliente = (string)Interaction.IIf(Information.IsDBNull(Rs["cliente_tipodoc"]), "", Rs["cliente_tipodoc"]);
                                withBlock.Nrodoccliente = (string)Interaction.IIf(Information.IsDBNull(Rs["cliente_nrodoc"]), "", Rs["cliente_nrodoc"]);
                                withBlock.Fecha = Rs["fecha"].ToString();
                                withBlock.Moneda = (string)Rs["moneda"];
                                withBlock.Valorpergravadas = (double)Interaction.IIf(Information.IsDBNull(Rs["valopergravadas"]), 0, Rs["valopergravadas"]);
                                withBlock.Valorperinafectas = (double)Interaction.IIf(Information.IsDBNull(Rs["valoperinafectas"]), 0, Rs["valoperinafectas"]);
                                withBlock.Valorperexoneradas = (double)Interaction.IIf(Information.IsDBNull(Rs["valoperexoneradas"]), 0, Rs["valoperexoneradas"]);
                                withBlock.Valorpergratuitas = (double)Interaction.IIf(Information.IsDBNull(Rs["valopergratuitas"]), 0, Rs["valopergratuitas"]);
                                withBlock.Valorperiscreferenc = (double)Interaction.IIf(Information.IsDBNull(Rs["valoperiscreferenc"]), 0, Rs["valoperiscreferenc"]);
                                withBlock.Valorperexportacion = (double)Interaction.IIf(Information.IsDBNull(Rs["valoperexportacion"]), 0, Rs["valoperexportacion"]);
                                withBlock.OtrosCargos = (double)Interaction.IIf(Information.IsDBNull(Rs["otroscargos"]), 0, Rs["otroscargos"]);
                                withBlock.Iscventa = (double)Interaction.IIf(Information.IsDBNull(Rs["iscventa"]), 0, Rs["iscventa"]);
                                withBlock.Igvventa = (double)Interaction.IIf(Information.IsDBNull(Rs["igvventa"]), 0, Rs["igvventa"]);
                                withBlock.OtrosTributos = (double)Interaction.IIf(Information.IsDBNull(Rs["otrostributos"]), 0, Rs["otrostributos"]);
                                withBlock.Importeventa =(double) Rs["importeventa"];
                                withBlock.Regimenpercep = (string)Interaction.IIf(Information.IsDBNull(Rs["regimenpercep"]), "", Rs["regimenpercep"]);
                                withBlock.Porcentpercep = (double)Interaction.IIf(Information.IsDBNull(Rs["porcentpercep"]), 0, Rs["porcentpercep"]);
                                withBlock.Importepercep = (double)Interaction.IIf(Information.IsDBNull(Rs["importepercep"]), 0, Rs["importepercep"]);
                                withBlock.Importefinal = (double)Rs["importefinal"];
                                withBlock.Estado = (string)Rs["estado"];
                                withBlock.Situacion = (string)Rs["situacion"];
                            }
                            olstDocumento.Add(oDocumento_EN);
                        }
                    }
                    Rs.Close();
                   
                    return olstDocumento;
                }
                catch (Exception ex)
                {
                    
                    throw ex;
                }
                finally
                {
                    _sqlConexion.Close();
                }
            }





        }
    

}
