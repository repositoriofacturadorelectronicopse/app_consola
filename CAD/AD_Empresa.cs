﻿using System;
using System.Collections.Generic;
using Microsoft.VisualBasic;
using CEN;
using System.Data.SqlClient;
using System.Collections;
using System.Data;

namespace CAD
{
    public class AD_Empresa
    {
        private SqlConnection connection;   // Variable de conexion
        private readonly AD_Cado _datosConexion;
        private readonly SqlConnection _sqlConexion;
        private readonly SqlCommand _sqlCommand;
         private string TiempoEspera {get;}
        public AD_Empresa()
        {
            _datosConexion = new AD_Cado();
            // _sqlConexion = new SqlConnection(_datosConexion.CadenaConexion(true));
            _sqlConexion = new SqlConnection(_datosConexion.CadenaConexion());
            _sqlCommand = new SqlCommand();
             TiempoEspera=EN_ConfigConstantes.Instance.const_TiempoEspera;
        }
        public EN_Empresa listarEmpresa(EN_Empresa pEmpresa, int Idpuntoventa)
        {
            //DESCRIPCION: FUNCION PAARA LISTAR LOS DATOS DE LA EMPRESA

            var withBlock = new EN_Empresa(); // Entidad Empresa 
            SqlCommand cmd ;                    // Comando para empresa
            SqlCommand cmdPar ;                    // Comando para parámetro
            SqlDataReader Rs;                   // DataReader para empresa
            SqlDataReader RsPar;                   // DataReader para parámetro
            string uspName= "";                 // variable nombre de procedimiento
            
            try
            {
                    if(Idpuntoventa==0) uspName="Usp_listar_empresaresumen";
                    else uspName="Usp_listar_empresa";

                    _sqlConexion.Open();
                    cmd = new SqlCommand(uspName,_sqlConexion);
                    cmd.CommandType= CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@IdEmpresa",  pEmpresa.Id);
                    cmd.Parameters.AddWithValue("@Nrodocumento",  pEmpresa.Nrodocumento);
                    cmd.Parameters.AddWithValue("@RazonSocial", pEmpresa.RazonSocial);
                    cmd.Parameters.AddWithValue("@Estado",pEmpresa.Estado);
                    if(Idpuntoventa!=0)  cmd.Parameters.AddWithValue("@Idpuntoventa", Idpuntoventa);

                    Rs = cmd.ExecuteReader();
                if (Rs.HasRows)
                {
                    while (Rs.Read())
                    {
                            withBlock.Id = (int)Rs["id"];
                            withBlock.Tipoempresa = (string)Rs["tipoempresa"];
                            withBlock.Nrodocumento = (string)Rs["nrodocumento"];
                            withBlock.RazonSocial = (string)Rs["razonsocial"];
                            withBlock.Nomcomercial = (string)Interaction.IIf(Information.IsDBNull(Rs["nomcomercial"]), "", Rs["nomcomercial"]);
                            withBlock.Propietario = (string)Interaction.IIf(Information.IsDBNull(Rs["propietario"]), "", Rs["propietario"]);
                            withBlock.Tipodocumento = (string)Rs["tipodocumento"];
                            withBlock.Nrodoc = (string)Interaction.IIf(Information.IsDBNull(Rs["nrodoc"]), "", Rs["nrodoc"]);
                            withBlock.Apellido = (string)Interaction.IIf(Information.IsDBNull(Rs["apellido"]), "", Rs["apellido"]);
                            withBlock.Nombre = (string)Interaction.IIf(Information.IsDBNull(Rs["nombre"]), "", Rs["nombre"]);
                            withBlock.Direccion = (string)Interaction.IIf(Information.IsDBNull(Rs["direccion"]), "", Rs["direccion"]);
                            withBlock.Sector = (string)Interaction.IIf(Information.IsDBNull(Rs["sector"]), "", Rs["sector"]);
                            withBlock.Telefono1 = (string)Interaction.IIf(Information.IsDBNull(Rs["telefono1"]), "", Rs["telefono1"]);
                            withBlock.Telefono2 = (string)Interaction.IIf(Information.IsDBNull(Rs["telefono2"]), "", Rs["telefono2"]);
                            withBlock.Email = (string)Interaction.IIf(Information.IsDBNull(Rs["email"]), "", Rs["email"]);
                            withBlock.Web = (string)Interaction.IIf(Information.IsDBNull(Rs["web"]), "", Rs["web"]);
                            withBlock.Imagen = (string)Interaction.IIf(Information.IsDBNull(Rs["imagen"]), "", Rs["imagen"]);
                            if(Idpuntoventa!=0)  withBlock.Codpais = (string)Interaction.IIf(Information.IsDBNull(Rs["codpais"]), "", Rs["codpais"]);
                            if(Idpuntoventa!=0) withBlock.Coddis = (string)Interaction.IIf(Information.IsDBNull(Rs["coddis"]), "", Rs["coddis"]);
                            if(Idpuntoventa!=0)  withBlock.Departamento = (string)Interaction.IIf(Information.IsDBNull(Rs["departamento"]), "", Rs["departamento"]);
                            if(Idpuntoventa!=0)  withBlock.Provincia = (string)Interaction.IIf(Information.IsDBNull(Rs["provincia"]), "", Rs["provincia"]);
                            if(Idpuntoventa!=0)  withBlock.Distrito = (string)Interaction.IIf(Information.IsDBNull(Rs["distrito"]), "", Rs["distrito"]);
                            withBlock.SignatureID = (string)Interaction.IIf(Information.IsDBNull(Rs["signatureID"]), "", Rs["signatureID"]);
                            withBlock.SignatureURI = (string)Interaction.IIf(Information.IsDBNull(Rs["signatureURI"]), "", Rs["signatureURI"]);
                            withBlock.NroResolucion = (string)Interaction.IIf(Information.IsDBNull(Rs["numeroresolucion"]), "", Rs["numeroresolucion"]);
                            withBlock.CuentaBcoDet = (string)Interaction.IIf(Information.IsDBNull(Rs["cuentabcodet"]), "", Rs["cuentabcodet"]);
                            withBlock.AplicaISC = (string)Interaction.IIf(Information.IsDBNull(Rs["aplicaisc"]), "0", Rs["aplicaisc"]);
                            withBlock.AplicaDctoItem = (string)Interaction.IIf(Information.IsDBNull(Rs["aplicadctoitem"]), "0", Rs["aplicadctoitem"]);
                            withBlock.AplicaCargoItem = (string)Interaction.IIf(Information.IsDBNull(Rs["aplicacargoitem"]), "0", Rs["aplicacargoitem"]);
                            withBlock.MuestraIgv = (string)Interaction.IIf(Information.IsDBNull(Rs["muestraigv"]), "0", Rs["muestraigv"]);
                            withBlock.MuestraSubTotal = (string)Interaction.IIf(Information.IsDBNull(Rs["muestrasubtotal"]), "0", Rs["muestrasubtotal"]);
                            withBlock.AplicaDctoGlobal = (string)Interaction.IIf(Information.IsDBNull(Rs["aplicadctoglobal"]), "0", Rs["aplicadctoglobal"]);
                            withBlock.AplicaCargoGlobal = (string)Interaction.IIf(Information.IsDBNull(Rs["aplicacargoglobal"]), "0", Rs["aplicacargoglobal"]);
                            withBlock.Estado = (string)Rs["estado"];
                        
                    }
                }
                Rs.Close();

                cmdPar = new SqlCommand("Usp_buscar_parametro",_sqlConexion);
                //buscamos en tabla parámetro ; flag=5: busca ruta, enviarxml y produccion 
                cmdPar.CommandType= CommandType.StoredProcedure;
                cmdPar.Parameters.AddWithValue("@flag",  EN_Constante.g_const_5);
                cmdPar.Parameters.AddWithValue("@idBusqueda1", pEmpresa.Id );
                cmdPar.Parameters.AddWithValue("@idBusqueda2", Idpuntoventa );
                RsPar = cmdPar.ExecuteReader();
                if(RsPar.HasRows)
                {
                    while(RsPar.Read())
                    {
                        withBlock.enviarxml = (string)Interaction.IIf(Information.IsDBNull(RsPar["envio"].ToString()), "NO", RsPar["envio"].ToString());  
                        withBlock.Produccion = (string)Interaction.IIf(Information.IsDBNull(RsPar["produccion"].ToString()), "NO", RsPar["produccion"].ToString());  
                    }
                }
                RsPar.Close();
                return withBlock;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                _sqlConexion.Close();
            }
        }


        public EN_RespuestaEmpresa listarEmpresaGuia(EN_Empresa pEmpresa, int IdpuntoEmision)
        {
            EN_RespuestaEmpresa respuesta = new EN_RespuestaEmpresa();
            AD_Conector conector = new AD_Conector();
            EN_ErrorWebService errorservice = new EN_ErrorWebService();
            SqlDataReader dr = null;
            int contEmp = EN_Constante.g_const_0;
            string nomProc = string.Empty;
            try
            {
                if (IdpuntoEmision == EN_Constante.g_const_0) {
                    nomProc = "Usp_listar_empresaresumen";
                } else {
                    nomProc = "Usp_listar_empresa";
                }

            using (connection = new SqlConnection(conector.CxSQLFacturacion()))
            {
                connection.Open();
                using (SqlCommand Command = new SqlCommand(nomProc, connection)) 
                {
                    Command.CommandType = CommandType.StoredProcedure;
                    Command.Parameters.Add("@IdEmpresa", SqlDbType.Int).Value = pEmpresa.Id;
                    Command.Parameters.Add("@Nrodocumento", SqlDbType.VarChar,255).Value = pEmpresa.Nrodocumento;
                    Command.Parameters.Add("@RazonSocial", SqlDbType.VarChar,255).Value = pEmpresa.RazonSocial;
                    Command.Parameters.Add("@Estado", SqlDbType.VarChar,255).Value = pEmpresa.Estado;
                    if(IdpuntoEmision!=0)  Command.Parameters.Add("@Idpuntoventa", SqlDbType.Int).Value = IdpuntoEmision;
                    Command.CommandTimeout = EN_Constante.g_const_0;
                     dr = Command.ExecuteReader();
                     while(dr.Read())  
                     {

                         EN_Empresa empresa = new EN_Empresa();
                        if (dr["id"] != null && dr["id"].ToString() != "" )
                        empresa.Id= Convert.ToInt32(dr["id"].ToString());

                        if (dr["tipoempresa"] != null )
                        empresa.Tipodocumento= dr["tipoempresa"].ToString();

                        if (dr["nrodocumento"] != null )
                        empresa.Nrodocumento= dr["nrodocumento"].ToString();

                        if (dr["razonsocial"] != null )
                        empresa.RazonSocial= dr["razonsocial"].ToString();

                        if (dr["propietario"] != null )
                        empresa.Propietario= dr["propietario"].ToString();

                        if (dr["tipodocumento"] != null )
                        empresa.Tipodocumento= dr["tipodocumento"].ToString();

                        if (dr["nomcomercial"] != null )
                        empresa.Nomcomercial= dr["nomcomercial"].ToString();

                        if (dr["nrodoc"] != null )
                        empresa.Nrodoc= dr["nrodoc"].ToString();

                        if (dr["apellido"] != null )
                        empresa.Apellido= dr["apellido"].ToString();

                        if (dr["nombre"] != null )
                        empresa.Nombre= dr["nombre"].ToString();

                        if (dr["direccion"] != null )
                        empresa.Direccion= dr["direccion"].ToString();

                        if (dr["sector"] != null )
                        empresa.Sector= dr["sector"].ToString();

                        if (dr["telefono1"] != null )
                        empresa.Telefono1= dr["telefono1"].ToString();

                        if (dr["telefono2"] != null )
                        empresa.Telefono2= dr["telefono2"].ToString();

                        if (dr["email"] != null )
                        empresa.Email= dr["email"].ToString();

                        if (dr["web"] != null )
                        empresa.Web= dr["web"].ToString();

                        if (dr["imagen"] != null )
                        empresa.Imagen= dr["imagen"].ToString();

                        if (IdpuntoEmision != EN_Constante.g_const_0) {

                            if (dr["codpais"] != null )
                            empresa.Codpais= dr["codpais"].ToString();

                            if (dr["coddis"] != null )
                            empresa.Coddis= dr["coddis"].ToString();

                            if (dr["departamento"] != null )
                            empresa.Departamento= dr["departamento"].ToString();

                            if (dr["provincia"] != null )
                            empresa.Provincia= dr["provincia"].ToString();

                            if (dr["distrito"] != null )
                            empresa.Distrito= dr["distrito"].ToString();

                        }
                
                        if (dr["signatureID"] != null )
                        empresa.SignatureID= dr["signatureID"].ToString();

                        if (dr["signatureURI"] != null )
                        empresa.SignatureURI= dr["signatureURI"].ToString();

                        if (dr["numeroresolucion"] != null )
                        empresa.NroResolucion= dr["numeroresolucion"].ToString();

                        if (dr["cuentabcodet"] != null )
                        empresa.CuentaBcoDet= dr["cuentabcodet"].ToString();

                        if (dr["aplicaisc"] != null )
                        empresa.AplicaISC= dr["aplicaisc"].ToString();

                        if (dr["aplicadctoitem"] != null )
                        empresa.AplicaDctoItem= dr["aplicadctoitem"].ToString();

                        if (dr["aplicacargoitem"] != null )
                        empresa.AplicaCargoItem= dr["aplicacargoitem"].ToString();

                        if (dr["muestraigv"] != null )
                        empresa.MuestraIgv= dr["muestraigv"].ToString();

                        if (dr["muestrasubtotal"] != null )
                        empresa.MuestraSubTotal= dr["muestrasubtotal"].ToString();

                        if (dr["aplicadctoglobal"] != null )
                        empresa.AplicaDctoGlobal= dr["aplicadctoglobal"].ToString();

                        if (dr["aplicacargoglobal"] != null )
                        empresa.AplicaCargoGlobal= dr["aplicacargoglobal"].ToString();

                        respuesta.objEmpresa = empresa;
                        contEmp = contEmp + EN_Constante.g_const_1;
                     }
                        if (contEmp > EN_Constante.g_const_0) {
                            respuesta.RespRegistro.FlagVerificacion = true;
                            respuesta.RespRegistro.DescRespuesta = EN_Constante.g_const_consultaexitosa;
                            errorservice.TipoError = EN_Constante.g_const_0;
                            errorservice.CodigoError = EN_Constante.g_const_2000;
                            errorservice.DescripcionErr = EN_Constante.g_const_consultaexitosa;
                            respuesta.errorService = errorservice;


                        } else {
                            respuesta.RespRegistro.FlagVerificacion = false;
                            respuesta.RespRegistro.DescRespuesta = EN_Constante.g_const_empreNoEnc;
                            errorservice.TipoError = EN_Constante.g_const_1;
                            errorservice.CodigoError = EN_Constante.g_const_3000;
                            errorservice.DescripcionErr = EN_Constante.g_const_error_interno;
                            respuesta.errorService = errorservice;

                        }

                     }
                     dr.Close();
                }
                return respuesta; 
            }
            catch (Exception ex)
            {   
                respuesta.RespRegistro.FlagVerificacion = false;
                respuesta.RespRegistro.DescRespuesta = EN_Constante.g_const_error_interno;
                errorservice.TipoError = EN_Constante.g_const_1;
                errorservice.CodigoError = EN_Constante.g_const_3000;
                errorservice.DescripcionErr = ex.Message;
                respuesta.errorService = errorservice;
                return respuesta;
            }
             finally 
            {
                connection.Close();
            }


        }
         public EN_RespuestaEmpresa buscarParametroEmpresa(int codconcepto, int codcorrelativo)
            {
                //DESCRIPCION: buscamos en tabla parámetro ; flag=5: busca ruta, enviarxml y produccion 
                EN_RespuestaEmpresa respuesta = new EN_RespuestaEmpresa(); //RESPUESTA 
                SqlDataReader reader ; //Data reader
                AD_Conector CadCx = new AD_Conector(); // CONEXION

                int contEmp = EN_Constante.g_const_0;
                try 
                {
                using (connection = new SqlConnection(CadCx.CxSQLFacturacion()))
                {
                    connection.Open();
                    using (SqlCommand Comando = new SqlCommand("Usp_buscar_parametro", connection))
                    {
                        Comando.CommandType= CommandType.StoredProcedure;
                        Comando.Parameters.AddWithValue("@flag",  EN_Constante.g_const_5);
                        Comando.Parameters.AddWithValue("@idBusqueda1", codconcepto);
                        Comando.Parameters.AddWithValue("@idBusqueda2", codcorrelativo);
                        Comando.CommandTimeout = EN_Constante.g_const_0;
                        reader = Comando.ExecuteReader();     
                        while(reader.Read())
                        {
                            EN_Empresa objEmpresa = new EN_Empresa();
                            objEmpresa.enviarxml = (string)Interaction.IIf(Information.IsDBNull(reader["envio"].ToString()), "NO", reader["envio"].ToString());  
                            objEmpresa.Produccion = (string)Interaction.IIf(Information.IsDBNull(reader["produccion"].ToString()), "NO", reader["produccion"].ToString());  
                            respuesta.objEmpresa = objEmpresa;
                            contEmp = contEmp + EN_Constante.g_const_1;
                        }

                        if (contEmp > EN_Constante.g_const_0) 
                        {
                            respuesta.RespRegistro.FlagVerificacion = true;
                            respuesta.RespRegistro.DescRespuesta = EN_Constante.g_const_consultaexitosa;;
                            respuesta.errorService.CodigoError = EN_Constante.g_const_2000;
                            respuesta.errorService.DescripcionErr = EN_Constante.g_const_consultaexitosa;
                            respuesta.errorService.TipoError = EN_Constante.g_const_0;

                        } else {
                            respuesta.RespRegistro.FlagVerificacion = false;
                            respuesta.RespRegistro.DescRespuesta = EN_Constante.g_const_consultaexitosa;;
                            respuesta.errorService.CodigoError = EN_Constante.g_const_3000;
                            respuesta.errorService.DescripcionErr = EN_Constante.g_const_error_interno;
                            respuesta.errorService.TipoError = EN_Constante.g_const_1;
                        }


                    }
                    reader.Close();
                }                                 
                 return respuesta;   
                }
                catch (Exception ex)
                {
                   
                     respuesta.RespRegistro.FlagVerificacion = false;
                     respuesta.RespRegistro.DescRespuesta = EN_Constante.g_const_error_interno;
                     respuesta.errorService.CodigoError = EN_Constante.g_const_3000;
                     respuesta.errorService.DescripcionErr = ex.Message;
                     respuesta.errorService.TipoError = EN_Constante.g_const_1;
                     return respuesta;
                   
                }
                finally 
                {
                    connection.Close();
                  
                }
                
            }
       

    }
}
