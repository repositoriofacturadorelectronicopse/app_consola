﻿using System;
using System.Collections;


using System.Data;
using System.Data.SqlClient;
using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices; // Install-Package Microsoft.VisualBasic
using CEN;
using System.Text;

namespace CAD
{

 
        public partial class AD_Facturacion
        {
            private readonly AD_Cado _datosConexion;
            private readonly SqlConnection _sqlConexion;
            private readonly SqlCommand _sqlCommand;
             private string TiempoEspera {get;}

            public AD_Facturacion()
            {
                _datosConexion = new AD_Cado();
                _sqlConexion = new SqlConnection(_datosConexion.CadenaConexion());
                _sqlCommand = new SqlCommand();
                 TiempoEspera=EN_ConfigConstantes.Instance.const_TiempoEspera;
            }

            public string buscarConstantesTablaSunat(string codtabla,string codalterno)
            {
                int contador=0;             // CONTADOR
                string response="";         // RESPUESTA
                StringBuilder bld ;         // ACUMULADOR DE STRING  
              
                try 
                {
                    
            
                    _sqlConexion.Open();
                    SqlCommand cmd = new SqlCommand("Usp_buscar_tabla_codigosunat",_sqlConexion);
                    cmd.CommandType= CommandType.StoredProcedure;
                    // flag=0 para buscar por cod alterno, y 1 para codelemento, 2 obtener descripcion y buscar por codsunat
                    // 3= obtienes descripcion y busqueda codelemento, 4 obtienes codalterno y busqueda codelemento

                    cmd.Parameters.AddWithValue("@flag", 0);
                    cmd.Parameters.AddWithValue("@codtabla", codtabla);
                    cmd.Parameters.AddWithValue("@codbusqueda", codalterno);

                    SqlDataReader reader = cmd.ExecuteReader();

                  
                    bld = new StringBuilder();

                    while(reader.Read())
                    {
                        if(codalterno=="RG"){
                            if(contador!=0)
                            {
                                bld.Append(",");
                            }
                            //    bld.Append("'"+reader["descpcampo"].ToString()+"'");
                               bld.Append(reader["descpcampo"].ToString());
                               contador++;
                        }else{
                              bld.Append(reader["descpcampo"].ToString());
                        }
                    }
                    
                    response = bld.ToString();
                         return response;
                    
                }
                catch (Exception ex)
                {
                     _sqlConexion.Close();
                    throw ex;
                }
                finally {
                    _sqlConexion.Close();
                }

            }

            public string ObtenerCodigoSunat(string codtabla, string codigo)
            {
                try
                {
                    string CodigoSunat = "";
               

                      _sqlConexion.Open();
                    SqlCommand cmd = new SqlCommand("Usp_buscar_tabla_codigosunat",_sqlConexion);
                    cmd.CommandType= CommandType.StoredProcedure;
                    // flag=0 para buscar por cod alterno, y 1 para codelemento, 2 obtener descripcion y buscar por codsunat
                    // 3= obtienes descripcion y busqueda codelemento, 4 obtienes codalterno y busqueda codelemento
                    cmd.Parameters.AddWithValue("@flag", 1);
                    cmd.Parameters.AddWithValue("@codtabla", codtabla.Trim());
                    cmd.Parameters.AddWithValue("@codbusqueda", codigo.Trim());

                    SqlDataReader Rs = cmd.ExecuteReader();
                    if (Rs.HasRows)
                    {
                        while (Rs.Read())
                            CodigoSunat = Conversions.ToString(Rs["descpcampo"]);
                    }

                    Rs.Close();
                    return CodigoSunat.Trim();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    _sqlConexion.Close();
                }
            }

         
            public string ObtenerDescripcionPorCodElemento(string strCodTabla, string strCodSistema)
            {
                try
                {
                    string descripcion = "";
              
                 _sqlConexion.Open();
                    SqlCommand cmd = new SqlCommand("Usp_buscar_tabla_codigosunat",_sqlConexion);
                    cmd.CommandType= CommandType.StoredProcedure;
                    // flag=0 para buscar por cod alterno, y 1 para codelemento, 2 obtener descripcion y buscar por codsunat
                    // 3= obtienes descripcion y busqueda codelemento, 4 obtienes codalterno y busqueda codelemento
                    cmd.Parameters.AddWithValue("@flag", 3);
                    cmd.Parameters.AddWithValue("@codtabla",strCodTabla.Trim());
                    cmd.Parameters.AddWithValue("@codbusqueda",strCodSistema.Trim());

                    SqlDataReader Rs = cmd.ExecuteReader();
                    if (Rs.HasRows)
                    {
                        while (Rs.Read())
                            descripcion = Conversions.ToString(Rs["descpcampo"]);
                    }

                    Rs.Close();
                    return descripcion.Trim();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    _sqlConexion.Close();
                }
            }

            
          


            public string buscarParametroAppSettings(int codconcepto, int codcorrelativo)
            {
                // DESCRIPCION: BUSCAR CONSTANTES EN TABLA SUNAT
                
                SqlCommand cmd ; //comando
                SqlDataReader reader ; //Data reader
                try 
                {
                    _sqlConexion.Open();
                    cmd = new SqlCommand("Usp_buscar_parametro",_sqlConexion);
                    cmd.CommandType= CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@flag", 0);
                    cmd.Parameters.AddWithValue("@idBusqueda1", codconcepto);
                    cmd.Parameters.AddWithValue("@idBusqueda2", codcorrelativo);

                    reader = cmd.ExecuteReader();

                    string response="";
                    while(reader.Read())
                    {
                        response = (reader["par_descripcion"]==null)?"":reader["par_descripcion"].ToString();
                    }

                    return response;
                    
                }
                catch (Exception ex)
                {
                     _sqlConexion.Close();
                    
                    throw ex;
                }
                finally {
                    _sqlConexion.Close();
                }
                
            }


        
        }
    
}
