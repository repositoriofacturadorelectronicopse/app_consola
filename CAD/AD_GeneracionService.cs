﻿using System;
using Microsoft.VisualBasic;
using System.Data.SqlClient;
using CEN;
using System.Data;
using System.Globalization;


namespace CAD
{


    public class AD_GeneracionService
    {
        private readonly AD_Cado _datosConexion;
        private readonly SqlConnection _sqlConexion;
        private readonly SqlCommand _sqlCommand;

        public AD_GeneracionService()
        {
            _datosConexion = new AD_Cado();
            _sqlConexion = new SqlConnection(_datosConexion.CadenaConexion());
            _sqlCommand = new SqlCommand();
        }

       

        ﻿public int GuardarResumenSunat(ResumenSunat resumenSunat)
        {
            int idresumen = 0;
            SqlTransaction transaction = null/* TODO Change to default(_) if this is not a reference type */;
            SqlCommand detCommand = new SqlCommand();
            SqlCommand docrefCommand = new SqlCommand();
          

            // var en_Bitacora = new EN_Bitacora();
            // var oBitacora =  new AD_Bitacora();
            string idDocRes="";
            try
            {
                _sqlConexion.Open();
                transaction = _sqlConexion.BeginTransaction();

                _sqlCommand.CommandType = CommandType.StoredProcedure;
                _sqlCommand.Transaction = transaction;
                _sqlCommand.Connection = _sqlConexion;
                _sqlCommand.CommandText = "Usp_registrar_resumen";
                _sqlCommand.Transaction = transaction;

                detCommand.CommandType = CommandType.StoredProcedure;
                detCommand.Transaction = transaction;
                detCommand.Connection = _sqlConexion;
                detCommand.CommandText = "Usp_registrar_deta_resumen";
                detCommand.Transaction = transaction;

                _sqlCommand.Parameters.AddWithValue("@empresa_id", resumenSunat.Empresa_id);
                _sqlCommand.Parameters.AddWithValue("@tiporesumen", resumenSunat.Tiporesumen);
                _sqlCommand.Parameters.AddWithValue("@fecemision_documento", Convert.ToDateTime(resumenSunat.FechaEmisionDocumento.ToString(), new CultureInfo(EN_ConfigConstantes.Instance.const_CultureInfoPeru)).ToString(EN_Constante.g_const_formfechaGuion));
                _sqlCommand.Parameters.AddWithValue("@fecgenera_resumen", Convert.ToDateTime(resumenSunat.FechaGeneraResumen.ToString(), new CultureInfo(EN_ConfigConstantes.Instance.const_CultureInfoPeru)).ToString(EN_Constante.g_const_formfechaGuion));
                _sqlCommand.Parameters.AddWithValue("@estado", resumenSunat.Estado);
                _sqlCommand.Parameters.AddWithValue("@situacion", resumenSunat.Situacion);
                _sqlCommand.Parameters.AddWithValue("@usureg", resumenSunat.UsuarioSession);
                _sqlCommand.Parameters.AddWithValue("@estadoresumen", resumenSunat.EstadoResumen);
                
                _sqlCommand.Parameters.Add("@idresumen", SqlDbType.Int).Direction = ParameterDirection.Output;
                _sqlCommand.Parameters.Add("@idDocRes", SqlDbType.VarChar, 50).Direction = ParameterDirection.Output;
                _sqlCommand.ExecuteNonQuery();

                idresumen = (int)_sqlCommand.Parameters["@idresumen"].Value;
                idDocRes = (string)_sqlCommand.Parameters["@idDocRes"].Value;

                // guardamos en bitacora
                // en_Bitacora.btc_documentoID =  idDocRes;
                // en_Bitacora.btc_tipo= "info";
                // en_Bitacora.btc_empresaId = resumenSunat.Empresa_id;
                // en_Bitacora.btc_claseOrigen = typeof(ResumenSunat).ToString();
                // en_Bitacora.btc_nombreLog = null;
                // en_Bitacora.btc_metodoFuncionClase = "Iniciar Guardar Resumen";
                // oBitacora.GuardarBitacora(en_Bitacora);

                int i = 1;
                foreach (EN_DetalleResumen detalle in resumenSunat.ListaDetalleResumen)
                {
                    detCommand.Parameters.AddWithValue("@resumen_id", idresumen);
                    detCommand.Parameters.AddWithValue("@item", i);
                    detCommand.Parameters.AddWithValue("@tipodocumento_id", detalle.Tipodocumentoid);
                    detCommand.Parameters.AddWithValue("@serie", detalle.Serie);
                    detCommand.Parameters.AddWithValue("@correlativo", detalle.Correlativo);
                    detCommand.Parameters.AddWithValue("@doccliente", detalle.DocCliente);
                    detCommand.Parameters.AddWithValue("@nrodoccliente", detalle.NroDocCliente);
                    detCommand.Parameters.AddWithValue("@condicion", detalle.Condicion);
                    detCommand.Parameters.AddWithValue("@moneda", detalle.Moneda);
                    detCommand.Parameters.AddWithValue("@opegravadas", detalle.Opegravadas);
                    detCommand.Parameters.AddWithValue("@opeexoneradas", detalle.Opeexoneradas);
                    detCommand.Parameters.AddWithValue("@opeinafectas", detalle.Opeinafectas);
                    detCommand.Parameters.AddWithValue("@opegratuitas", detalle.Opegratuitas);
                    detCommand.Parameters.AddWithValue("@otroscargos", detalle.Otroscargos);
                    detCommand.Parameters.AddWithValue("@totalISC", detalle.TotalISC);
                    detCommand.Parameters.AddWithValue("@totalIGV", detalle.TotalIGV);
                    detCommand.Parameters.AddWithValue("@otrostributos", detalle.Otrostributos);
                    detCommand.Parameters.AddWithValue("@importeventa", detalle.Importeventa);
                    detCommand.Parameters.AddWithValue("@tipodocref", Interaction.IIf(Information.IsNothing(detalle.Tipodocref), DBNull.Value, detalle.Tipodocref));
                    detCommand.Parameters.AddWithValue("@nrodocref", Interaction.IIf(Information.IsNothing(detalle.Nrodocref), DBNull.Value, detalle.Nrodocref));
                    detCommand.Parameters.AddWithValue("@regimenpercep", Interaction.IIf(Information.IsNothing(detalle.Regimenpercep), DBNull.Value, detalle.Regimenpercep));
                    detCommand.Parameters.AddWithValue("@porcentpercep", detalle.Porcentpercep);
                    detCommand.Parameters.AddWithValue("@importepercep", detalle.Importepercep);
                    detCommand.Parameters.AddWithValue("@importefinal", detalle.Importefinal);
                    detCommand.ExecuteNonQuery();
                    detCommand.Parameters.Clear();
                    i = i + 1;
                }
                transaction.Commit();

                 // guardamos en bitacora
                // en_Bitacora.btc_metodoFuncionClase = "Guardar Resumen Correctamente";
                // oBitacora.GuardarBitacora(en_Bitacora);
                return idresumen;
            }
            catch (Exception ex)
            {
                transaction.Rollback();
               
                // guardamos en bitacora
            
                // en_Bitacora.btc_tipo= "error";
                // en_Bitacora.btc_claseOrigen = typeof(AD_GeneracionService).ToString();
                // en_Bitacora.btc_metodoFuncionClase ="Error al Guardar: " + ex.Message;
                // oBitacora.GuardarBitacora(en_Bitacora);
                
                throw ex;
            }
        }
               
        
    }

}
