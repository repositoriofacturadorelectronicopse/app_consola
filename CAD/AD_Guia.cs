
/****************************************************************************************************************************************************************************************
 PROGRAMA: AD_Guia.cs
 VERSION : 1.0
 OBJETIVO: Clase acceso a datos de Guia
 FECHA   : 18/06/2021
 AUTOR   : JOSE CHUMIOQUE -  IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 ***************************************************************************************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using CEN;
namespace CAD
{
    public class AD_Guia
    {
        private SqlConnection connection;   // Variable de conexion
        public EN_RespuestaListaGuia listarGuiasPendientes (EN_Guia obGuia, string tipDoc) 
        {
        //DESCRIPCION: Clase que lista las guias pendientes de envío a SUNAT
        EN_RespuestaListaGuia ResListaGuia = new EN_RespuestaListaGuia();
        AD_Conector conector = new AD_Conector();
        SqlDataReader dr = null;
        try
        {
            using (connection = new SqlConnection(conector.CxSQLFacturacion()))
            {
                connection.Open();
                using (SqlCommand Command = new SqlCommand("Usp_listar_guias_pendientes", connection)) 
                {
                    Command.CommandType = CommandType.StoredProcedure;
                    Command.Parameters.Add("@idempresa", SqlDbType.Int).Value = obGuia.empresa_id;
                    Command.Parameters.Add("@estado", SqlDbType.VarChar,10).Value = obGuia.estado;
                    Command.Parameters.Add("@fechaIni", SqlDbType.Date).Value = obGuia.fechaIni;
                    Command.Parameters.Add("@fechaFin", SqlDbType.Date).Value = obGuia.fechaFin;
                    Command.Parameters.Add("@tipoDoc", SqlDbType.VarChar,10).Value = tipDoc;
                    Command.CommandTimeout = EN_Constante.g_const_0;
                     dr = Command.ExecuteReader();
                    while(dr.Read()) 
                    {
                        EN_Guia objGuia = new EN_Guia();
                        if (dr["id"] != null)
                        objGuia.id = dr["id"].ToString();

                        if(dr["empresa_id"] != null)
                        objGuia.empresa_id = Convert.ToInt32(dr["empresa_id"].ToString());

                        if (dr["tipodocumento_id"] != null)
                        objGuia.tipodocumento_id = dr["tipodocumento_id"].ToString();

                        if (dr["serie"] != null)
                        objGuia.serie = dr["serie"].ToString();

                        if (dr["numero"] != null) 
                        objGuia.numero = dr["numero"].ToString();


                        if (dr["ptoemision"] != null)
                        {
                             if (dr["ptoemision"].ToString() !="")
                             {
                                 objGuia.puntoemision = int.Parse(dr["ptoemision"].ToString());
                             }
                       
                        }
                        if (dr["fechaemision"] != null)
                        objGuia.fechaemision = dr["fechaemision"].ToString();

                        if(dr["fechatraslado"]!= null)
                        objGuia.fechatraslado = dr["fechatraslado"].ToString();

                        if(dr["estado"]!= null)
                        objGuia.estado = dr["estado"].ToString();

                        if(dr["situacion"]!=null)
                        objGuia.situacion = dr["situacion"].ToString();

                        if(dr["enviado_email"]!= null)
                        objGuia.enviado_email = dr["enviado_email"].ToString();

                        if(dr["nombrexml"]!= null)
                        objGuia.nombreXML = dr["nombrexml"].ToString();

                        if(dr["valorresumen"]!= null)
                        objGuia.vResumen = dr["valorresumen"].ToString();

                        if(dr["valorfirma"]!= null)
                        objGuia.vFirma = dr["valorfirma"].ToString();

                        ResListaGuia.listDetGuia.Add(objGuia);
                        
                    }
                     if (ResListaGuia.listDetGuia.Count > EN_Constante.g_const_0)
                    {
                        ResListaGuia.ResplistaGuia.FlagVerificacion = true;
                        ResListaGuia.ResplistaGuia.dato = 2000;
                        ResListaGuia.ResplistaGuia.DescRespuesta = EN_Constante.g_const_consultaexitosa;
                    } else {
                        ResListaGuia.ResplistaGuia.FlagVerificacion = false;
                        ResListaGuia.ResplistaGuia.dato = 1000;
                        ResListaGuia.ResplistaGuia.DescRespuesta = "";

                    }

                }
                dr.Close();
            }
             return ResListaGuia;
        }
        catch (Exception ex)
        {
            
            ResListaGuia.ResplistaGuia.FlagVerificacion = false;
            ResListaGuia.ResplistaGuia.DescRespuesta = ex.Message;
            ResListaGuia.ResplistaGuia.dato = 3000;
            return ResListaGuia;
        }
        finally 
        {
            connection.Close();
        }

            

        }
            public EN_RespuestaData listarConstantesTablaSunat(int flag,string codtabla,string codalterno)
            {
                //DESCRIPCION: Llenamos los valores para el LOG
                EN_RespuestaData respuesta = new EN_RespuestaData();
                AD_Conector conector = new AD_Conector();
                AD_Guia objGuia = new AD_Guia();
                EN_ErrorWebService errorservice = new EN_ErrorWebService();
                SqlDataReader dr = null;
                string response=string.Empty;
                int contador=EN_Constante.g_const_0;      
                try 
                {

                    using (connection = new SqlConnection(conector.CxSQLFacturacion()))
                    {
                        connection.Open();
                        using (SqlCommand Command = new SqlCommand("Usp_buscar_tabla_codigosunat", connection)) 
                        {
                            Command.CommandType = CommandType.StoredProcedure;
                            Command.Parameters.Add("@flag", SqlDbType.Int).Value = flag;
                            Command.Parameters.Add("@codtabla", SqlDbType.VarChar,50).Value = codtabla;
                            Command.Parameters.Add("@codbusqueda", SqlDbType.VarChar,50).Value = codalterno;
                            Command.CommandTimeout = EN_Constante.g_const_0;
                            dr = Command.ExecuteReader();
                            while(dr.Read()) 
                            {
                                if(codalterno=="RG"){
                                    if(contador!=0){response +=",";}
                                
                                    response+="'"+dr["descpcampo"].ToString()+"'";
                                    contador++;
                                }else{
                                    response= dr["descpcampo"].ToString();
                                }

                            }
                             if (response!= null && response.Trim() != "") 
                            {
                                respuesta.ResplistaGuia.FlagVerificacion = true;
                                respuesta.ResplistaGuia.DescRespuesta = response;
                                respuesta.errorService.TipoError = EN_Constante.g_const_0;
                                respuesta.errorService.DescripcionErr = EN_Constante.g_const_consultaexitosa;
                                respuesta.errorService.CodigoError = EN_Constante.g_const_2000;
                            } else {
                                respuesta.ResplistaGuia.FlagVerificacion = false;
                                respuesta.ResplistaGuia.DescRespuesta = EN_Constante.g_const_vacio;
                                respuesta.errorService.TipoError = EN_Constante.g_const_1;
                                respuesta.errorService.DescripcionErr = EN_Constante.g_const_error_interno;
                                respuesta.errorService.CodigoError = EN_Constante.g_const_3000;
                            }
                        }
                        dr.Close();
                    }                                             
                    return respuesta;                    
                }
                catch (Exception ex)
                {
                    respuesta.ResplistaGuia.FlagVerificacion = false;
                    respuesta.ResplistaGuia.DescRespuesta = EN_Constante.g_const_error_interno;
                    respuesta.errorService.TipoError = EN_Constante.g_const_1;
                    respuesta.errorService.DescripcionErr = ex.Message;
                    respuesta.errorService.CodigoError = EN_Constante.g_const_3000;
                    return respuesta;
                   
                }
                finally {
                    connection.Close();
                }
            }
             public EN_RespuestaRegistro buscarParametroAppSettings(int codconcepto, int codcorrelativo)
            {
                // DESCRIPCION: BUSCAR CONSTANTES EN TABLA SUNAT
                EN_RespuestaRegistro respuesta = new EN_RespuestaRegistro();
                SqlDataReader reader ; //Data reader
                AD_Conector conector = new AD_Conector();
                string response = string.Empty;
                try 
                {
                using (connection = new SqlConnection(conector.CxSQLFacturacion()))
                {
                    connection.Open();
                    using (SqlCommand Comando = new SqlCommand("Usp_buscar_parametro", connection))
                    {
                        Comando.CommandType= CommandType.StoredProcedure;
                        Comando.Parameters.AddWithValue("@flag", 0);
                        Comando.Parameters.AddWithValue("@idBusqueda1", codconcepto);
                        Comando.Parameters.AddWithValue("@idBusqueda2", codcorrelativo);
                        Comando.CommandTimeout = EN_Constante.g_const_0;
                        reader = Comando.ExecuteReader();

      
                        while(reader.Read())
                        {
                            response = (reader["par_descripcion"]==null)?"":reader["par_descripcion"].ToString();
                        }

                        if (response != null && response.Trim() != "") 
                        {
                            respuesta.FlagVerificacion = true;
                            respuesta.DescRespuesta = response;

                        } else {
                            respuesta.FlagVerificacion = false;
                            respuesta.DescRespuesta = response;
                        }


                    }
                    reader.Close();
                }
                    
               
                 return respuesta;   
                }
                catch (Exception ex)
                {                  
                     respuesta.FlagVerificacion = false;
                     respuesta.DescRespuesta = ex.Message;
                     return respuesta;
                   
                }
                finally 
                {
                    connection.Close();
                  
                }
                
            }
        public Int32 registrar_log_inicio(string p_prog,string p_idoc,  string p_tdoc,int p_iemp,int p_ipvt,string p_nemp, int p_nive,
                                        string p_nwba, string p_durl, int p_tser,int p_tipo,int p_ferr,  int p_oerr, string p_erro,string p_derr,string p_fech,
                                        string p_hini, string p_date, string p_hfin, string p_dats,string p_user)
        {
            //DESCRIPCION: función para registrar log inicio
            AD_Conector conector = new AD_Conector();
            SqlDataReader dr; // Data Reader
            Int32 ntraLog = EN_Constante.g_const_0; // Número de transacción
            try
            {
                
                using (connection = new SqlConnection(conector.CxSQLFacturacion()))
                {
                    connection.Open();
                    using(SqlCommand cmd = new SqlCommand("Usp_registrar_log_inicio_webapi", connection))
                    {
                        cmd.CommandType= CommandType.StoredProcedure;

                        cmd.Parameters.Add("@p_prog", SqlDbType.VarChar).Value= p_prog;
                        cmd.Parameters.Add("@p_idoc", SqlDbType.VarChar).Value= p_idoc;
                        cmd.Parameters.Add("@p_tdoc", SqlDbType.VarChar).Value= p_tdoc;
                        cmd.Parameters.Add("@p_iemp", SqlDbType.SmallInt).Value= p_iemp;
                        cmd.Parameters.Add("@p_ipvt", SqlDbType.VarChar).Value= p_ipvt;
                        cmd.Parameters.Add("@p_nemp", SqlDbType.VarChar).Value= p_nemp;
                        cmd.Parameters.Add("@p_nive", SqlDbType.SmallInt).Value= p_nive;
                        cmd.Parameters.Add("@p_nwba", SqlDbType.VarChar).Value=  p_nwba;
                        cmd.Parameters.Add("@p_durl", SqlDbType.VarChar).Value= p_durl;
                        cmd.Parameters.Add("@p_tser", SqlDbType.SmallInt).Value = p_tser;
                        cmd.Parameters.Add("@p_tipo", SqlDbType.SmallInt).Value= p_tipo;
                        cmd.Parameters.Add("@p_ferr", SqlDbType.SmallInt).Value= p_ferr;
                        cmd.Parameters.Add("@p_oerr", SqlDbType.SmallInt).Value= p_oerr;
                        cmd.Parameters.Add("@p_erro", SqlDbType.VarChar).Value= p_erro;
                        cmd.Parameters.Add("@p_derr", SqlDbType.VarChar).Value= p_derr;
                        cmd.Parameters.Add("@p_fech", SqlDbType.Date).Value = p_fech;
                        cmd.Parameters.Add("@p_hini", SqlDbType.VarChar).Value= p_hini;
                        cmd.Parameters.Add("@p_date", SqlDbType.VarChar).Value=  p_date;
                        cmd.Parameters.Add("@p_hfin", SqlDbType.VarChar).Value= p_hfin;
                        cmd.Parameters.Add("@p_dats", SqlDbType.VarChar).Value= p_dats;
                        cmd.Parameters.Add("@p_user", SqlDbType.VarChar).Value= p_user;
                        cmd.CommandTimeout = EN_Constante.g_const_0;
                        dr = cmd.ExecuteReader();

                        while(dr.Read())
                        {
                            if(dr["estado"] != DBNull.Value && Convert.ToInt32(dr["estado"].ToString())== EN_Constante.g_const_1)
                            {
                                return Convert.ToInt32(dr["codigo"].ToString());
                            }
                            else
                            {
                                return EN_Constante.g_const_0;
                            }


                        }

                    }
                    dr.Close();
                }
                
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
            finally
            {
                connection.Close();

            }
            return ntraLog;
            
        }
         public bool registrar_log_fin(int p_codi, int p_ferr, int p_oerr, string p_erro, string p_derr, string p_hfin,string p_dats, string p_date)
        {
            //DESCRIPCION: funcion para registrar log de fin
            AD_Conector conector = new AD_Conector(); // CONEXION
            SqlDataReader dr; //Data reader
            try
            {
                using (connection = new SqlConnection(conector.CxSQLFacturacion()))
                {
                    connection.Open();
                    using (SqlCommand Comando = new SqlCommand("Usp_registrar_log_fin_webapi", connection))
                    {
                        Comando.CommandType = CommandType.StoredProcedure;

                        Comando.Parameters.Add("@p_codi", SqlDbType.Int).Value = p_codi;
                        Comando.Parameters.Add("@p_ferr", SqlDbType.SmallInt).Value = p_ferr;
                        Comando.Parameters.Add("@p_oerr", SqlDbType.SmallInt).Value = p_oerr;
                        Comando.Parameters.Add("@p_erro", SqlDbType.Char).Value = p_erro;
                        Comando.Parameters.Add("@p_derr", SqlDbType.Char).Value = p_derr;
                        Comando.Parameters.Add("@p_hfin", SqlDbType.Char).Value = p_hfin;
                        Comando.Parameters.Add("@p_dats", SqlDbType.Char).Value = p_dats;
                        Comando.Parameters.Add("@p_date", SqlDbType.Char).Value = p_date;                        
                        Comando.CommandTimeout = EN_Constante.g_const_0;
                        dr = Comando.ExecuteReader();
                        while (dr.Read())
                        {
                            if (dr["estado"] != DBNull.Value && Convert.ToInt32(dr["estado"].ToString()) == EN_Constante.g_const_1)
                            {
                                return true;
                            }
                            else
                            {
                                return false;
                            }

                        }

                    }
                    dr.Close();
                    
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
            finally
            {
                connection.Close();
            }

        }
    }
}