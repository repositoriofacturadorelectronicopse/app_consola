﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualBasic;
using CEN;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.Collections;
using Microsoft.VisualBasic.CompilerServices;
namespace CAD
{


    public class AD_Proceso
    {
        private SqlConnection connection;   // Variable de conexion
        private readonly AD_Cado _datosConexion;
        private readonly SqlConnection _sqlConexion;
        private readonly SqlCommand _sqlCommand;
         private string TiempoEspera {get;}
        public AD_Proceso()
        {
            _datosConexion = new AD_Cado();
            _sqlConexion = new SqlConnection(_datosConexion.CadenaConexion());
            _sqlCommand = new SqlCommand();
             TiempoEspera=EN_ConfigConstantes.Instance.const_TiempoEspera;
        }
        // Lista los procesos

    
    // Lista procesos de Facturas
        public List<EN_Proceso> listarProcesosBatchFacturaNCD(EN_Proceso pProceso, EN_ComprobanteSunat pCompSunat)
        {
            try
            {
                List<EN_Proceso> olstDP = new List<EN_Proceso>();
                EN_Proceso oDP_EN;
                string sql;
                sql = "select p.id, p.empresa_id, p.tipodocumento_id, p.descripcion, p.estado, e.nrodocumento,"+ 
                "e.razonsocial from proceso p inner join empresa e on p.empresa_id=e.id where e.borrado='0' and p.borrado='0' and p.tipodocumento_id IN ('" + pCompSunat.IdFC + "','" + pCompSunat.IdNC + "', '" + pCompSunat.IdND + "' ) ";
                if ((pProceso.Empresa_id != 0))
                    sql = sql + " and p.empresa_id = @param1";
                if ((pProceso.Estado != ""))
                    sql = sql + " and p.estado = @param2";
                SqlCommand cmd = new SqlCommand(sql, _sqlConexion);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add(new SqlParameter("param1", System.Convert.ToString(pProceso.Empresa_id)));
                cmd.Parameters.Add(new SqlParameter("param2", System.Convert.ToString(pProceso.Estado)));

                cmd.CommandTimeout = Convert.ToInt32(TiempoEspera);
                _sqlConexion.Open();
                SqlDataReader Rs = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                if (Rs.HasRows)
                {
                    while (Rs.Read())
                    {
                        oDP_EN = new EN_Proceso();
                        {
                            var withBlock = oDP_EN;
                            withBlock.Id = (int)Rs["id"];
                            withBlock.Empresa_id = (int)Rs["empresa_id"];
                            withBlock.Tipodocumento_id = (string)Rs["tipodocumento_id"];
                            withBlock.Descripcion = (string)Interaction.IIf(Information.IsDBNull(Rs["descripcion"]), "", Rs["descripcion"]);
                            withBlock.Estado = (string)Rs["estado"];
                            withBlock.Nrodocumento = (string)Rs["nrodocumento"];
                            withBlock.RazonSocial =(string)Rs["razonsocial"];
                        }
                        olstDP.Add(oDP_EN);
                    }
                }
                Rs.Close();
                return olstDP;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                _sqlConexion.Close();
            }
        }

        // Lista procesos de OTROS CPE
        public List<EN_Proceso> listarProcesosBatchOtrosCpe(EN_Proceso pProceso, EN_ComprobanteSunat pCompSunat)
        {

            
            try
            {
                List<EN_Proceso> olstDP = new List<EN_Proceso>();
                EN_Proceso oDP_EN;
                string sql;
                sql = "select p.id, p.empresa_id, p.tipodocumento_id, p.descripcion, p.estado, e.nrodocumento,"+ 
                "e.razonsocial from proceso p inner join empresa e on p.empresa_id=e.id where e.borrado='0' and p.borrado='0' and p.tipodocumento_id IN ('" + pCompSunat.IdCP + "', '" + pCompSunat.IdCR + "' ) ";
                if ((pProceso.Empresa_id != 0))
                    sql = sql + " and p.empresa_id = @param1";
                if ((pProceso.Estado != ""))
                    sql = sql + " and p.estado = @param2";
                SqlCommand cmd = new SqlCommand(sql, _sqlConexion);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add(new SqlParameter("param1", System.Convert.ToString(pProceso.Empresa_id)));
                cmd.Parameters.Add(new SqlParameter("param2", System.Convert.ToString(pProceso.Estado)));

                cmd.CommandTimeout = Convert.ToInt32(TiempoEspera);
                _sqlConexion.Open();
                SqlDataReader Rs = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                if (Rs.HasRows)
                {
                    while (Rs.Read())
                    {
                        oDP_EN = new EN_Proceso();
                        {
                            var withBlock = oDP_EN;
                            withBlock.Id = (int)Rs["id"];
                            withBlock.Empresa_id = (int)Rs["empresa_id"];
                            withBlock.Tipodocumento_id = (string)Rs["tipodocumento_id"];
                            withBlock.Descripcion = (string)Interaction.IIf(Information.IsDBNull(Rs["descripcion"]), "", Rs["descripcion"]);
                            withBlock.Estado = (string)Rs["estado"];
                            withBlock.Nrodocumento = (string)Rs["nrodocumento"];
                            withBlock.RazonSocial =(string)Rs["razonsocial"];
                        }
                        olstDP.Add(oDP_EN);
                    }
                }
                Rs.Close();
                return olstDP;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                _sqlConexion.Close();
            }
        }


// Lista procesos de Resumen Diario
    public List<EN_Proceso> listarProcesosBatchRD(EN_Proceso pProceso, EN_ComprobanteSunat pCompSunat)
    {
        try
        {
            List<EN_Proceso> olstDP = new List<EN_Proceso>();
            EN_Proceso oDP_EN;
            string sql;
            sql = "select p.id, p.empresa_id, p.tipodocumento_id, p.descripcion, p.estado, e.nrodocumento, e.razonsocial "+
            "from proceso p inner join empresa e on p.empresa_id=e.id where e.borrado='0' and p.borrado='0' and p.tipodocumento_id IN ('" + pCompSunat.IdRD + "') ";
            if ((pProceso.Empresa_id != 0))
                sql = sql + " and p.empresa_id = @param1";
            if ((pProceso.Estado != ""))
                sql = sql + " and p.estado = @param2";
            SqlCommand cmd = new SqlCommand(sql, _sqlConexion);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.Add(new SqlParameter("param1", System.Convert.ToString(pProceso.Empresa_id)));
            cmd.Parameters.Add(new SqlParameter("param2", System.Convert.ToString(pProceso.Estado)));

            cmd.CommandTimeout = Convert.ToInt32(TiempoEspera);
            _sqlConexion.Open();
            SqlDataReader Rs = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            if (Rs.HasRows)
            {
                while (Rs.Read())
                {
                    oDP_EN = new EN_Proceso();
                    {
                        var withBlock = oDP_EN;
                        withBlock.Id = (int)Rs["id"];
                        withBlock.Empresa_id = (int)Rs["empresa_id"];
                        withBlock.Tipodocumento_id = (string)Rs["tipodocumento_id"];
                        withBlock.Descripcion = (string)Interaction.IIf(Information.IsDBNull(Rs["descripcion"]), "", Rs["descripcion"]);
                        withBlock.Estado = (string)Rs["estado"];
                        withBlock.Nrodocumento = (string)Rs["nrodocumento"];
                        withBlock.RazonSocial =(string)Rs["razonsocial"];
                    }
                    olstDP.Add(oDP_EN);
                }
            }
            Rs.Close();
            return olstDP;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            _sqlConexion.Close();
        }
    }


    // Lista procesos de Comunicado de Bajas
        public List<EN_Proceso> listarProcesosBatchCB(EN_Proceso pProceso, EN_ComprobanteSunat pCompSunat)
        {
            try
            {
                List<EN_Proceso> olstDP = new List<EN_Proceso>();
                EN_Proceso oDP_EN;
                string sql;
                sql = "select p.id, p.empresa_id, p.tipodocumento_id, p.descripcion, p.estado, e.nrodocumento, e.razonsocial from proceso p inner join empresa e on p.empresa_id=e.id where e.borrado='0' and p.borrado='0' and p.tipodocumento_id IN ('" + pCompSunat.IdCB + "') ";
                if ((pProceso.Empresa_id != 0))
                    sql = sql + " and p.empresa_id = @param1";
                if ((pProceso.Estado != ""))
                    sql = sql + " and p.estado = @param2";

                SqlCommand cmd = new SqlCommand(sql, _sqlConexion);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add(new SqlParameter("param1", System.Convert.ToString(pProceso.Empresa_id)));
                cmd.Parameters.Add(new SqlParameter("param2", System.Convert.ToString(pProceso.Estado)));

                cmd.CommandTimeout =    Convert.ToInt32(TiempoEspera);
                _sqlConexion.Open();
                SqlDataReader Rs = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                if (Rs.HasRows)
                {
                    while (Rs.Read())
                    {
                        oDP_EN = new EN_Proceso();
                        {
                            var withBlock = oDP_EN;
                            withBlock.Id = (int) Rs["id"];
                            withBlock.Empresa_id = (int)Rs["empresa_id"];
                            withBlock.Tipodocumento_id =(string) Rs["tipodocumento_id"];
                            withBlock.Descripcion = (string)Interaction.IIf(Information.IsDBNull(Rs["descripcion"]), "", Rs["descripcion"]);
                            withBlock.Estado = (string)Rs["estado"];
                            withBlock.Nrodocumento =(string)Rs["nrodocumento"];
                            withBlock.RazonSocial = (string)Rs["razonsocial"];
                        }
                        olstDP.Add(oDP_EN);
                    }
                }
                Rs.Close();
                return olstDP;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                _sqlConexion.Close();
            }
        }

        public int EmpresaIdEnvio()
        {
            //DESCRIPCION: ID DE EMPRESA PARA PROCESAR RESUMENES (0 PARA TODOS, ID PARA EMPRESA ESPECIFICO)

            SqlCommand cmd; // comando para certificado
            SqlDataReader Rs;   //Data Reader para certificado
            int empresaid=0; // empresa id

            try
            {
                _sqlConexion.Open();
                cmd = new SqlCommand("Usp_buscar_parametro",_sqlConexion);
                 //buscamos en tabla parámetro  flagose 
                cmd.CommandType= CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@flag",  EN_Constante.g_const_6);
                cmd.Parameters.AddWithValue("@idBusqueda1",null);
                cmd.Parameters.AddWithValue("@idBusqueda2", null );
                Rs = cmd.ExecuteReader();

                if(Rs.HasRows)
                {
                    while(Rs.Read())
                    {
                       empresaid= (int)Interaction.IIf(Information.IsDBNull(Rs["empresaid"]), 0, Rs["empresaid"]);           
                    }
                }
                Rs.Close();
                return empresaid;

            }
            catch (System.Exception ex)
            {
                
                throw ex;
            }
            finally
            {
                _sqlConexion.Close();
            }
        }
        public int NumDiasEnvio()
        {
            //DESCRIPCION: NUMERO DE DIAS PERMITIDOS PARA ENVIAR A SUNAT

            SqlCommand cmd; // comando para certificado
            SqlDataReader Rs;   //Data Reader para certificado
            int numdias=0; // numero de días para envio a sunat 

            try
            {
                _sqlConexion.Open();
                cmd = new SqlCommand("Usp_buscar_parametro",_sqlConexion);
                 //buscamos en tabla parámetro  flagose 
                cmd.CommandType= CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@flag",  EN_Constante.g_const_7);
                cmd.Parameters.AddWithValue("@idBusqueda1",null);
                cmd.Parameters.AddWithValue("@idBusqueda2", null );
                Rs = cmd.ExecuteReader();

                if(Rs.HasRows)
                {
                    while(Rs.Read())
                    {
                       numdias= (int)Interaction.IIf(Information.IsDBNull(Rs["numdias"]), 0, Rs["numdias"]);           
                    }
                }
                Rs.Close();
                return numdias;

            }
            catch (System.Exception ex)
            {
                
                throw ex;
            }
            finally
            {
                _sqlConexion.Close();
            }
        }

        public EN_Parametro buscar_tablaParametro(EN_Parametro par_busqueda)
        {
            //DESCRIPCION: NUMERO DE DIAS PERMITIDOS PARA ENVIAR A SUNAT

            SqlCommand cmd; // comando para certificado
            SqlDataReader Rs;   //Data Reader para certificado
            EN_Parametro res_par = new EN_Parametro();

            try
            {
                _sqlConexion.Open();
                cmd = new SqlCommand("Usp_buscar_parametroGeneral",_sqlConexion);
                 
    
                cmd.CommandType= CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@par_concepto",  par_busqueda.par_conceptopfij);
                cmd.Parameters.AddWithValue("@par_correlativo", par_busqueda.par_conceptocorr);
                cmd.Parameters.AddWithValue("@p_par_descripcion", par_busqueda.par_descripcion);
                cmd.Parameters.AddWithValue("@p_par_descripcion2",  par_busqueda.par_descripcion2 );
                cmd.Parameters.AddWithValue("@p_par_int1",  par_busqueda.par_int1 );
                cmd.Parameters.AddWithValue("@p_par_int2", par_busqueda.par_int2 );
                cmd.Parameters.AddWithValue("@p_par_tipo", par_busqueda.par_tipo );
                Rs = cmd.ExecuteReader();

                if(Rs.HasRows)
                {
                    while(Rs.Read())
                    {
                       res_par.par_descripcion= (string)Interaction.IIf(Information.IsDBNull(Rs["par_descripcion"]), "", Rs["par_descripcion"]);           
                       res_par.par_descripcion2= (string)Interaction.IIf(Information.IsDBNull(Rs["par_descripcion2"]), "", Rs["par_descripcion2"]);           
                       res_par.par_int1= (int)Interaction.IIf(Information.IsDBNull(Rs["par_int1"]), 0, Rs["par_int1"]);           
                       res_par.par_int2= (int)Interaction.IIf(Information.IsDBNull(Rs["par_int2"]), 0, Rs["par_int2"]);   
                       res_par.par_float1= (decimal)Interaction.IIf(Information.IsDBNull(Rs["par_float1"]),(decimal)0, Rs["par_float1"]);   
                       res_par.par_float2= (decimal)Interaction.IIf(Information.IsDBNull(Rs["par_float2"]), (decimal)0, Rs["par_float2"]);   
                       res_par.par_date1= (string)Interaction.IIf(Information.IsDBNull(Rs["par_date1"]), "", Rs["par_date1"].ToString());   
                       res_par.par_date2= (string)Interaction.IIf(Information.IsDBNull(Rs["par_date1"]), "", Rs["par_date2"].ToString()); 
                       res_par.par_tipo= (int)Interaction.IIf(Information.IsDBNull(Rs["par_tipo"]), 0, Rs["par_tipo"]);   
                    }
                }
                Rs.Close();
                return res_par;

            }
            catch (System.Exception ex)
            {
                
                throw ex;
            }
            finally
            {
                _sqlConexion.Close();
            }
        }

          public List<EN_Proceso> listarProcesosActivos(int empresa_id, string tipo_docs, string estado)
        {
            //DESCRIPCION: LISTA DE PROCESOS ACTIVOS 

            SqlCommand cmd;                                     // comando para certificado
            SqlDataReader Rs;                                   //Data Reader para certificado
            List<EN_Proceso> olstDP = new List<EN_Proceso>();   // Lista de clase entidad proceso
            EN_Proceso oDP_EN;                                  // Clase entidad proceso

            try
            {
                _sqlConexion.Open();
                cmd = new SqlCommand("Usp_listar_procesosActivos",_sqlConexion);
              
                cmd.CommandType= CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@empresa_id",empresa_id);
                cmd.Parameters.AddWithValue("@tipodocs", tipo_docs );
                cmd.Parameters.AddWithValue("@estado", estado );
                Rs = cmd.ExecuteReader();


                if(Rs.HasRows)
                {
                    while(Rs.Read())
                    {
                      oDP_EN = new EN_Proceso();
                        {
                            var withBlock = oDP_EN;
                            withBlock.Id = (int) Rs["id"];
                            withBlock.Empresa_id = (int)Rs["empresa_id"];
                            withBlock.Tipodocumento_id =(string) Rs["tipodocumento_id"];
                            withBlock.Descripcion = (string)Interaction.IIf(Information.IsDBNull(Rs["descripcion"]), "", Rs["descripcion"]);
                            withBlock.Estado = (string)Rs["estado"];
                            withBlock.Nrodocumento =(string)Rs["nrodocumento"];
                            withBlock.RazonSocial = (string)Rs["razonsocial"];

                            
                        }
                        olstDP.Add(oDP_EN);         
                    }
                }
                Rs.Close();
                return olstDP;

            }
            catch (System.Exception ex)
            {
                
                throw ex;
            }
            finally
            {
                _sqlConexion.Close();
            }
        }


         public EN_RespuestaListaProceso ListarProcesosBatchGuia(EN_Proceso pProceso, EN_ComprobanteSunat pCompSunat)
        {
            EN_RespuestaListaProceso respuesta = new EN_RespuestaListaProceso();
            List<EN_Proceso> olstDP = new List<EN_Proceso>();
            AD_Conector conector = new AD_Conector();
            SqlDataReader dr = null;
             string sql = string.Empty;
            try
            {
                sql = "select p.id, p.empresa_id, p.tipodocumento_id, p.descripcion, p.estado, e.nrodocumento,"+ 
                        "e.razonsocial from proceso p inner join empresa e on p.empresa_id=e.id where e.borrado='0' and p.borrado='0' and p.tipodocumento_id IN ('" + pCompSunat.IdGR + "' ) ";
                
                if ((pProceso.Empresa_id != 0))
                        sql = sql + " and p.empresa_id = @param1";
                if ((pProceso.Estado != ""))
                     sql = sql + " and p.estado = @param2";


            using (connection = new SqlConnection(conector.CxSQLFacturacion()))
            {
                connection.Open();
                using (SqlCommand Command = new SqlCommand(sql, connection)) 
                {
                    Command.CommandType = CommandType.Text;
                    Command.Parameters.Add("param1", SqlDbType.VarChar,10).Value = pProceso.Empresa_id;
                    Command.Parameters.Add("param2", SqlDbType.Char,1).Value = pProceso.Estado;
                    Command.CommandTimeout = EN_Constante.g_const_0;
                    dr = Command.ExecuteReader();
                    if (dr.HasRows) {
                        while(dr.Read()) 
                        {
                            EN_Proceso oDP_EN = new EN_Proceso();
                             if(dr["id"] != DBNull.Value )
                             oDP_EN.Id = Convert.ToInt32(dr["id"].ToString());

                              if(dr["empresa_id"] != DBNull.Value )
                             oDP_EN.Empresa_id = Convert.ToInt32(dr["empresa_id"].ToString());

                              if (dr["tipodocumento_id"] != DBNull.Value)
                             oDP_EN.Tipodocumento_id = dr["tipodocumento_id"].ToString();

                             if (dr["descripcion"] != DBNull.Value)
                             oDP_EN.Descripcion = dr["descripcion"].ToString();

                             if (dr["estado"] != DBNull.Value)
                             oDP_EN.Estado = dr["estado"].ToString();

                             if (dr["nrodocumento"] != DBNull.Value)
                             oDP_EN.Nrodocumento = dr["nrodocumento"].ToString();

                              if (dr["razonsocial"] != DBNull.Value)
                             oDP_EN.RazonSocial = dr["razonsocial"].ToString();

                             respuesta.listProceso.Add(oDP_EN);
                            
                        }
                      if (respuesta.listProceso.Count > EN_Constante.g_const_0)
                        {
                            respuesta.ResplistaGuia.FlagVerificacion = true;
                            respuesta.ResplistaGuia.dato = EN_Constante.g_const_2000;
                            respuesta.ResplistaGuia.DescRespuesta = EN_Constante.g_const_consultaexitosa;
                        } else {
                            respuesta.ResplistaGuia.FlagVerificacion = false;
                            respuesta.ResplistaGuia.dato = EN_Constante.g_const_1000;
                            respuesta.ResplistaGuia.DescRespuesta = EN_Constante.g_const_NotFound;

                        }
                    }

                }
                dr.Close();
             }
            return respuesta;
            }
            catch (Exception ex)
            {
                respuesta.ResplistaGuia.FlagVerificacion = false;
                respuesta.ResplistaGuia.DescRespuesta = ex.Message;;
                return respuesta;
            }
            finally
            {
                connection.Close();
            }
        }
         public EN_RespuestaData EmpresaIdEnvioGuia() 
         {
            EN_RespuestaData respuesta = new EN_RespuestaData();
            AD_Conector conector = new AD_Conector();
            EN_ErrorWebService errorservice = new EN_ErrorWebService();
            SqlDataReader dr = null;
            try
            {
                using (connection = new SqlConnection(conector.CxSQLFacturacion()))
                {
                    connection.Open();
                    using (SqlCommand Command = new SqlCommand("Usp_buscar_parametro", connection)) 
                    {
                        Command.CommandType = CommandType.StoredProcedure;
                        Command.Parameters.Add("@flag", SqlDbType.Int).Value = EN_Constante.g_const_6;
                        Command.CommandTimeout = EN_Constante.g_const_0;
                        dr = Command.ExecuteReader();
                        while(dr.Read()) 
                        {
                            if(dr["empresaid"] != null && dr["empresaid"].ToString() != "")
                            respuesta.ResplistaGuia.dato = Convert.ToInt32(dr["empresaid"].ToString());                       
                        }
                        if (respuesta.ResplistaGuia.dato > 0) {
                            respuesta.ResplistaGuia.FlagVerificacion = true;
                            respuesta.ResplistaGuia.DescRespuesta = EN_Constante.g_const_consultaexitosa;
                            errorservice.TipoError = EN_Constante.g_const_0;
                            errorservice.CodigoError = EN_Constante.g_const_2000;
                            errorservice.DescripcionErr = EN_Constante.g_const_consultaexitosa;
                            respuesta.errorService = errorservice;

                        } else {
                            respuesta.ResplistaGuia.FlagVerificacion = false;
                            respuesta.ResplistaGuia.DescRespuesta = EN_Constante.g_const_numDias;
                            errorservice.TipoError = EN_Constante.g_const_1;
                            errorservice.CodigoError = EN_Constante.g_const_2000;
                            errorservice.DescripcionErr = EN_Constante.g_const_error_interno;
                            respuesta.errorService = errorservice;

                        }

                    }
                    dr.Close();
                }
                 return respuesta;
            }
            catch (Exception ex)
            {        
                respuesta.ResplistaGuia.FlagVerificacion = false;
                respuesta.ResplistaGuia.DescRespuesta = EN_Constante.g_const_error_interno;
                errorservice.TipoError = EN_Constante.g_const_1;
                errorservice.CodigoError = EN_Constante.g_const_3000;
                errorservice.DescripcionErr = ex.Message;
                respuesta.errorService = errorservice;
                return respuesta;
            }
            finally
            {
                connection.Close();
            }

         }
        public EN_RespuestaData NumDiasEnvioGuia() 
        {
            EN_RespuestaData respuesta = new EN_RespuestaData();
            AD_Conector conector = new AD_Conector();
            EN_ErrorWebService errorservice = new EN_ErrorWebService();
            SqlDataReader dr = null;
            try
            {
                using (connection = new SqlConnection(conector.CxSQLFacturacion()))
                {
                    connection.Open();
                    using (SqlCommand Command = new SqlCommand("Usp_buscar_parametro", connection)) 
                    {
                        Command.CommandType = CommandType.StoredProcedure;
                        Command.Parameters.Add("@flag", SqlDbType.Int).Value = EN_Constante.g_const_7;
                        Command.CommandTimeout = EN_Constante.g_const_0;
                        dr = Command.ExecuteReader();
                        while(dr.Read()) 
                        {
                            if(dr["numdias"] != null)
                            respuesta.ResplistaGuia.dato = Convert.ToInt32(dr["numdias"].ToString());                       
                        }
                        if (respuesta.ResplistaGuia.dato > 0) {
                            respuesta.ResplistaGuia.FlagVerificacion = true;
                            respuesta.ResplistaGuia.DescRespuesta = EN_Constante.g_const_consultaexitosa;
                            errorservice.TipoError = EN_Constante.g_const_0;
                            errorservice.CodigoError = EN_Constante.g_const_2000;
                            errorservice.DescripcionErr = EN_Constante.g_const_consultaexitosa;
                            respuesta.errorService = errorservice;

                        } else {
                            respuesta.ResplistaGuia.FlagVerificacion = false;
                            respuesta.ResplistaGuia.DescRespuesta = EN_Constante.g_const_numDias;
                            errorservice.TipoError = EN_Constante.g_const_1;
                            errorservice.CodigoError = EN_Constante.g_const_2000;
                            errorservice.DescripcionErr = EN_Constante.g_const_error_interno;
                            respuesta.errorService = errorservice;

                        }

                    }
                    dr.Close();
                }
                 return respuesta;
            }
            catch (Exception ex)
            {        
                respuesta.ResplistaGuia.FlagVerificacion = false;
                respuesta.ResplistaGuia.DescRespuesta = EN_Constante.g_const_error_interno;
                errorservice.TipoError = EN_Constante.g_const_1;
                errorservice.CodigoError = EN_Constante.g_const_3000;
                errorservice.DescripcionErr = ex.Message;
                respuesta.errorService = errorservice;
                return respuesta;
            }
            finally
            {
                connection.Close();
            }


        }
        public EN_RespuestaParametro buscar_tablaParametroGuia(EN_Parametro par_busqueda) 
        {
            EN_RespuestaParametro respuesta = new EN_RespuestaParametro();
            AD_Conector conector = new AD_Conector();
            EN_ErrorWebService errorservice = new EN_ErrorWebService();
            SqlDataReader dr = null;
            int contparam = EN_Constante.g_const_0;
            try
            {
                using (connection = new SqlConnection(conector.CxSQLFacturacion()))
                {
                    connection.Open();
                    using (SqlCommand Command = new SqlCommand("Usp_buscar_parametroGeneral", connection)) 
                    {
                        Command.CommandType = CommandType.StoredProcedure;
                        Command.Parameters.Add("@par_concepto", SqlDbType.Int).Value = par_busqueda.par_conceptopfij;
                        Command.Parameters.Add("@par_correlativo", SqlDbType.Int).Value = par_busqueda.par_conceptocorr;
                        Command.Parameters.Add("@p_par_descripcion", SqlDbType.VarChar,250).Value = par_busqueda.par_descripcion;
                        Command.Parameters.Add("@p_par_descripcion2", SqlDbType.VarChar,250).Value = par_busqueda.par_descripcion2;
                        Command.Parameters.Add("@p_par_int1", SqlDbType.Int).Value = par_busqueda.par_int1;
                        Command.Parameters.Add("@p_par_int2", SqlDbType.Int).Value = par_busqueda.par_int2;
                        Command.Parameters.Add("@p_par_tipo", SqlDbType.Int).Value = par_busqueda.par_tipo;
                        Command.CommandTimeout = EN_Constante.g_const_0;
                        dr = Command.ExecuteReader();
                        while(dr.Read()) 
                        {
                            EN_Parametro  parametro = new EN_Parametro();
                             if(dr["par_descripcion"] != null)
                            parametro.par_descripcion= dr["par_descripcion"].ToString();   

                            if(dr["par_descripcion2"] != null)
                            parametro.par_descripcion2= dr["par_descripcion2"].ToString();  

                            if(dr["par_int1"] != null)
                            {
                                if (dr["par_int1"].ToString() != "") 
                                {
                                    parametro.par_int1= Convert.ToInt32(dr["par_int1"].ToString());
                                }
                            }  

                            if(dr["par_int2"] != null)
                            {
                              if (dr["par_int2"].ToString() != "") 
                              {
                                  parametro.par_int2= Convert.ToInt32(dr["par_int2"].ToString());
                              }  
                            }                           

                            if(dr["par_float1"] != null)
                            {
                                if (dr["par_float1"].ToString() != "") 
                                {
                                    parametro.par_float1= Convert.ToDecimal(dr["par_float1"].ToString());   
                                }
                            }
                            
                            if(dr["par_float2"] != null)
                            {
                                if (dr["par_float2"].ToString() != "") 
                                {
                                    parametro.par_float2= Convert.ToDecimal(dr["par_float2"].ToString()); 
                                }
                            }               

                            if(dr["par_date1"] != null)
                            parametro.par_date1= dr["par_date1"].ToString();  

                            if(dr["par_date2"] != null)
                            parametro.par_date2= dr["par_date2"].ToString();  

                            if(dr["par_tipo"] != null)
                            {
                                if (dr["par_tipo"].ToString() != "") 
                                {
                                    parametro.par_int2= Convert.ToInt32(dr["par_tipo"].ToString()); 
                                }
                            }
                            respuesta.objParametro = parametro;
                            contparam = contparam +1;                          
                        }
                        if (contparam > EN_Constante.g_const_0) {
                            respuesta.RespRegistro.FlagVerificacion = true;
                            respuesta.RespRegistro.DescRespuesta = EN_Constante.g_const_consultaexitosa;
                            errorservice.TipoError = EN_Constante.g_const_0;
                            errorservice.CodigoError = EN_Constante.g_const_2000;
                            errorservice.DescripcionErr = EN_Constante.g_const_consultaexitosa;
                            respuesta.errorService = errorservice;


                        } else {
                            respuesta.RespRegistro.FlagVerificacion = false;
                            respuesta.RespRegistro.DescRespuesta = EN_Constante.g_const_paramNoEnc;
                            errorservice.TipoError = EN_Constante.g_const_1;
                            errorservice.CodigoError = EN_Constante.g_const_3000;
                            errorservice.DescripcionErr = EN_Constante.g_const_error_interno;
                            respuesta.errorService = errorservice;

                        }

                        dr.Close();
                    }
                }
               return   respuesta;
            }
            catch (Exception ex)
            {
                
                respuesta.RespRegistro.FlagVerificacion = false;
                respuesta.RespRegistro.DescRespuesta = EN_Constante.g_const_error_interno;
                errorservice.TipoError = EN_Constante.g_const_1;
                errorservice.CodigoError = EN_Constante.g_const_3000;
                errorservice.DescripcionErr = ex.Message;
                respuesta.errorService = errorservice;
                return respuesta;
            }
            finally
            {
                connection.Close();
            }

       }
       public EN_RespuestaListaProceso listarProcesosActivosGuia(int empresa_id, string tipo_doc)
       {
           EN_RespuestaListaProceso respuesta = new EN_RespuestaListaProceso();
           AD_Conector conector = new AD_Conector();
            EN_ErrorWebService errorservice = new EN_ErrorWebService();
            SqlDataReader dr = null;
            try
            {
                using (connection = new SqlConnection(conector.CxSQLFacturacion()))
                {
                    connection.Open();
                    using (SqlCommand Command = new SqlCommand("Usp_listar_procesosActivos_guia", connection)) 
                    {
                        Command.CommandType = CommandType.StoredProcedure;
                        Command.Parameters.Add("@empresa_id", SqlDbType.Int).Value = empresa_id;
                        Command.Parameters.Add("@tipodoc", SqlDbType.VarChar,10).Value = tipo_doc;
                        Command.CommandTimeout = EN_Constante.g_const_0;
                        dr = Command.ExecuteReader();
                        while(dr.Read())
                        {
                             EN_Proceso oDP_EN = new EN_Proceso();
                            if(dr["id"] != null && dr["id"].ToString() != "" )
                            oDP_EN.Id= Convert.ToInt32(dr["id"].ToString()); 

                            if(dr["empresa_id"] != null && dr["empresa_id"].ToString() != "" )
                            oDP_EN.Empresa_id= Convert.ToInt32(dr["empresa_id"].ToString()); 

                            if(dr["tipodocumento_id"] != null )
                            oDP_EN.Tipodocumento_id = dr["tipodocumento_id"].ToString();

                             if(dr["descripcion"] != null )
                             oDP_EN.Descripcion = dr["descripcion"].ToString();

                             if(dr["estado"] != null )
                             oDP_EN.Estado = dr["estado"].ToString();

                              if(dr["nrodocumento"] != null )
                             oDP_EN.Nrodocumento = dr["nrodocumento"].ToString();

                              if(dr["razonsocial"] != null )
                             oDP_EN.RazonSocial = dr["razonsocial"].ToString();

                            respuesta.listProceso.Add(oDP_EN);

                        }
                        if (respuesta.listProceso.Count > EN_Constante.g_const_0) 
                        {
                            respuesta.ResplistaGuia.FlagVerificacion = true;
                            respuesta.ResplistaGuia.DescRespuesta = EN_Constante.g_const_consultaexitosa;                   
                        } else {
                            respuesta.ResplistaGuia.FlagVerificacion = false;
                            respuesta.ResplistaGuia.DescRespuesta = EN_Constante.g_const_error_interno;

                        }
                    }
                    dr.Close();
                }
               return respuesta;
            }
            catch (Exception ex)
            {
                respuesta.ResplistaGuia.FlagVerificacion = false;
                respuesta.ResplistaGuia.DescRespuesta = ex.Message;
                return respuesta;
            }
            finally
            {
                connection.Close();
            }

       }



        
        
        

       
    }

}

