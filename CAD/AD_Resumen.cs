﻿/****************************************************************************************************************************************************************************************
 PROGRAMA: AD_Resumen.cs
 VERSION : 1.0
 OBJETIVO: Clase de acceso a datos de resumen
 FECHA   : 21/06/2021
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/

using System;
using System.Collections.Generic;
using Microsoft.VisualBasic;
using CEN;
using System.Data.SqlClient;
using System.Data;
using Microsoft.VisualBasic.CompilerServices;
using System.Globalization;

namespace CAD
{
    public class AD_Resumen
    {
        private readonly AD_Cado _datosConexion;
        private readonly SqlConnection _sqlConexion;
        private readonly SqlCommand _sqlCommand;
        private string TiempoEspera {get;}

        public AD_Resumen()
        {
            _datosConexion = new AD_Cado();
            _sqlConexion = new SqlConnection(_datosConexion.CadenaConexion());
            _sqlCommand = new SqlCommand();
            TiempoEspera=EN_ConfigConstantes.Instance.const_TiempoEspera;
        }
        public List<EN_Resumen> listarResumen(EN_Resumen pResumen)
        {
            //DESCRIPCION: FUNCIÓN PARA LISTAR RESUMEN

            SqlCommand cmd;                                             // comando para certificado
            SqlDataReader Rs;                                           //Data Reader para certificado
            List<EN_Resumen> olstResumen = new List<EN_Resumen>();      // LISTA DE CLASE ENTIDAD RESUMEN
            EN_Resumen oResumen_EN;                                     //CLASE ENTIDAD RESUMEN

            try
            {
                

                _sqlConexion.Open();
                cmd = new SqlCommand("Usp_listar_Resumen",_sqlConexion);
                cmd.CommandType= CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Id",          pResumen.Id.ToString());
                cmd.Parameters.AddWithValue("@Idempresa",   pResumen.Empresa_id.ToString());
                cmd.Parameters.AddWithValue("@Estado",      pResumen.Estado.ToString() );
                cmd.Parameters.AddWithValue("@Situacion",   pResumen.Situacion.ToString() );
                cmd.Parameters.AddWithValue("@Correlativo", pResumen.Correlativo.ToString() );
                cmd.Parameters.AddWithValue("@FechaIni",    (pResumen.FechaIni==EN_Constante.g_const_vacio)?EN_Constante.g_const_vacio:Strings.Format(Conversions.ToDate(pResumen.FechaIni), "yyyy-MM-dd") );
                cmd.Parameters.AddWithValue("@FechaFin",    (pResumen.FechaFin==EN_Constante.g_const_vacio)?EN_Constante.g_const_vacio:Strings.Format(Conversions.ToDate(pResumen.FechaFin), "yyyy-MM-dd") );
                Rs = cmd.ExecuteReader();

    
                if (Rs.HasRows)
                {
                    while (Rs.Read())
                    {
                        oResumen_EN = new EN_Resumen();
                        {
                            var withBlock = oResumen_EN;
                            withBlock.Id = (int)Rs["id"];
                            withBlock.Empresa_id = (int)Rs["empresa_id"];
                            withBlock.Nrodocumento = (string)Rs["nrodocumento"];
                            withBlock.DescripcionEmpresa = (string)Rs["razonsocial"];
                            withBlock.Tiporesumen = (string)Rs["tiporesumen"];
                            withBlock.Correlativo = (int)Rs["correlativo"];
                            withBlock.FechaEmisionDocumento = Rs["fecemision_documento"].ToString();
                            withBlock.FechaGeneraResumen = Rs["fecgenera_resumen"].ToString();
                            withBlock.IdentificaResumen = (string)Interaction.IIf(Information.IsDBNull(Rs["identifica_resumen"]), "", Rs["identifica_resumen"]);
                            withBlock.Estado = (string)Rs["estado"];
                            withBlock.Situacion = (string)Rs["situacion"];
                            withBlock.EnviadoMail = (string)Interaction.IIf(Information.IsDBNull(Rs["enviado_email"]), "NO", Rs["enviado_email"]);
                            withBlock.nombreXML = (string)Interaction.IIf(Information.IsDBNull(Rs["nombrexml"]), "", Rs["nombrexml"]);
                            withBlock.vResumen = (string)Interaction.IIf(Information.IsDBNull(Rs["valorresumen"]), "", Rs["valorresumen"]);
                            withBlock.vFirma = (string)Interaction.IIf(Information.IsDBNull(Rs["valorfirma"]), "", Rs["valorfirma"]);
                            withBlock.XMLEnviado = (string)Interaction.IIf(Information.IsDBNull(Rs["xmlgenerado"]), "", Rs["xmlgenerado"]);
                            withBlock.estado_resumen = (string)Interaction.IIf(Information.IsDBNull(Rs["estado_resumen"]), "1", Rs["estado_resumen"]);
                        }
                        olstResumen.Add(oResumen_EN);
                    }
                }
                Rs.Close();
            
                return olstResumen;
            }
            catch (Exception ex)
            {
            
                throw ex;
            }
            finally
            {
                _sqlConexion.Close();
            }
        }
        public List<EN_DetalleResumen> listarDetalleResumen(EN_Resumen pResumen)
        {
            
            SqlCommand cmd;                                             // comando para certificado
            SqlDataReader Rs;                                           //Data Reader para certificado
            List<EN_DetalleResumen> olstDetalleResumen = new List<EN_DetalleResumen>();      // LISTA DE CLASE ENTIDAD RESUMEN
            EN_DetalleResumen oDetalleResumen_EN;                                     //CLASE ENTIDAD RESUMEN

            try
            {
            
                _sqlConexion.Open();
                cmd             = new SqlCommand("Usp_listar_detalleResumen",_sqlConexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Id", pResumen.Id.ToString());
            
                Rs = cmd.ExecuteReader();

                if (Rs.HasRows)
                {
                    while (Rs.Read())
                    {
                        oDetalleResumen_EN = new EN_DetalleResumen();
                        {
                            var withBlock = oDetalleResumen_EN;
                            withBlock.IdResumen = (int)Rs["resumen_id"];
                            withBlock.Item = (int)Rs["item"];
                            withBlock.Tipodocumentoid = (string)Rs["tipodocumento_id"];
                            withBlock.DescTipodocumento = (string)Rs["descripcion"];
                            withBlock.Serie = (string)Rs["serie"];
                            withBlock.Correlativo = (string)Rs["correlativo"];
                            withBlock.DocCliente = (string)Rs["doccliente"];
                            withBlock.NroDocCliente = (string)Rs["nrodoccliente"];
                            withBlock.Condicion = (int)Rs["condicion"];
                            withBlock.Moneda = (string)Rs["moneda"];
                            withBlock.Opegravadas = (decimal)Interaction.IIf(Information.IsDBNull(Rs["opegravadas"]), 0, Rs["opegravadas"]);
                            withBlock.Opeexoneradas = (decimal)Interaction.IIf(Information.IsDBNull(Rs["opeexoneradas"]), 0, Rs["opeexoneradas"]);
                            withBlock.Opeinafectas = (decimal)Interaction.IIf(Information.IsDBNull(Rs["opeinafectas"]), 0, Rs["opeinafectas"]);
                            withBlock.Opegratuitas = (decimal)Interaction.IIf(Information.IsDBNull(Rs["opegratuitas"]), 0, Rs["opegratuitas"]);
                            withBlock.Otroscargos = (decimal)Interaction.IIf(Information.IsDBNull(Rs["otroscargos"]), 0, Rs["otroscargos"]);
                            withBlock.TotalISC = (decimal)Interaction.IIf(Information.IsDBNull(Rs["totalISC"]), 0, Rs["totalISC"]);
                            withBlock.TotalIGV = (decimal)Interaction.IIf(Information.IsDBNull(Rs["totalIGV"]), 0, Rs["totalIGV"]);
                            withBlock.Otrostributos = (decimal)Interaction.IIf(Information.IsDBNull(Rs["otrostributos"]), 0, Rs["otrostributos"]);
                            withBlock.Importeventa = (decimal)Rs["importeventa"];
                            withBlock.Tipodocref = (string)Interaction.IIf(Information.IsDBNull(Rs["tipodocref"]), "", Rs["tipodocref"]);
                            withBlock.Nrodocref = (string)Interaction.IIf(Information.IsDBNull(Rs["nrodocref"]), "", Rs["nrodocref"]);
                            withBlock.Regimenpercep = (string)Interaction.IIf(Information.IsDBNull(Rs["regimenpercep"]), "", Rs["regimenpercep"]);
                            withBlock.Porcentpercep = (decimal)Interaction.IIf(Information.IsDBNull(Rs["porcentpercep"]), 0, Rs["porcentpercep"]);
                            withBlock.Importepercep = (decimal)Interaction.IIf(Information.IsDBNull(Rs["importepercep"]), 0, Rs["importepercep"]);
                            withBlock.Importefinal = (decimal)Rs["importefinal"];
                        }
                        olstDetalleResumen.Add(oDetalleResumen_EN);
                    }
                }
                Rs.Close();
                
                return olstDetalleResumen;
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
            finally
            {
                _sqlConexion.Close();
            }
        }
        public bool GuardarResumenNombreValorResumenFirma(EN_Resumen oResumen, string nombreXML, string valorResumen, string valorFirma, string xmlgenerado)
        {
            
            try
            {
                _sqlConexion.Open();
                _sqlCommand.CommandType = CommandType.StoredProcedure;
                _sqlCommand.Connection = _sqlConexion;
                _sqlCommand.CommandText = "actualizar_resumenxml";
                _sqlCommand.Parameters.AddWithValue("@empresa_id", oResumen.Empresa_id);
                _sqlCommand.Parameters.AddWithValue("@resumen_id", oResumen.Id);
                _sqlCommand.Parameters.AddWithValue("@nombrexml", nombreXML);
                _sqlCommand.Parameters.AddWithValue("@valorresumen", valorResumen);
                _sqlCommand.Parameters.AddWithValue("@valorfirma", valorFirma);
                _sqlCommand.Parameters.AddWithValue("@xmlgenerado", xmlgenerado);

            
                _sqlCommand.ExecuteNonQuery();
                
                return true;
            }
            catch (Exception ex)
            {
            
                throw ex;
            }
            finally
            {
                _sqlConexion.Close();
            }
        }

        public List<EN_Documento> listarDocumentosBajaResumen(string idDoc, int idEmpresa, int idPuntoVenta)
        {
                // DESCRIPCION: FUNCION DEVUELVE LISTA DE DOCUMENTO  DE RESUMEN DE BAJA

            string opeafec = EN_ConfigConstantes.Instance.const_TipoAfectOnerosas;      // TIPO DE AFECTACION ONEROSA
            string opeinafec = EN_ConfigConstantes.Instance.const_TipoInafectOnerosas;  // TIPO INAFECTO ONEROSA
            string operetir = EN_ConfigConstantes.Instance.const_TipoRetiroGratuito;    // TIPO RETIRO GRATUITO
            string opexoner = EN_ConfigConstantes.Instance.const_TipoExonerado ;        // TIPO EXONERADO
            string opexport = EN_ConfigConstantes.Instance.const_TipoExportacion;       // TIPO EXPORTACION

            SqlCommand cmd;                 // COMANDO SQL
            SqlDataReader Rs;              //Data Reader para certificado
            
            try
            {
                List<EN_Documento> olstDocumento = new List<EN_Documento>();
                EN_Documento oDocumento_EN;

                    _sqlConexion.Open();
                cmd = new SqlCommand("Usp_listar_documentosBajaResumen",_sqlConexion);
                cmd.CommandType= CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Idempresa", idEmpresa);
                cmd.Parameters.AddWithValue("@Idtipodocumento",idPuntoVenta);
                cmd.Parameters.AddWithValue("@IdDoc", idDoc);
                cmd.Parameters.AddWithValue("@opeafec", opeafec);
                cmd.Parameters.AddWithValue("@opeinafec", opeinafec);
                cmd.Parameters.AddWithValue("@operetir",operetir);
                cmd.Parameters.AddWithValue("@opexoner", opexoner);
                cmd.Parameters.AddWithValue("@opexport", opexport);

                Rs = cmd.ExecuteReader();
        
                if (Rs.HasRows)
                {
                    while (Rs.Read())
                    {
                        oDocumento_EN = new EN_Documento();
                        {
                            var withBlock = oDocumento_EN;
                            withBlock.Idempresa = (int)Rs["empresa_id"];
                            withBlock.Idpuntoventa = (int) Rs["puntoventa_id"];
                            withBlock.Id = (string)Rs["id"];
                            withBlock.Idtipodocumento = (string)Rs["tipodocumento_id"];
                            withBlock.Serie = (string)Rs["serie_comprobante"];
                            withBlock.Numero = (string)Rs["nro_comprobante"];
                            withBlock.Tipodoccliente = (string)Interaction.IIf(Information.IsDBNull(Rs["cliente_tipodoc"]), "", Rs["cliente_tipodoc"]);
                            withBlock.Nrodoccliente = (string)Interaction.IIf(Information.IsDBNull(Rs["cliente_nrodoc"]), "", Rs["cliente_nrodoc"]);
                            withBlock.Fecha = Rs["fecha"].ToString();
                            withBlock.Moneda = (string)Rs["moneda"];
                            withBlock.Valorpergravadas = (double)Interaction.IIf(Information.IsDBNull(Rs["valopergravadas"]), 0, Rs["valopergravadas"]);
                            withBlock.Valorperinafectas = (double)Interaction.IIf(Information.IsDBNull(Rs["valoperinafectas"]), 0, Rs["valoperinafectas"]);
                            withBlock.Valorperexoneradas = (double)Interaction.IIf(Information.IsDBNull(Rs["valoperexoneradas"]), 0, Rs["valoperexoneradas"]);
                            withBlock.Valorpergratuitas = (double)Interaction.IIf(Information.IsDBNull(Rs["valopergratuitas"]), 0, Rs["valopergratuitas"]);
                            withBlock.Valorperiscreferenc = (double)Interaction.IIf(Information.IsDBNull(Rs["valoperiscreferenc"]), 0, Rs["valoperiscreferenc"]);
                            withBlock.Valorperexportacion = (double)Interaction.IIf(Information.IsDBNull(Rs["valoperexportacion"]), 0, Rs["valoperexportacion"]);
                            withBlock.OtrosCargos = (double)Interaction.IIf(Information.IsDBNull(Rs["otroscargos"]), 0, Rs["otroscargos"]);
                            withBlock.Iscventa = (double)Interaction.IIf(Information.IsDBNull(Rs["iscventa"]), 0, Rs["iscventa"]);
                            withBlock.Igvventa = (double)Interaction.IIf(Information.IsDBNull(Rs["igvventa"]), 0, Rs["igvventa"]);
                            withBlock.OtrosTributos = (double)Interaction.IIf(Information.IsDBNull(Rs["otrostributos"]), 0, Rs["otrostributos"]);
                            withBlock.Importeventa =(double) Rs["importeventa"];
                            withBlock.Regimenpercep = (string)Interaction.IIf(Information.IsDBNull(Rs["regimenpercep"]), "", Rs["regimenpercep"]);
                            withBlock.Porcentpercep = (double)Interaction.IIf(Information.IsDBNull(Rs["porcentpercep"]), 0, Rs["porcentpercep"]);
                            withBlock.Importepercep = (double)Interaction.IIf(Information.IsDBNull(Rs["importepercep"]), 0, Rs["importepercep"]);
                            withBlock.Importefinal = (double)Rs["importefinal"];
                            withBlock.Estado = (string)Rs["estado"];
                            withBlock.Situacion = (string)Rs["situacion"];
                        }
                        olstDocumento.Add(oDocumento_EN);
                    }
                }
                Rs.Close();
                
                return olstDocumento;
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
            finally
            {
                _sqlConexion.Close();
            }
        }


        ﻿public int GuardarResumenSunat(ResumenSunat resumenSunat)
        {
            int idresumen = 0;
            SqlTransaction transaction = null/* TODO Change to default(_) if this is not a reference type */;
            SqlCommand detCommand = new SqlCommand();
            SqlCommand docrefCommand = new SqlCommand();
          
            string idDocRes="";
            try
            {
                _sqlConexion.Open();
                transaction = _sqlConexion.BeginTransaction();

                _sqlCommand.CommandType = CommandType.StoredProcedure;
                _sqlCommand.Transaction = transaction;
                _sqlCommand.Connection = _sqlConexion;
                _sqlCommand.CommandText = "Usp_registrar_resumen";
                _sqlCommand.Transaction = transaction;

                detCommand.CommandType = CommandType.StoredProcedure;
                detCommand.Transaction = transaction;
                detCommand.Connection = _sqlConexion;
                detCommand.CommandText = "Usp_registrar_deta_resumen";
                detCommand.Transaction = transaction;

                _sqlCommand.Parameters.AddWithValue("@empresa_id", resumenSunat.Empresa_id);
                _sqlCommand.Parameters.AddWithValue("@tiporesumen", resumenSunat.Tiporesumen);
                _sqlCommand.Parameters.AddWithValue("@fecemision_documento", Convert.ToDateTime(resumenSunat.FechaEmisionDocumento.ToString(), new CultureInfo(EN_ConfigConstantes.Instance.const_CultureInfoPeru)).ToString(EN_Constante.g_const_formfechaGuion));
                _sqlCommand.Parameters.AddWithValue("@fecgenera_resumen", Convert.ToDateTime(resumenSunat.FechaGeneraResumen.ToString(), new CultureInfo(EN_ConfigConstantes.Instance.const_CultureInfoPeru)).ToString(EN_Constante.g_const_formfechaGuion));
                _sqlCommand.Parameters.AddWithValue("@estado", resumenSunat.Estado);
                _sqlCommand.Parameters.AddWithValue("@situacion", resumenSunat.Situacion);
                _sqlCommand.Parameters.AddWithValue("@usureg", resumenSunat.UsuarioSession);
                _sqlCommand.Parameters.AddWithValue("@estadoresumen", resumenSunat.EstadoResumen);
                _sqlCommand.Parameters.Add("@idresumen", SqlDbType.Int).Direction = ParameterDirection.Output;
                _sqlCommand.Parameters.Add("@idDocRes", SqlDbType.VarChar, 50).Direction = ParameterDirection.Output;
                _sqlCommand.ExecuteNonQuery();

                idresumen = (int)_sqlCommand.Parameters["@idresumen"].Value;
                idDocRes = (string)_sqlCommand.Parameters["@idDocRes"].Value;


                int i = 1;
                foreach (EN_DetalleResumen detalle in resumenSunat.ListaDetalleResumen)
                {
                    detCommand.Parameters.AddWithValue("@resumen_id", idresumen);
                    detCommand.Parameters.AddWithValue("@item", i);
                    detCommand.Parameters.AddWithValue("@tipodocumento_id", detalle.Tipodocumentoid);
                    detCommand.Parameters.AddWithValue("@serie", detalle.Serie);
                    detCommand.Parameters.AddWithValue("@correlativo", detalle.Correlativo);
                    detCommand.Parameters.AddWithValue("@doccliente", detalle.DocCliente);
                    detCommand.Parameters.AddWithValue("@nrodoccliente", detalle.NroDocCliente);
                    detCommand.Parameters.AddWithValue("@condicion", resumenSunat.EstadoResumen);
                    detCommand.Parameters.AddWithValue("@moneda", detalle.Moneda);
                    detCommand.Parameters.AddWithValue("@opegravadas", detalle.Opegravadas);
                    detCommand.Parameters.AddWithValue("@opeexoneradas", detalle.Opeexoneradas);
                    detCommand.Parameters.AddWithValue("@opeinafectas", detalle.Opeinafectas);
                    detCommand.Parameters.AddWithValue("@opegratuitas", detalle.Opegratuitas);
                    detCommand.Parameters.AddWithValue("@otroscargos", detalle.Otroscargos);
                    detCommand.Parameters.AddWithValue("@totalISC", detalle.TotalISC);
                    detCommand.Parameters.AddWithValue("@totalIGV", detalle.TotalIGV);
                    detCommand.Parameters.AddWithValue("@otrostributos", detalle.Otrostributos);
                    detCommand.Parameters.AddWithValue("@importeventa", detalle.Importeventa);
                    detCommand.Parameters.AddWithValue("@tipodocref", Interaction.IIf(Information.IsNothing(detalle.Tipodocref), DBNull.Value, detalle.Tipodocref));
                    detCommand.Parameters.AddWithValue("@nrodocref", Interaction.IIf(Information.IsNothing(detalle.Nrodocref), DBNull.Value, detalle.Nrodocref));
                    detCommand.Parameters.AddWithValue("@regimenpercep", Interaction.IIf(Information.IsNothing(detalle.Regimenpercep), DBNull.Value, detalle.Regimenpercep));
                    detCommand.Parameters.AddWithValue("@porcentpercep", detalle.Porcentpercep);
                    detCommand.Parameters.AddWithValue("@importepercep", detalle.Importepercep);
                    detCommand.Parameters.AddWithValue("@importefinal", detalle.Importefinal);
                    detCommand.ExecuteNonQuery();
                    detCommand.Parameters.Clear();
                    i = i + 1;
                }
                transaction.Commit();

                return idresumen;
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                throw ex;
            }
        }


        public void ActualizarEstadoResumenBajaPend(int idResBaja, string estado)
        {
            
            try
            {
                _sqlConexion.Open();
                _sqlCommand.CommandType = CommandType.StoredProcedure;
                _sqlCommand.Connection = _sqlConexion;
                _sqlCommand.CommandText = "Usp_actualizar_estadoResumenBajaPend";
                _sqlCommand.Parameters.AddWithValue("@id", idResBaja);
                _sqlCommand.Parameters.AddWithValue("@estado", estado);
            
                _sqlCommand.ExecuteNonQuery();
                
            }
            catch (Exception ex)
            {
            
                throw ex;
            }
            finally
            {
                _sqlConexion.Close();
            }
        }

    
    
     
    }
}