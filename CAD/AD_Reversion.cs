﻿using System;
using System.Collections.Generic;
using Microsoft.VisualBasic;
using log4net;
using CEN;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Collections;
using System.Data;
using Microsoft.VisualBasic.CompilerServices;

namespace CAD
{
    public class AD_Reversion
    {
       private readonly AD_Cado _datosConexion;
    private readonly SqlConnection _sqlConexion;
    private readonly SqlCommand _sqlCommand;
    private string TiempoEspera {get;}

    public AD_Reversion()
    {
        _datosConexion = new AD_Cado();
        _sqlConexion = new SqlConnection(_datosConexion.CadenaConexion());
        _sqlCommand = new SqlCommand();
         TiempoEspera=EN_ConfigConstantes.Instance.const_TiempoEspera;
    }
    
    public List<EN_RequestEnvioResDiario>  listarDocumentosReversion(EN_Empresa oEmpresa,EN_Certificado oCertificado,int empresa_id, string fecha_comunicacion, string estado, string situacion)
    {
        // DESCRIPCION: FUNCION LISTAR DOCUMENTOS DE REVERSION

        EN_RequestEnvioResDiario reqRes;                                                    // Clase entidad Envio Resumen Diario
        List<EN_RequestEnvioResDiario> ListreqRes = new List<EN_RequestEnvioResDiario>();   // Lista de Clase entidad Envio Resumen Diario
        SqlDataReader Rs;                                                                   // DATA READER
        try
        {
            _sqlConexion.Open();
            _sqlCommand.CommandType = CommandType.StoredProcedure;
            _sqlCommand.Connection = _sqlConexion;
            _sqlCommand.CommandText = "Usp_listar_reversion";
            _sqlCommand.Parameters.AddWithValue("@empresa_id", empresa_id);
            _sqlCommand.Parameters.AddWithValue("@fecha_comunicacion", fecha_comunicacion);
            _sqlCommand.Parameters.AddWithValue("@estado", estado);
            _sqlCommand.Parameters.AddWithValue("@situacion", situacion);
            
            

            Rs = _sqlCommand.ExecuteReader();
              if (Rs.HasRows)
                    {
                        while (Rs.Read())
                        {
                            // id,empresa_id, tiporesumen, correlativo, fecgenera_resumen, nombrexml, situacion
                            reqRes = new EN_RequestEnvioResDiario();
                            reqRes.flagOse = Conversions.ToString(oCertificado.flagOSE);
                            reqRes.Produccion = Conversions.ToString(oEmpresa.Produccion);
                            reqRes.certUserName = Conversions.ToString(oCertificado.UserName);
                            reqRes.certClave= Conversions.ToString(oCertificado.Clave);
                            reqRes.ruc = Conversions.ToString(oEmpresa.Nrodocumento);
                            reqRes.nombreArchivo = Conversions.ToString(Rs["nombrexml"]);
                            reqRes.resumenId= Conversions.ToString(Rs["id"]);
                            reqRes.empresaId= Conversions.ToString(Rs["empresa_id"]);
                            reqRes.Idpuntoventa = Conversions.ToString(Rs["puntoventa_id"]);
                            reqRes.tiporesumenId = Conversions.ToString(Rs["tiporesumen"]);
                            
                            reqRes.FechaGeneraResumen= Conversions.ToString(Rs["fecha"]);
                            reqRes.FechaComunicacion= Conversions.ToString(Rs["fecha"]);
                            
                            reqRes.correlativo= Conversions.ToString(Rs["correlativo"]);
                                                
                            ListreqRes.Add(reqRes);
                        }
                           
                    }

                    Rs.Close();
                    return ListreqRes;
        }
        catch (Exception ex)
        {
           
            throw ex;
        }
        finally
        {
            _sqlConexion.Close();
        }
    }
  
    
     
    }
}