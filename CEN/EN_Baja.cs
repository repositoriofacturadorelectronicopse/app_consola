/****************************************************************************************************************************************************************************************
 PROGRAMA: EN_Baja.cs
 VERSION : 1.0
 OBJETIVO: Clase de entidad de baja
 FECHA   : 19/07/2021
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/
using System.Collections.Generic;

namespace CEN
{
    public class EN_Baja
    {
       // Para Validar Usuario Certificado Empresa
        public string Usuario { get; set; }                 // Usuario de certificado
        public string Clave { get; set; }                   // Clave de certificado
        // Datos de baja
        public int Id { get; set; }                         // Id de documento de baja
        public int Empresa_id { get; set; }                 // Id de empresa
        public int Idpuntoventa { get; set; }               // Id punto de venta
        public string Nrodocumento { get; set; }            // Número de documento de baja
        public string DescripcionEmpresa { get; set; }      // Description de empresa
        public string Tiporesumen { get; set; }             // Tipo Resumen
        public int Correlativo { get; set; }                // Correlativo
        public string FechaDocumento { get; set; }          // Fecha documento
        public string FechaComunicacion { get; set; }       // Fecha comunicado de baja
        public string Identificacomunicacion { get; set; }  // Identificación de comunicación
        public string Estado { get; set; }                  // Estado de baja
        public string Situacion { get; set; }               // Situación de baja
        public string EnviadoMail { get; set; }             // Envio de correo
        // Para Busquedas
        public string FechaIni { get; set; }                // Fecha de inicio
        public string FechaFin { get; set; }                // Fecha fin
        public string MensajeCDR { get; set; }              // Mensaje CDR
        public string FechaEnvio { get; set; }              // Fecha de envío
        // Para Errores Rechazos
        public string MensajeError { get; set; }            // Mensaje de error
        public string FechaError { get; set; }              // Fecha de error
        // Informacion Auditoria
        public string UsuarioSession { get; set; }          // Usuario session
        public string CadenaAleatoria { get; set; }         // Cadena Aleatoria
        // Para reportes
        public string Cantidad { get; set; }                // Cantidad
        // Ultimos Parametros Agregados
        public string nombreXML { get; set; }               // Nombre XML
        public string XMLEnviado { get; set; }              // XML Enviado
        public string CDRXML { get; set; }                  // CDR xml
        public string vResumen { get; set; }                // Valor de Resumen
        public string vFirma { get; set; }                  // Valor de firma
        public string ticket { get; set; }                  // ticket
        
    }


    public partial class BajaSunat : EN_Baja
{
    public BajaSunat()
    {
        ListaDetalleBaja = new List<EN_DetalleBaja>();
    }

        public string ValorResumen { get; set; }                    // Valor resumen
        public string ValorFirma { get; set; }                      // Valor de firma
        public string NombreFichero { get; set; }                   // Nombre Fichero
        public List<EN_DetalleBaja> ListaDetalleBaja { get; set; }     // Lista detalle baja
    }
}