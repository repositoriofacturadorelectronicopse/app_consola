namespace CEN
{
  ﻿public class EN_ComprobanteSunat
{
    private string _IdBV;
    public string IdBV
    {
        get
        {
            return _IdBV;
        }
        set
        {
            _IdBV = value;
        }
    }

    private string _IdFC;
    public string IdFC
    {
        get
        {
            return _IdFC;
        }
        set
        {
            _IdFC = value;
        }
    }

    private string _IdNC;
    public string IdNC
    {
        get
        {
            return _IdNC;
        }
        set
        {
            _IdNC = value;
        }
    }

    private string _IdND;
    public string IdND
    {
        get
        {
            return _IdND;
        }
        set
        {
            _IdND = value;
        }
    }

    private string _IdCB;
    public string IdCB
    {
        get
        {
            return _IdCB;
        }
        set
        {
            _IdCB = value;
        }
    }

    private string _IdRR;
    public string IdRR
    {
        get
        {
            return _IdRR;
        }
        set
        {
            _IdRR = value;
        }
    }

    private string _IdRD;
    public string IdRD
    {
        get
        {
            return _IdRD;
        }
        set
        {
            _IdRD = value;
        }
    }

    private string _IdGR;
    public string IdGR
    {
        get
        {
            return _IdGR;
        }
        set
        {
            _IdGR = value;
        }
    }

    private string _IdCP;
    public string IdCP
    {
        get
        {
            return _IdCP;
        }
        set
        {
            _IdCP = value;
        }
    }

    private string _IdCR;
    public string IdCR
    {
        get
        {
            return _IdCR;
        }
        set
        {
            _IdCR = value;
        }
    }

    private string _IdTrPub;
    public string IdTrPub
    {
        get
        {
            return _IdTrPub;
        }
        set
        {
            _IdTrPub = value;
        }
    }

    private string _IdTrPriv;
    public string IdTrPriv
    {
        get
        {
            return _IdTrPriv;
        }
        set
        {
            _IdTrPriv = value;
        }
    }

    private string _codigoEtiquetaErrorDoc;
    public string codigoEtiquetaErrorDoc
    {
        get
        {
            return _codigoEtiquetaErrorDoc;
        }
        set
        {
            _codigoEtiquetaErrorDoc = value;
        }
    }

    private string _codigoEtiquetaErrorRes;
    public string codigoEtiquetaErrorRes
    {
        get
        {
            return _codigoEtiquetaErrorRes;
        }
        set
        {
            _codigoEtiquetaErrorRes = value;
        }
    }

    private string _codigoEtiquetaErrorCpe;
    public string codigoEtiquetaErrorCpe
    {
        get
        {
            return _codigoEtiquetaErrorCpe;
        }
        set
        {
            _codigoEtiquetaErrorCpe = value;
        }
    }

    private string _codigoEtiquetaErrorGui;
    public string codigoEtiquetaErrorGui
    {
        get
        {
            return _codigoEtiquetaErrorGui;
        }
        set
        {
            _codigoEtiquetaErrorGui = value;
        }
    }
}
}