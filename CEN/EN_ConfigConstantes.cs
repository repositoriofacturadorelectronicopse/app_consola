/****************************************************************************************************************************************************************************************
 PROGRAMA: EN_ConfigConstantes.cs
 VERSION : 1.0
 OBJETIVO: Clase de entidad configuración de constantes
 FECHA   : 21/06/2021
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/
namespace CEN
{
    public class EN_ConfigConstantes
    {
        private EN_ConfigConstantes(){}
        public static readonly EN_ConfigConstantes Instance = new EN_ConfigConstantes();

        public string const_cadenaCnxBdFE {get; set;} // ccadena conexión Bd Facturación Electrónica
        public string const_CultureInfoPeru {get; set;} //Culture Info Perú
        public string const_TiempoEspera  {get; set;} // tiempo de espera
        public string const_flagEnvioDirecto {get; set;} // Flag de envio directo
        public string const_flagEnvioCorreo {get; set;} // Flag de envio Correo
        public string const_flagRutaServ {get; set;}    // Flag de Ruta Servidor
        public string const_rutaWSDocs {get; set;}  // ruta WS Documento
        public string const_rutaApDirectorio {get; set;} // ruta del aplicativo
        public string const_RutaFisica {get; set;}  // ruta física
        public string const_RutaCarpetaDocs {get; set;} // ruta de carpeta de documento
        public string const_versionUBL {get; set;} //version UBL
        public string const_codigoEtiquetaErrorDoc {get; set;}  //código de etiqueta de error de documento
        public string const_ResumenNumDias {get; set;}  // número de días para envio de resumen

        public string const_TipoResumenBaja {get; set;} // tiempo de resumen de baja
        public string const_TipoResumenDiario {get; set;}   // tiempo de resumen diario
        public string const_TipoResumenReversion {get; set;}    // tiempo de resumen reversión

        // para grabar llamadas de tablas
        public string const_TipoAfectOnerosas {get;set;}    // tipo de afectación onerosa
        public string const_TipoInafectOnerosas {get;set;}  // tipo de inafecto onerosa
        public string const_TipoRetiroGratuito {get;set;}   // tipo de retiro gratuito
        public string const_TipoExonerado  {get;set;}   // tipo exonerado
        public string const_TipoExportacion{get;set;}   // tipo exonerado

        public string const_IdFC {get;set;}// TipoFactura
        public string const_IdBV {get;set;}  //TipoBoletaVenta
        public string const_IdNC {get;set;} // TipoNotaCredito
        public string const_IdND {get;set;}// TipoNotaDebito
        public string  const_IdGR {get;set;} // TipoGuiaRemision
        public string const_IdCB {get;set;} // TipoResumenBaja
        public string const_IdRD {get;set;}// TipoResumenDiario
        public string const_IdRR {get;set;}// TipoResumenReversion
        public string const_IdCP {get;set;}// TipoComprobantePercepcion
        public string const_IdCR {get;set;}// TipoComprobanteRetencion

        // constantes para endpoints
        public string const_BetaSunatDoc {get;set;}     //Endpoint Beta de envio de documento a sunat 
        public string const_BetaSunatOtrosCpe {get;set;}    //Endpoint Beta de envio de otros comprobantes a sunat 
        public string const_BetaSunatGuias  {get;set;}       //Endpoint Beta de envio de guía a sunat 
        public string const_ProdSunatDoc {get;set;}  //Endpoint Producción de envio documento a sunat 
        public string const_ProdSunatOtrosCpe {get;set;}    //Endpoint Producción de envio de otros comprobantes a sunat 
        public string const_ProdSunatGuias  {get;set;}      //Endpoint Producción de envio de guía a sunat 
        public string const_consultaSunatService {get;set;} //Endpoint servicio de consulta a sunat
            public string const_BetaNubefact {get;set;}     //Endpoint Beta Nubefact
        public string const_ProdNubefact {get;set;}     //Endpoint Producción Nubefact
        public string const_BetaEfac {get;set;}      //Endpoint Beta Efact
        public string const_ProdEfac {get;set;}      //Endpoint Producción Efact
        public string const_ConsultaSunatDoc {get;set;} // Endpoint ConsultaSunatDoc
         public string const_codigoEtiquetaErrorRes {get;set;}      // etiqueta de error de resumen


         public string const_urlServiceDoc {get; set;}      // url de web api para envio de Doc
        public string const_urlServiceOtroscpe {get; set;}      // url de web api para envio de otros Doc
         
         public string const_apiSendDoc {get; set;}     // api de web api para envio de Doc
         public string const_apiSendOtroscpe {get; set;}     // api de web api para envio de Otros Doc
         public string const_apiSendReversion {get; set;}     // api de web api para envio de reversión
         public string const_apiSendResumenDiario {get; set;}     // api de web api para envio de resumen diario
         public string const_apiSendComunicadoBaja {get; set;}     // api de web api para envio de comunicado de baja
         
        public string const_s3_accessKey {get;set;}         //ACCESS KEY ID DE CONEXION A S3
        public string const_s3_secretKey {get;set;}         //SECRET KEY DE CONEXION A S3
        public string const_s3_region {get;set;}            //REGION DE CONEXION A S3
        public string const_s3_bucketName {get;set;}        //NOMBRE DE BUCKET DE CONEXION A S3
        public string const_rutaTemporalPse_archivos {get;set;}        //NOMBRE DE BUCKET DE CONEXION A S3
        

        
        
    }
}