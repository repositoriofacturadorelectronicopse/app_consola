/****************************************************************************************************************************************************************************************
 PROGRAMA: EN_Constante.cs
 VERSION : 1.0
 OBJETIVO: Clase de entidad  constantes
 FECHA   : 21/06/2021
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/
namespace CEN
{
    public class EN_Constante
    {

        public const int g_const_menos1 = -1;   // Constante -1
        public const int g_const_0 = 0; // Constante 0
        public const int g_const_1 = 1; // Constante 1
         public const int g_const_2 = 2; // Constante 2
         public const int g_const_3 = 3; // Constante 3
         public const int g_const_4 = 4; // Constante 4
         public const int g_const_5 = 5; // Constante 5 
         public const int g_const_6 = 6; // Constante 6
         public const int g_const_7 = 7; // Constante 7 
         public const int g_const_8 = 8; // Constante 8
         public const int g_const_9 = 9; // Constante 9 
         public const int g_const_10 = 10; // Constante 10
         public const int g_const_11 = 11; // Constante 11 
         public const int g_const_22 = 22; // Constante 22
         public const int g_const_23 = 23; // Constante 23 
         public const int g_const_24 = 24; // Constante 24
         public const int g_const_25 = 25; // Constante 25 
         public const int g_const_110 = 110; // Constante 110
         public const int g_const_111 = 111; // Constante 111
         public const int g_const_112 = 112; // Constante 112
         public const int g_const_113 = 113; // Constante 113
         public const int g_const_114 = 114; // Constante 114
         public const int g_const_116 = 116; // Constante 116
         public const int g_const_117 = 117; // Constante 117

         public const int g_const_150 = 150; // Constante 150
         public const int g_const_151 = 151; // Constante 151
         public const int g_const_152 = 152; // Constante 152
         public const int g_const_153 = 153; // Constante 153
         public const int g_const_154 = 154; // Constante 154
         public const int g_const_155 = 155; // Constante 155
         public const int g_const_156 = 156; // Constante 156
         public const int g_const_157 = 157; // Constante 157
         public const int g_const_158 = 158; // Constante 158
         
         public const int g_const_2004 = 2004; // Constante 2004
         public const int g_const_2005 = 2005; // Constante 2005
        public const int g_const_2006 = 2006; // Constante 2006
        public const int g_const_2007 = 2007; // Constante 2007
        public const int g_const_2008 = 2008; // Constante 2008
        public const int g_const_2009 = 2009; // Constante 2009
         public const int g_const_4002 = 4002; // Constante 4002

        public const string g_const_null = null; // Constante null
        public const string g_const_vacio = ""; // Constante vacio
        public const string g_const_formfech = "dd/MM/yyyy"; // formato de fecha
        public const string g_const_formfechaGuion = "yyyy-MM-dd"; // formato de fecha con guión
        public const string g_const_formhora = "HH:mm:ss"; // formato de hora
        public const string g_const_rango_num = "[0-9]"; // Rango número

        public const int g_const_prefijo_cpe_doc = 2; // Prefijo de comprobante electronico doc
        public const int g_const_1000 = 1000; // Constante 1000
        public const int g_const_1001 = 1001; // Constante 1001
        public const int g_const_1003 = 1003; // Constante 1003
        public const int g_const_1005 = 1005; // Constante 1005
        public const int g_const_2000 = 2000; // Constante 2000
        public const int g_const_2001 = 2001; // Constante 2001
        public const int g_const_3000 = 3000; // Constante 3000
        public const int g_const_4000 = 4000; // Constante 4000
        public const int g_const_1020 = 1020; // Constante 1020
        
        public const int g_const_1021 = 1021; // Constante 1021

        public const string g_const_estado_proceso= "A"; // estado de proceso
        public const string g_const_situacion_pendiente= "P"; // situación pendiente
        public const string g_const_situacion_error= "E"; // situación error
        public const string g_const_estado_nuevo= "N"; // estado nuevo
        public const string g_const_inicial_boleta= "B"; // inicial de serie Boleta
        public const string g_const_usuario_session= "Sistema"; // usuario session
        public const string g_const_estadoResumen_baja= "3"; // ESTADO DE RESUMEN PARA DAR DE BAJA
        public const string g_const_estadoNo= "NO"; // ESTADO NO
        public const string g_const_estadoSi= "SI"; // ESTADO SI


         public const string g_const_rutaSufijo_xml = "XML/"; // ruta sufijo para xml
        public const string g_const_rutaSufijo_pdf = "PDF/"; // ruta sufijo para pdf
        public const string g_const_rutaSufijo_cdr = "CDR/"; // ruta sufijo para cdr
        public const string g_const_extension_xml = ".XML"; //  extensión para xml
        public const string g_const_extension_zip = ".ZIP"; //  extensión para zip
        public const string g_const_extension_pdf = ".PDF"; //  extensión para pdf
        public const string g_const_prefijoXmlCdr_respuesta = "R-"; // prefijo para xml y cdr respuesta


        public const string g_const_cultureEsp="es-PE"; //constante culture español
        public const string g_cost_nameTimeDefect= "America/Bogota"; // Constante timeStandar
        public const int g_const_length_numero_cpe_doc = 8; // Conteo de caracteres campo numero de serie de documento electrónico
        public const int g_const_length_id_cpe_doc = 15; // Conteo de caracteres campo ID de documento electrónico


        public const string g_const_valExito = "Operación Exitosa"; // Constante de éxito para registro log fin
        public const string g_const_errlogCons = "Error al ejecutar método "; // Constante de error para registro log fin
        
        

        //errores
          public const string g_const_error_interno = "Hubo un problema interno, por favor vuelva a intentar en unos momentos."; // Error interno general
          public const string g_const_err_idcpedoc = "POR FAVOR VERIFIQUE EL ID EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de ID de documento electrónico
          public const string g_const_err_usuario_cpedoc = "POR FAVOR VERIFIQUE EL USUARIO SOAP DE SUNAT EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de USUARIO de documento electrónico
          public const string g_const_err_pass_cpedoc = "POR FAVOR VERIFIQUE EL PASSWORD SOAP DE SUNAT EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de USUARIO de documento electrónico
          public const string g_const_err_idempresa_cpedoc = "POR FAVOR VERIFIQUE EL ID DE EMPRESA EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de IDE EMPRESA de documento electrónico
          public const string g_const_err_idtipodoc_cpedoc = "POR FAVOR VERIFIQUE EL ID DE TIPO DE DOCUMENTO EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de IDE EMPRESA de documento electrónico
          public const string g_const_err_serie_cpedoc = "POR FAVOR VERIFIQUE LA SERIE EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de SERIE de documento electrónico
          public const string g_const_err_numero_cpedoc = "POR FAVOR VERIFIQUE EL NUMERO EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de NUMERO de documento electrónico
          public const string g_const_err_idpntvnt_cpedoc = "POR FAVOR VERIFIQUE EL ID DE PUNTO DE VENTA EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de IDE DE PUNTO DE VENTA de documento electrónico
          public const string g_const_err_tipoop_cpedoc = "POR FAVOR VERIFIQUE EL TIPO DE OPERACION EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de TIPO DE OPERACION de documento electrónico
          public const string g_const_err_tipodoccl_cpedoc = "POR FAVOR VERIFIQUE EL TIPO DE DOCUMENTO DEL CLIENTE EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de TIPO DE DOCUMENTO DEL CLIENTE de documento electrónico
          public const string g_const_err_nrodoccl_cpedoc = "POR FAVOR VERIFIQUE EL NUMERO DE DOCUMENTO DEL CLIENTE EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de NUMERO DE DOCUMENTO DEL CLIENTE de documento electrónico
          public const string g_const_err_nombrecl_cpedoc = "POR FAVOR VERIFIQUE EL NOMBRE DEL CLIENTE EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de NOMBRE DEL CLIENTE de documento electrónico
          public const string g_const_err_fecha_cpedoc = "POR FAVOR VERIFIQUE LA FECHA EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de FECHA de documento electrónico
          public const string g_const_err_hora_cpedoc = "POR FAVOR VERIFIQUE LA HORA EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de FECHA de documento electrónico
          public const string g_const_err_moneda_cpedoc = "POR FAVOR VERIFIQUE LA MONEDA EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de MONEDA de documento electrónico
          public const string g_const_err_igvventa_cpedoc = "POR FAVOR VERIFIQUE EL IGV DE VENTA EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de IGV DE VENTA de documento electrónico
          public const string g_const_err_importeventa_cpedoc = "POR FAVOR VERIFIQUE EL IMPORTE DE VENTA EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de IMPORTE DE VENTA de documento electrónico
          public const string g_const_err_importefinal_cpedoc = "POR FAVOR VERIFIQUE EL IMPORTE FINAL EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de IMPORTE FINAL de documento electrónico
          public const string g_const_err_estado_cpedoc = "POR FAVOR VERIFIQUE EL ESTADO EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de ESTADO de documento electrónico
          public const string g_const_err_situacion_cpedoc = "POR FAVOR VERIFIQUE SITUACIÓN EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de SITUACIÓN de documento electrónico
          public const string g_const_err_estransgratuita_cpedoc = "POR FAVOR VERIFIQUE SI ES TRANSACCIÓN GRATUITA EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de TRANSACCIÓN GRATUITA de documento electrónico
          public const string g_const_err_formapago_cpedoc = "POR FAVOR VERIFIQUE LA FORMA DE PAGO EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de FORMA DE PAGO de documento electrónico
          public const string g_const_err_idddcpedoc = "POR FAVOR VERIFIQUE EL ID DE DETALLE DOCUMENTO EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de ID DE DETALLE DOCUMENTO  de documento electrónico
          public const string g_const_err_idempresadd_cpedoc = "POR FAVOR VERIFIQUE EL ID DE EMPRESA DE DETALLE DOCUMENTO  EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de IDE EMPRESA  DE DETALLE DOCUMENTO de documento electrónico
          public const string g_const_err_idtipodocdd_cpedoc = "POR FAVOR VERIFIQUE EL ID DE TIPO DE DOCUMENTO DE DETALLE DOCUMENTO EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de TIPO DE DOCUMENTO DE DETALLE DOCUMENTO de documento electrónico
          public const string g_const_err_lineiddd_cpedoc = "POR FAVOR VERIFIQUE LINEID DE DETALLE DOCUMENTO EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de LINEID DE DETALLE DOCUMENTO de documento electrónico
          public const string g_const_err_productodd_cpedoc = "POR FAVOR VERIFIQUE EL PRODUCTO DE DETALLE DOCUMENTO EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de PRODUCTO DE DETALLE  DOCUMENTO de documento electrónico
          public const string g_const_err_afectoigvdd_cpedoc = "POR FAVOR VERIFIQUE EL AFECTO IGV DE DETALLE DOCUMENTO EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de AFECTO IGV DE DETALLE  DOCUMENTO de documento electrónico
          public const string g_const_err_afectoiscdd_cpedoc = "POR FAVOR VERIFIQUE EL AFECTO ISC DE DETALLE DOCUMENTO EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de AFECTO ISC DE DETALLE  DOCUMENTO de documento electrónico
          public const string g_const_err_afectacionigvdd_cpedoc = "POR FAVOR VERIFIQUE LA AFECTACION IGV DE DETALLE DOCUMENTO EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de AFECTACION IGV DE DETALLE  DOCUMENTO de documento electrónico
          public const string g_const_err_unidaddd_cpedoc = "POR FAVOR VERIFIQUE LA UNIDAD DE DETALLE DOCUMENTO EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de UNIDAD DE DETALLE  DOCUMENTO de documento electrónico
          public const string g_const_err_cantidaddd_cpedoc = "POR FAVOR VERIFIQUE LA CANTIDAD DE DETALLE DOCUMENTO EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de CANTIDAD DE DETALLE  DOCUMENTO de documento electrónico
          public const string g_const_err_preciounitariodd_cpedoc = "POR FAVOR VERIFIQUE EL PRECIO UNITARIO DE DETALLE DOCUMENTO EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de PRECIO UNITARIO DE DETALLE  DOCUMENTO de documento electrónico
          public const string g_const_err_preciorefdd_cpedoc = "POR FAVOR VERIFIQUE EL PRECIO REFERENCIAL DE DETALLE DOCUMENTO EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de PRECIO REFERENCIAL DE DETALLE  DOCUMENTO de documento electrónico
          public const string g_const_err_valorunitariodd_cpedoc = "POR FAVOR VERIFIQUE EL VALOR UNITARIO DE DETALLE DOCUMENTO EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de VALOR UNITARIO DE DETALLE  DOCUMENTO de documento electrónico
          public const string g_const_err_valorbrutodd_cpedoc = "POR FAVOR VERIFIQUE EL VALOR BRUTO DE DETALLE DOCUMENTO EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de VALOR BRUTO DE DETALLE  DOCUMENTO de documento electrónico
          public const string g_const_err_valorventadd_cpedoc = "POR FAVOR VERIFIQUE EL VALOR VENTA DE DETALLE DOCUMENTO EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de VALOR VENTA DE DETALLE  DOCUMENTO de documento electrónico
          public const string g_const_err_igvdd_cpedoc = "POR FAVOR VERIFIQUE EL IGV DE DETALLE DOCUMENTO EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de IGV DE DETALLE  DOCUMENTO de documento electrónico
          public const string g_const_err_totaldd_cpedoc = "POR FAVOR VERIFIQUE EL TOTAL DE DETALLE DOCUMENTO EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de TOTAL DE DETALLE  DOCUMENTO de documento electrónico
          public const string g_const_err_factorigvdd_cpedoc = "POR FAVOR VERIFIQUE EL FACTOR IGV DE DETALLE DOCUMENTO EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de FACTOR IGV DE DETALLE  DOCUMENTO de documento electrónico
          public const string g_const_err_valordscdd_cpedoc = "POR FAVOR VERIFIQUE EL VALOR DESCUENTO DE DETALLE DOCUMENTO EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de VALOR DESCUENTO DE DETALLE  DOCUMENTO de documento electrónico
          public const string g_const_err_valorcargodd_cpedoc = "POR FAVOR VERIFIQUE EL VALOR CARGO DE DETALLE DOCUMENTO EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de VALOR CARGO DE DETALLE  DOCUMENTO de documento electrónico
          public const string g_const_err_iscdd_cpedoc = "POR FAVOR VERIFIQUE EL ISC DE DETALLE DOCUMENTO EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de ISC DE DETALLE  DOCUMENTO de documento electrónico
          public const string g_const_err_factoriscdd_cpedoc = "POR FAVOR VERIFIQUE EL FACTOR ISC DE DETALLE DOCUMENTO EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de FACTOR ISC DE DETALLE  DOCUMENTO de documento electrónico

        public const string g_const_programa = "app_consola" ; // Nombre del Web Api
        public const string g_const_url = "CONSOLE CRON BAJA" ; // url de servicio
        
        public const string g_const_user_log= "SYS" ; // usuario de log
        public const string g_const_funcion_generarCpe= "BatchComunicadoBaja.CronBaja.EnviarComunicadoBajas" ; // nombre método
        
         
        public const string g_const_url_res = "CONSOLE CRON RESUMEN" ; // url de servicio
        public const string g_const_funcion_generarRes= "BatchResumen.Resumen.CrearResumenBVPendientes" ; // nombre método
        public const string g_const_funcion_crearResBajasPend= "BatchResumen.Resumen.CrearResumenBajasPendientes" ; // nombre método
        public const string g_const_funcion_generarResProc= "BatchResumen.Resumen.ProcesaResumenSunat" ; // nombre método
        public const string g_const_url_fac = "CONSOLE CRON DOCUMENTOS PENDIENTES ENVIO SUNAT " ; // url de servicio
        public const string g_const_funcion_generarFac= "BatchFactura.CronFactura.EnnviarFacturasPendientes" ; // nombre método
        public const string g_const_funcion_otrosCpe= "BatchFactura.CronFactura.EnnviarOtrosCpePendientes" ; // nombre método
        public const string g_const_funcion_reversionCpe= "BatchFactura.CronFactura.EnnviarReversionPendiente" ; // nombre método
        public const string g_const_url_reversion = "CONSOLE CRON REVERSION" ; // url de servicio

        public const string g_const_consultaexitosa = "CONSULTA EXITOSA";
        
        public const string g_const_numDias = "Numero de dias enviado a SUNAT, no encontrado";
        public const string g_const_paramNoEnc = "Parametro no encontrado";
        public const string g_const_empreNoEnc = "Empresa no encontrada";

        public const string g_const_cnxexitosa = "Conexión Exitosa con Servicio WebApi_Guia"; // COnexion exitosa con el servicio de web api guia
        public const string g_const_funcion_generarGuia= "BatchGuia.Program.EnnviarGuiasPendientes" ; // nombre método
         public const string g_const_url_Guia = "CONSOLE CRON GUIA " ; // url de servicio

         public const string g_const_batch_guia_exito = "BATCH ENVIO DE GUIA EXITOSO";
         public const string g_const_batch_guia_error = "BATCH ENVIO DE GUIA CON ERROR";

         public const string g_const_funcion_EnvioSunat= "EnviarGuiasPendientes" ; // nombre método
         
         public const string g_const_NotFound = "Lista de Proceso no encontrado"; 

         public const string g_const_sufijoRutaTemporalPSE = "/PSEtmp"; //sufijo de ruta temporal

      

    }
}