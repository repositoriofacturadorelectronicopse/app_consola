/****************************************************************************************************************************************************************************************
 PROGRAMA: EN_DetalleBaja.cs
 VERSION : 1.0
 OBJETIVO: Clase de entidad detalle baja
 FECHA   : 19/07/2021
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/
namespace CEN
{
    public class EN_DetalleBaja
    {
   

        public int IdBaja { get; set; }                 // Id baja
        public int Item { get; set; }                   // item
        public string Tipodocumentoid { get; set; }     // Id tipo de documento
        public string DescTipodocumento { get; set; }   // Descripción de documento
        public string Serie { get; set; }               // Serie de documento
        public string Numero { get; set; }              // Número de documento
        public string MotivoBaja { get; set; }          // Motivo de baja
     
    }
}