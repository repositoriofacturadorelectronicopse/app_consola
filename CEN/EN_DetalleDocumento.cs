namespace CEN
{
  public class EN_DetalleDocumento
{
    private int _Idempresa;
    public int Idempresa
    {
        get
        {
            return _Idempresa;
        }
        set
        {
            _Idempresa = value;
        }
    }

    private string _Iddocumento;
    public string Iddocumento
    {
        get
        {
            return _Iddocumento;
        }
        set
        {
            _Iddocumento = value;
        }
    }

    private string _Idtipodocumento;
    public string Idtipodocumento
    {
        get
        {
            return _Idtipodocumento;
        }
        set
        {
            _Idtipodocumento = value;
        }
    }

    private int _Lineid;
    public int Lineid
    {
        get
        {
            return _Lineid;
        }
        set
        {
            _Lineid = value;
        }
    }

    private string _Codigo;
    public string Codigo
    {
        get
        {
            return _Codigo;
        }
        set
        {
            _Codigo = value;
        }
    }

    private string _CodigoSunat;
    public string CodigoSunat
    {
        get
        {
            return _CodigoSunat;
        }
        set
        {
            _CodigoSunat = value;
        }
    }

    private string _Producto;
    public string Producto
    {
        get
        {
            return _Producto;
        }
        set
        {
            _Producto = value;
        }
    }

    private string _AfectoIgv;
    public string AfectoIgv
    {
        get
        {
            return _AfectoIgv;
        }
        set
        {
            _AfectoIgv = value;
        }
    }

    private string _AfectoIsc;
    public string AfectoIsc
    {
        get
        {
            return _AfectoIsc;
        }
        set
        {
            _AfectoIsc = value;
        }
    }

    private string _Afectacionigv;
    public string Afectacionigv
    {
        get
        {
            return _Afectacionigv;
        }
        set
        {
            _Afectacionigv = value;
        }
    }

    private string _SistemaISC;
    public string SistemaISC
    {
        get
        {
            return _SistemaISC;
        }
        set
        {
            _SistemaISC = value;
        }
    }

    private string _Unidad;
    public string Unidad
    {
        get
        {
            return _Unidad;
        }
        set
        {
            _Unidad = value;
        }
    }

    private double _Cantidad;
    public double Cantidad
    {
        get
        {
            return _Cantidad;
        }
        set
        {
            _Cantidad = value;
        }
    }

    private double _Preciounitario;
    public double Preciounitario
    {
        get
        {
            return _Preciounitario;
        }
        set
        {
            _Preciounitario = value;
        }
    }

    private double _Precioreferencial;
    public double Precioreferencial
    {
        get
        {
            return _Precioreferencial;
        }
        set
        {
            _Precioreferencial = value;
        }
    }

    private double _Valorunitario;
    public double Valorunitario
    {
        get
        {
            return _Valorunitario;
        }
        set
        {
            _Valorunitario = value;
        }
    }

    private double _Valorbruto;
    public double Valorbruto
    {
        get
        {
            return _Valorbruto;
        }
        set
        {
            _Valorbruto = value;
        }
    }

    private double _Valordscto;
    public double Valordscto
    {
        get
        {
            return _Valordscto;
        }
        set
        {
            _Valordscto = value;
        }
    }

    private double _Valorcargo;
    public double Valorcargo
    {
        get
        {
            return _Valorcargo;
        }
        set
        {
            _Valorcargo = value;
        }
    }

    private double _Valorventa;
    public double Valorventa
    {
        get
        {
            return _Valorventa;
        }
        set
        {
            _Valorventa = value;
        }
    }

    private double _Isc;
    public double Isc
    {
        get
        {
            return _Isc;
        }
        set
        {
            _Isc = value;
        }
    }

    private double _Igv;
    public double Igv
    {
        get
        {
            return _Igv;
        }
        set
        {
            _Igv = value;
        }
    }

    private double _Total;
    public double Total
    {
        get
        {
            return _Total;
        }
        set
        {
            _Total = value;
        }
    }

    private double _factorIsc;
    public double factorIsc
    {
        get
        {
            return _factorIsc;
        }
        set
        {
            _factorIsc = value;
        }
    }

    private double _factorIgv;
    public double factorIgv
    {
        get
        {
            return _factorIgv;
        }
        set
        {
            _factorIgv = value;
        }
    }

    private string _RHEmbarcacionPesquera;
    public string RHEmbarcacionPesquera
    {
        get
        {
            return _RHEmbarcacionPesquera;
        }
        set
        {
            _RHEmbarcacionPesquera = value;
        }
    }

    private string _RHNombreEmbarcacionPesquera;
    public string RHNombreEmbarcacionPesquera
    {
        get
        {
            return _RHNombreEmbarcacionPesquera;
        }
        set
        {
            _RHNombreEmbarcacionPesquera = value;
        }
    }

    private string _RHDescripcionEspecieVendida;
    public string RHDescripcionEspecieVendida
    {
        get
        {
            return _RHDescripcionEspecieVendida;
        }
        set
        {
            _RHDescripcionEspecieVendida = value;
        }
    }

    private string _RHLugarDescarga;
    public string RHLugarDescarga
    {
        get
        {
            return _RHLugarDescarga;
        }
        set
        {
            _RHLugarDescarga = value;
        }
    }

    private string _RHUnidadEspecieVendida;
    public string RHUnidadEspecieVendida
    {
        get
        {
            return _RHUnidadEspecieVendida;
        }
        set
        {
            _RHUnidadEspecieVendida = value;
        }
    }

    private string _RHCantidadEspecieVendida;
    public string RHCantidadEspecieVendida
    {
        get
        {
            return _RHCantidadEspecieVendida;
        }
        set
        {
            _RHCantidadEspecieVendida = value;
        }
    }

    private string _RHFechaDescarga;
    public string RHFechaDescarga
    {
        get
        {
            return _RHFechaDescarga;
        }
        set
        {
            _RHFechaDescarga = value;
        }
    }

    private string _TBVTPuntoOrigen;
    public string TBVTPuntoOrigen
    {
        get
        {
            return _TBVTPuntoOrigen;
        }
        set
        {
            _TBVTPuntoOrigen = value;
        }
    }

    private string _TBVTDescripcionOrigen;
    public string TBVTDescripcionOrigen
    {
        get
        {
            return _TBVTDescripcionOrigen;
        }
        set
        {
            _TBVTDescripcionOrigen = value;
        }
    }

    private string _TBVTPuntoDestino;
    public string TBVTPuntoDestino
    {
        get
        {
            return _TBVTPuntoDestino;
        }
        set
        {
            _TBVTPuntoDestino = value;
        }
    }

    private string _TBVTDescripcionDestino;
    public string TBVTDescripcionDestino
    {
        get
        {
            return _TBVTDescripcionDestino;
        }
        set
        {
            _TBVTDescripcionDestino = value;
        }
    }

    private string _TBVTDetalleViaje;
    public string TBVTDetalleViaje
    {
        get
        {
            return _TBVTDetalleViaje;
        }
        set
        {
            _TBVTDetalleViaje = value;
        }
    }

    private string _TBVTValorRefPreliminar;
    public string TBVTValorRefPreliminar
    {
        get
        {
            return _TBVTValorRefPreliminar;
        }
        set
        {
            _TBVTValorRefPreliminar = value;
        }
    }

    private string _TBVTValorCargaEfectiva;
    public string TBVTValorCargaEfectiva
    {
        get
        {
            return _TBVTValorCargaEfectiva;
        }
        set
        {
            _TBVTValorCargaEfectiva = value;
        }
    }

    private string _TBVTValorCargaUtil;
    public string TBVTValorCargaUtil
    {
        get
        {
            return _TBVTValorCargaUtil;
        }
        set
        {
            _TBVTValorCargaUtil = value;
        }
    }

    private string _TBVTNumRegistroMtc;
    public string TBVTNumRegistroMtc
    {
        get
        {
            return _TBVTNumRegistroMtc;
        }
        set
        {
            _TBVTNumRegistroMtc = value;
        }
    }

    private string _TBVTConfiguracionVehicular;
    public string TBVTConfiguracionVehicular
    {
        get
        {
            return _TBVTConfiguracionVehicular;
        }
        set
        {
            _TBVTConfiguracionVehicular = value;
        }
    }


    private string _COCodUnicConcesMinera;
    public string COCodUnicConcesMinera
    {
        get
        {
            return _COCodUnicConcesMinera;
        }
        set
        {
            _COCodUnicConcesMinera = value;
        }
    }

    private string _CONroDeclaracComprom;
    public string CONroDeclaracComprom
    {
        get
        {
            return _CONroDeclaracComprom;
        }
        set
        {
            _CONroDeclaracComprom = value;
        }
    }

    private string _CONroRegEspecial;
    public string CONroRegEspecial
    {
        get
        {
            return _CONroRegEspecial;
        }
        set
        {
            _CONroRegEspecial = value;
        }
    }

    private string _CONroResolucPlanta;
    public string CONroResolucPlanta
    {
        get
        {
            return _CONroResolucPlanta;
        }
        set
        {
            _CONroResolucPlanta = value;
        }
    }

    private string _COLeyMineral;
    public string COLeyMineral
    {
        get
        {
            return _COLeyMineral;
        }
        set
        {
            _COLeyMineral = value;
        }
    }

    private string _BHCodPaisEmisionPasap;
    public string BHCodPaisEmisionPasap
    {
        get
        {
            return _BHCodPaisEmisionPasap;
        }
        set
        {
            _BHCodPaisEmisionPasap = value;
        }
    }

    private string _BHCodPaisResidSujetoND;
    public string BHCodPaisResidSujetoND
    {
        get
        {
            return _BHCodPaisResidSujetoND;
        }
        set
        {
            _BHCodPaisResidSujetoND = value;
        }
    }

    private string _BHFechaIngresoPais;
    public string BHFechaIngresoPais
    {
        get
        {
            return _BHFechaIngresoPais;
        }
        set
        {
            _BHFechaIngresoPais = value;
        }
    }

    private string _BHFechaIngresoEstab;
    public string BHFechaIngresoEstab
    {
        get
        {
            return _BHFechaIngresoEstab;
        }
        set
        {
            _BHFechaIngresoEstab = value;
        }
    }

    private string _BHFechaSalidaEstab;
    public string BHFechaSalidaEstab
    {
        get
        {
            return _BHFechaSalidaEstab;
        }
        set
        {
            _BHFechaSalidaEstab = value;
        }
    }

    private string _BHNroDiasPermanencia;
    public string BHNroDiasPermanencia
    {
        get
        {
            return _BHNroDiasPermanencia;
        }
        set
        {
            _BHNroDiasPermanencia = value;
        }
    }

    private string _BHFechaConsumo;
    public string BHFechaConsumo
    {
        get
        {
            return _BHFechaConsumo;
        }
        set
        {
            _BHFechaConsumo = value;
        }
    }

    private string _BHNombreApellidoHuesped;
    public string BHNombreApellidoHuesped
    {
        get
        {
            return _BHNombreApellidoHuesped;
        }
        set
        {
            _BHNombreApellidoHuesped = value;
        }
    }

    private string _BHTipoDocumentoHuesped;
    public string BHTipoDocumentoHuesped
    {
        get
        {
            return _BHTipoDocumentoHuesped;
        }
        set
        {
            _BHTipoDocumentoHuesped = value;
        }
    }

    private string _BHNroDocumentoHuesped;
    public string BHNroDocumentoHuesped
    {
        get
        {
            return _BHNroDocumentoHuesped;
        }
        set
        {
            _BHNroDocumentoHuesped = value;
        }
    }

    private string _PENroExpediente;
    public string PENroExpediente
    {
        get
        {
            return _PENroExpediente;
        }
        set
        {
            _PENroExpediente = value;
        }
    }

    private string _PECodUnidadEjecutora;
    public string PECodUnidadEjecutora
    {
        get
        {
            return _PECodUnidadEjecutora;
        }
        set
        {
            _PECodUnidadEjecutora = value;
        }
    }

    private string _PENroProcesoSeleccion;
    public string PENroProcesoSeleccion
    {
        get
        {
            return _PENroProcesoSeleccion;
        }
        set
        {
            _PENroProcesoSeleccion = value;
        }
    }

    private string _PENroContrato;
    public string PENroContrato
    {
        get
        {
            return _PENroContrato;
        }
        set
        {
            _PENroContrato = value;
        }
    }
}

}