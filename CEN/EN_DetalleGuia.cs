/****************************************************************************************************************************************************************************************
 PROGRAMA: EN_DetalleGuia.cs
 VERSION : 1.0
 OBJETIVO: Clase entidad de detalle de guia
 FECHA   : 23/11/2021
 AUTOR   : JOSE CHUMIOQUE -  IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 ***************************************************************************************************************************************************************************************/

namespace CEN
{
    public class EN_DetalleGuia
    {
        //DESCRIPCION: Clase de Entidad de detalle de guia
        public string guia_id { get; set; } // guia ID
        public int empresa_id { get; set; } // empresa
        public string tipodocumento_id  { get; set; } // tipo de documento
        public string line_id { get; set; } // line ID
        public string codigo { get; set; } // codigo
        public string descripcion { get; set; } // descripcion
        public string unidad { get; set; } // unidad
        public double cantidad { get; set; } // cantidad
    }
}