﻿using System;

namespace CEN
{
public class EN_DetalleOtrosCpe
{
    
    public int empresa_id  {get; set;}
    
    public string otroscpe_id {get;set;}
   

    
    public string tipodocumento_id {get;set;}
  


    public int line_id { get; set; }

    
    public string docrelac_tipodoc_id {get;set;}

    
    public string docrelac_tipodoc_desc {get; set;}
    
    public string docrelac_numerodoc { get; set; }

    
    public string docrelac_fechaemision {get;set;}
  

    private decimal _docrelac_importetotal;
    public decimal docrelac_importetotal
    {
        get
        {
            return _docrelac_importetotal;
        }

        set
        {
            _docrelac_importetotal = value;
        }
    }

    private string _docrelac_moneda;
    public string docrelac_moneda
    {
        get
        {
            return _docrelac_moneda;
        }

        set
        {
            _docrelac_moneda = value;
        }
    }

    private string _pagcob_fecha;
    public string pagcob_fecha
    {
        get
        {
            return _pagcob_fecha;
        }

        set
        {
            _pagcob_fecha = value;
        }
    }

    private string _pagcob_fechaft;
    public string pagcob_fechaft
    {
        get
        {
            return _pagcob_fechaft;
        }

        set
        {
            _pagcob_fechaft = value;
        }
    }

    private string _pagcob_numero;
    public string pagcob_numero
    {
        get
        {
            return _pagcob_numero;
        }

        set
        {
            _pagcob_numero = value;
        }
    }

    private decimal _pagcob_importe;
    public decimal pagcob_importe
    {
        get
        {
            return _pagcob_importe;
        }

        set
        {
            _pagcob_importe = value;
        }
    }

    private string _pagcob_moneda;
    public string pagcob_moneda
    {
        get
        {
            return _pagcob_moneda;
        }

        set
        {
            _pagcob_moneda = value;
        }
    }

    private decimal _retper_importe;
    public decimal retper_importe
    {
        get
        {
            return _retper_importe;
        }

        set
        {
            _retper_importe = value;
        }
    }

    private string _retper_moneda;
    public string retper_moneda
    {
        get
        {
            return _retper_moneda;
        }

        set
        {
            _retper_moneda = value;
        }
    }

   
    public string retper_fecha   {get; set;}

    
    public  string retper_fechaft { get; set; }
  

    private decimal _retper_importe_pagcob;
    public decimal retper_importe_pagcob
    {
        get
        {
            return _retper_importe_pagcob;
        }

        set
        {
            _retper_importe_pagcob = value;
        }
    }

    private string _retper_moneda_pagcob;
    public string retper_moneda_pagcob
    {
        get
        {
            return _retper_moneda_pagcob;
        }

        set
        {
            _retper_moneda_pagcob = value;
        }
    }

    private string _tipocambio_monedaref;
    public string tipocambio_monedaref
    {
        get
        {
            return _tipocambio_monedaref;
        }

        set
        {
            _tipocambio_monedaref = value;
        }
    }

    private string _tipocambio_monedaobj;
    public string tipocambio_monedaobj
    {
        get
        {
            return _tipocambio_monedaobj;
        }

        set
        {
            _tipocambio_monedaobj = value;
        }
    }

    private Nullable<decimal> _tipocambio_factor;
    public Nullable<decimal> tipocambio_factor
    {
        get
        {
            return _tipocambio_factor;
        }

        set
        {
            _tipocambio_factor = value;
        }
    }

    
    public string tipocambio_fecha {get; set;}
}

}