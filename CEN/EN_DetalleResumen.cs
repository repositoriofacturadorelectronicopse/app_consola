namespace CEN
{
    public class EN_DetalleResumen
    {
         private int _IdResumen;
    public int IdResumen
    {
        get
        {
            return _IdResumen;
        }
        set
        {
            _IdResumen = value;
        }
    }

    private int _Item;
    public int Item
    {
        get
        {
            return _Item;
        }
        set
        {
            _Item = value;
        }
    }

    private string _Tipodocumentoid;
    public string Tipodocumentoid
    {
        get
        {
            return _Tipodocumentoid;
        }
        set
        {
            _Tipodocumentoid = value;
        }
    }

    private string _DescTipodocumento;
    public string DescTipodocumento
    {
        get
        {
            return _DescTipodocumento;
        }
        set
        {
            _DescTipodocumento = value;
        }
    }

    private string _Serie;
    public string Serie
    {
        get
        {
            return _Serie;
        }
        set
        {
            _Serie = value;
        }
    }

    private string _Correlativo;
    public string Correlativo
    {
        get
        {
            return _Correlativo;
        }
        set
        {
            _Correlativo = value;
        }
    }

    private string _DocCliente;
    public string DocCliente
    {
        get
        {
            return _DocCliente;
        }
        set
        {
            _DocCliente = value;
        }
    }

    private string _NroDocCliente;
    public string NroDocCliente
    {
        get
        {
            return _NroDocCliente;
        }
        set
        {
            _NroDocCliente = value;
        }
    }

    private int _Condicion;
    public int Condicion
    {
        get
        {
            return _Condicion;
        }
        set
        {
            _Condicion = value;
        }
    }

    private string _Moneda;
    public string Moneda
    {
        get
        {
            return _Moneda;
        }
        set
        {
            _Moneda = value;
        }
    }

    private decimal _Opegravadas;
    public decimal Opegravadas
    {
        get
        {
            return _Opegravadas;
        }
        set
        {
            _Opegravadas = value;
        }
    }

    private decimal _Opeexoneradas;
    public decimal Opeexoneradas
    {
        get
        {
            return _Opeexoneradas;
        }
        set
        {
            _Opeexoneradas = value;
        }
    }

    private decimal _Opeinafectas;
    public decimal Opeinafectas
    {
        get
        {
            return _Opeinafectas;
        }
        set
        {
            _Opeinafectas = value;
        }
    }

    private decimal _Opeexportacion;
    public decimal Opeexportacion
    {
        get
        {
            return _Opeexportacion;
        }
        set
        {
            _Opeexportacion = value;
        }
    }

    private decimal _Opegratuitas;
    public decimal Opegratuitas
    {
        get
        {
            return _Opegratuitas;
        }
        set
        {
            _Opegratuitas = value;
        }
    }

    private decimal _Otroscargos;
    public decimal Otroscargos
    {
        get
        {
            return _Otroscargos;
        }
        set
        {
            _Otroscargos = value;
        }
    }

    private decimal _TotalISC;
    public decimal TotalISC
    {
        get
        {
            return _TotalISC;
        }
        set
        {
            _TotalISC = value;
        }
    }

    private decimal _TotalIGV;
    public decimal TotalIGV
    {
        get
        {
            return _TotalIGV;
        }
        set
        {
            _TotalIGV = value;
        }
    }

    private decimal _Otrostributos;
    public decimal Otrostributos
    {
        get
        {
            return _Otrostributos;
        }
        set
        {
            _Otrostributos = value;
        }
    }

    private decimal _Importeventa;
    public decimal Importeventa
    {
        get
        {
            return _Importeventa;
        }
        set
        {
            _Importeventa = value;
        }
    }

    private string _Regimenpercep;
    public string Regimenpercep
    {
        get
        {
            return _Regimenpercep;
        }
        set
        {
            _Regimenpercep = value;
        }
    }

    private decimal _Porcentpercep;
    public decimal Porcentpercep
    {
        get
        {
            return _Porcentpercep;
        }
        set
        {
            _Porcentpercep = value;
        }
    }

    private decimal _Importepercep;
    public decimal Importepercep
    {
        get
        {
            return _Importepercep;
        }
        set
        {
            _Importepercep = value;
        }
    }

    private decimal _Importefinal;
    public decimal Importefinal
    {
        get
        {
            return _Importefinal;
        }
        set
        {
            _Importefinal = value;
        }
    }

    private string _Tipodocref;
    public string Tipodocref
    {
        get
        {
            return _Tipodocref;
        }
        set
        {
            _Tipodocref = value;
        }
    }

    private string _Nrodocref;
    public string Nrodocref
    {
        get
        {
            return _Nrodocref;
        }
        set
        {
            _Nrodocref = value;
        }
    }
    }
}