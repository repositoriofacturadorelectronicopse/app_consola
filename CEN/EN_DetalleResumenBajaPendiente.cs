/****************************************************************************************************************************************************************************************
 PROGRAMA: EN_DetalleResumenBajaPendiente.cs
 VERSION : 1.0
 OBJETIVO: Clase entidad de detalle resumen baja pendiente
 FECHA   : 28/01/2022
 AUTOR   : JUAN ALARCON -  IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 ***************************************************************************************************************************************************************************************/

namespace CEN
{
    public class EN_DetalleResumenBajaPendiente
    {
        //DESCRIPCION: Clase de Entidad de resumen baja pendiente
        public int id { get; set;}
        public int resumenbaja_id { get; set; }
        public string documento_id { get; set; }
        public string documento_idtipodocumento { get; set; } 
        public string  motivo_baja { get; set; }

        //parte de ResumenBajaPendiente
        public int empresa_id { get; set; }
        public int puntoventa_id { get; set; }
        public string fecemision_documento { get; set; } 
        public string  fecgenera_resumen { get; set; }

    }

}