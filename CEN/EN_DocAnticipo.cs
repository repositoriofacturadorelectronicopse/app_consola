namespace CEN
{
   public class EN_DocAnticipo
{
    private string _documento_id;
    public string documento_id
    {
        get
        {
            return _documento_id;
        }
        set
        {
            _documento_id = value;
        }
    }

    private int _empresa_id;
    public int empresa_id
    {
        get
        {
            return _empresa_id;
        }
        set
        {
            _empresa_id = value;
        }
    }

    private string _tipodocumento_id;
    public string tipodocumento_id
    {
        get
        {
            return _tipodocumento_id;
        }
        set
        {
            _tipodocumento_id = value;
        }
    }

    private int _line_id;
    public int line_id
    {
        get
        {
            return _line_id;
        }
        set
        {
            _line_id = value;
        }
    }

    private string _tipodocanticipo;
    public string tipodocanticipo
    {
        get
        {
            return _tipodocanticipo;
        }
        set
        {
            _tipodocanticipo = value;
        }
    }

    private string _desctipodocanticipo;
    public string desctipodocanticipo
    {
        get
        {
            return _desctipodocanticipo;
        }
        set
        {
            _desctipodocanticipo = value;
        }
    }

    private string _desctipodocemisor;
    public string desctipodocemisor
    {
        get
        {
            return _desctipodocemisor;
        }
        set
        {
            _desctipodocemisor = value;
        }
    }

    private string _nrodocanticipo;
    public string nrodocanticipo
    {
        get
        {
            return _nrodocanticipo;
        }
        set
        {
            _nrodocanticipo = value;
        }
    }

    private decimal _montoanticipo;
    public decimal montoanticipo
    {
        get
        {
            return _montoanticipo;
        }
        set
        {
            _montoanticipo = value;
        }
    }

    private string _tipodocemisor;
    public string tipodocemisor
    {
        get
        {
            return _tipodocemisor;
        }
        set
        {
            _tipodocemisor = value;
        }
    }

    private string _nrodocemisor;
    public string nrodocemisor
    {
        get
        {
            return _nrodocemisor;
        }
        set
        {
            _nrodocemisor = value;
        }
    }

    // Informacion Auditoria
    private string _UsuarioSession;
    public string UsuarioSession
    {
        get
        {
            return _UsuarioSession;
        }
        set
        {
            _UsuarioSession = value;
        }
    }

    private string _CadenaAleatoria;
    public string CadenaAleatoria
    {
        get
        {
            return _CadenaAleatoria;
        }
        set
        {
            _CadenaAleatoria = value;
        }
    }
}

}