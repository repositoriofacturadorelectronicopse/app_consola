﻿

namespace CEN
{
    public class EN_DocPersona
    {
        private string _Codigo;
        public string Codigo
        {
            get
            {
                return _Codigo;
            }
            set
            {
                _Codigo = value;
            }
        }
        private string _CodigoSunat;
        public string CodigoSunat
        {
            get
            {
                return _CodigoSunat;
            }
            set
            {
                _CodigoSunat = value;
            }
        }
        private string _Nombre;
        public string Nombre
        {
            get
            {
                return _Nombre;
            }
            set
            {
                _Nombre = value;
            }
        }
        private string _Longitud;
        public string Longitud
        {
            get
            {
                return _Longitud;
            }
            set
            {
                _Longitud = value;
            }
        }
        private string _Tipo;
        public string Tipo
        {
            get
            {
                return _Tipo;
            }
            set
            {
                _Tipo = value;
            }
        }
        private string _Indcontrib;
        public string Indcontrib
        {
            get
            {
                return _Indcontrib;
            }
            set
            {
                _Indcontrib = value;
            }
        }
        private string _Indlongexacta;
        public string Indlongexacta
        {
            get
            {
                return _Indlongexacta;
            }
            set
            {
                _Indlongexacta = value;
            }
        }
        private string _Indpersona;
        public string Indpersona
        {
            get
            {
                return _Indpersona;
            }
            set
            {
                _Indpersona = value;
            }
        }
        private string _Orden;
        public string Orden
        {
            get
            {
                return _Orden;
            }
            set
            {
                _Orden = value;
            }
        }
        private string _Estado;
        public string Estado
        {
            get
            {
                return _Estado;
            }
            set
            {
                _Estado = value;
            }
        }
    }

}
