namespace CEN
{
  public class EN_DocRefeGuia
{
    private string _documento_id;
    public string documento_id
    {
        get
        {
            return _documento_id;
        }
        set
        {
            _documento_id = value;
        }
    }

    private int _empresa_id;
    public int empresa_id
    {
        get
        {
            return _empresa_id;
        }
        set
        {
            _empresa_id = value;
        }
    }

    private string _tipodocumento_id;
    public string tipodocumento_id
    {
        get
        {
            return _tipodocumento_id;
        }
        set
        {
            _tipodocumento_id = value;
        }
    }

    private string _tipo_guia;
    public string tipo_guia
    {
        get
        {
            return _tipo_guia;
        }
        set
        {
            _tipo_guia = value;
        }
    }

    private string _numero_guia;
    public string numero_guia
    {
        get
        {
            return _numero_guia;
        }
        set
        {
            _numero_guia = value;
        }
    }

    private string _desctipoguia;
    public string desctipoguia
    {
        get
        {
            return _desctipoguia;
        }
        set
        {
            _desctipoguia = value;
        }
    }

    // Informacion Auditoria
    private string _UsuarioSession;
    public string UsuarioSession
    {
        get
        {
            return _UsuarioSession;
        }
        set
        {
            _UsuarioSession = value;
        }
    }

    private string _CadenaAleatoria;
    public string CadenaAleatoria
    {
        get
        {
            return _CadenaAleatoria;
        }
        set
        {
            _CadenaAleatoria = value;
        }
    }
}

}