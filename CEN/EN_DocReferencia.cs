namespace CEN
{
public class EN_DocReferencia
{
    private int _Idempresa;
    public int Idempresa
    {
        get
        {
            return _Idempresa;
        }
        set
        {
            _Idempresa = value;
        }
    }

    private string _Iddocumento;
    public string Iddocumento
    {
        get
        {
            return _Iddocumento;
        }
        set
        {
            _Iddocumento = value;
        }
    }

    private string _Idtipodocumento;
    public string Idtipodocumento
    {
        get
        {
            return _Idtipodocumento;
        }
        set
        {
            _Idtipodocumento = value;
        }
    }

    private string _Iddocumentoref;
    public string Iddocumentoref
    {
        get
        {
            return _Iddocumentoref;
        }
        set
        {
            _Iddocumentoref = value;
        }
    }

    private string _Idtipodocumentoref;
    public string Idtipodocumentoref
    {
        get
        {
            return _Idtipodocumentoref;
        }
        set
        {
            _Idtipodocumentoref = value;
        }
    }

    private string _Numerodocref;
    public string Numerodocref
    {
        get
        {
            return _Numerodocref;
        }
        set
        {
            _Numerodocref = value;
        }
    }

    private string _DescripcionTipoDoc;
    public string DescripcionTipoDoc
    {
        get
        {
            return _DescripcionTipoDoc;
        }
        set
        {
            _DescripcionTipoDoc = value;
        }
    }

    // Informacion Auditoria
    private string _UsuarioSession;
    public string UsuarioSession
    {
        get
        {
            return _UsuarioSession;
        }
        set
        {
            _UsuarioSession = value;
        }
    }

    private string _CadenaAleatoria;
    public string CadenaAleatoria
    {
        get
        {
            return _CadenaAleatoria;
        }
        set
        {
            _CadenaAleatoria = value;
        }
    }
}


}