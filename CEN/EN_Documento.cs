using System;
using System.Collections.Generic;


namespace CEN
{
    public class EN_Documento
    {

        private int _Idempresa;
    public int Idempresa
    {
        get
        {
            return _Idempresa;
        }
        set
        {
            _Idempresa = value;
        }
    }

    private string _Id;
    public string Id
    {
        get
        {
            return _Id;
        }
        set
        {
            _Id = value;
        }
    }
    

    private string _Idtipodocumento;
    public string Idtipodocumento
    {
        get
        {
            return _Idtipodocumento;
        }
        set
        {
            _Idtipodocumento = value;
        }
    }

    private string _NroDocEmpresa;
    public string NroDocEmpresa
    {
        get
        {
            return _NroDocEmpresa;
        }
        set
        {
            _NroDocEmpresa = value;
        }
    }

    private string _DescripcionEmpresa;
    public string DescripcionEmpresa
    {
        get
        {
            return _DescripcionEmpresa;
        }
        set
        {
            _DescripcionEmpresa = value;
        }
    }

    private string _Tipodocumento;
    public string Tipodocumento
    {
        get
        {
            return _Tipodocumento;
        }
        set
        {
            _Tipodocumento = value;
        }
    }

    private string _Serie;
    public string Serie
    {
        get
        {
            return _Serie;
        }
        set
        {
            _Serie = value;
        }
    }

    private string _Numero;
    public string Numero
    {
        get
        {
            return _Numero;
        }
        set
        {
            _Numero = value;
        }
    }

    private int _Idpuntoventa;
    public int Idpuntoventa
    {
        get
        {
            return _Idpuntoventa;
        }
        set
        {
            _Idpuntoventa = value;
        }
    }

    private string _DescripcionPuntoVenta;
    public string DescripcionPuntoVenta
    {
        get
        {
            return _DescripcionPuntoVenta;
        }
        set
        {
            _DescripcionPuntoVenta = value;
        }
    }

    private string _CodigoEstablecimiento;
    public string CodigoEstablecimiento
    {
        get
        {
            return _CodigoEstablecimiento;
        }
        set
        {
            _CodigoEstablecimiento = value;
        }
    }

    private string _TipoOperacion;
    public string TipoOperacion
    {
        get
        {
            return _TipoOperacion;
        }
        set
        {
            _TipoOperacion = value;
        }
    }

    private string _TipoNotaCredNotaDeb;
    public string TipoNotaCredNotaDeb
    {
        get
        {
            return _TipoNotaCredNotaDeb;
        }
        set
        {
            _TipoNotaCredNotaDeb = value;
        }
    }

    private string _Tipodoccliente;
    public string Tipodoccliente
    {
        get
        {
            return _Tipodoccliente;
        }
        set
        {
            _Tipodoccliente = value;
        }
    }

    private string _Nrodoccliente;
    public string Nrodoccliente
    {
        get
        {
            return _Nrodoccliente;
        }
        set
        {
            _Nrodoccliente = value;
        }
    }

    private string _Nombrecliente;
    public string Nombrecliente
    {
        get
        {
            return _Nombrecliente;
        }
        set
        {
            _Nombrecliente = value;
        }
    }

    private string _Direccioncliente;
    public string Direccioncliente
    {
        get
        {
            return _Direccioncliente;
        }
        set
        {
            _Direccioncliente = value;
        }
    }

    private string _Emailcliente;
    public string Emailcliente
    {
        get
        {
            return _Emailcliente;
        }
        set
        {
            _Emailcliente = value;
        }
    }

    private string _Fecha;
    public string Fecha
    {
        get
        {
            return _Fecha;
        }
        set
        {
            _Fecha = value;
        }
    }

    private string _Hora;
    public string Hora
    {
        get
        {
            return _Hora;
        }
        set
        {
            _Hora = value;
        }
    }

    private string _Fechavencimiento;
    public string Fechavencimiento
    {
        get
        {
            return _Fechavencimiento;
        }
        set
        {
            _Fechavencimiento = value;
        }
    }

    private string _Glosa;
    public string Glosa
    {
        get
        {
            return _Glosa;
        }
        set
        {
            _Glosa = value;
        }
    }

    private string _Formapago;
    public string Formapago
    {
        get
        {
            return _Formapago;
        }
        set
        {
            _Formapago = value;
        }
    }

    private string _Moneda;
    public string Moneda
    {
        get
        {
            return _Moneda;
        }
        set
        {
            _Moneda = value;
        }
    }

    private double _Tipocambio;
    public double Tipocambio
    {
        get
        {
            return _Tipocambio;
        }
        set
        {
            _Tipocambio = value;
        }
    }

    private double _Descuento;
    public double Descuento
    {
        get
        {
            return _Descuento;
        }
        set
        {
            _Descuento = value;
        }
    }

    private double _Igvventa;
    public double Igvventa
    {
        get
        {
            return _Igvventa;
        }
        set
        {
            _Igvventa = value;
        }
    }

    private double _Iscventa;
    public double Iscventa
    {
        get
        {
            return _Iscventa;
        }
        set
        {
            _Iscventa = value;
        }
    }

    private double _OtrosTributos;
    public double OtrosTributos
    {
        get
        {
            return _OtrosTributos;
        }
        set
        {
            _OtrosTributos = value;
        }
    }

    private double _OtrosCargos;
    public double OtrosCargos
    {
        get
        {
            return _OtrosCargos;
        }
        set
        {
            _OtrosCargos = value;
        }
    }

    private double _Importeventa;
    public double Importeventa
    {
        get
        {
            return _Importeventa;
        }
        set
        {
            _Importeventa = value;
        }
    }

    private string _Regimenpercep;
    public string Regimenpercep
    {
        get
        {
            return _Regimenpercep;
        }
        set
        {
            _Regimenpercep = value;
        }
    }

    private string _DescRegimenpercep;
    public string DescRegimenpercep
    {
        get
        {
            return _DescRegimenpercep;
        }
        set
        {
            _DescRegimenpercep = value;
        }
    }

    private double _Porcentpercep;
    public double Porcentpercep
    {
        get
        {
            return _Porcentpercep;
        }
        set
        {
            _Porcentpercep = value;
        }
    }

    private double _Importepercep;
    public double Importepercep
    {
        get
        {
            return _Importepercep;
        }
        set
        {
            _Importepercep = value;
        }
    }

    private double _Porcentdetrac;
    public double Porcentdetrac
    {
        get
        {
            return _Porcentdetrac;
        }
        set
        {
            _Porcentdetrac = value;
        }
    }

    private double _Importedetrac;
    public double Importedetrac
    {
        get
        {
            return _Importedetrac;
        }
        set
        {
            _Importedetrac = value;
        }
    }

    private double _Importerefdetrac;
    public double Importerefdetrac
    {
        get
        {
            return _Importerefdetrac;
        }
        set
        {
            _Importerefdetrac = value;
        }
    }

    private double _Importefinal;
    public double Importefinal
    {
        get
        {
            return _Importefinal;
        }
        set
        {
            _Importefinal = value;
        }
    }

    private double _Importeanticipo;
    public double Importeanticipo
    {
        get
        {
            return _Importeanticipo;
        }
        set
        {
            _Importeanticipo = value;
        }
    }

    private string _IndicaAnticipo;
    public string IndicaAnticipo
    {
        get
        {
            return _IndicaAnticipo;
        }
        set
        {
            _IndicaAnticipo = value;
        }
    }

    private string _Estado;
    public string Estado
    {
        get
        {
            return _Estado;
        }
        set
        {
            _Estado = value;
        }
    }

    private string _Situacion;
    public string Situacion
    {
        get
        {
            return _Situacion;
        }
        set
        {
            _Situacion = value;
        }
    }

    private string _Numeroelectronico;
    public string Numeroelectronico
    {
        get
        {
            return _Numeroelectronico;
        }
        set
        {
            _Numeroelectronico = value;
        }
    }

    private string _Estransgratuita;
    public string Estransgratuita
    {
        get
        {
            return _Estransgratuita;
        }
        set
        {
            _Estransgratuita = value;
        }
    }

    private string _EnviadoMail;
    public string EnviadoMail
    {
        get
        {
            return _EnviadoMail;
        }
        set
        {
            _EnviadoMail = value;
        }
    }

    private string _Envioexterno;
    public string Envioexterno
    {
        get
        {
            return _Envioexterno;
        }
        set
        {
            _Envioexterno = value;
        }
    }

    private double _Valorpergravadas;
    public double Valorpergravadas
    {
        get
        {
            return _Valorpergravadas;
        }
        set
        {
            _Valorpergravadas = value;
        }
    }

    private double _Valorperinafectas;
    public double Valorperinafectas
    {
        get
        {
            return _Valorperinafectas;
        }
        set
        {
            _Valorperinafectas = value;
        }
    }

    private double _Valorperexoneradas;
    public double Valorperexoneradas
    {
        get
        {
            return _Valorperexoneradas;
        }
        set
        {
            _Valorperexoneradas = value;
        }
    }

    private double _Valorpergratuitas;
    public double Valorpergratuitas
    {
        get
        {
            return _Valorpergratuitas;
        }
        set
        {
            _Valorpergratuitas = value;
        }
    }

    private double _Valorperiscreferenc;
    public double Valorperiscreferenc
    {
        get
        {
            return _Valorperiscreferenc;
        }
        set
        {
            _Valorperiscreferenc = value;
        }
    }

    private double _Valorperexportacion;
    public double Valorperexportacion
    {
        get
        {
            return _Valorperexportacion;
        }
        set
        {
            _Valorperexportacion = value;
        }
    }

    private double _SumaIgvGratuito;
    public double SumaIgvGratuito
    {
        get
        {
            return _SumaIgvGratuito;
        }
        set
        {
            _SumaIgvGratuito = value;
        }
    }

    private double _SumaIscGratuito;
    public double SumaIscGratuito
    {
        get
        {
            return _SumaIscGratuito;
        }
        set
        {
            _SumaIscGratuito = value;
        }
    }

    private double _TotalDsctoItem;
    public double TotalDsctoItem
    {
        get
        {
            return _TotalDsctoItem;
        }
        set
        {
            _TotalDsctoItem = value;
        }
    }

    private double _TotalCargoItem;
    public double TotalCargoItem
    {
        get
        {
            return _TotalCargoItem;
        }
        set
        {
            _TotalCargoItem = value;
        }
    }

    private string _TipoGuiaRemision;
    public string TipoGuiaRemision
    {
        get
        {
            return _TipoGuiaRemision;
        }
        set
        {
            _TipoGuiaRemision = value;
        }
    }

    private string _GuiaRemision;
    public string GuiaRemision
    {
        get
        {
            return _GuiaRemision;
        }
        set
        {
            _GuiaRemision = value;
        }
    }

    private string _TipoDocOtroDocRef;
    public string TipoDocOtroDocRef
    {
        get
        {
            return _TipoDocOtroDocRef;
        }
        set
        {
            _TipoDocOtroDocRef = value;
        }
    }

    private string _OtroDocumentoRef;
    public string OtroDocumentoRef
    {
        get
        {
            return _OtroDocumentoRef;
        }
        set
        {
            _OtroDocumentoRef = value;
        }
    }

    private string _OrdenCompra;
    public string OrdenCompra
    {
        get
        {
            return _OrdenCompra;
        }
        set
        {
            _OrdenCompra = value;
        }
    }

    private string _PlacaVehiculo;
    public string PlacaVehiculo
    {
        get
        {
            return _PlacaVehiculo;
        }
        set
        {
            _PlacaVehiculo = value;
        }
    }

    private double _MontoFise;
    public double MontoFise
    {
        get
        {
            return _MontoFise;
        }
        set
        {
            _MontoFise = value;
        }
    }

    private string _TipoRegimen;
    public string TipoRegimen
    {
        get
        {
            return _TipoRegimen;
        }
        set
        {
            _TipoRegimen = value;
        }
    }

    private string _CodigoBBSSSujetoDetrac;
    public string CodigoBBSSSujetoDetrac
    {
        get
        {
            return _CodigoBBSSSujetoDetrac;
        }
        set
        {
            _CodigoBBSSSujetoDetrac = value;
        }
    }

    private string _NumCtaBcoNacion;
    public string NumCtaBcoNacion
    {
        get
        {
            return _NumCtaBcoNacion;
        }
        set
        {
            _NumCtaBcoNacion = value;
        }
    }

    private string _BienTransfAmazonia;
    public string BienTransfAmazonia
    {
        get
        {
            return _BienTransfAmazonia;
        }
        set
        {
            _BienTransfAmazonia = value;
        }
    }

    private string _ServiTransfAmazonia;
    public string ServiTransfAmazonia
    {
        get
        {
            return _ServiTransfAmazonia;
        }
        set
        {
            _ServiTransfAmazonia = value;
        }
    }

    private string _ContratoConstAmazonia;
    public string ContratoConstAmazonia
    {
        get
        {
            return _ContratoConstAmazonia;
        }
        set
        {
            _ContratoConstAmazonia = value;
        }
    }

    // Parametros para el XML
    private string _nombreXML;
    public string nombreXML
    {
        get
        {
            return _nombreXML;
        }
        set
        {
            _nombreXML = value;
        }
    }
    private string _vResumen;
    public string vResumen
    {
        get
        {
            return _vResumen;
        }
        set
        {
            _vResumen = value;
        }
    }
    private string _vFirma;
    public string vFirma
    {
        get
        {
            return _vFirma;
        }
        set
        {
            _vFirma = value;
        }
    }
    private string _xmlEnviado;
    public string xmlEnviado
    {
        get
        {
            return _xmlEnviado;
        }
        set
        {
            _xmlEnviado = value;
        }
    }
    private string _xmlCDR;
    public string xmlCDR
    {
        get
        {
            return _xmlCDR;
        }
        set
        {
            _xmlCDR = value;
        }
    }

    // Para Adicionales
    private string _cod_aux_01;
    public string cod_aux_01
    {
        get
        {
            return _cod_aux_01;
        }
        set
        {
            _cod_aux_01 = value;
        }
    }
    private string _text_aux_01;
    public string text_aux_01
    {
        get
        {
            return _text_aux_01;
        }
        set
        {
            _text_aux_01 = value;
        }
    }

    private string _cod_aux_02;
    public string cod_aux_02
    {
        get
        {
            return _cod_aux_02;
        }
        set
        {
            _cod_aux_02 = value;
        }
    }
    private string _text_aux_02;
    public string text_aux_02
    {
        get
        {
            return _text_aux_02;
        }
        set
        {
            _text_aux_02 = value;
        }
    }

    private string _cod_aux_03;
    public string cod_aux_03
    {
        get
        {
            return _cod_aux_03;
        }
        set
        {
            _cod_aux_03 = value;
        }
    }
    private string _text_aux_03;
    public string text_aux_03
    {
        get
        {
            return _text_aux_03;
        }
        set
        {
            _text_aux_03 = value;
        }
    }

    private string _cod_aux_04;
    public string cod_aux_04
    {
        get
        {
            return _cod_aux_04;
        }
        set
        {
            _cod_aux_04 = value;
        }
    }
    private string _text_aux_04;
    public string text_aux_04
    {
        get
        {
            return _text_aux_04;
        }
        set
        {
            _text_aux_04 = value;
        }
    }

    private string _cod_aux_05;
    public string cod_aux_05
    {
        get
        {
            return _cod_aux_05;
        }
        set
        {
            _cod_aux_05 = value;
        }
    }
    private string _text_aux_05;
    public string text_aux_05
    {
        get
        {
            return _text_aux_05;
        }
        set
        {
            _text_aux_05 = value;
        }
    }

    private string _cod_aux_06;
    public string cod_aux_06
    {
        get
        {
            return _cod_aux_06;
        }
        set
        {
            _cod_aux_06 = value;
        }
    }
    private string _text_aux_06;
    public string text_aux_06
    {
        get
        {
            return _text_aux_06;
        }
        set
        {
            _text_aux_06 = value;
        }
    }

    private string _cod_aux_07;
    public string cod_aux_07
    {
        get
        {
            return _cod_aux_07;
        }
        set
        {
            _cod_aux_07 = value;
        }
    }
    private string _text_aux_07;
    public string text_aux_07
    {
        get
        {
            return _text_aux_07;
        }
        set
        {
            _text_aux_07 = value;
        }
    }

    private string _cod_aux_08;
    public string cod_aux_08
    {
        get
        {
            return _cod_aux_08;
        }
        set
        {
            _cod_aux_08 = value;
        }
    }
    private string _text_aux_08;
    public string text_aux_08
    {
        get
        {
            return _text_aux_08;
        }
        set
        {
            _text_aux_08 = value;
        }
    }

    private string _cod_aux_09;
    public string cod_aux_09
    {
        get
        {
            return _cod_aux_09;
        }
        set
        {
            _cod_aux_09 = value;
        }
    }
    private string _text_aux_09;
    public string text_aux_09
    {
        get
        {
            return _text_aux_09;
        }
        set
        {
            _text_aux_09 = value;
        }
    }

    private string _cod_aux_10;
    public string cod_aux_10
    {
        get
        {
            return _cod_aux_10;
        }
        set
        {
            _cod_aux_10 = value;
        }
    }
    private string _text_aux_10;
    public string text_aux_10
    {
        get
        {
            return _text_aux_10;
        }
        set
        {
            _text_aux_10 = value;
        }
    }

    private string _cod_aux_11;
    public string cod_aux_11
    {
        get
        {
            return _cod_aux_11;
        }
        set
        {
            _cod_aux_11 = value;
        }
    }
    private string _text_aux_11;
    public string text_aux_11
    {
        get
        {
            return _text_aux_11;
        }
        set
        {
            _text_aux_11 = value;
        }
    }

    private string _desctiponotcreddeb;
    public string desctiponotcreddeb
    {
        get
        {
            return _desctiponotcreddeb;
        }
        set
        {
            _desctiponotcreddeb = value;
        }
    }

    private string _descripciondetrac;
    public string descripciondetrac
    {
        get
        {
            return _descripciondetrac;
        }
        set
        {
            _descripciondetrac = value;
        }
    }

    // Para Busquedas
    private string _FechaIni;
    public string FechaIni
    {
        get
        {
            return _FechaIni;
        }
        set
        {
            _FechaIni = value;
        }
    }

    private string _FechaFin;
    public string FechaFin
    {
        get
        {
            return _FechaFin;
        }
        set
        {
            _FechaFin = value;
        }
    }

    private DateTime _FechaCDR;
    public DateTime FechaCDR
    {
        get
        {
            return _FechaCDR;
        }
        set
        {
            _FechaCDR = value;
        }
    }

    // Informacion Auditoria
    private string _UsuarioSession;
    public string UsuarioSession
    {
        get
        {
            return _UsuarioSession;
        }
        set
        {
            _UsuarioSession = value;
        }
    }

    private string _CadenaAleatoria;
    public string CadenaAleatoria
    {
        get
        {
            return _CadenaAleatoria;
        }
        set
        {
            _CadenaAleatoria = value;
        }
    }

    // Para Aceptados
    private string _Mensaje;
    public string Mensaje
    {
        get
        {
            return _Mensaje;
        }
        set
        {
            _Mensaje = value;
        }
    }

    private string _FechaEnvio;
    public string FechaEnvio
    {
        get
        {
            return _FechaEnvio;
        }
        set
        {
            _FechaEnvio = value;
        }
    }

    private string _Ticket;
    public string Ticket
    {
        get
        {
            return _Ticket;
        }
        set
        {
            _Ticket = value;
        }
    }

    // Para Errores Rechazos
    private string _MensajeError;
    public string MensajeError
    {
        get
        {
            return _MensajeError;
        }
        set
        {
            _MensajeError = value;
        }
    }

    private string _FechaError;
    public string FechaError
    {
        get
        {
            return _FechaError;
        }
        set
        {
            _FechaError = value;
        }
    }

    // Para reportes
    private string _Cantidad;
    public string Cantidad
    {
        get
        {
            return _Cantidad;
        }
        set
        {
            _Cantidad = value;
        }
    }

    // Para Validar Usuario Certificado Empresa
    private string _Usuario;
    public string Usuario
    {
        get
        {
            return _Usuario;
        }
        set
        {
            _Usuario = value;
        }
    }

    private string _Clave;
    public string Clave
    {
        get
        {
            return _Clave;
        }
        set
        {
            _Clave = value;
        }
    }

    // Para docreferencias
    private string _dr_tipodocumento_id;
    public string dr_tipodocumento_id
    {
        get
        {
            return _dr_tipodocumento_id;
        }
        set
        {
            _dr_tipodocumento_id = value;
        }
    }
    private string _dr_tipodoc_desc;
    public string dr_tipodoc_desc
    {
        get
        {
            return _dr_tipodoc_desc;
        }
        set
        {
            _dr_tipodoc_desc = value;
        }
    }
    private string _dr_serie;
    public string dr_serie
    {
        get
        {
            return _dr_serie;
        }
        set
        {
            _dr_serie = value;
        }
    }
    private string _dr_numero;
    public string dr_numero
    {
        get
        {
            return _dr_numero;
        }
        set
        {
            _dr_numero = value;
        }
    }
    private string _dr_fecha;
    public string dr_fecha
    {
        get
        {
            return _dr_fecha;
        }
        set
        {
            _dr_fecha = value;
        }
    }
    private string _dr_motivo;
    public string dr_motivo
    {
        get
        {
            return _dr_motivo;
        }
        set
        {
            _dr_motivo = value;
        }
    }
        private string _jsonpagocredito;
    public string jsonpagocredito
    {
        get
        {
            return _jsonpagocredito;
        }
        set
        {
            _jsonpagocredito = value;
        }
    }

    private string _hasRetencionIgv;
    public string hasRetencionIgv
    {
        get
        {
            return _hasRetencionIgv;
        }
        set
        {
            _hasRetencionIgv = value;
        }
    }

    public double porcRetencionIgv {get;set;}
    public decimal impRetencionIgv {get;set;}
    public decimal impOperacionRetencionIgv {get;set;}
    
}

public class DocumentoSunat : EN_Documento
{
    public DocumentoSunat()
    {
        ListaDetalleDocumento = new List<EN_DetalleDocumento>();
        ListaDocumentoReferencia = new List<EN_DocReferencia>();
        ListaDocumentoAnticipo = new List<EN_DocAnticipo>();
        ListaDocRefeGuia = new List<EN_DocRefeGuia>();
        FacturaGuia = new EN_FacturaGuia();
        FacturaRemitente = new EN_FacturaRemitente();

        // agregado para formas de pago en credito
        FormaPagoCredito = new List<EN_FormaPagoCredito>();
        // agregado para retencion del igv
        
    }

    public string ValorResumen { get; set; }
    public string ValorFirma { get; set; }
    public string NombreFichero { get; set; }
    public List<EN_DetalleDocumento> ListaDetalleDocumento { get; set; }
    public List<EN_DocReferencia> ListaDocumentoReferencia { get; set; }
    public List<EN_DocAnticipo> ListaDocumentoAnticipo { get; set; }
    public EN_FacturaGuia FacturaGuia { get; set; }
    public EN_FacturaRemitente FacturaRemitente { get; set; }
    public List<EN_DocRefeGuia> ListaDocRefeGuia { get; set; }


    // agregado para forma de pago credito
     public List<EN_FormaPagoCredito> FormaPagoCredito { get; set; }
        
    }
}