

namespace CEN
{
public class EN_Empresa
{
    public string api_token { get; set; }
    private int _Id;
    public int Id
    {
        get
        {
            return _Id;
        }
        set
        {
            _Id = value;
        }
    }

    private string _Tipoempresa;
    public string Tipoempresa
    {
        get
        {
            return _Tipoempresa;
        }
        set
        {
            _Tipoempresa = value;
        }
    }

    private string _Nrodocumento;
    public string Nrodocumento
    {
        get
        {
            return _Nrodocumento;
        }
        set
        {
            _Nrodocumento = value;
        }
    }

    private string _RazonSocial;
    public string RazonSocial
    {
        get
        {
            return _RazonSocial;
        }
        set
        {
            _RazonSocial = value;
        }
    }

    private string _Nomcomercial;
    public string Nomcomercial
    {
        get
        {
            return _Nomcomercial;
        }
        set
        {
            _Nomcomercial = value;
        }
    }

    private string _Propietario;
    public string Propietario
    {
        get
        {
            return _Propietario;
        }
        set
        {
            _Propietario = value;
        }
    }

    private string _Tipodocumento;
    public string Tipodocumento
    {
        get
        {
            return _Tipodocumento;
        }
        set
        {
            _Tipodocumento = value;
        }
    }

    private string _Nrodoc;
    public string Nrodoc
    {
        get
        {
            return _Nrodoc;
        }
        set
        {
            _Nrodoc = value;
        }
    }

    private string _Apellido;
    public string Apellido
    {
        get
        {
            return _Apellido;
        }
        set
        {
            _Apellido = value;
        }
    }

    private string _Nombre;
    public string Nombre
    {
        get
        {
            return _Nombre;
        }
        set
        {
            _Nombre = value;
        }
    }

    private string _Direccion;
    public string Direccion
    {
        get
        {
            return _Direccion;
        }
        set
        {
            _Direccion = value;
        }
    }

    private string _Sector;
    public string Sector
    {
        get
        {
            return _Sector;
        }
        set
        {
            _Sector = value;
        }
    }

    private string _Telefono1;
    public string Telefono1
    {
        get
        {
            return _Telefono1;
        }
        set
        {
            _Telefono1 = value;
        }
    }

    private string _Telefono2;
    public string Telefono2
    {
        get
        {
            return _Telefono2;
        }
        set
        {
            _Telefono2 = value;
        }
    }

    private string _Email;
    public string Email
    {
        get
        {
            return _Email;
        }
        set
        {
            _Email = value;
        }
    }

    private string _Web;
    public string Web
    {
        get
        {
            return _Web;
        }
        set
        {
            _Web = value;
        }
    }

    private string _Imagen;
    public string Imagen
    {
        get
        {
            return _Imagen;
        }
        set
        {
            _Imagen = value;
        }
    }

    private string _Codpais;
    public string Codpais
    {
        get
        {
            return _Codpais;
        }
        set
        {
            _Codpais = value;
        }
    }

    private string _Coddep;
    public string Coddep
    {
        get
        {
            return _Coddep;
        }
        set
        {
            _Coddep = value;
        }
    }

    private string _Codpro;
    public string Codpro
    {
        get
        {
            return _Codpro;
        }
        set
        {
            _Codpro = value;
        }
    }

    private string _Coddis;
    public string Coddis
    {
        get
        {
            return _Coddis;
        }
        set
        {
            _Coddis = value;
        }
    }

    private string _Departamento;
    public string Departamento
    {
        get
        {
            return _Departamento;
        }
        set
        {
            _Departamento = value;
        }
    }

    private string _Provincia;
    public string Provincia
    {
        get
        {
            return _Provincia;
        }
        set
        {
            _Provincia = value;
        }
    }

    private string _Distrito;
    public string Distrito
    {
        get
        {
            return _Distrito;
        }
        set
        {
            _Distrito = value;
        }
    }

    private string _SignatureID;
    public string SignatureID
    {
        get
        {
            return _SignatureID;
        }
        set
        {
            _SignatureID = value;
        }
    }

    private string _SignatureURI;
    public string SignatureURI
    {
        get
        {
            return _SignatureURI;
        }
        set
        {
            _SignatureURI = value;
        }
    }

    private string _NroResolucion;
    public string NroResolucion
    {
        get
        {
            return _NroResolucion;
        }
        set
        {
            _NroResolucion = value;
        }
    }

    private string _CuentaBcoDet;
    public string CuentaBcoDet
    {
        get
        {
            return _CuentaBcoDet;
        }
        set
        {
            _CuentaBcoDet = value;
        }
    }

    private string _AplicaDctoItem;
    public string AplicaDctoItem
    {
        get
        {
            return _AplicaDctoItem;
        }
        set
        {
            _AplicaDctoItem = value;
        }
    }

    private string _AplicaCargoItem;
    public string AplicaCargoItem
    {
        get
        {
            return _AplicaCargoItem;
        }
        set
        {
            _AplicaCargoItem = value;
        }
    }

    private string _AplicaISC;
    public string AplicaISC
    {
        get
        {
            return _AplicaISC;
        }
        set
        {
            _AplicaISC = value;
        }
    }

    private string _MuestraIgv;
    public string MuestraIgv
    {
        get
        {
            return _MuestraIgv;
        }
        set
        {
            _MuestraIgv = value;
        }
    }

    private string _MuestraSubTotal;
    public string MuestraSubTotal
    {
        get
        {
            return _MuestraSubTotal;
        }
        set
        {
            _MuestraSubTotal = value;
        }
    }

    private string _AplicaDctoGlobal;
    public string AplicaDctoGlobal
    {
        get
        {
            return _AplicaDctoGlobal;
        }
        set
        {
            _AplicaDctoGlobal = value;
        }
    }

    private string _AplicaCargoGlobal;
    public string AplicaCargoGlobal
    {
        get
        {
            return _AplicaCargoGlobal;
        }
        set
        {
            _AplicaCargoGlobal = value;
        }
    }

    private string _Estado;
    public string Estado
    {
        get
        {
            return _Estado;
        }
        set
        {
            _Estado = value;
        }
    }

    private string _Usureg;
    public string Usureg
    {
        get
        {
            return _Usureg;
        }
        set
        {
            _Usureg = value;
        }
    }

    private string _Borrado;
    public string Borrado
    {
        get
        {
            return _Borrado;
        }
        set
        {
            _Borrado = value;
        }
    }

    // Informacion Auditoria
    private string _UsuarioSession;
    public string UsuarioSession
    {
        get
        {
            return _UsuarioSession;
        }
        set
        {
            _UsuarioSession = value;
        }
    }

    private string _CadenaAleatoria;
    public string CadenaAleatoria
    {
        get
        {
            return _CadenaAleatoria;
        }
        set
        {
            _CadenaAleatoria = value;
        }
    }

    public string Rutadocumento{get;set;}
    public string enviarxml{get;set;}
    public string Produccion {get; set;}
}
}