using System;
namespace CEN
{



public class EN_FacturaGuia
{
    private string _documento_id;
    public string documento_id
    {
        get
        {
            return _documento_id;
        }

        set
        {
            _documento_id = value;
        }
    }

    private Int32 _empresa_id;
    public Int32 empresa_id
    {
        get
        {
            return _empresa_id;
        }

        set
        {
            _empresa_id = value;
        }
    }

    private string _tipodocumento_id;
    public string tipodocumento_id
    {
        get
        {
            return _tipodocumento_id;
        }

        set
        {
            _tipodocumento_id = value;
        }
    }

    private string _partida_direccion;
    public string partida_direccion
    {
        get
        {
            return _partida_direccion;
        }

        set
        {
            _partida_direccion = value;
        }
    }

    private string _partida_urbanizacion;
    public string partida_urbanizacion
    {
        get
        {
            return _partida_urbanizacion;
        }

        set
        {
            _partida_urbanizacion = value;
        }
    }

    private string _partida_departamento;
    public string partida_departamento
    {
        get
        {
            return _partida_departamento;
        }

        set
        {
            _partida_departamento = value;
        }
    }

    private string _partida_provincia;
    public string partida_provincia
    {
        get
        {
            return _partida_provincia;
        }

        set
        {
            _partida_provincia = value;
        }
    }

    private string _partida_distrito;
    public string partida_distrito
    {
        get
        {
            return _partida_distrito;
        }

        set
        {
            _partida_distrito = value;
        }
    }

    private string _partida_departamentodesc;
    public string partida_departamentodesc
    {
        get
        {
            return _partida_departamentodesc;
        }

        set
        {
            _partida_departamentodesc = value;
        }
    }

    private string _partida_provinciadesc;
    public string partida_provinciadesc
    {
        get
        {
            return _partida_provinciadesc;
        }

        set
        {
            _partida_provinciadesc = value;
        }
    }

    private string _partida_distritodesc;
    public string partida_distritodesc
    {
        get
        {
            return _partida_distritodesc;
        }

        set
        {
            _partida_distritodesc = value;
        }
    }

    private string _partida_pais;
    public string partida_pais
    {
        get
        {
            return _partida_pais;
        }

        set
        {
            _partida_pais = value;
        }
    }

    private string _partida_ubigeo;
    public string partida_ubigeo
    {
        get
        {
            return _partida_ubigeo;
        }

        set
        {
            _partida_ubigeo = value;
        }
    }

    private string _llegada_direccion;
    public string llegada_direccion
    {
        get
        {
            return _llegada_direccion;
        }

        set
        {
            _llegada_direccion = value;
        }
    }

    private string _llegada_urbanizacion;
    public string llegada_urbanizacion
    {
        get
        {
            return _llegada_urbanizacion;
        }

        set
        {
            _llegada_urbanizacion = value;
        }
    }

    private string _llegada_departamento;
    public string llegada_departamento
    {
        get
        {
            return _llegada_departamento;
        }

        set
        {
            _llegada_departamento = value;
        }
    }

    private string _llegada_provincia;
    public string llegada_provincia
    {
        get
        {
            return _llegada_provincia;
        }

        set
        {
            _llegada_provincia = value;
        }
    }

    private string _llegada_distrito;
    public string llegada_distrito
    {
        get
        {
            return _llegada_distrito;
        }

        set
        {
            _llegada_distrito = value;
        }
    }

    private string _llegada_departamentodesc;
    public string llegada_departamentodesc
    {
        get
        {
            return _llegada_departamentodesc;
        }

        set
        {
            _llegada_departamentodesc = value;
        }
    }

    private string _llegada_provinciadesc;
    public string llegada_provinciadesc
    {
        get
        {
            return _llegada_provinciadesc;
        }

        set
        {
            _llegada_provinciadesc = value;
        }
    }

    private string _llegada_distritodesc;
    public string llegada_distritodesc
    {
        get
        {
            return _llegada_distritodesc;
        }

        set
        {
            _llegada_distritodesc = value;
        }
    }

    private string _llegada_pais;
    public string llegada_pais
    {
        get
        {
            return _llegada_pais;
        }

        set
        {
            _llegada_pais = value;
        }
    }

    private string _vehiculo_placa;
    public string vehiculo_placa
    {
        get
        {
            return _vehiculo_placa;
        }

        set
        {
            _vehiculo_placa = value;
        }
    }

    private string _vehiculo_constancia;
    public string vehiculo_constancia
    {
        get
        {
            return _vehiculo_constancia;
        }

        set
        {
            _vehiculo_constancia = value;
        }
    }

    private string _vehiculo_marca;
    public string vehiculo_marca
    {
        get
        {
            return _vehiculo_marca;
        }

        set
        {
            _vehiculo_marca = value;
        }
    }

    private string _licencia_conducir;
    public string licencia_conducir
    {
        get
        {
            return _licencia_conducir;
        }

        set
        {
            _licencia_conducir = value;
        }
    }

    private string _transportista_ruc;
    public string transportista_ruc
    {
        get
        {
            return _transportista_ruc;
        }

        set
        {
            _transportista_ruc = value;
        }
    }

    private string _transportista_razonsocial;
    public string transportista_razonsocial
    {
        get
        {
            return _transportista_razonsocial;
        }

        set
        {
            _transportista_razonsocial = value;
        }
    }

    private string _modalidad_transporte;
    public string modalidad_transporte
    {
        get
        {
            return _modalidad_transporte;
        }

        set
        {
            _modalidad_transporte = value;
        }
    }

    private string _modalidad_transportedesc;
    public string modalidad_transportedesc
    {
        get
        {
            return _modalidad_transportedesc;
        }

        set
        {
            _modalidad_transportedesc = value;
        }
    }

    private string _peso_unidad;
    public string peso_unidad
    {
        get
        {
            return _peso_unidad;
        }

        set
        {
            _peso_unidad = value;
        }
    }

    private decimal _peso_cantidad;
    public decimal peso_cantidad
    {
        get
        {
            return _peso_cantidad;
        }

        set
        {
            _peso_cantidad = value;
        }
    }
}


 
}