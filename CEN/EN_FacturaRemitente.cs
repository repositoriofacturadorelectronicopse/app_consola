
     using System;
namespace CEN
{
    public class EN_FacturaRemitente
    {
   


    private string _documento_id;
    public string documento_id
    {
        get
        {
            return _documento_id;
        }

        set
        {
            _documento_id = value;
        }
    }

    private Int32 _empresa_id;
    public Int32 empresa_id
    {
        get
        {
            return _empresa_id;
        }

        set
        {
            _empresa_id = value;
        }
    }

    private string _tipodocumento_id;
    public string tipodocumento_id
    {
        get
        {
            return _tipodocumento_id;
        }

        set
        {
            _tipodocumento_id = value;
        }
    }

    private Int32 _correlativo;
    public Int32 correlativo
    {
        get
        {
            return _correlativo;
        }

        set
        {
            _correlativo = value;
        }
    }

    private string _rmotivo_traslado;
    public string rmotivo_traslado
    {
        get
        {
            return _rmotivo_traslado;
        }

        set
        {
            _rmotivo_traslado = value;
        }
    }

    private decimal _rpeso_bruto;
    public decimal rpeso_bruto
    {
        get
        {
            return _rpeso_bruto;
        }

        set
        {
            _rpeso_bruto = value;
        }
    }

    private string _rmodalidad_transporte;
    public string rmodalidad_transporte
    {
        get
        {
            return _rmodalidad_transporte;
        }

        set
        {
            _rmodalidad_transporte = value;
        }
    }

    private DateTime _rfecha_traslado;
    public DateTime rfecha_traslado
    {
        get
        {
            return _rfecha_traslado;
        }

        set
        {
            _rfecha_traslado = value;
        }
    }

    private string _rtransportista_tipodoc;
    public string rtransportista_tipodoc
    {
        get
        {
            return _rtransportista_tipodoc;
        }

        set
        {
            _rtransportista_tipodoc = value;
        }
    }

    private string _rtransportista_ruc;
    public string rtransportista_ruc
    {
        get
        {
            return _rtransportista_ruc;
        }

        set
        {
            _rtransportista_ruc = value;
        }
    }

    private string _rtransportista_razonsocial;
    public string rtransportista_razonsocial
    {
        get
        {
            return _rtransportista_razonsocial;
        }

        set
        {
            _rtransportista_razonsocial = value;
        }
    }

    private string _rtransportista_registromtc;
    public string rtransportista_registromtc
    {
        get
        {
            return _rtransportista_registromtc;
        }

        set
        {
            _rtransportista_registromtc = value;
        }
    }
    private string _rtransportista_constancia;
    public string rtransportista_constancia
    {
        get
        {
            return _rtransportista_constancia;
        }

        set
        {
            _rtransportista_constancia = value;
        }
    }

    private string _rvehiculo_placa;
    public string rvehiculo_placa
    {
        get
        {
            return _rvehiculo_placa;
        }

        set
        {
            _rvehiculo_placa = value;
        }
    }

    private string _rvehiculo_secundario;
    public string rvehiculo_secundario
    {
        get
        {
            return _rvehiculo_secundario;
        }

        set
        {
            _rvehiculo_secundario = value;
        }
    }

    private string _rconductor_tipodoc;
    public string rconductor_tipodoc
    {
        get
        {
            return _rconductor_tipodoc;
        }

        set
        {
            _rconductor_tipodoc = value;
        }
    }

    private string _rconductor_nrodoc;
    public string rconductor_nrodoc
    {
        get
        {
            return _rconductor_nrodoc;
        }

        set
        {
            _rconductor_nrodoc = value;
        }
    }

    private string _rllegada_ubigeo;
    public string rllegada_ubigeo
    {
        get
        {
            return _rllegada_ubigeo;
        }

        set
        {
            _rllegada_ubigeo = value;
        }
    }

    private string _rllegada_direccion;
    public string rllegada_direccion
    {
        get
        {
            return _rllegada_direccion;
        }

        set
        {
            _rllegada_direccion = value;
        }
    }

    private string _rpartida_ubigeo;
    public string rpartida_ubigeo
    {
        get
        {
            return _rpartida_ubigeo;
        }

        set
        {
            _rpartida_ubigeo = value;
        }
    }

    private string _rpartida_direccion;
    public string rpartida_direccion
    {
        get
        {
            return _rpartida_direccion;
        }

        set
        {
            _rpartida_direccion = value;
        }
    }

    private string _rindicador_subcontratacion;
    public string rindicador_subcontratacion
    {
        get
        {
            return _rindicador_subcontratacion;
        }

        set
        {
            _rindicador_subcontratacion = value;
        }
    }


    }
}