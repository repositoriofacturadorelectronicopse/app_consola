﻿

namespace CEN
{
    public class EN_Proceso
    {
        private int _Id;
        public int Id
        {
            get
            {
                return _Id;
            }
            set
            {
                _Id = value;
            }
        }

        private int _Empresa_id;
        public int Empresa_id
        {
            get
            {
                return _Empresa_id;
            }
            set
            {
                _Empresa_id = value;
            }
        }

        private string _Tipodocumento_id;
        public string Tipodocumento_id
        {
            get
            {
                return _Tipodocumento_id;
            }
            set
            {
                _Tipodocumento_id = value;
            }
        }

        private string _Descripcion;
        public string Descripcion
        {
            get
            {
                return _Descripcion;
            }
            set
            {
                _Descripcion = value;
            }
        }

        private string _Estado;
        public string Estado
        {
            get
            {
                return _Estado;
            }
            set
            {
                _Estado = value;
            }
        }

        // Datos de la empresa asociada al proceso
        private string _Nrodocumento;
        public string Nrodocumento
        {
            get
            {
                return _Nrodocumento;
            }
            set
            {
                _Nrodocumento = value;
            }
        }

        private string _RazonSocial;
        public string RazonSocial
        {
            get
            {
                return _RazonSocial;
            }
            set
            {
                _RazonSocial = value;
            }
        }

        // Informacion Auditoria
        private string _UsuarioSession;
        public string UsuarioSession
        {
            get
            {
                return _UsuarioSession;
            }
            set
            {
                _UsuarioSession = value;
            }
        }

        private string _CadenaAleatoria;
        public string CadenaAleatoria
        {
            get
            {
                return _CadenaAleatoria;
            }
            set
            {
                _CadenaAleatoria = value;
            }
        }
    }

}
