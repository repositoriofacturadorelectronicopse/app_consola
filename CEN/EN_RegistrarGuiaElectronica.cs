namespace CEN
{
    public class EN_RegistrarGuiaElectronica
    {
          //DESCRIPCION: Clase de respuesta de registrar guia electronica
          public EN_RespuestaRegistro RespRegistrarGuiaElectronica { get; set; }  
          public EN_ErrorWebService ErrorWebServ { get; set; }  

          public EN_RegistrarGuiaElectronica() {
            RespRegistrarGuiaElectronica = new EN_RespuestaRegistro();
            ErrorWebServ = new EN_ErrorWebService();
         }
    }
}