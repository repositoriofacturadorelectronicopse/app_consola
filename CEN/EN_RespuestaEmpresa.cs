using System.Collections.Generic;
namespace CEN
{
    public class EN_RespuestaEmpresa
    {
         public EN_RespuestaRegistro RespRegistro{ get; set; } 
         public EN_Empresa objEmpresa { get; set; } 
         public List<EN_Empresa> listaEmpresa = new List<EN_Empresa>();
         public EN_ErrorWebService errorService { get; set; } 
         public EN_RespuestaEmpresa()
         {
             RespRegistro = new EN_RespuestaRegistro();
             objEmpresa = new EN_Empresa();
             errorService = new EN_ErrorWebService();

         }
    }
}