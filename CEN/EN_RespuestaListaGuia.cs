/****************************************************************************************************************************************************************************************
 PROGRAMA: EN_RespuestaListaGuia.cs
 VERSION : 1.0
 OBJETIVO: Clase respuesta de guia 
 FECHA   : 24/11/2021
 AUTOR   : JOSE CHUMIOQUE -  IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 ***************************************************************************************************************************************************************************************/
using System.Collections.Generic;
namespace CEN
{
    public class EN_RespuestaListaGuia
    {
                //DESCRIPCION: Listar guia
        public EN_RespuestaRegistro ResplistaGuia{ get; set; } 
        public List<EN_Guia> listDetGuia = new List<EN_Guia>();

        public EN_RespuestaListaGuia() {
            ResplistaGuia = new EN_RespuestaRegistro();
        }
        
    }
        
    }
