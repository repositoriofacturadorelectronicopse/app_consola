using System.Collections.Generic;
namespace CEN
{
    public class EN_RespuestaListaProceso
    {
        public EN_RespuestaRegistro ResplistaGuia{ get; set; } 
        public List<EN_Proceso> listProceso = new List<EN_Proceso>();

        public EN_RespuestaListaProceso() {
            ResplistaGuia = new EN_RespuestaRegistro();
        }
        
    }
}