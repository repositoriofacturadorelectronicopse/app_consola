using System.Collections.Generic;
namespace CEN
{
    public class EN_RespuestaParametro
    {
         public EN_RespuestaRegistro RespRegistro{ get; set; } 
         public EN_Parametro objParametro { get; set; } 
         public List<EN_Parametro> listaParametro = new List<EN_Parametro>();
         public EN_ErrorWebService errorService { get; set; } 
         public EN_RespuestaParametro() {
              RespRegistro = new EN_RespuestaRegistro();
              objParametro = new EN_Parametro();
              errorService = new EN_ErrorWebService();

          }
        
    }
}