using System.Collections.Generic;
namespace CEN
{
    public class EN_Resumen
    {
       
        public int Id { get; set; }
        public int Empresa_id { get; set; }
        public string Nrodocumento   { get; set; }
        public string DescripcionEmpresa { get; set; }
        public string Tiporesumen { get; set; }
        public int Correlativo { get; set; }
        public string FechaEmisionDocumento { get; set; }
        public string FechaGeneraResumen { get; set; }
        public string IdentificaResumen { get; set; }
        public string Estado { get; set; }
        public string Situacion { get; set;}
        public string estado_resumen { get; set; } 
        public string EnviadoMail { get; set; }
        // Ultimos Parametros Agregados
        public string nombreXML { get; set; }
        public string XMLEnviado  { get; set; }
        public string CDRXML { get; set; }
        public string vResumen { get; set; }
        public string vFirma { get; set; }
        public string ticket { get; set; }
        // Para Busquedas
        public string FechaIni { get; set; }
        public string FechaFin { get; set; }
        // Para Resumen Enviado
        public string MensajeCDR { get; set; }
        public string FechaEnvio { get; set; }
        // Para Errores Rechazos
        public string MensajeError { get; set; }
        public string FechaError { get; set; }
        // Informacion Auditoria
        public string UsuarioSession { get; set; }
        public string CadenaAleatoria { get; set; }
        // Para reportes
        public string Cantidad { get; set; }
        // Para Validar Usuario Certificado Empresa
        public string Usuario { get; set; }
        public string Clave { get; set; }
      
    }

    public class ResumenSunat : EN_Resumen
{
    public ResumenSunat()
    {
        ListaDetalleResumen = new List<EN_DetalleResumen>();
    }
    public string ValorResumen { get; set; }
    public string ValorFirma { get; set; }
    public string NombreFichero { get; set; }
      public string EstadoResumen { get; set; }
    public List<EN_DetalleResumen> ListaDetalleResumen { get; set; }
}
}