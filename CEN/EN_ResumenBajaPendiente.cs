/****************************************************************************************************************************************************************************************
 PROGRAMA: EN_ResumenBajaPendiente.cs
 VERSION : 1.0
 OBJETIVO: Clase entidad de resumen baja pendiente
 FECHA   : 28/01/2022
 AUTOR   : JUAN ALARCON -  IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 ***************************************************************************************************************************************************************************************/

using System.Collections.Generic;
using System;
namespace CEN
{
    public class EN_ResumenBajaPendiente
    {
        //DESCRIPCION: Clase de Entidad de resumen baja pendiente
        public int id { get; set;}
        public int empresa_id { get; set; }
        public int puntoventa_id { get; set; }
        public string fecemision_documento { get; set; } 
        public string  fecgenera_resumen { get; set; }
        public string estado_generado { get; set; }
        public string  usureg { get; set; }
        public string fecreg { get; set; }
        public int marcabaja { get; set; }
    }
    
}