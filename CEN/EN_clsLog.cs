﻿

namespace CEN
{

    public partial class EN_clsLog
    {
        private string _CadenaAleatoria;

        public string CadenaAleatoria
        {
            get
            {
                return _CadenaAleatoria;
            }

            set
            {
                _CadenaAleatoria = value;
            }
        }

        private string _ClaseDatos;

        public string ClaseDatos
        {
            get
            {
                return _ClaseDatos;
            }

            set
            {
                _ClaseDatos = value;
            }
        }

        private string _MetodoFuncionClase;

        public string MetodoFuncionClase
        {
            get
            {
                return _MetodoFuncionClase;
            }

            set
            {
                _MetodoFuncionClase = value;
            }
        }

        private string _MensajeRespuesta;

        public string MensajeRespuesta
        {
            get
            {
                return _MensajeRespuesta;
            }

            set
            {
                _MensajeRespuesta = value;
            }
        }

        private string _Path;

        public string Path
        {
            get
            {
                return _Path;
            }

            set
            {
                _Path = value;
            }
        }

        private string _UsuarioSession;

        public string UsuarioSession
        {
            get
            {
                return _UsuarioSession;
            }

            set
            {
                _UsuarioSession = value;
            }
        }

        private string _MensajeAuditoriaOk;

        public string MensajeAuditoriaOk
        {
            get
            {
                return _MensajeAuditoriaOk;
            }

            set
            {
                _MensajeAuditoriaOk = value;
            }
        }

        private string _MensajeAuditoriaError;

        public string MensajeAuditoriaError
        {
            get
            {
                return _MensajeAuditoriaError;
            }

            set
            {
                _MensajeAuditoriaError = value;
            }
        }
    }
}
