﻿/****************************************************************************************************************************************************************************************
 PROGRAMA: NE_Baja.cs
 VERSION : 1.0
 OBJETIVO: Clase entidad de negocio baja
 FECHA   : 28/01/2022
 AUTOR   : JUAN ALARCON -  IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 ***************************************************************************************************************************************************************************************/
using System;
using CEN;
using CAD;

namespace CLN
{
    public class NE_Baja
    {

        public object listarDocumentosComunicadoBaja(EN_Empresa oEmpresa,EN_Certificado oCertificado,int empresa_id, string fecha_comunicacion, string estado, string situacion, int flag)
        {
            try
            {
                AD_Baja obj = new AD_Baja();
                return obj.listarDocumentosComunicadoBaja( oEmpresa, oCertificado, empresa_id,  fecha_comunicacion,  estado,  situacion,  flag);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public object listarResumenBajaPendientes(int empresa_id)
        {
            try
            {
                AD_Baja obj = new AD_Baja();
                return obj.listarResumenBajaPendientes( empresa_id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

            public object listarDetalleResBajaPend(int baja_id)
        {
            try
            {
                AD_Baja obj = new AD_Baja();
                return obj.listarDetalleResBajaPend( baja_id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

       
     
    }

}
