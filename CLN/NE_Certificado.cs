﻿using System;
using AppConfiguracion;
using CAD;
using CEN;

namespace CLN
{
   

    public partial class NE_Certificado
    {

        public object listarCertificado(EN_Certificado pCertif)
        {
            try
            {
                var obj = new AD_Certificado();
                return obj.listarCertificado(pCertif);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

         public object validarEmpresaCertificado(int p_idempresa, string p_usuario, string p_clave, string documentoId, int Idempresa)
        {
            try
            {
                var obj = new AD_Certificado();
                return obj.validarEmpresaCertificado(p_idempresa, p_usuario, p_clave);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        public object listarCertificadoGuia(EN_Certificado pCertif, string documentoId, int Idempresa)
        {
            try
            {
                var obj = new AD_CertificadoGuia();
                return obj.listarCertificadoGuia(pCertif);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

            
    }
}
