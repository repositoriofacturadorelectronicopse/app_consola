/****************************************************************************************************************************************************************************************
 PROGRAMA: NE_Consulta.cs
 VERSION : 1.0
 OBJETIVO: Clase de consultas
 FECHA   : 21/06/2021
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 ***************************************************************************************************************************************************************************************/

using System;
using CAD;
using CEN;
namespace CLN
{
    public class NE_Consulta
    {
        
    

    public EN_ErrorWebService LlenarErrorWebSer(short TipoErr, int CodigoErr, string DescripcionErr)
    {
        //DESCRIPCIÓN: Llenar clase de error web service
        EN_ErrorWebService data = new EN_ErrorWebService(); //Clase error weservice
        try
        {
            data.TipoError = TipoErr;
            data.CodigoError = CodigoErr;
            data.DescripcionErr = DescripcionErr;
            return data;
        }
        catch(Exception ex)
        {
            throw ex;
        }
        
    }
    public EN_Concepto ObtenerDescConcepto(int prefijo_cpe_doc, int const_error)
    {
         //DESCRIPCIÓN: Obtener descripcion de conceptos.
        AD_Concepto cad_consentimiento = new AD_Concepto();//Concepto
        EN_Concepto gbcon = new EN_Concepto();//entidad concepto
        
        gbcon = cad_consentimiento.Obtener_desc_gbcon(prefijo_cpe_doc,const_error);
        return gbcon;
    }
   

   
   
    

    }
}