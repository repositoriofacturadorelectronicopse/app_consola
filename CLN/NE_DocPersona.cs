﻿using System;

using CAD;
using CEN;
using AppConfiguracion;

namespace CLN
{
   

    public class NE_DocPersona
    {
        private string pClase = "NE_DocPersona";
        public object listarDocPersona(EN_DocPersona pDocPersona , string documentoId, int empresaId )
        {
            try
            {
                AD_DocPersona obj = new AD_DocPersona();
                return obj.listarDocPersona(pDocPersona);
            }
            catch (Exception ex)
            {
                // guardamos en bitacora
            //  var en_Bitacora = new EN_Bitacora();
            // en_Bitacora.btc_documentoID =documentoId;
            // en_Bitacora.btc_tipo= "error";
            // en_Bitacora.btc_empresaId= empresaId;
            // en_Bitacora.btc_claseOrigen = typeof(NE_Certificado).ToString();
            // en_Bitacora.btc_nombreLog = pClase + " listarDocPersona";
            // en_Bitacora.btc_metodoFuncionClase =ex.ToString();


            // var oBitacoraE =  new AD_Bitacora();
            // oBitacoraE.GuardarBitacora(en_Bitacora);
                throw ex;
            }
        }
    }
}
