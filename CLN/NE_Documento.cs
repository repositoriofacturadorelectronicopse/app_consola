﻿using System;

using AppConfiguracion;
using CEN;
using CAD;

namespace CLN
{


    public class NE_Documento
    {
        private string pClase = "NE_Documento";


        
         public object listarFacturaNCD(EN_Documento pDoc, string tipo_docs)
        {
            try
            {
                AD_Documento obj = new AD_Documento();
                return obj.listarDocumentosBatch( pDoc,  tipo_docs);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

         public object listarOtrosCpe(EN_OtrosCpe pDoc, string tipo_docs)
        {
            try
            {
                AD_Documento obj = new AD_Documento();
                return obj.listarOtrosCpe( pDoc,  tipo_docs);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public object listarDocumentoReferencia(EN_DocReferencia pDocumento , string documentoId, int Idempresa)
        {
            try
            {
                AD_Documento obj = new AD_Documento();
                return obj.listarDocumentoReferencia(pDocumento);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        

        public object listarDocumentosResumen(EN_Documento pDocumento)
        {
            try
            {
                AD_Documento obj = new AD_Documento();
                return obj.listarDocumentosResumen(pDocumento);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

 

  
       
     
    }

}
