﻿using System;

using AppConfiguracion;
using CAD;
using CEN;

namespace CLN
{
  

    public partial class NE_Facturacion
    {
        private string pClase = "NE_Facturacion";

        public string buscarConstantesTablaSunat(string codtabla,string codalterno)
        {

                   try
            {
                var obj = new AD_Facturacion();
                return obj.buscarConstantesTablaSunat(codtabla,codalterno);
            }
            catch (Exception ex)
            {
                
                throw ex;
            }

        }
        public string ObtenerCodigoSunat(string codtabla, string codigo)
        {
            try
            {
                var obj = new AD_Facturacion();
                return obj.ObtenerCodigoSunat(codtabla, codigo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string ObtenerDescripcionPorCodElemento(string codtabla, string codigo)
        {
            try
            {
                var obj = new AD_Facturacion();
                return obj.ObtenerDescripcionPorCodElemento(codtabla, codigo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

     

      

    

      
        public string buscarParametroAppSettings(int codconcepto,int codcorrelativo)
        {
            // DESCRIPCION: BUSCAR PARAMETRO DE CONFIGURACION

            var obj = new AD_Facturacion();        // CLASE DE CONEXION FACTURACION

            try
            {
                return obj.buscarParametroAppSettings(codconcepto,codcorrelativo);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }



  




  

    }
}
