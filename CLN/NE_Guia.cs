/****************************************************************************************************************************************************************************************
 PROGRAMA: NE_Guia.cs
 VERSION : 1.0
 OBJETIVO: Clase lógica de negocio GUIA
 FECHA   : 18/06/2021
 AUTOR   : JOSE CHUMIOQUE -  IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 ***************************************************************************************************************************************************************************************/

using CEN;
using CAD;
using System;
namespace CLN
{
    public class NE_Guia
    {
        public EN_RespuestaListaGuia listarGuiasPendientes (EN_Guia obGuia, string tipDoc) 
        {
            EN_RespuestaListaGuia respuesta = new EN_RespuestaListaGuia();
            AD_Guia guia = new AD_Guia();
            try
            {
                respuesta = guia.listarGuiasPendientes(obGuia,tipDoc);
                return respuesta;
                 
            }
            catch (Exception ex)
            {
                
                throw ex;
            }


        }
         public EN_RespuestaData listarConstantesTablaSunat(int flag,string codtabla,string codalterno)
         {
             EN_RespuestaData respuesta = new EN_RespuestaData();
             AD_Guia guia = new AD_Guia();
             try
             {
                 respuesta = guia.listarConstantesTablaSunat(flag,codtabla,codalterno);
                return respuesta;
                  
             }
             catch (Exception ex)
             {
                 
                 throw ex;
             }

         }
         public EN_RespuestaRegistro buscarParametroAppSettings(int codconcepto, int codcorrelativo)
         {
             AD_Guia guia = new AD_Guia();
             EN_RespuestaRegistro respuesta = new EN_RespuestaRegistro();
             try
             {
                  respuesta = guia.buscarParametroAppSettings(codconcepto,codcorrelativo);
                  return respuesta;

             }
             catch (Exception ex)
             {
                 
                 throw ex;
             }

         }
        
        
    }
}