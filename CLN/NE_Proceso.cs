﻿

using System;
using AppConfiguracion;
using CAD;
using CEN;

namespace CLN
{
  

    public class NE_Proceso
    {
   

        public object listarProcesosBatchRD(EN_Proceso pProceso, EN_ComprobanteSunat pCompSunat)
        {
            try
            {
                AD_Proceso obj = new AD_Proceso();
                return obj.listarProcesosBatchRD(pProceso, pCompSunat);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public object listarProcesosBatchFacturaNCD(EN_Proceso pProceso, EN_ComprobanteSunat pCompSunat)
        {
            try
            {
                AD_Proceso obj = new AD_Proceso();
                return obj.listarProcesosBatchFacturaNCD(pProceso, pCompSunat);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

         public object listarProcesosBatchOtrosCpe(EN_Proceso pProceso, EN_ComprobanteSunat pCompSunat)
        {
            try
            {
                AD_Proceso obj = new AD_Proceso();
                return obj.listarProcesosBatchOtrosCpe(pProceso, pCompSunat);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

       

         public int EmpresaIdEnvio()
        {
            try
            {
                AD_Proceso obj = new AD_Proceso();
                return obj.EmpresaIdEnvio();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

         public int NumDiasEnvio()
        {
            try
            {
                AD_Proceso obj = new AD_Proceso();
                return obj.NumDiasEnvio();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public EN_Parametro buscar_tablaParametro(EN_Parametro par_busqueda)
        {
            try
            {
                AD_Proceso obj = new AD_Proceso();
                return obj.buscar_tablaParametro(par_busqueda);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public object listarProcesosActivos(int empresa_id, string tipo_docs, string estado)
        {
            try
            {
            AD_Proceso obj = new AD_Proceso();
            return obj.listarProcesosActivos( empresa_id,  tipo_docs, estado);
            }
            catch (Exception ex)
            {
            throw ex;
            }   
        }


         public EN_RespuestaListaProceso ListarProcesosBatchGuia(EN_Proceso pProceso, EN_ComprobanteSunat pCompSunat)
        {
            EN_RespuestaListaProceso respuesta = new EN_RespuestaListaProceso();
            AD_Proceso proceso = new AD_Proceso();
            try
            {
                respuesta = proceso.ListarProcesosBatchGuia(pProceso,pCompSunat);
                return respuesta;
                 
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
        public EN_RespuestaData EmpresaIdEnvioGuia() 
        {
            EN_RespuestaData respuesta = new EN_RespuestaData();
            AD_Proceso proceso = new AD_Proceso();
            try
            {
                respuesta = proceso.EmpresaIdEnvioGuia();
                return respuesta;
                 
            }
            catch (Exception ex)
            {
                
                throw ex;
            }

        }
        public EN_RespuestaData NumDiasEnvioGuia() 
        {
            EN_RespuestaData respuesta = new EN_RespuestaData();
            AD_Proceso proceso = new AD_Proceso();
            try
            {
                 respuesta = proceso.NumDiasEnvioGuia();
                 return respuesta;
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
        public EN_RespuestaParametro buscar_tablaParametroGuia(EN_Parametro par_busqueda) 
        {
            EN_RespuestaParametro respuesta = new EN_RespuestaParametro();
            AD_Proceso proceso = new AD_Proceso();
            try
            {
                respuesta = proceso.buscar_tablaParametroGuia(par_busqueda);
                return respuesta;
                 
            }
            catch (Exception ex)
            {
                
                throw ex;
            }

        }
         public EN_RespuestaListaProceso listarProcesosActivosGuia(int empresa_id, string tipo_doc)
         {
             EN_RespuestaListaProceso respuesta = new EN_RespuestaListaProceso();
              AD_Proceso proceso = new AD_Proceso();
              try
              {
                  respuesta = proceso.listarProcesosActivosGuia(empresa_id,tipo_doc);
                  return respuesta;
                   
              }
              catch (Exception ex)
              {
                  
                  throw ex;
              }
             

         }
     
    }

}
