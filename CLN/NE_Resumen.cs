﻿using System;
using System.Collections.Generic;
using AppConfiguracion;
using CAD;
using CEN;


namespace CLN
{
    public class NE_Resumen
    {
        private string pClase = "NE_Resumen";
        public object listarResumen(EN_Resumen pResumen)
        {
            try
            {
                AD_Resumen obj = new AD_Resumen();
                return obj.listarResumen(pResumen);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public object listarDetalleResumen(EN_Resumen pResumen)
        {
            try
            {
                AD_Resumen obj = new AD_Resumen();
                return obj.listarDetalleResumen(pResumen);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool GuardarResumenNombreValorResumenFirma(EN_Resumen oResumen, string nombreXML, string valorResumen, string valorFirma, string xmlgenerado)
        {
            try
            {
                AD_Resumen obj = new AD_Resumen();
                return obj.GuardarResumenNombreValorResumenFirma(oResumen, nombreXML, valorResumen, valorFirma, xmlgenerado);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
      

        public object listarDocumentosBajaResumen(string idDoc, int idEmpresa, int idPuntoVenta)
        {
             // DESCRIPCION: LISTAR DOCREFEGUIA
            
            AD_Resumen obj = new AD_Resumen(); // CLASE DE CONEXION DOCUMENTO
            try
            {
                return obj.listarDocumentosBajaResumen( idDoc,  idEmpresa,  idPuntoVenta);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int GuardarResumenSunat(ResumenSunat ResumenoSunat)
        {
            AD_Resumen obj = new AD_Resumen();
            try
            {
                return obj.GuardarResumenSunat(ResumenoSunat);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ActualizarEstadoResumenBajaPend(int idResBaja, string estado)
        {
            try
            {
                AD_Resumen obj = new AD_Resumen();
                obj.ActualizarEstadoResumenBajaPend(idResBaja, estado);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
      

    
    }
}